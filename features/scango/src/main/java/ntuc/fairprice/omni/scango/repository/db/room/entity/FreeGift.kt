package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity

@Entity
data class FreeGift(
    var imageId: String?,
    var title: String?,
    var content: String?
)