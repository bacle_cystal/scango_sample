package ntuc.fairprice.omni.ui.utils

import android.content.Context
import android.util.TypedValue

fun Float.dpToPx(context: Context) =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this,
        context.resources.displayMetrics
    )

fun Float.spToPx(context: Context) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, this, context.resources.displayMetrics)

fun Float.pxToSp(context: Context) =
    (this / context.resources.displayMetrics.scaledDensity + 0.5f)
