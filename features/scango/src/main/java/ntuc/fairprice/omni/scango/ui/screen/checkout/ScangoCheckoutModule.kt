package ntuc.fairprice.omni.scango.ui.screen.checkout

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class  ScangoCheckoutBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoCheckoutActivity): Activity
}

@Module
class ScangoCartCheckoutProvidesModule {
}