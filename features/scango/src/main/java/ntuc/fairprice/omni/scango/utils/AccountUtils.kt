package ntuc.fairprice.omni.scango.utils

import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository

val AccountRepository.isLoggedIn
        get() = this.getCustomer() != null