package ntuc.fairprice.omni.scango.repository.linkpoint

import android.app.Application
import android.content.SharedPreferences
import ntuc.fairprice.omni.scango.Constants.KEY_LP_IS_DISMISSED
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoLinkPointService
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.*
import thor.zopsmart.com.thor.repository.db.room.repo.Repository
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.set
import zendesk.support.requestlist.RequestInfoDataSource
import javax.inject.Inject

class ScangoLinkPointRepositoryImpl @Inject constructor(
    val scangoLinkPointService: ScangoLinkPointService,
    val application: Application
): ScangoLinkPointRepository, Repository(application) {

    override var userDismissedForSession: Boolean
        get() = localStorage.getBoolean(KEY_LP_IS_DISMISSED, false)
        set(value) {
            localStorage.set(KEY_LP_IS_DISMISSED, value)
        }

    override var linkPointStatus = LinkPointStatus()

    override suspend fun getCustomers(customerId: String): Result<LinkPointStatus> = scangoLinkPointService.getCustomers(customerId)

    override suspend fun updateCustomerRecordWithNumber(
        customerId: String,
        phoneNumber: String
    ): Result<UpdateCustomerLinkPointResponse> = scangoLinkPointService.updateCustomerRecordWithNumber(customerId, phoneNumber)

    override suspend fun updateCustomerRecordWithCard(
        customerId: String,
        cardNumber: String,
        isHashed: Boolean
    ): Result<UpdateCustomerLinkPointResponse> = scangoLinkPointService.updateCustomerRecordWithCard(customerId, cardNumber, isHashed)

    override suspend fun sendOtp(resendOTP: Boolean): Result<SendOtpResponse> = scangoLinkPointService.sendOtp(resendOTP)

    override suspend fun verifyOtp(otp: String): Result<VerifyOtpResponse> = scangoLinkPointService.verifyOtp(otp)

    override suspend fun unLinkCustomer(customerId: String): Result<UnLinkCustomerResponse> = scangoLinkPointService.unLinkCustomer(customerId)
}