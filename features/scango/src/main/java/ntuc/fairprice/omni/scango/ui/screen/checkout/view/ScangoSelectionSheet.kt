package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyRecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoCardSelection
import ntuc.fairprice.omni.scango.base.ScangoComponent
import ntuc.fairprice.omni.scango.base.ScangoDelegate
import ntuc.fairprice.omni.scango.base.ScangoSelectionDelegate
import thor.zopsmart.com.thor.model.Card

class ScangoSelectionSheet(val mContext: Context): ScangoSelectionDelegate, EpoxyRecyclerView.ModelBuilderCallback {

    private val tvTitle: TextView
    private val ivAddNew: ImageView
    private val recyclerView: EpoxyRecyclerView

    var addClickListener: () -> Unit = {}

    var selectionListener: (selectedId: Long) -> Unit = {}

    private val rootView: View = View.inflate(mContext, R.layout.scango_selection_sheet, null)

    private val bottomSheetDialog: BottomSheetDialog by lazy {
        BottomSheetDialog(mContext, R.style.BottomSheetDialog)
    }

    private var data: List<ScangoComponent<*>> = arrayListOf()

    init {
        bottomSheetDialog.setContentView(rootView)

        recyclerView = rootView.findViewById(R.id.rv_selecion)

        tvTitle = rootView.findViewById(R.id.tv_title)

        ivAddNew = rootView.findViewById(R.id.iv_add_new)

        ivAddNew.setOnClickListener {
            addClickListener()
            closeBottomDialog()
        }

        // Set up epoxy recycler view
        val gridLayoutManager = GridLayoutManager(mContext, 30)
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.buildModelsWith(this)
    }

    override fun buildModels(controller: EpoxyController) {
        controller.spanCount = 30

        data.forEach {
            (it as? ScangoComponent<ScangoDelegate>)
                ?.render(this@ScangoSelectionSheet)
                ?.forEach { model ->
                    model.addTo(controller)
                }
            if(it is ScangoCardSelection) {
                renderCardSheet()
            }
        }
    }


    override fun onCardSelection(card: Card) {
        selectionListener(card.id)
        closeBottomDialog()
    }

    private fun closeBottomDialog() {
        bottomSheetDialog.hide()
    }

    fun show(selectionData: List<ScangoComponent<*>>) {
        if(selectionData.isEmpty())
            return

        data = selectionData

        recyclerView.requestModelBuild()

        bottomSheetDialog.show()
    }

    private fun activateAddButton(activate: Boolean) {
        ivAddNew.visibility = if (activate) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
    }

    private fun renderCardSheet() {
        tvTitle.text = mContext.resources.getString(R.string.label_select_payment_method)
        activateAddButton(true)
    }
}