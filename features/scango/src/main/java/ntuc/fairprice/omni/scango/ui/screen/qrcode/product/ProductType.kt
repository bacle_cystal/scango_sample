package ntuc.fairprice.omni.scango.ui.screen.qrcode.product

data class ProductType(
    val id: Int?,
    var name: String
)