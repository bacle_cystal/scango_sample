package ntuc.fairprice.omni.scango.ui.screen.receipt

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.airbnb.epoxy.EpoxyModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.scango.*
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.Payment
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoItemInvoice
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderDetail
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.VerificationStatus
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoOrderRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.ui.screen.cart.data.toViewData
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.*
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartDealDetailModelBuilder
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartDealDetailModel_
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartFreeGiftDetailModelBuilder
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartFreeGiftDetailModel_
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview.ScangoPaymentSummary
import ntuc.fairprice.omni.scango.utils.BarcodeQRCodeUtils
import ntuc.fairprice.omni.scango.utils.DateTimeUtils
import ntuc.fairprice.omni.scango.utils.px
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.base.extensions.getAutoGenId
import thor.zopsmart.com.thor.epoxy.PageBuilder
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ScangoReceiptActivityViewModel @Inject constructor(
    private val context: Application,
    private val repository: ScangoOrderRepository,
    private val cartRepository: ScangoCartRepository,
    private val scangoStoreRepository: ScangoStoreRepository
) : ScangoBaseViewModel(context) {
    lateinit var payment: Payment
    lateinit var qrcodeBitmap: Bitmap
    var isOrderVerified = MutableLiveData<Boolean>().default(false)
    var isNeedToShowFeedback = MutableLiveData<Boolean>().default(false)
    val orderDetailStatus = MutableLiveData<OrderDetailStatus>()
    val paymentVerifyStatus = MutableLiveData<VerificationStatus>()

    //TODO handle payment verify change status
    fun getPaymentVerify(orderDetail: ScangoOrderDetail) {
        paymentVerifyStatus.value = orderDetail.verificationStatus
    }

    fun getPaymentData(orderID: String) {
        orderDetailStatus.value = OrderDetailLoading
        loadOrder(orderID)
    }

    private val pageModelInternal = MutableLiveData<List<EpoxyModel<*>>>()

    private val pageBuilder = ScangoPageBuilder(context)

    val pageModel: LiveData<List<EpoxyModel<*>>>
        get() = pageModelInternal

    fun loadOrder(refNo: String) {
        launch {
            repository.getOrderDetails(refNo).onSuccess {
                payment = Payment(
                    "123",
                    it.customer?.name,
                    it.cardNumber,
                    it.cardType,
                    DateTimeUtils.getDayEEEddyyyyFromString(it.orderPlacedDate),
                    DateTimeUtils.getHourFromString(it.orderPlacedDate),
                    it.storeAddress,
                    it.store?.name,
                    it.orderNumber,
                    it.totalAmount,
                    getListItemInvoice(it)
                )
                diplayReceiptList(payment, it, scangoStoreRepository.getSessionId())
            }.onError {
                orderDetailStatus.value = OrderDetailFailure(it)
                Log.d("Error", "Test" + it.message)
                /*pageModelInternal.value =
                    pageBuilder.buildModels(ScangoReceiptDataView(context, OrderViewData(context, MemberDetail()), paymentData))        */
            }
        }
        cartRepository.clearCart()
    }

    class ScangoPageBuilder(val context: Application) : PageBuilder() {
        fun sgCartItem(modelInitializer: ItemCartReceiptBindingModelBuilder.() -> Unit): ItemCartReceiptBindingModel_ {
            return ItemCartReceiptBindingModel_().apply {
                modelInitializer()
            }
        }

        inline fun headerScango(modelInitializer: ScangoReceiptHeaderModelBuilder.() -> Unit): ScangoReceiptHeaderModel_ {
            return ScangoReceiptHeaderModel_().apply {
                modelInitializer()
            }
        }

        inline fun itemInvoice(modelInitializer: ItemTaxInvoiceBindingModelBuilder.() -> Unit): ItemTaxInvoiceBindingModel_ {
            return ItemTaxInvoiceBindingModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        inline fun headerFreegiftScango(modelInitializer: ScangoReceiptHeaderFreegiftModelBuilder.() -> Unit): ScangoReceiptHeaderFreegiftModel_ {
            return ScangoReceiptHeaderFreegiftModel_().apply {
                modelInitializer()
            }
        }

        inline fun twoLineText(modelInitializer: ScangoTwoLineTextModelBuilder.() -> Unit): ScangoTwoLineTextModel_ {
            return ScangoTwoLineTextModel_().apply {
                modelInitializer()
            }
        }

        inline fun freeGiftItem(modelInitializer: ScangoFreeGiftItemModelBuilder.() -> Unit): ScangoFreeGiftItemModel_ {
            return ScangoFreeGiftItemModel_().apply {
                modelInitializer()
            }
        }

        inline fun textArrowItem(modelInitializer: ScangoTextArrorItemModelBuilder.() -> Unit): ScangoTextArrorItemModel_ {
            return ScangoTextArrorItemModel_().apply {
                modelInitializer()
            }
        }

        inline fun textRedirectBottomItem(modelInitializer: ScangoTextRedirectModelBuilder.() -> Unit): ScangoTextRedirectModel_ {
            return ScangoTextRedirectModel_().apply {
                modelInitializer()
            }
        }

        inline fun scangoOrderPayment(modelInitializer: OrderPaymentBindingModelBuilder.() -> Unit): OrderPaymentBindingModel_ {
            return OrderPaymentBindingModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        inline fun freeGifts(modelInitializer: ScangoCartFreeGiftDetailModelBuilder.() -> Unit): ScangoCartFreeGiftDetailModel_ {
            return ScangoCartFreeGiftDetailModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        inline fun offerHeader(modelInitializer: OfferHeaderBindingModelBuilder.() -> Unit): OfferHeaderBindingModel_ {
            return OfferHeaderBindingModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        inline fun offerDetail(modelInitializer: ScangoCartDealDetailModelBuilder.() -> Unit): ScangoCartDealDetailModel_ {
            return ScangoCartDealDetailModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        inline fun headerLinkPoint(modelInitializer: ReceiptTopLinkPointBindingModelBuilder.() -> Unit): ReceiptTopLinkPointBindingModel_ {
            return ReceiptTopLinkPointBindingModel_().apply {
                id(hashCode())
                modelInitializer()
            }
        }

        fun buildModels(scangoOrderViewData: ScangoOrderViewData): MutableList<out EpoxyModel<*>> {
            val models: MutableList<EpoxyModel<*>> = mutableListOf()
            //Binh 14/04 this should be remove when api ready
            val data = scangoOrderViewData.payment
            val qrcodeBitmap = scangoOrderViewData.qrcodeBitmap
            var i: Long = 0
            val (offers, offerMap, freeItemMap, freeItems, buyItems, jwxcOffer) = scangoOrderViewData.cartItems.toViewData()

            headerScango {
                data.let {
                    imageId(it.imageId)
                    id(i++)
                    dayTime(it.dayTime)
                    cardNumber(it.cardNumber)
                    hourTime(it.hourTime)
                    moneyNumber(it.moneyNumber)
                    nameStore(it.storeName)
                    orderId(it.qrcode)
                    isVerified(scangoOrderViewData.orderDetail.verificationStatus)
                }
                qrcodeBitmap.let {
                    qrCodeBitmap(it)
                }
                scangoOrderViewData.orderDetail.linkPointReceipt?.pointExtra?.let {
                    pointNumber(it)
                }
            }.run {
                models.add(this)
            }


            //Binh.TH 29.05 TODO Free gift + deal handle
            /*
            horizontalGap {
                size(8)
            }.run {
                models.add(this)
            }
            headerFreegiftScango {
            }.run {
                id(i++)
                models.add(this)
            }

            horizontalGap {
                  size(8)
              }.run {
                  models.add(this)
              }

              headerLinkPoint {}.run {
                  id(i++)
                  models.add(this)
              }*/

            horizontalGap {
                size(24)
            }.run {
                models.add(this)
            }


            textRedirectBottomItem {
            }.run {
                id(i++)
                models.add(this)
            }

            horizontalGap {
                size(24)
            }.run {
                models.add(this)
            }

            textArrowItem {
                id(i++)
                titleText(scangoOrderViewData.cartItems
                    .filter { !it.isFreeItem() }
                    .sumBy {
                        if (it.isSoldByWeight()) 1 else (it.quantity-(it.refundQuantity?.toInt()?:0))
                    }.toString() + " items in total"
                )
            }.run {
                models.add(this)
            }

            scangoOrderViewData.cartItems.filter { !it.isFreeItem() }.map { cartItem ->
                sgCartItem {
                    id(getAutoGenId())
                    cartItem(cartItem)
                    allowToRemove(false)
                    disableWeight(true)
                    paddingValue(16F)
                    numberItem(cartItem.refundQuantity)
                    isRefundItem(cartItem.refundPrice ?: 0.0 > 0.0)
                }
            }.run {
                models.addAll(this)
            }

            horizontalGap {
                size(16)
            }.run {
                models.add(this)
            }

            val finalOffers = offers.plus(scangoOrderViewData.orderDetail.offers)

            val isFreeGiftInCart = freeItems.isNotEmpty()
            val isOfferInCart = finalOffers.isNotEmpty()

            if (isFreeGiftInCart) {
                offerHeader {
                    id(i++)
                    message("Your Free Gifts")
                    count(freeItems.size)
                    spanSizeOverride { totalSpanCount, _, _ ->
                        totalSpanCount
                    }
                }.run {
                    models.add(this)
                }
                freeItems.map { freeGift ->
                    val offer = freeGift.offer?.getOrNull(0)
                    freeGifts {
                        id(i++)
                        item(freeGift)
                        offerProductMap(freeItemMap[offer!!.id] ?: arrayListOf())
                        spanSizeOverride { span, _, _ ->
                            span
                        }
                    }
                }.run {
                    models.addAll(this)
                }
            }

            if (isOfferInCart) {
                offerHeader {
                    id(i++)
                    message("Your Savings from Deals")
                    spanSizeOverride { totalSpanCount, _, _ ->
                        totalSpanCount
                    }
                }.run {
                    models.add(this)
                }
                finalOffers.map { offer ->
                    offerDetail {
                        id("${offer.id}")
                        offer(offer)
                        offerProductMap(offerMap[offer.id] ?: arrayListOf())
                        spanSizeOverride { count, _, _ ->
                            count
                        }
                    }
                }.run {
                    models.addAll(this)
                }
            }

            horizontalGap {
                size(24)
            }.run {
                models.add(this)
            }

            scangoOrderPayment {
                paymentSummary(scangoOrderViewData.paymentSummary)
            }.run {
                models.add(this)
            }

            data.listInvoice?.forEach {
                horizontalGap {
                    size(16)
                }.run {
                    models.add(this)
                }
                itemInvoice {
                    taxInvoice(it)
                }.run {
                    models.add(this)
                }
            }

            horizontalGap {
                size(40)
            }.run {
                models.add(this)
            }

            return models
        }
    }

    suspend fun getBitmapFromSessionId(
        textInput: String
    ) = BarcodeQRCodeUtils.getQRImageFromText(textInput, 144.px, 144.px)

    fun diplayReceiptList(
        payment: Payment,
        orderDetail: ScangoOrderDetail,
        textInput: String
    ) {
        launch {
            withContext(Dispatchers.Main) {
                //Get bitmap from session ID
                qrcodeBitmap = getBitmapFromSessionId(textInput)
                pageModelInternal.value =
                    pageBuilder.buildModels(
                        ScangoOrderViewData(
                            context,
                            orderDetail,
                            payment,
                            qrcodeBitmap
                        )
                    )
                orderDetailStatus.value = OrderDetailSuccess(orderDetail)
            }
        }
    }

    fun getListItemInvoice(scangoOrderDetail: ScangoOrderDetail): List<ScangoItemInvoice>? {
        val listItemInvoice = mutableListOf<ScangoItemInvoice>()
        scangoOrderDetail.let {
            val taxInvoice = ScangoItemInvoice(
                it.cardType,
                DateTimeUtils.getDayyyyyMMddFromString(it.orderPlacedDate),
                it.store?.name,
                it.store?.metaData?.Phone,
                it.storeAddress,
                context.resources.getString(R.string.text_tax_invoice_ue_text),
                context.resources.getString(R.string.text_tax_invoice_gst_text),
                it.orderNumber,
                it.invoiceAmount,
                it.gst7percent,
                it.cardNumber,
                it.customer?.name,
                it.customer?.defaultAddress?.address
            )
            listItemInvoice.add(taxInvoice)
            it.listReturn.forEach { itemReturn ->
                val returnInvoice = ScangoItemInvoice(
                    taxInvoice.cartType,
                    DateTimeUtils.getDayyyyyMMddFromString(itemReturn.getDate()),
                    taxInvoice.nameStore,
                    taxInvoice.phoneNumber,
                    taxInvoice.addressStore,
                    context.resources.getString(R.string.text_tax_invoice_ue_text),
                    context.resources.getString(R.string.text_tax_invoice_gst_text),
                    itemReturn.id?:taxInvoice.orderNumber,
                    itemReturn.total,
                    itemReturn.total?.div(107)?.times(7) ?: 0.0,
                    it.cardNumber,
                    it.customer?.name,
                    it.customer?.defaultAddress?.address,
                    true
                ).run { listItemInvoice.add(this) }
            }
        }
        return listItemInvoice
    }
}

sealed class OrderDetailStatus
object OrderDetailLoading : OrderDetailStatus()
class OrderDetailSuccess(val orderDetail: ScangoOrderDetail) : OrderDetailStatus()
class OrderDetailFailure(val e: Throwable) : OrderDetailStatus()

class ScangoOrderViewData(
    val context: Application,
    val orderDetail: ScangoOrderDetail,
    val payment: Payment,
    val qrcodeBitmap: Bitmap
) {
    val paymentSummary = ScangoPaymentSummary(
        orderDetail.totalAmount ?: 0.0,
        orderDetail.subtotal ?: 0.0,
        orderDetail.discountAmount?.toDoubleOrNull() ?: 0.0,
        orderDetail.coupons,
        orderDetail.cardType,
        orderDetail.cardNumber,
        false,
        totalCouponDiscount = orderDetail.couponDiscount,
        linkPointReceipt = orderDetail.linkPointReceipt
    )

    val cartItems = orderDetail.cartItems
}