package ntuc.fairprice.omni.scango.ui.screen.orders.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.R2

@EpoxyModelClass(layout = R2.layout.order_list_header)
abstract class ScangoOrderListHeader : EpoxyModelWithHolder<ScangoOrderListHeader.HeaderHolder>() {

    @EpoxyAttribute var header : String? = null

    override fun bind(holder : HeaderHolder) {
        holder.headerTv.setText(header)
    }

    class HeaderHolder : ScangoBaseEpoxyHolder() {

        val headerTv : TextView by bind(R.id.orderListHeaderTv)

    }

}