package ntuc.fairprice.omni.dialog

import android.content.Context
import ntuc.fairprice.omni.R


class ScangoNetworkErrorDialog(context: Context): ScangoBaseErrorDialog(context) {

    init {
        title.text = context.resources.getString(R.string.network_error_dialog_title)
        message.text = context.resources.getString(R.string.network_error_dialog_message)
        erroImage.setImageResource(R.drawable.sg_ic_no_network)
    }
}