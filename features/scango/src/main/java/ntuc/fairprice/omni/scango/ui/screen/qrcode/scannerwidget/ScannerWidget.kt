package ntuc.fairprice.omni.scango.ui.screen.qrcode.scannerwidget

import android.app.Activity
import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScannerWidget: LayoutContainer {

    fun initWidget(view: View, scanType: Int)

    fun start(activity: Activity)

    fun destroy()

    fun stop()

    fun updateFacing(isChecked: Boolean)
}