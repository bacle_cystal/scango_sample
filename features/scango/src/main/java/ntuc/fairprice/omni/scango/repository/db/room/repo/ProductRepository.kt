package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.OpenForTesting
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoProductService
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.room.repo.Repository
import javax.inject.Inject

@OpenForTesting
@FeatureScope
class ProductRepository @Inject constructor(
    mApplication: Application,
    private val scangoProductService: ScangoProductService
) : Repository(mApplication) {

    // Network calls getProductByBarcode
    suspend fun getProductByBarcode(barcode: String): Result<Product> {
        return scangoProductService.getProductByBarcode(
            barcode,
            localStorage.getInt(Constants.SHARE_PRE_STORE_ID, 0)
        )
    }
}