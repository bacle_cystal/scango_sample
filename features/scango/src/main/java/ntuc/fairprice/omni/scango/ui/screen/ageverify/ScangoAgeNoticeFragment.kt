package ntuc.fairprice.omni.scango.ui.screen.ageverify

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_age_verify_notice_fragment.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoAgeVerifyNoticeFragmentBinding
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanActivity
import ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutActivity
import javax.inject.Inject

class ScangoAgeNoticeFragment :
    ScangoBaseBottomSheetFragment<ScangoAgeVerifyNoticeFragmentBinding, ScangoAgeNoticeViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "ScangoAgeNoticeFragment"
        fun newInstance() = ScangoAgeNoticeFragment()
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ScangoAgeNoticeViewModel by lazy {
        viewModelFactory.create(ScangoAgeNoticeViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_age_verify_notice_fragment

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog)
        btn_age_notice_got_it.setOnClickListener {
            (activity as ScangoCheckoutActivity).startActivityForResult(
                Intent(activity, ScangoAgeVerifyScanActivity::class.java),
                Constants.REQUEST_AGE_VERIFY
            )
            dismiss()
        }
        text_do_it_later.setOnClickListener {
            dismiss()
        }
    }

}
