package ntuc.fairprice.omni.scango.ui.screen.tutorial

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoItemTutorialHomeBinding

class TutorialHomeAdapter(val mContext: Context) : RecyclerView.Adapter<TutorialHomeAdapter.TutorialHolder>() {
    var items: List<TutorialHomeItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TutorialHolder {
        val binding: ScangoItemTutorialHomeBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.scango_item_tutorial_home, parent, false)
        return TutorialHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TutorialHolder, position: Int) = holder.bind(items[position])

    inner class TutorialHolder(val binding: ScangoItemTutorialHomeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TutorialHomeItem) {
            binding.apply {
                textTutorialHeader.text = item.header
                textTutorialDetail.text = item.detail
                imageTutorialHome.setImageDrawable(ContextCompat.getDrawable(mContext, item.imageTutorialId))
            }.executePendingBindings()
        }
    }
}