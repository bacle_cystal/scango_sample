package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder
import thor.zopsmart.com.thor.base.extensions.setImageDrawable

@EpoxyModelClass(layout = R.layout.scango_layout_item_free_gift)
abstract class ScangoFreeGiftItemModel : EpoxyModelWithHolder<ScangoFreeGiftItemModel.ScangoFreeGiftHeaderHolder>() {
    @EpoxyAttribute
    var imageId: String? = null
    @EpoxyAttribute
    var titleText: String? = null
    @EpoxyAttribute
    var contentText: String? = null

    override fun bind(holder: ScangoFreeGiftHeaderHolder) {
        //holder.parent.setOnClickListener(clickListener)
        holder.apply {
            title.text = titleText
            content.text = contentText
            //image.setImageDrawable(ContextCompat.getDrawable(R.drawable.sg_free_gift))
        }
    }

    class ScangoFreeGiftHeaderHolder : ScangoBaseEpoxyHolder() {
        val title: TextView by bind(R.id.tv_item_name)
        val content: TextView by bind(R.id.tv_item_quantity_desc)
        val image: ImageView by bind(R.id.iv_item_image)
    }
}
