package ntuc.fairprice.omni.toggle

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ntuc.fairprice.omni.ui.utils.default

class ToggleFragmentViewModel : ViewModel() {
    var companyName = MutableLiveData<String>().default("")
    var isOnStore = MutableLiveData<Boolean>(false)
    var isVisiableCompanyLayout = MutableLiveData<Boolean>().default(false)
    var changeScreen = SingleLiveEvent<Boolean>()
    var isEnableScanGoFeature = false

    fun changeFeatureFlag(isEnable: Boolean) {

    }

    fun changeState(state: Boolean) {
        isOnStore.value = !state
        changeScreen.call()
    }

    fun changeStateCompanyLayout(name: String) {
        companyName.value = name
        isVisiableCompanyLayout.value = !name.isBlank()
    }
}