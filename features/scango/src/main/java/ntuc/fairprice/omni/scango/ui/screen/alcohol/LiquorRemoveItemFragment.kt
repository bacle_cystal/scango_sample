package ntuc.fairprice.omni.scango.ui.screen.alcohol

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_liquor_remove_item_fragment.*
import kotlinx.android.synthetic.main.scango_layout_stylish_toolbar.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.MessageEvent
import ntuc.fairprice.omni.scango.base.ScangoBaseDialogFragment
import ntuc.fairprice.omni.scango.databinding.ScangoLiquorRemoveItemFragmentBinding
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutActivity
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class LiquorRemoveItemFragment :
    ScangoBaseDialogFragment<ScangoLiquorRemoveItemFragmentBinding, LiquorRemoveItemViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "LiquorRemoveItemFragment"

        fun newInstance() = LiquorRemoveItemFragment()
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: LiquorRemoveItemViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(LiquorRemoveItemViewModel::class.java)
    }

    private val cartViewModel: ScangoCartViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoCartViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_liquor_remove_item_fragment

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler_remove_item.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = liquorAdapter
        }

        btnLiquorCheckout.setOnClickListener {
            viewModel.sendEventRemoveSuccess()
            dismiss()
        }

        iv_close_back.setOnClickListener {
            viewModel.sendEventRemoveSuccess()
            dismiss()
        }

        text_warning_time.text = cartViewModel.getTextTimeWarning(null)

        viewModel.apply {
            liquorsLiveData.observe(viewLifecycleOwner, Observer {
                liquorAdapter.items = it
                liquorAdapter.notifyDataSetChanged()
            })

            removeCompleteEvent.observe(viewLifecycleOwner, Observer {
                EventBus.getDefault().post(
                    MessageEvent(Constants.MESSAGE_EVENT_REMOVE_LIQUOR_COMPLETE)
                )
                callBackListener?.onRemovedItemLiquor()
            })
            checkRemoveLiquorItem(cartViewModel)
        }
    }

    private val liquorAdapter: LiquorAdapter by lazy {
        LiquorAdapter()
    }


    private var callBackListener: LiquorRemoveListener? = null

    fun setCallbackListener(callback: LiquorRemoveListener) {
        this.callBackListener = callback
    }

    interface LiquorRemoveListener {
        fun onRemovedItemLiquor()
    }

    fun onBack() {
        viewModel.sendEventRemoveSuccess()
        dismiss()
    }
}


