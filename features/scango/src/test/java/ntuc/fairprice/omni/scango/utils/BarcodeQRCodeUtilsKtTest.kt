package ntuc.fairprice.omni.scango.utils

import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import ntuc.fairprice.omni.scango.repository.db.room.entity.LooseItemBarcode
import org.junit.Assert
import org.junit.Test

class BarcodeQRCodeUtilsKtTest {

    // ------------- Test UPC-E to UPC-A -----------------------------------------
    @Test
    fun test_UPC_E_to_UPC_A_case_1() {
        val input = "04016308"
        val result = BarcodeQRCodeUtils.getScanData(input, FirebaseVisionBarcode.FORMAT_UPC_E)
        Assert.assertEquals("4000000163", result)
    }

    @Test
    fun test_UPC_E_to_UPC_A_case_2() {
        val input = "04016201"
        val result = BarcodeQRCodeUtils.getScanData(input, FirebaseVisionBarcode.FORMAT_UPC_E)
        Assert.assertEquals("4000000162", result)
    }

    @Test
    fun test_UPC_E_to_UPC_A_case_3() {
        val input = "04078803"
        val result = BarcodeQRCodeUtils.getScanData(input, FirebaseVisionBarcode.FORMAT_UPC_E)
        Assert.assertEquals("4000000788", result)
    }

    // ------------- End Test UPC-E to UPC-A -----------------------------------------

    // ------------- Test no need to extract series -----------------------------------------
    @Test
    fun test_is22Series_not22_false() {
        val input = "33123456789"
        val result = BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertNull(result)
    }
    // ------------- End Test no need to extract series -----------------------------------------


    @Test
    fun test_barcodeToEAN_invalid() {
        val input = ""
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertNull(result)
    }


    // ------------- Test 22 generate -----------------------------------------
    @Test
    fun test_barcodeToEAN_22_1stCase() {
        val input = "225730903000001453"
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertEquals(LooseItemBarcode("220000057309", 3.0, 1.45), result)
    }
    @Test
    fun test_barcodeToEAN_22_2ndCase() {
        val input = "226005000926001395"
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertEquals(LooseItemBarcode("220000060050", 0.926, 1.39), result)
    }
    // ------------- End Test 22 generate -----------------------------------------


    // ------------- Test 21 generate -----------------------------------------
    @Test
    fun test_barcodeToEAN_21_1stCase() {
        val input = "2192800309532"
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertEquals(LooseItemBarcode("210000928003", 0.0, 9.53), result)
    }
    @Test
    fun test_barcodeToEAN_21_2ndCase() {
        val input = "2192821504909"
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertEquals(LooseItemBarcode("210000928215", 0.0, 4.9), result)
    }
    // ------------- End Test 21 generate -----------------------------------------


    // ------------- Test 25 generate -----------------------------------------
    @Test
    fun test_barcodeToEAN_25_1stCase() {
        val input = "258880090000111113"
        val result= BarcodeQRCodeUtils.barcodeToEAN(input)
        Assert.assertEquals(LooseItemBarcode("250000888009", 0.0, 111.11), result)
    }
    // ------------- End Test 25 generate -----------------------------------------
}