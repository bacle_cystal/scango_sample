package ntuc.fairprice.omni.scango.repository.db.room.typeconverter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer

class ScangoConverters {
    companion object {
        private const val DELIMITER = ","

        @TypeConverter
        @JvmStatic
        fun toStringOfferList(offers: List<ScangoOffer>?): String? {
            val offerz: MutableList<String> = mutableListOf()
            if (offers != null) {
                for (offer in offerz) {
                    val gson = Gson()
                    offerz.add(gson.toJson(offer))
                }
            }
            val gson = Gson()
            return gson.toJson(offerz)
        }

        @TypeConverter
        @JvmStatic
        fun toOfferList(offers: String?): List<ScangoOffer>? {
            if(offers == null)
                return arrayListOf()
            val gson = Gson()
            val listType = object : TypeToken<List<String>>() {}.type
            val stringList = gson.fromJson<List<String>>(offers, listType) ?: listOf()

            val offerz = arrayListOf<ScangoOffer>()

            for (offerString in stringList) {
                val offerItem = gson.fromJson(offerString, ScangoOffer::class.java)
                offerz.add(offerItem)
            }
            return offerz
        }

        @TypeConverter
        @JvmStatic
        fun toStringCouponList(coupons: List<ScangoCoupon>): String {
            val couponzz: MutableList<String> = mutableListOf()
            if (coupons != null) {
                for (c in coupons) {
                    val gson = Gson()
                    couponzz.add(gson.toJson(c))
                }
            }
            val gson = Gson()
            return gson.toJson(couponzz)
        }

        @TypeConverter
        @JvmStatic
        fun toCouponList(couponListString: String?): List<ScangoCoupon>? {

            if (couponListString == null)
                return listOf()

            val gson = Gson()
            val listType = object : TypeToken<List<String>>() {}.type
            val coupons = gson.fromJson<List<String>>(couponListString, listType) ?: listOf()

            val couponzz: MutableList<ScangoCoupon> = mutableListOf()

            for (couponString in coupons) {
                val coupon = gson.fromJson(couponString, ScangoCoupon::class.java)
                couponzz.add(coupon)
            }
            return couponzz
        }
    }


}