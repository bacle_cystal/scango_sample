package ntuc.fairprice.omni.scango.ui.screen.qrcode.product

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import javax.inject.Inject

class BarcodeProductRapidViewModel @Inject constructor(application: Application) : ScangoBaseViewModel(application) {
    var productItem = MutableLiveData<Product>()
    var isMaxItem = MutableLiveData<Boolean>(false)
    var isLiquorLate = MutableLiveData<Boolean>(false)
    var screenType = MutableLiveData<Int>()

    val imageProduct = MediatorLiveData<String>().apply {
        addSource(productItem) { product ->
            value = product.getImage()
        }
    }

    fun showMaxItemUI() {
        isMaxItem.value = true
        isLiquorLate.value = false
    }

    fun showLiquorLate() {
        isMaxItem.value = false
        isLiquorLate.value = true
    }
}
