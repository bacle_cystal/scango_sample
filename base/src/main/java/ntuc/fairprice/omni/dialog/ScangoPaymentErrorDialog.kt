package ntuc.fairprice.omni.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.TextView
import ntuc.fairprice.omni.R

class ScangoPaymentErrorDialog(val context: Context) {

    val message: TextView
    val buttonAction: TextView
    val textAction: TextView
    var onActionClick: () -> Unit = {}
    var onReviewActionClick: () -> Unit = {}

    private var dialog: Dialog

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.scango_error_payment_dialog_layout, null)

        message = view.findViewById(R.id.scango_dialog_error_textView_message)
        buttonAction = view.findViewById(R.id.scango_dialog_error_textView_button)
        textAction = view.findViewById(R.id.text_review_payment)
        buttonAction.setOnClickListener {
            onActionClick.invoke()
            dismiss()
        }
        buttonAction.setOnClickListener {
            onReviewActionClick.invoke()
            dismiss()
        }

        dialog = AlertDialog.Builder(context).setView(view).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }
}