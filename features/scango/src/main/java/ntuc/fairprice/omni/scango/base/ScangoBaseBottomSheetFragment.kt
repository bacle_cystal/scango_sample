package ntuc.fairprice.omni.scango.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.interfaces.ScangoAppView

abstract class ScangoBaseBottomSheetFragment<ViewBinding : ViewDataBinding, ViewModel : androidx.lifecycle.ViewModel> :
    BottomSheetDialogFragment(), ScangoAppView {
    lateinit var viewBinding: ViewBinding

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutId: Int

    override val mContext: Context
        get() = activity as Context

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialog)
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.apply {
            setVariable(BR.viewModel, viewModel)
            root.isClickable = true
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }
    }

    fun addFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            setTransactionAnimation(this, transit)
            add(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun replaceFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            setTransactionAnimation(this, transit)
            replace(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun setTransactionAnimation(
        fragmentTransaction: FragmentTransaction,
        transitionFrom: TransitionFrom
    ) {
        fragmentTransaction.apply {
            when (transitionFrom) {
                TransitionFrom.BOTTOM -> setCustomAnimations(
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom,
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom
                )
                TransitionFrom.RIGHT -> setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_right,
                    R.anim.enter_from_right,
                    R.anim.exit_to_right
                )
                TransitionFrom.NONE -> { // do nothing
                }
                else -> { // do nothing
                }
            }
        }
    }

    private fun commitTransaction(
        transaction: FragmentTransaction,
        addToBackStack: Boolean = false
    ) {
        if (addToBackStack) transaction.addToBackStack(null)
        transaction.commit()
    }

    enum class TransitionFrom {
        RIGHT, BOTTOM, LEFT, TOP, NONE
    }

}