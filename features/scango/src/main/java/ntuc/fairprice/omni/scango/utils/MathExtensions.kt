package ntuc.fairprice.omni.scango.utils

import thor.zopsmart.com.thor.base.configurations.Config
import java.lang.IllegalArgumentException
import java.text.DecimalFormat


/**
 * Rounds double value to the specified number of decimal places
 *
 * @param places number of places to round to
 */
internal fun Double.round(places: Int = 2): Double {
    if(places <= 0) throw IllegalArgumentException()

    val factor = Math.pow(10.0, places.toDouble())
    return Math.round(this * factor) / factor
}

fun Double.format(): String {
    val decim = DecimalFormat("0.00")
    return decim.format(this)
}

fun Double.toCurrencyString(): String {
    return Config.CURRENCY + round(2).format()
}

fun Double.removeRedundant0(): String {
    var numberString = this.toString()
    val decimalPlacePosition = numberString.indexOf(".")
    return if (decimalPlacePosition > 0) {
        for (i in numberString.length-1 downTo decimalPlacePosition) {
            if (numberString.substring(i, i+1) == "0") numberString = numberString.removeRange(i, i+1)
            else break
        }
        if (numberString.endsWith(".")) numberString.replace(".", "") else numberString
    } else {
        this.toInt().toString()
    }
}

fun Double.hasDecimalValue() = ((this % 1.0) == 0.0)