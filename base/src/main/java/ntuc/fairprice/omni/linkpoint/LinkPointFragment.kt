package ntuc.fairprice.omni.linkpoint

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ViewFlipper
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_bottom_sheet_unlink.view.*
import kotlinx.android.synthetic.main.fragment_link_point.*
import ntuc.fairprice.omni.Constants.scangoLinkPlusPhoneNumberActivityClassname
import ntuc.fairprice.omni.R
import ntuc.fairprice.omni.domain.model.linkpoint.LinkPointStatus
import ntuc.fairprice.omni.util.CustomerHelpUtils
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog

class LinkPointFragment : Fragment() {

    private lateinit var viewFlipper: ViewFlipper
    private var isAnotherLPAccount : Boolean = false

    companion object {
        val TAG = "LinkPointFragment"

        const val FLIPPER_STATE_LP_PROGRESS = 0
        const val FLIPPER_STATE_LP_NOT_LINKED = 1
        const val FLIPPER_STATE_LP_LINKED_OR_PARTIAL = 2
        const val FLIPPER_STATE_LP_SOMETHING_WRONG = 3

        private const val INIT_LINK_POINT_STATE = "init_link_point_state"
        fun newInstance(isOnStore: Boolean = false) = LinkPointFragment().apply {
            arguments = Bundle().apply {
                putBoolean(INIT_LINK_POINT_STATE, isOnStore)
            }
        }
    }

    private lateinit var scanGoProgressDialog: ScanGoProgressDialog
    private lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
    private lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    private lateinit var viewModel: LinkPointViewModel
    private lateinit var bottomSheetDialog: BottomSheetDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_link_point, container, false)
        viewFlipper = view.findViewById(R.id.link_point_view_flipper)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(LinkPointViewModel::class.java)

        context?.let {
            scanGoProgressDialog = ScanGoProgressDialog(it)
            scangoGeneralErrorDialog = ScangoGeneralErrorDialog(it)
            scangoNetworkErrorDialog = ScangoNetworkErrorDialog(it)

            bottomSheetDialog = BottomSheetDialog(it)
            initUnlinkBottomSheet()
        }

        viewFlipper.displayedChild = FLIPPER_STATE_LP_NOT_LINKED
        viewModel.resultLiveData.observe(this, Observer(::onViewModelStateChanged))
        initListeners()
    }

    private fun processLinkPointStatus(it: LinkPointStatus){

        cl_link_point_contact.visibility = View.GONE

        if (it.isFullyLinked) {
            //Fully linked. All operations allowed.
            viewFlipper.displayedChild = FLIPPER_STATE_LP_LINKED_OR_PARTIAL

            txt_link_point_point.text = getString(R.string.two_decimal_formatted_val, it.pointBalance)
            txt_link_point_dollar.text = getString(R.string.equal_dollar_value, it.dollarBalance)
        } else if (it.isPartiallyLinked) {
            //Partially linked. Spending of Link point is not supported
            viewFlipper.displayedChild = FLIPPER_STATE_LP_LINKED_OR_PARTIAL
            cl_link_point_contact.visibility = View.VISIBLE
            txt_link_point_point.text = getString(R.string.two_decimal_formatted_val, it.pointBalance)
            txt_link_point_dollar.text = getString(R.string.equal_dollar_value, it.dollarBalance)
        } else {
            //Not linked. -- hence do nothing!
            viewFlipper.displayedChild = FLIPPER_STATE_LP_NOT_LINKED
        }
    }

    private fun apiCallToGetLpStatus(){
        viewFlipper.displayedChild = FLIPPER_STATE_LP_PROGRESS
        viewModel.getLinkPointStatus()
    }

    private fun initUnlinkBottomSheet() {

        val view = layoutInflater.inflate(R.layout.fragment_bottom_sheet_unlink, null)
        bottomSheetDialog.setContentView(view)

        view.txt_link_plus.setOnClickListener {
            unlink()
        }
        view.txt_link_plus_try_with_card.setOnClickListener {
            tryAnotherLP()
        }

    }

    private fun unlink(){
        scanGoProgressDialog.show()
        viewModel.unlinkLPAccount()

    }

    private fun tryAnotherLP(){
        isAnotherLPAccount = true
        scanGoProgressDialog.show()
        viewModel.unlinkLPAccount()

    }

    private fun openLinkWithPhoneNumberScreen() {
        startActivity(Intent().apply {
            setClassName(requireContext(), scangoLinkPlusPhoneNumberActivityClassname)
        })
    }

    override fun onResume() {
        super.onResume()
        apiCallToGetLpStatus()
    }

    private fun initListeners(){

        txt_link_point_contact_verify.setOnClickListener {
            openLinkWithPhoneNumberScreen()
        }
        btn_linkpoint_call_us.setOnClickListener {
            context?.let {
                CustomerHelpUtils.dialSupport(it)
            }
        }
        btn_linkpoint_email_us.setOnClickListener {
            context?.let {
                CustomerHelpUtils.emailSupport(it)
            }
        }

        cl_link_point.setOnClickListener {
            openLinkWithPhoneNumberScreen()
        }

        cl_link_point_try.setOnClickListener {
            apiCallToGetLpStatus()
        }

        img_link_point_narrow_verified.setOnClickListener {
            bottomSheetDialog.show()
        }

        txt_link_point_email.setOnClickListener {
            context?.let {
                CustomerHelpUtils.emailSupport(it)
            }
        }

        btn_linkpoint_call_us.setOnClickListener {
            context?.let {
                CustomerHelpUtils.dialSupport(it)
            }
        }
    }

    private fun onViewModelStateChanged(state: LinkPointStates) {
        when (state) {
            is LinkPointStates.UnlinkSuccess -> {
                scanGoProgressDialog.dismiss()
                bottomSheetDialog.hide()
                if (isAnotherLPAccount){
                    openLinkWithPhoneNumberScreen()
                } else {
                    //Refreshing LP Status
                    apiCallToGetLpStatus()
                }
            }
            is LinkPointStates.UnlinkFailedGeneral -> {
                scanGoProgressDialog.dismiss()
                bottomSheetDialog.hide()
                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        if (isAnotherLPAccount){
                            tryAnotherLP()
                        } else {
                            unlink()
                        }
                    }
                    show()
                }
            }
            is LinkPointStates.UnlinkFailedNetwork -> {
                scanGoProgressDialog.dismiss()
                bottomSheetDialog.hide()
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        if (isAnotherLPAccount){
                            tryAnotherLP()
                        } else {
                            unlink()
                        }
                    }
                    show()
                }
            }
            is LinkPointStates.LinkPointStatusSuccess -> {
                processLinkPointStatus(state.linkPointStatus)
            }
            is LinkPointStates.LinkPointStatusFailedGeneral -> {
                viewFlipper.displayedChild = FLIPPER_STATE_LP_SOMETHING_WRONG
            }
            is LinkPointStates.LinkPointStatusFailedNetwork -> {
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        apiCallToGetLpStatus()
                    }
                    show()
                }
            }
        }
    }
}