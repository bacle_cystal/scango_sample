package ntuc.fairprice.omni.scango.ui.screen.productdetail.ui

import android.app.Activity
import android.content.Context
import dagger.Binds
import dagger.Module
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget.ScangoLinkPlusPhoneNumberBoxWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget.ScangoLinkPlusPhoneNumberBoxWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoProductDetailBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoProductDetailActivity): Activity

}