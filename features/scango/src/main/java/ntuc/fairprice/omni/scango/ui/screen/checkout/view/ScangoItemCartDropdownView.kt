package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.scango_list_itemcart_drop_down_view.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.epoxy.SGCartItemListCheckout
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.utils.dp
import thor.zopsmart.com.thor.base.extensions.image
import thor.zopsmart.com.thor.base.extensions.onClick
import thor.zopsmart.com.thor.epoxy.FPDelegate
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.R as RFP

class ScangoItemCartDropdownView : ConstraintLayout, FPDelegate {
    lateinit var clickHandler: (CartItem) -> Unit?

    constructor(context: Context) : super(context) {
        initialise(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialise(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialise(attrs)
    }

    private fun initialise(attrs: AttributeSet?) {
        View.inflate(context, R.layout.scango_list_itemcart_drop_down_view, this).apply {
            val params = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            )
            params.setMargins(16.dp, 0, 16.dp, 0)
            layoutParams = params
        }

        setBackgroundResource(R.drawable.scango_edit_text_border)
        arrayListOf(tv_drop_down, iv_dropdown_arrow).onClick {
            if (scango_epoxy_cart.visibility == View.GONE) {
                iv_dropdown_arrow.image(RFP.drawable.ic_keyboard_arrow_up_black_24dp)
                scango_epoxy_cart.visibility = View.VISIBLE
            } else {
                iv_dropdown_arrow.image(RFP.drawable.ic_keyboard_arrow_down_black_24dp)
                scango_epoxy_cart.visibility = View.GONE
            }
        }
    }

    fun updateListItemCart(listCartitem: List<ScangoCartItem>) {
        with(scango_epoxy_cart) {
            val epoxyVisibilityTracker = com.airbnb.epoxy.EpoxyVisibilityTracker()
            epoxyVisibilityTracker.attach(this)
            layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 30)

            buildModelsWith { controller ->
                controller.spanCount = 30
                SGCartItemListCheckout(listCartitem)
                    .render(this@ScangoItemCartDropdownView).forEach {
                        it.addTo(controller)
                    }

            }
        }

        postInvalidate()
    }

}