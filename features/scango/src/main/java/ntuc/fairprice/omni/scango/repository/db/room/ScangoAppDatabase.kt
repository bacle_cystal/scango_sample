@file:Suppress("unused")

package ntuc.fairprice.omni.scango.repository.db.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import ntuc.fairprice.omni.scango.repository.db.room.dao.ScangoCartItemDao
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem

@Database(entities = [ScangoCartItem::class],  version = 1, exportSchema = false)
abstract class ScangoAppDatabase : RoomDatabase() {

    abstract fun getScangoCartItemDao(): ScangoCartItemDao

    companion object {
        private const val DB_NAME = "scango"
        private var INSTANCE: ScangoAppDatabase? = null

        fun getInstance(context: Context): ScangoAppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder<ScangoAppDatabase>(
                    context.applicationContext,
                    ScangoAppDatabase::class.java,
                    DB_NAME
                )
                    .addMigrations()
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}