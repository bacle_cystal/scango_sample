package ntuc.fairprice.omni.scango.ui.screen.linkplus.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ntuc.fairprice.omni.scango.repository.webservice.model.ApiResponse
import thor.zopsmart.com.thor.base.extensions.double
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.safeJson
import java.io.Serializable

@Parcelize
data class LinkPointStatus(
    var linkStatus: Int = 0,
    var pointBalance: Double = 0.0,
    var dollarBalance: Double = 0.0
): Parcelable {
    val isPartiallyLinked
        get() = linkStatus == 2

    val isFullyLinked
        get() = linkStatus == 1

    val isNotLinked
        get() = linkStatus == 0
}

fun ApiResponse.toLinkPointStatus(): LinkPointStatus {

    val data = this.toJSONObject() safeJson "data"
    val linkStatus = data int "linkStatus" ?: 0
    val pointBalance = data double "pointBalance" ?: 0.0
    val dollarBalance = data double "dollarBalance" ?: 0.0
    return LinkPointStatus(linkStatus, pointBalance, dollarBalance)
}