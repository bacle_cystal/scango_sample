## Integrated OmniCore,OmniToggle to Scan-Go Module
1. OmniCore has been configured in OmniApplication
2. You can use this in anywhere by using
	OmniCore.getInstance()
    OmniToggle.getInstance()
3. In case build fail because of unresolve "sg.ntucenterprise:omni-core{version}.omnicore" or "sg.ntucenterprise:omni-toggle:{version}.omnitoggle". Please check for lastest version.
4. The value of feature-toggle-app-id in omni-config uses for call API. It can be change if required 
5. Including below option in application graddle file to fix incompatible version 
```groovy
	compileOptions {
       sourceCompatibility JavaVersion.VERSION_1_8
		targetCompatibility JavaVersion.VERSION_1_8
   }
```
## Integrated InstaBug SDK to ScanGo Module
1. User can provide feedback and send it to the Instabug by SCREENSHOT event
2. Developer can add more envent by updating 
```groovy
 		Instabug.Builder(this, Constants.INSTABUG_TOKEN)
        	.setInvocationEvents(InstabugInvocationEvent.SCREENSHOT,
            InstabugInvocationEvent.SHAKE,
            InstabugInvocationEvent.FLOATING_BUTTON,
            InstabugInvocationEvent.TWO_FINGER_SWIPE_LEFT).build()
```