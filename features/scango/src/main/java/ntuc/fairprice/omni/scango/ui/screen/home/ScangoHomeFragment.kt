package ntuc.fairprice.omni.scango.ui.screen.home

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyVisibilityTracker
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_home.*
import kotlinx.android.synthetic.main.scango_item_home_header.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.base.ScangoHomeCampaignDelegate
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentHomeBinding
import thor.zopsmart.com.thor.epoxy.FPAbstractDelegates
import javax.inject.Inject

class ScangoHomeFragment :
    ScangoBaseFragment<ScangoFragmentHomeBinding, ScangoHomeFragmentViewModel>(),
    HasSupportFragmentInjector, FPAbstractDelegates, ScangoHomeCampaignDelegate {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override val layoutId: Int = R.layout.scango_fragment_home

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ScangoHomeFragmentViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoHomeFragmentViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCartItemsLiveData()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.apply {
            numberItemInCart.observe(viewLifecycleOwner, Observer {
                icon_cart.update(it)
            })
        }

//        val gridLayoutManager = GridLayoutManager(mContext, 2)

        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(epoxy_recycler_view)

        epoxy_recycler_view.layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
        epoxy_recycler_view.buildModelsWith { controller ->
            viewModel.pageData.value?.forEach { it ->
                it.render(this@ScangoHomeFragment).forEach {
                    it.addTo(controller)
                }
            }
        }

        viewModel.pageData.observe(this, Observer {
            if (it != null) {
                epoxy_recycler_view.requestModelBuild()
            }
        })
        viewModel.loadPage()

        home_header.setOnClickListener {
            mainActivity.launchActivity(Constants.cartClassname)
        }
        /* val gridLayoutManager = GridLayoutManager(mContext)

         val epoxyVisibility
         epoxy_recycler_view.layoutManager Tracker = EpoxyVisibilityTracker()
         epoxyVisibilityTracker.attach(epoxy_recycler_view)
= gridLayoutManager

         epoxy_recycler_view.buildModelsWith { controller ->
             viewModel.pageData.value?.forEach { it ->
                 it.render(this@ScangoHomeFragment).forEach {
                     it.addTo(controller)
                 }
             }
         }*/
    }

    override fun onCampaignClick(campaign: String) {
        if(activity is ScangoFeatureActivity && activity != null) {
            val parseCampaignId = campaign.replace("/", "").replace("#", "").trim()
            if(parseCampaignId.isNotEmpty()) {
                (activity as ScangoFeatureActivity).onBannerClicked(parseCampaignId)
            }
        }
    }

}
