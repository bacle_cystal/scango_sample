package ntuc.fairprice.omni.scango

object Constants {
    const val INTENT_RESULT_CITY_NAME = "intent_result_city_name"
    const val INTENT_ALARMSERVICE_REQUESTCODE = "intent_alarm_service_requestcode"
    const val INTENT_LINK_POINT_REFRESH = "intent_link_point_refresh"
    const val SHARE_PRE_STORE_ID = "intent_result_store_id"
    const val SHARE_PRE_STORE_NAME = "intent_result_store_name"
    const val SHARE_PRE_TIME_STAMP = "current_timestamp"
    const val SHARE_PRE_STORE_SESSION_ID = "intent_result_store_session_id"
    const val SHARE_PRE_APIDELAY_TIME = "api_delay_time"
    const val SHARE_PRE_SERVERGAP_TIME = "server_gap_time"
    const val SHARE_PRE_SHOW_RAPID_GUIDE = "intent_result_show_rapid_guide"
    const val SHARE_PRE_AGE_VERIFY_CODE = "age_vefity_code"

    const val packageName = "ntuc.fairprice.omni"

    //Request code
    const val REQUEST_NAME_STORE = 10001
    const val REQUEST_NAME_PRODUCT = 10002
    const val REQUEST_ALARM_NOTICE_10h20 = 10003
    const val REQUEST_ALARM_REMOVE_LIQUOR_10h30 = 10004
    const val REQUEST_PROMO = 10008

    const val REQUEST_AGE_VERIFY = 10010
    const val ANIM_TIME_CONFIG = 0.6

    //Sound service
    const val INTENT_SOUNDSERVICE_ACTION = "intent_sound_service_action"
    const val INTENT_SOUNDSERVICE_FILE_NAME = "intent_bundle_sound_file_name"


    //Event bus
    const val MESSAGE_EVENT_NOTICE_10h20 = "notice"
    const val MESSAGE_EVENT_REMOVE_LIQUOR_10h30 = "remove-liquor"
    const val MESSAGE_EVENT_REMOVE_LIQUOR_COMPLETE = "remove-liquor-complete"
    const val MESSAGE_EVENT_ERROR_TOKEN_EXPIRED = "error-token-expired"


    const val scanGoClassname = "$packageName.scango.ScangoFeatureActivity"
    const val fairpriceOnClassname = "$packageName.fairpriceon.FairpriceOnFeatureActivity"
    const val instantClassname = "$packageName.instant.InstantActivity"
    //    const val cartClassname = "thor.zopsmart.com.thor.features.cart.ui.CartActivity"
    const val cartClassname = "ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivity"
    const val checkoutClassname = "ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutActivity"
    const val searchClassname = "thor.zopsmart.com.thor.features.search.ui.SearchActivity"

    //SharedPreference Keys
    const val KEY_RESTRICTIONS_CONFIG = "key_restrictions_config"
    const val KEY_LP_IS_DISMISSED = "key_lp_isdismissed"

    //Intent Extras
    const val EXTRA_OTP_NUMBER = "otp_number"
    const val EXTRA_LINK_POINT_STATUS = "extra_lp_status"

    const val EXTRA_FEEDBACK_CUSTOMER_NAME = "extra_feedback_customer_name"
    const val EXTRA_RECEIPT_TOTAL_AMOUNT = "extra_total_amount"
    const val EXTRA_RECEIPT_CARD_NUMBER = "extra_card_number"
    const val EXTRA_RECEIPT_CARD_TYPE = "extra_card_type"
    const val EXTRA_BYTE_ARRAY_QR = "extra_byte_array_qr"


    const val EXTRA_PRODUCT_DETAIL_URL = "extra_product_detail_url"
    const val EXTRA_PRODUCT_DETAIL_ID = "extra_product_detail_id"

    //Permission Requests
    const val RUNTIME_PERMISSION_REQUEST = 9000

    //Config KEY
    const val FIREBASE_RESTRICTIONS_CONFIG_KEY = "restrictions_config"
    const val FIREBASE_MAX_CART_SIZE_CONFIG_KEY = "max_cart_size"
    const val MAX_CART_SIZE = 100

    const val UOM_KG = "KGM"

}
