package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import ntuc.fairprice.omni.scango.OpenForTesting
import ntuc.fairprice.omni.scango.repository.db.room.entity.*
import ntuc.fairprice.omni.scango.repository.webservice.model.ApiResponse
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoFeedbackService
import org.json.JSONObject
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.room.repo.Repository
import javax.inject.Inject

@OpenForTesting
@FeatureScope
class FeedbackRepository @Inject constructor(
    mApplication: Application,
    private val scangoFeedbackService: ScangoFeedbackService
) : Repository(mApplication) {

    suspend fun getFeedbackOptions(): Result<List<FeedbackOption>> = scangoFeedbackService.getFeedbackOptions()

    suspend fun postFeedbackOptions(requestBody: JSONObject): Result<ApiResponse> =
        scangoFeedbackService.postFeedbackOptions(requestBody)
}