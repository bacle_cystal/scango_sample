package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.fabric.sdk.android.services.common.CommonUtils.hideKeyboard
import ntuc.fairprice.omni.scango.utils.hideKeyBoard
import org.json.JSONObject
import thor.zopsmart.com.thor.R

class ScangoAddPaymentSheet(val context: Context) {

    var addCard: (JSONObject, Boolean) -> Unit = { _, _ ->
    }

    val bottomSheetDialog: BottomSheetDialog by lazy {
        BottomSheetDialog(context, ntuc.fairprice.omni.scango.R.style.BottomSheetDialog)
    }

    private var scangoPaymentForm: View

    init {
         scangoPaymentForm = ScangoPaymentForm(context).apply {
            setTitle(resources.getString(R.string.label_add_payment_method))
            layoutParams =
                LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
            padded(true)
            buttonAction { jsonObject, b ->
                addCard(jsonObject, b)
                closeBottomDialog()
            }
            cancelAction {
                closeBottomDialog()
            }
        }

        bottomSheetDialog.setContentView(scangoPaymentForm)
    }

    private fun closeBottomDialog() {
        clearViews()
        bottomSheetDialog.hide()
    }

    fun show() {
        bottomSheetDialog.show()
    }

    fun clearViews() {
        (scangoPaymentForm as ScangoPaymentForm).run {
            clearViews()
            hideKeyBoard()
        }
    }


}