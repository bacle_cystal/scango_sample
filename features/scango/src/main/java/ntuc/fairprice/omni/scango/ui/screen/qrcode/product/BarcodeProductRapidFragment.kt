package ntuc.fairprice.omni.scango.ui.screen.qrcode.product

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_barcode_product_rapid.*
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScanFragmentRapidType
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.base.ScangoMaxItemDelegate
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentBarcodeProductRapidBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import javax.inject.Inject

class BarcodeProductRapidFragment :
    ScangoBaseFragment<ScangoFragmentBarcodeProductRapidBinding, BarcodeProductRapidViewModel>(),
    HasSupportFragmentInjector {
    companion object {
        const val TAG = "BarcodeProductRapidFragment"
        const val BUNDLE_PRODUCT = "bundle-product"
        const val SCAN_RAPID_SCREEN_TYPE = "scan_rapid_screen_type"
        //const val NUMBER_ITEM_IN_CART = "number_item_in_cart"
        const val BUNDLE_TEXT_WARNING_IN_CART = "bundle-text-warning"
        const val MAX_QUANTITY = 15
        fun newInstance(product: Product?, screenType: Int, textWarning: String): BarcodeProductRapidFragment {
            Log.d(TAG, "newInstance product.quantity ${product?.quantity}")
            val gson = Gson()
            val barcodeProductRapidFragment = BarcodeProductRapidFragment()
            val jsonString = gson.toJson(product)

            barcodeProductRapidFragment.apply {
                arguments = Bundle().apply {
                    putString(BUNDLE_PRODUCT, jsonString)
                    putInt(SCAN_RAPID_SCREEN_TYPE, screenType)
                    putString(BUNDLE_TEXT_WARNING_IN_CART, textWarning)
                }
            }
            return barcodeProductRapidFragment
        }
    }

    lateinit var product: Product

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: BarcodeProductRapidViewModel by lazy {
        viewModelFactory.create(BarcodeProductRapidViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_fragment_barcode_product_rapid

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    var screenType: Int = 0

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        var gson = Gson()
        val jsonString = arguments?.getString(BUNDLE_PRODUCT)
        screenType = arguments?.getInt(SCAN_RAPID_SCREEN_TYPE) ?: ScanFragmentRapidType.MAX_ITEM.value

        Log.d(TAG, "jsonString $jsonString")
        product = gson.fromJson(jsonString, Product::class.java) as Product
        product.stock = 100
        Log.d(TAG, product.toString())

        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            setVariable(BR.viewModel, viewModel)
            executePendingBindings()
        }

        viewModel.apply {
            productItem.value = product
            screenType.observe(this@BarcodeProductRapidFragment, Observer {
                when (it ?: ScanFragmentRapidType.MAX_ITEM) {
                    ScanFragmentRapidType.LIQUOR_ITEM.value -> showLiquorLate()
                    ScanFragmentRapidType.MAX_ITEM.value -> showMaxItemUI()
                    else -> showMaxItemUI()
                }
            })
        }

        Log.d(TAG, "productItem.quantity ${product.quantity}")


        btn_scan_barcode_gotit.setOnClickListener {
            when (screenType) {
                ScanFragmentRapidType.MAX_ITEM.value -> callBackListener?.onOpenCartToEdit()
                ScanFragmentRapidType.LIQUOR_ITEM.value -> {
                    backToScanner()
                }
            }
        }

        btn_back.setOnClickListener {
            backToScanner()
        }

        product_item_image.setOnClickListener {
            //TODO 28/03 call product detail
            /*val item = product.toItem()
            val intent = Intent(mainActivity, ScangoProductDetailActivity::class.java)
            intent.putExtra("item", item.data.toString())*/
//            mainActivity.startActivity(intent)
        }
        liquor_cart_warning_detail.text = arguments?.getString(BUNDLE_TEXT_WARNING_IN_CART)
        viewModel.screenType.value = screenType
    }

    private fun backToScanner() {
        if (activity is CodeScannerActivity) {
            (activity as CodeScannerActivity).reloadData()
        }
        mainActivity.supportFragmentManager.popBackStack()
    }

    private var callBackListener: ScangoMaxItemDelegate? = null

    fun setCallbackListener(callback: ScangoMaxItemDelegate) {
        this.callBackListener = callback
    }
}
