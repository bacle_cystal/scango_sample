package ntuc.fairprice.omni.scango.utils

import android.content.Context
import android.widget.Toast

object ToastUtils {

    fun show(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun show(context: Context, message: Int) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}
