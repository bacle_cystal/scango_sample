package ntuc.fairprice.omni.scango.ui.screen.cart

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.dialog.ScangoCartFullErrorDialog
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoCartActivityBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoCartActivity): Activity
}

@Module
class ScangoCartActivityProvidesModule {

    @Provides
    @ActivityScope
    fun provideScangoCartViewModel(
        factory: ViewModelProvider.Factory,
        activity: ScangoCartActivity
    ): ScangoCartViewModel =
        ViewModelProviders.of(activity, factory).get()

    @Provides
    @ActivityScope
    fun provideScangoBasketFullErrorDialog(activity: ScangoCartActivity): ScangoCartFullErrorDialog = ScangoCartFullErrorDialog(activity)
}
