package ntuc.fairprice.omni.scango.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.ScangoFeatureBindsModule
import ntuc.fairprice.omni.scango.SplashScreenActivity
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyActivity
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgePincodeBindsModule
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgePincodeProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanActivity
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanActivityBindsModule
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivity
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivityBindsModule
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivityProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoRestrictionBindsModule
import ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutActivity
import ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutBindsModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberBindsModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.ScangoLinkPlusOtpActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.ScangoLinkPlusOtpBindsModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.ScangoLinkPlusOtpProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberBindsModule
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.orders.ScangoOrderListActivity
import ntuc.fairprice.omni.scango.ui.screen.productdetail.ui.ScangoProductDetailActivity
import ntuc.fairprice.omni.scango.ui.screen.productdetail.ui.ScangoProductDetailBindsModule
import ntuc.fairprice.omni.scango.ui.screen.promo.ScangoPromoActivity
import ntuc.fairprice.omni.scango.ui.screen.promo.ScangoPromoBindsModule
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivityBindsModule
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivityProvidesModule
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivity
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivityBindsModule
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListDetailActivity
import thor.zopsmart.com.thor.di.*

@Module
abstract class ScangoActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            // FPOn
            ShoppingListPageModule::class,
            CategoryListingFragmentModule::class,
            CategoryPageModule::class,
            FponDynamicPageModule::class,
            BrandPageModule::class,
            ShoppingListBottomFragmentModule::class,
            // Scango
            BarcodeProductRapidFragmentModule::class,
            ExitStoreFragmentModule::class,
            FeedbackDialogFragmentModule::class,
            FeedbackCommentDialogFragmentModule::class,
            LiquorNoticeFragmentModule::class,
            ScangoSessionExpiredFragmentModule::class,
            LiquorRemoveItemFragmentModule::class,
            UiCheckFragmentModule::class,
            BarcodeErrorFragmentModule::class,
            ScangoHomeFragmentModule::class,
            ScangoShoppingListPageModule::class,
            ScangoShoppingListDetailPageModule::class,
            ScangoTutorialFragmentModule::class,
            ScangoFeatureBindsModule::class,
            ScangoCommonDialogModule::class,
            ScangoDynamicPageModule::class
        ]
    )
    abstract fun bindScangoActivityInjector(): ScangoFeatureActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            BarcodeProductRapidFragmentModule::class,
            LiquorNoticeFragmentModule::class,
            ScangoSessionExpiredFragmentModule::class,
            QrCodeManuallyFragmentModule::class,
            LiquorRemoveItemFragmentModule::class,
            BarcodeErrorFragmentModule::class,
            CodeScannerActivityBindsModule::class,
            CodeScannerActivityProvidesModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindCodeScannerActivityInjector(): CodeScannerActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoAgeVerifyScanProvidesModule::class,
            ScangoAgeVerifyScanActivityBindsModule::class,
            BarcodeErrorFragmentModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoAgeVerifyScanActivityInjector(): ScangoAgeVerifyScanActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [ShoppingListPageModule::class,
            ScangoShoppingListPageModule::class,
            LiquorNoticeFragmentModule::class,
            ScangoSessionExpiredFragmentModule::class,
            LiquorRemoveItemFragmentModule::class,
            FeedbackDialogFragmentModule::class,
            FeedbackCommentDialogFragmentModule::class,
            ScangoCartActivityBindsModule::class,
            ScangoCartActivityProvidesModule::class,
            ScangoCommonDialogModule::class,
            ScangoRestrictionBindsModule::class
        ]
    )
    abstract fun bindScangoCartActivityInjector(): ScangoCartActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ShoppingListBottomFragmentModule::class,
            ScangoProductDetailBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoProductDetailActivityInjector(): ScangoProductDetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    abstract fun bindSplashScreenActivityInjector(): SplashScreenActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoAgeNoticeFragmentModule::class,
            LiquorNoticeFragmentModule::class,
            ScangoSessionExpiredFragmentModule::class,
            LiquorRemoveItemFragmentModule::class,
            ScangoCheckoutBindsModule::class,
            ScangoCommonDialogModule::class,
            ScangoReceiptConfirmFragmentModule::class,
            FeedbackDialogFragmentModule::class
        ]
    )
    abstract fun bindScangoCheckoutActivityInjector(): ScangoCheckoutActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ShoppingListPageModule::class,
            ScangoShoppingListPageModule::class,
            ScangoShoppingListDetailPageModule::class
        ]
    )
    abstract fun bindScangoShoppingListDetailActivityInjector(): ScangoShoppingListDetailActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoLinkPlusPhoneNumberProvidesModule::class,
            ScangoLinkPlusPhoneNumberBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoLinkPlusPhoneNumberActivityInjector(): ScangoLinkPlusPhoneNumberActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoLinkPlusOtpProvidesModule::class,
            ScangoLinkPlusOtpBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoLinkPlusOtpActivityInjector(): ScangoLinkPlusOtpActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoAgePincodeProvidesModule::class,
            ScangoAgePincodeBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoAgePincodeActivityInjector(): ScangoAgeVerifyManuallyActivity


    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoLinkPlusCardNumberProvidesModule::class,
            ScangoLinkPlusCardNumberBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoLinkPlusCardNumberActivityInjector(): ScangoLinkPlusCardNumberActivity


    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoPromoBindsModule::class,
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoPromoActivityInjector(): ScangoPromoActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoCommonDialogModule::class
        ]
    )
    abstract fun bindScangoOrderListActivityInjector(): ScangoOrderListActivity

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            ScangoReceiptConfirmFragmentModule::class,
            ScangoCommonDialogModule::class,
            ScangoReceiptActivityBindsModule::class
        ]
    )
    abstract fun bindScangoReceiptActivityInjector(): ScangoReceiptActivity
}