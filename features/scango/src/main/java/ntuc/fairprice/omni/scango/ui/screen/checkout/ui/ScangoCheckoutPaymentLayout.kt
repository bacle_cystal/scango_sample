package ntuc.fairprice.omni.scango.ui.screen.checkout.ui

import android.app.ActionBar
import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.scango_checkout_payment_selection.view.*
import ntuc.fairprice.omni.scango.ui.screen.checkout.view.ScangoPaymentForm
import ntuc.fairprice.omni.scango.utils.setCardText
import ntuc.fairprice.omni.scango.utils.setCreaditCardImage
import org.json.JSONObject
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.setCardImage
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.model.Card

class ScangoCheckoutPaymentLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ScangoBaseCheckoutLayout(context, attrs, defStyleAttr) {
    lateinit var changeCardAction: (Long) -> Unit

    var cardAddAction: (JSONObject, Boolean) -> Unit = { _, _ -> }

    fun renderCard(card: Card) {
        payment_layout.visible()
        cl_credit_card.visible()
        payment_container.gone()
        tv_card_num.setCardText(card.cardNumber)

        iv_card_type.setCreaditCardImage(card.cardType)

        tv_change_card.setOnClickListener {
            changeCardAction(card.id)
        }
    }

    fun showForm() {
        cl_credit_card.gone()
        payment_container.visible()
        payment_layout.visible()

        val paymentForm =
            ScangoPaymentForm(context).apply {
                setTitle(resources.getString(R.string.label_payment_method))
                layoutParams =
                    LinearLayout.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                buttonAction { jsonObject, b ->
                    cardAddAction(jsonObject, b)
                }
                buttonText(resources.getString(R.string.btn_use_this_card))
            }
        payment_container.removeAllViews()
        payment_container.addView(paymentForm)
    }
}