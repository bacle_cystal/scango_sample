package ntuc.fairprice.omni.scango.ui.screen.orders.epoxy

import android.app.Application
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.ui.screen.orders.paging.ScangoOrderListDataSource
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivity
import thor.zopsmart.com.thor.model.orders.ApiOrderStatus
import thor.zopsmart.com.thor.model.orders.DeliveryMode
import thor.zopsmart.com.thor.model.orders.OrderStatus
import ntuc.fairprice.omni.R as RB
import javax.inject.Inject

class ScangoOrderListEpoxyController @Inject constructor(private val app : Application) : PagedListEpoxyController<ScangoOrderData.Data.Order>() {

    companion object {
        const val SUPPORT_PLACEHOLDERS = false
    }

    private var state = 0

    override fun buildItemModel(currentPosition: Int, o: ScangoOrderData.Data.Order?): EpoxyModel<*> {

        return ScangoOrderListModel_().apply {
            if(o == null && SUPPORT_PLACEHOLDERS) {
                id(-currentPosition)
                placeHolderItem(true)
            } else {
                id(o?.id)
                orderId(o?.id)
                orderDate(o?.placedOn)
                storeName(o?.store?.name)
                orderType(DeliveryMode.fromValue(o?.type?.name))
                orderTotal(getTotalAmount(o))
                orderNum(o?.referenceNumber)
                orderStatus(ApiOrderStatus.fromValue(o?.status).orderStatus)
                slotStartEnd(o?.slotStartTime)
                slotEndTime(o?.slotEndTime)
                orderPaid(o?.paymentStatus == "PAID")
                clickListener { model, parentView, clickedView, pos ->
                    parentView.parent.context.run {
                        startActivity(ScangoReceiptActivity.getIntent(this, model.orderNum()!!.toLong()))
                    }
                }
            }
        }
    }

    private fun getTotalAmount(orderData: ScangoOrderData.Data.Order?): String {
        return app.getString(RB.string.two_decimal_formatted_val,
            orderData?.let {data ->
                data.payment?.let { paymentList ->
                    paymentList.find { "ONLINE".equals(it?.mode?:"", true) }?.amount?.toDouble() ?: 0.0
                } ?: 0.0
            }?: 0.0)
    }

    override fun addModels(models: List<EpoxyModel<*>>) {

        val upcomingOrders = models.filter {
            if(it is ScangoOrderListModel_) {
                it.orderStatus() !in setOf(OrderStatus.COMPLETED, OrderStatus.CANCELLED) &&
                        !it.placeHolderItem()
            } else {
                false
            }

        }

        val pastOrders = models.filter {
            if(it is ScangoOrderListModel_) {
                it.orderStatus() in setOf(OrderStatus.COMPLETED, OrderStatus.CANCELLED) &&
                        !it.placeHolderItem()
            } else {
                false
            }

        }

        val placeHolderItems = models.filter {
            if(it is ScangoOrderListModel_) {
                it.placeHolderItem()
            } else {
                false
            }

        }

        val header : (String) -> ScangoOrderListHeader_ = {
            ScangoOrderListHeader_().apply {
                id(it)
                header(it)
            }
        }

        val loader : (String) -> ScangoOrderListNetworkStateModel_ = {

            ScangoOrderListNetworkStateModel_().apply {
                id("loader")
               // header(it)
            }
        }

        val mainList: MutableList<EpoxyModel<*>> = mutableListOf()

        mainList.apply {
            var isCurrentOrder = false
            if (!upcomingOrders.isNullOrEmpty()) {
                add(header("Current Orders"))
                addAll(upcomingOrders)
                isCurrentOrder = true
            }

            if (!pastOrders.isNullOrEmpty()) {
                add(header(if (isCurrentOrder) "\n\nPast Orders" else "Past Orders"))  // \n will be removed once we have the Empty view holder merged
                addAll(pastOrders)
            }

            // No need of SUPPORT_PLACEHOLDERS check, as the placeHolder Items SHOULD be empty if placeholders are disabled,
            // just adding the check for extra safety
            if (!placeHolderItems.isNullOrEmpty() && SUPPORT_PLACEHOLDERS) {
                addAll(placeHolderItems)
            }

            // Show bottom loader only if placeholders are disabled
            if(state == ScangoOrderListDataSource.LOADING && !SUPPORT_PLACEHOLDERS) {
                mainList.add(loader("loading"))
            }
        }

        super.addModels(mainList)
    }

    fun setNetworkState(newNetworkState: Int) {

        if (newNetworkState != state) {
            state = newNetworkState
            requestModelBuild()
        }
    }

}