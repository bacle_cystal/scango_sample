package ntuc.fairprice.omni.scango.base.customview

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_scan_tab_view.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.match

/**
 * A customview cart with number indicator already use like a base icon cart
 */
class ScangoScanTabView @JvmOverloads constructor(
    val mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(mContext, attrs, defStyleAttr), View.OnTouchListener {


    private lateinit var sliderRectF: RectF
    private lateinit var buttonRectF: RectF

    private var sliderColor = ContextCompat.getColor(context, R.color.nice_blue)
    private var buttonBackgroundColor = ContextCompat.getColor(context, R.color.white10)

    private var sliderPaint = Paint()
    private var sliderLength = 0f
    private var sliderHeight = 0f
    private var buttonPaint = Paint()


    private var mWidth = 0
    private var mHeight = 0
    private var distanceToRight = 0f
    private var moveAnim = 0f
    private var isSelect = false
    private var rightMoveDis = 0f

    private val animDuration = 300L

    var scanIndividualTapped: (() -> Unit)? = null
    var scanRapidTapped: (() -> Unit)? = null

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
        buttonRectF = RectF(0f, 0f, mWidth.toFloat(), mHeight.toFloat())
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        distanceToRight = mWidth.times(0.5F)
        canvas?.drawRoundRect(
            buttonRectF,
            0f,
            0f,
            buttonPaint
        )

        drawSlider(canvas)
    }


    private fun drawSlider(canvas: Canvas?) {
        canvas?.save()

        //Draw a RectF move from margin -> sliderWidth+ rightMoveDis (Value of rightMoveDis gain by moveAnim)
        sliderLength = mWidth.times(0.5f)
        sliderHeight = mHeight.toFloat()

        rightMoveDis = distanceToRight * Math.min(1f, moveAnim * 2)

        sliderRectF = RectF(rightMoveDis, 0F, sliderLength + rightMoveDis, sliderHeight)

        canvas?.drawRoundRect(sliderRectF, 0f, 0f, sliderPaint)

        canvas?.restore()
    }


    init {
        val view = LayoutInflater.from(context).inflate(R.layout.scango_scan_tab_view, this, false)
        val set = ConstraintSet()
        addView(view)

        set.clone(this)
        set.match(view, this)

        setPadding(0, 0, 0, 0)
        setWillNotDraw(false)
        initPaint()
        btn_scan_barcode_one.setOnTouchListener(this)
        btn_scan_barcode_rapid.setOnTouchListener(this)
    }

    /*@SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (moveAnim == 0f || moveAnim == 1f) {
            isSelect = !isSelect
            startAnimator(isSelect)
        }
        if (isSelect) {
            scanRapidTapped?.invoke()
        } else {
            scanIndividualTapped?.invoke()
        }
        return super.onTouchEvent(event)
    }*/

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when (v?.id) {
            R.id.btn_scan_barcode_one -> {
                if (isSelect) {
                    scanIndividualTapped?.invoke()
                    isSelect = !isSelect
                    startAnimator(isSelect)
                }
            }
            R.id.btn_scan_barcode_rapid -> {
                if (!isSelect) {
                    scanRapidTapped?.invoke()
                    isSelect = !isSelect
                    startAnimator(isSelect)
                }
            }
        }
        return super.onTouchEvent(event)
    }

    private fun startAnimator(selectState: Boolean) {
        val anim = ValueAnimator.ofFloat(moveAnim, (if (selectState) 1f else 0f))
        anim.apply {
            duration = animDuration
            interpolator = AccelerateDecelerateInterpolator()
            addUpdateListener {
                moveAnim = it.animatedValue as Float
                invalidate()
            }
            doOnEnd {
                if (isSelect) {
                    image_rapid_off.visibility = View.GONE
                    thunder_animation_view.visibility = View.VISIBLE
                    thunder_animation_view.playAnimation()
                } else {
                    image_rapid_off.visibility = View.VISIBLE
                    thunder_animation_view.visibility = View.GONE
                }
            }
            start()
        }
    }


    private fun initPaint() {
        buttonPaint.isAntiAlias = true
        buttonPaint.color = buttonBackgroundColor
        sliderPaint.isAntiAlias = true
        sliderPaint.color = sliderColor
    }

    fun updateInitState() {
        moveAnim = 0f
        isSelect = false
        this.invalidate()
    }
}