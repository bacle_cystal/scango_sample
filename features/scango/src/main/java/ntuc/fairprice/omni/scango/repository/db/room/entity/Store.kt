package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.InvalidIdentifierException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.repository.db.room.typeconverter.Converters

@Entity
data class Store(
    @PrimaryKey var id: Long,
    var name: String?,
    var address: String?,
    var latitude: String?,
    var longitude: String?
)

fun JSONObject.toStore(): Store {
    val name = string("name") ?: throw InvalidIdentifierException("Customer must have a name.")

    val id = long("id") ?: throw InvalidIdentifierException("Customer must have an id.")

    val address = string("address")

    val latitude = string("latitude")

    val longitude = string("longitude")

    return Store(id, name, address,latitude, longitude)
}