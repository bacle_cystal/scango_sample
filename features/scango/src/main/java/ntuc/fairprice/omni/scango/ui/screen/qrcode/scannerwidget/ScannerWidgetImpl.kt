package ntuc.fairprice.omni.scango.ui.screen.qrcode.scannerwidget

import android.app.Activity
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.scango_activity_scan_code.*
import ntuc.fairprice.omni.common.qrcode.common.CameraSource
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivityViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScangoBarcodeProcessor
import ntuc.fairprice.omni.scango.utils.BarcodeQRCodeUtils
import ntuc.fairprice.omni.scango.utils.ScanData
import ntuc.fairprice.omni.scango.utils.createSoundService
import javax.inject.Inject

class ScannerWidgetImpl @Inject constructor(
    private val context: Activity,
    private val viewModel: CodeScannerActivityViewModel
) : ScannerWidget {

    private var scanType = ScanType.QR_CODE.value

    private var cameraSource: CameraSource? = null

    private var scanGoBarCodeProcessor: ScangoBarcodeProcessor? = null

    override lateinit var containerView: View

    override fun initWidget(view: View, scanType: Int) {
        containerView = view
        this.scanType = scanType
    }

    override fun start(activity: Activity) {
        try {
            if (cameraSource == null) {
                cameraSource = CameraSource(context, fireFaceOverlay)
            }
            firePreview?.start(cameraSource, fireFaceOverlay)

            when (scanType) {
                ScanType.QR_CODE.value -> {
                    initQrcodeProcessor()
                }
                ScanType.BAR_CODE.value -> {
                    initBarcodeProcessor()
                }
            }
        } catch (e: Exception) {
            cameraSource?.release()
            cameraSource = null
        }
    }

    override fun stop() {
        firePreview?.stop()
    }

    override fun destroy() {
        firePreview?.stop()
        scanGoBarCodeProcessor?.stop()
        cameraSource?.release()

    }

    override fun updateFacing(isChecked: Boolean) {
        cameraSource?.let {
            if (isChecked) {
                it.setFacing(CameraSource.CAMERA_FACING_FRONT)
            } else {
                it.setFacing(CameraSource.CAMERA_FACING_BACK)
            }
        }
        stop()
    }

    private fun initBarcodeProcessor() {

        if (scanGoBarCodeProcessor != null)
            return

        scanGoBarCodeProcessor = ScangoBarcodeProcessor(
            context, scanType = ScanType.BAR_CODE,
            onDetechListener = object : ScangoBarcodeProcessor.OnDetechListener {
                override fun onDetechSuccess(scanData: ScanData) {
                    onBarcodeDetachSuccess(scanData)
                }

                override fun onDetechFail(e: Exception) {
                    Log.d(CodeScannerActivity.TAG, "Barcode Detector Processor onDetachFail")
                }
            })

        cameraSource?.setMachineLearningFrameProcessor(scanGoBarCodeProcessor)
    }

    private fun initQrcodeProcessor() {

        if (scanGoBarCodeProcessor != null)
            return

        scanGoBarCodeProcessor = ScangoBarcodeProcessor(
            context, scanType = ScanType.QR_CODE
            , onDetechListener = object : ScangoBarcodeProcessor.OnDetechListener {
                override fun onDetechSuccess(scanData: ScanData) {
                    if (scanData is ScanData.QrCode) {
                        stop()
                        viewModel.getCheckIn(BarcodeQRCodeUtils.getStoreIDFromQRCode(scanData.code))
                    }
                }

                override fun onDetechFail(e: Exception) {
                    Log.d(CodeScannerActivity.TAG, "Qrcode Detector Processor onDetachFail")
                }
            })
        cameraSource?.setMachineLearningFrameProcessor(scanGoBarCodeProcessor)
    }

    private fun onBarcodeDetachSuccess(scanData: ScanData) {
        stop()
        createSoundService(
            context,
            Constants.INTENT_SOUNDSERVICE_ACTION,
            "add_to_cart.wav"
        )
        when(scanData) {
            is ScanData.NormalItem -> {
                viewModel.getProductByBarcode(scanData)
            }
            is ScanData.WeightedItemWithWeight -> {
                viewModel.getProductByBarcode(scanData)
            }

//            is ScanData.WeightedItemWithQuantity -> {
//                viewModel.getProductByBarcode(scanData)
//            }
        }
    }
}