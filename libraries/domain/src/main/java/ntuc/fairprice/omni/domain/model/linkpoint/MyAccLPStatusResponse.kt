package ntuc.fairprice.omni.domain.model.linkpoint

data class MyAccLPStatusResponseModel (
    val data : Data,
    val code : Int
)

data class Data (
    val linkStatus : Int,
    val pointBalance : Double,
    val dollarBalance : Double
)