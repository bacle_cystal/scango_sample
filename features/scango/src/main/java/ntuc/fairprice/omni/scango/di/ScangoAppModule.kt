package ntuc.fairprice.omni.scango.di

import android.app.Application
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.SharedPreferences
import android.view.inputmethod.InputMethodManager
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManagerImpl
import ntuc.fairprice.omni.scango.repository.db.room.ScangoAppDatabase
import ntuc.fairprice.omni.scango.repository.db.room.dao.ScangoCartItemDao
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage

/**
 * Created by Binh.TH on 3/28/2019.
 */
@Module
class ScangoAppModule {
    @Provides
    @FeatureScope
    fun providesDatabase(application: Application): ScangoAppDatabase {
        return ScangoAppDatabase.getInstance(application)
    }

    @Provides
    @FeatureScope
    fun providesCartItemDao(appDatabase: ScangoAppDatabase): ScangoCartItemDao {
        return appDatabase.getScangoCartItemDao()
    }

    @Provides
    @FeatureScope
    fun provideDefaultSharedPreferences(application: Application): SharedPreferences = LocalStorage.init(application)

    @Provides
    @FeatureScope
    fun provideKeyboardManager(application: Application): KeyboardManager = KeyboardManagerImpl(application.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
}