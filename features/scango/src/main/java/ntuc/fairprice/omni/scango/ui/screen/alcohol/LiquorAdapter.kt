package ntuc.fairprice.omni.scango.ui.screen.alcohol

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoItemLiquorListBinding
import ntuc.fairprice.omni.scango.utils.loadUrl
import ntuc.fairprice.omni.scango.utils.strikeThroughText

class LiquorAdapter : RecyclerView.Adapter<LiquorAdapter.LiquorHolder>() {
    var items: List<LiquorItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LiquorHolder {
        val binding: ScangoItemLiquorListBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.scango_item_liquor_list,
                parent,
                false
            )
        return LiquorHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: LiquorHolder, position: Int) = holder.bind(items[position])

    inner class LiquorHolder(val binding: ScangoItemLiquorListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: LiquorItem) {
            binding.apply {
                imgProductAlcohol.loadUrl(item.imageUrl)
                txtLiquorItemHeader.text = item.name
                txtLiquorItemDetail.text = item.detail
                txtLiquorItemQuantity.strikeThroughText(item.quantity.toString())
                txtLiquorItemPrice.strikeThroughText(item.price)
            }.executePendingBindings()
        }
    }
}