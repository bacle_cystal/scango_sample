package ntuc.fairprice.omni.scango.repository.webservice.serviceclient

import ntuc.fairprice.omni.scango.BuildConfig
import okhttp3.*
import org.json.JSONObject
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject
import javax.inject.Named

class ScangoRequestProviderImpl @Inject constructor(
    private val accountRepository: AccountRepository,
    @Named("okHttpClient") private val httpClient: OkHttpClient
) : ScangoRequestProvider {

    override fun get(
        method: ScangoRequestProvider.Method,
        url: String,
        headers: HashMap<String, String>,
        params: JSONObject,
        isGuest: Boolean,
        forWho: ScangoRequestProvider.RequestFor
    ): Call {
        val parsedHeaders = getRequestHeaders(headers, forWho, isGuest)
        val requestBuilder = okhttp3.Request.Builder()
            .headers(parsedHeaders)
            .url(getBaseUrl(forWho) + url)

        when (method) {
            ScangoRequestProvider.Method.GET -> requestBuilder.get()

            ScangoRequestProvider.Method.PUT -> requestBuilder.put(getRequestBody(params))

            ScangoRequestProvider.Method.POST -> requestBuilder.post(getRequestBody(params))

            ScangoRequestProvider.Method.DELETE -> if (params.length() > 0) requestBuilder.delete(getRequestBody(params)) else requestBuilder.delete()
        }

        val request = requestBuilder.build()

        return httpClient.newCall(request)
    }

    private fun getRequestBody(params: JSONObject): RequestBody =
        RequestBody.create(MediaType.parse("application/json; charset=utf-8"), params.toString())

    private fun getRequestHeaders(
        headers: HashMap<String, String>,
        forWho: ScangoRequestProvider.RequestFor,
        isGuest: Boolean
    ): Headers {
        when (forWho) {
            ScangoRequestProvider.RequestFor.SCANGO -> {
                headers["Dev-Token"] = "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ"
            }
            else -> {
                if (!isGuest) {
                    headers["Authorization"] = "Bearer ${accountRepository.getAccessToken()}"
                }
                headers["User-Agent"] = "com.zopsmart.thor/debug"
            }
        }
        headers["Content-Type"] = "application/json"
        headers["Cache-Control"] = "no-cache"
        return Headers.of(headers)
    }

    private fun getBaseUrl(forWho: ScangoRequestProvider.RequestFor): String = when (forWho) {
        ScangoRequestProvider.RequestFor.SCANGO -> BuildConfig.host_scango
        ScangoRequestProvider.RequestFor.ZOPSMART -> BuildConfig.host
        ScangoRequestProvider.RequestFor.SCANGO_V2 -> BuildConfig.host_scango_v2
        ScangoRequestProvider.RequestFor.OTHER -> ""
    }
}