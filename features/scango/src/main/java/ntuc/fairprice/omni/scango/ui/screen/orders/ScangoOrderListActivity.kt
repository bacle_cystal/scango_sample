package ntuc.fairprice.omni.scango.ui.screen.orders

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.scango_activity_order_list.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.ui.screen.orders.epoxy.ScangoOrderListEpoxyController
import ntuc.fairprice.omni.scango.ui.screen.orders.paging.ScangoOrderListDataSource
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.base.withLogin
import javax.inject.Inject
import ntuc.fairprice.omni.ui.R as ZR

class ScangoOrderListActivity : ScangoBaseActivity() {

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, ScangoOrderListActivity::class.java).withLogin(context)
        }
    }

    @Inject
    lateinit var pagingController: ScangoOrderListEpoxyController

    private val viewModel: ScangoOrderListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoOrderListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_order_list)

        epoxyRv.layoutManager = LinearLayoutManager(this)
        epoxyRv.adapter = pagingController.adapter

        scangoOrderListToolBar.setOnActionClickListener {
            finish()
        }

        viewModel.orderListLiveData.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                showError(getString(ZR.string.txt_no_orders_found))
            } else {
                loader.gone()
                pagingController.submitList(it)
            }
        })

        if (!ScangoOrderListEpoxyController.SUPPORT_PLACEHOLDERS) {
            viewModel.nextPageState.observe(this, Observer {
                pagingController.setNetworkState(it)
            })
        }

        viewModel.initialPageState.observe(this, Observer {

            when (it) {
                ScangoOrderListDataSource.ERROR_LOADING -> {
                    showError(getString(ZR.string.generic_error))
                }
            }
        })
    }

    private fun showError(errorMsg: String) {
        tvError.run {
            text = errorMsg
            visible()
        }.also {
            loader.gone()
            epoxyRv.gone()
        }
    }
}
