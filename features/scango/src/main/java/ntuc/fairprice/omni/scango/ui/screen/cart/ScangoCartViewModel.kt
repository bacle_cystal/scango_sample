package ntuc.fairprice.omni.scango.ui.screen.cart

import android.app.Application
import android.util.JsonReader
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidator
import ntuc.fairprice.omni.scango.epoxy.*
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import ntuc.fairprice.omni.scango.ui.screen.cart.data.toCartItems
import ntuc.fairprice.omni.scango.ui.screen.cart.data.toViewData
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview.ScangoPaymentSummary
import org.json.JSONObject
import org.json.JSONTokener
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import thor.zopsmart.com.thor.viewmodel.NextSlotStatus
import thor.zopsmart.com.thor.viewmodel.dependantLiveData
import javax.inject.Inject


class ScangoCartViewModel @Inject constructor(
    application: Application,
    val cartRepository: ScangoCartRepository,
    val accountRepository: AccountRepository,
    private val restrictionsValidator: RestrictionsValidator
) : BaseViewModel(application) {
    var isNoHasLiquodItem10h30pm = MutableLiveData<Boolean>()

    fun checkout() {
        //TODO check liquod type in card item now cart item not contains categoryID liquod
        isNoHasLiquodItem10h30pm.value = true
    }

    private val pageLDInternal = MutableLiveData<MutableList<SGComponent<*>>>()

    val slotLd = MutableLiveData<NextSlotStatus>()
    val syncCartEvent = MutableLiveData<ScangoCartSyncData>()

    var freeGiftPosition = -1

    var selectedCouponList = listOf<ScangoCoupon>()


    val pageLD: LiveData<List<SGComponent<*>>> = dependantLiveData(pageLDInternal) {
        pageLDInternal.value?.let { data ->
            // Change data as per customer address change?
            data
        }
    }

    val cartSyncStatus = MutableLiveData<ScangoCartResponse>()

    fun getCartItems(): List<ScangoCartItem> = cartRepository.getCartItems()

    fun getCartPrice() =
        cartSyncStatus.value?.let {
            when (it) {
                is ScangoCartSuccess -> it.data.sellingPrice
                else -> 0.0
            }
        } ?: 0.0

    fun delete(cartItem: ScangoCartItem) = launch {
        cartSyncStatus.value = ScangoCartLoading
        cartRepository.delete(cartItem)
        syncCart()
    }

    fun increment(cartItem: ScangoCartItem, isLocalOnly: Boolean = false, isFromScanning: Boolean = false) = launch {
        cartSyncStatus.value = ScangoCartLoading
        cartRepository.increment(cartItem, isFromScanning)
        if (!isLocalOnly) {
            syncCart()
        }
    }

    fun decrement(cartItem: ScangoCartItem) = launch {
        cartSyncStatus.value = ScangoCartLoading
        cartRepository.decrement(cartItem)
        syncCart()
    }



    fun syncCart() = launch {
//        cartSyncStatus.value = ScangoCartLoading
//        cartRepository.sync()
//            .onSuccess {
//                syncCartEvent.value = it
//                cartSyncStatus.value = ScangoCartSuccess(it)
//            }
//            .onError {
//                if (it is ScangoHttpServiceException && it.isConnectionError) {
//                    cartSyncStatus.value = ScangoCartFailNetwork
//                } else {
//                    cartSyncStatus.value = ScangoCartFailGeneral
//                }
//            }
        val responseJson  = JSONObject(dummyJson)

        val cartItem =responseJson.toCartItems()
        var syncData = ScangoCartSyncData(0.0,99.0,cartItem, listOf(),0.0)
        cartSyncStatus.value = ScangoCartSuccess(syncData)
    }


    fun loadPage(cartSyncData: ScangoCartSyncData) {
        val components = arrayListOf<SGComponent<*>>()

        val (offers, offerMap, freeItemMap, freeItems, buyitemsServer, jwcOffers) = cartSyncData.cartSyncItems.toViewData()

        val buyItems = cartRepository.getCartItems()

        if (buyItems.isEmpty()) {
            pageLDInternal.value = components
            return
        }

        val totalFreeItems = totalItem(freeItems)

        val finalOffers = offers.plus(cartSyncData.cartOffers)

//        pageLDInternal.value = components.apply {
//            val isFreeGiftInCart = (totalFreeItems > 0)
//            val isOfferInCart = finalOffers.isNotEmpty()
//
//            if (isFreeGiftInCart) {
//                add(SGFreeGiftBanner(totalFreeItems))
//                add(SGVerticalGap(16F))
//            }
//
//            // Add cart items list
//            add(SGCartItemList(buyItems, true))
//
//            if (isFreeGiftInCart) {
//                freeGiftPosition = this.size + buyItems.size
//                // Add free item header
//                add(SGOfferHeader("Your Free Gifts", totalFreeItems))
//                // Add free item list
//                add(SGFreeGiftList(freeItems, freeItemMap))
//            }
//
//            if (isOfferInCart) {
//                // Add offer header
//                add(SGOfferHeader("Your Savings from Deals", 0))
//                // Add offers item list
//
//                add(SGOfferList(finalOffers, offerMap))
//            }
//
//            add(
//                SGCartDetails(
//                    ScangoPaymentSummary(
//                        cartSyncData.sellingPrice,
//                        cartSyncData.sellingPrice,
//                        cartSyncData.discount,
//                        selectedCouponList,
//                        null,
//                        null,
//                        false,
//                        jwcOffers = jwcOffers
//                    )
//                )
//            )
//            add(SGVerticalGap(100f))
//        }
    }

    private fun totalItem(items: List<ScangoCartItem>): Int {
        return items.sumBy { it.quantity }
    }

    val promoHandler: (ScangoCoupon) -> Unit = {
        removeCoupon(it)
    }

    fun setSelectedCoupons(list: List<ScangoCoupon>) {
        selectedCouponList = list
        val components = pageLDInternal.value
        components?.apply {
            val index = indexOfFirst { it is SGCartDetails }
            val oldDetail = this[index] as SGCartDetails
            // add coupon discount
            this[index] = SGCartDetails(oldDetail.paymentSummary.copy(promoList = list))
        }
        pageLDInternal.value = components
    }

    fun removeCoupon(coupon: ScangoCoupon) {
        val components = pageLDInternal.value

        components?.apply {
            val index = indexOfFirst { it is SGCartDetails }
            val oldDetail = this[index] as SGCartDetails
            // add coupon discount

            val list = oldDetail.paymentSummary.promoList
            val newList = mutableListOf<ScangoCoupon>()
            list.forEach {
                if (it.coupon != coupon.coupon) {
                    newList.add(it)
                }
            }

            this[index] = SGCartDetails(oldDetail.paymentSummary.copy(promoList = newList))
        }

        pageLDInternal.value = components
    }

    fun getListTimeRestriction(): List<ScangoCartItem>? =
        getCartItems().filter {
            restrictionsValidator.isTimeViolated(it)
        }

    fun getTextTimeWarning(scangoCartItem: ScangoCartItem?): String =
        restrictionsValidator.getTimeWarningText(scangoCartItem)

    fun getListAgeRestriction(): List<ScangoCartItem>? =
        getCartItems().filter { restrictionsValidator.isAgeViolated(it) }

    fun isProductAgeViolated(productID: Long) =
        !getCartItems().filter { it.id == productID && restrictionsValidator.isAgeViolated(it) }.isNullOrEmpty()

    fun getCartItemsQuantity(): Int = getCartItems().sumBy { 10 }

}


sealed class ScangoCartResponse
class ScangoCartSuccess(val data: ScangoCartSyncData) : ScangoCartResponse()
object ScangoCartFailGeneral : ScangoCartResponse()
object ScangoCartFailNetwork : ScangoCartResponse()
object ScangoCartLoading : ScangoCartResponse()

var dummyStringJson = "{\"code\":200,\"status\":\"SUCCESS\",\"data\":{\"cart\":{\"youPay\":1.95,\"savings\":0,\"changes\":null,\"items\":[{\"id\":\"1140960\",\"q\":\"1\",\"p\":1.95,\"mrp\":1.95,\"priceOverridden\":false,\"discount\":0,\"isFree\":false,\"product\":{\"barcodes\":[\"8990800348056\"],\"brand\":{\"clientId\":null,\"description\":null,\"id\":23605,\"image\":null,\"logo\":null,\"name\":\"VAN MELLE\",\"productsCount\":2,\"slug\":\"van-melle\",\"status\":\"ENABLED\"},\"bulkOrderThreshold\":51,\"clientItemId\":\"248759\",\"createdAt\":\"2019-04-27T03:04:08+08:00\",\"description\":\"Approx 50 pieces\",\"handlingDays\":0,\"hasVariants\":0,\"id\":1140960,\"images\":[\"https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/XL/248759_XL1.jpg\",\"https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/XL/248759_LXL1.jpg\"],\"imagesExtra\":null,\"metaData\":{\"LinkPoint Eligible\":true,\"SAP SubClass\":140380103},\"name\":\"Mentos Chewy Mints - Fruit (Share-A-Bowl)\",\"organizationId\":\"2\",\"primaryCategory\":{\"clientId\":\"13493\",\"description\":null,\"id\":344,\"image\":null,\"name\":\"Chewy \\u0026 Marshmallow\",\"parentCategory\":{\"clientId\":\"12960\",\"description\":null,\"id\":690,\"image\":\"http://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/uat8/media/images/banners/web/category/Sweets_L2.jpg\",\"name\":\"Sweets\",\"parentCategory\":{\"clientId\":\"12704\",\"description\":null,\"id\":615,\"image\":\"banners/web/category/DrinksSnacks_L1.jpg\",\"name\":\"Drinks \\u0026 Snacks\",\"organizationId\":\"2\",\"parentCategory\":null,\"productsCount\":0,\"slug\":\"drinks--snacks-3\",\"status\":\"ENABLED\"},\"productsCount\":193,\"slug\":\"sweets\",\"status\":\"ENABLED\"},\"productsCount\":35,\"slug\":\"chewy--marshmallow\",\"status\":\"ENABLED\"},\"secondaryCategoryIds\":[154],\"slug\":\"mentos-chewy-mints-fruit-share-a-bowl-248759\",\"\t\":0,\"status\":\"ENABLED\",\"stockOverride\":{\"maxPurchasableStock\":50,\"stockBuffer\":null},\"storeSpecificData\":{\"discount\":\"0.000000\",\"mrp\":\"1.950000\",\"stock\":0,\"unlimitedStock\":false},\"tagIds\":[1,4],\"tags\":[{\"description\":null,\"filterable\":false,\"id\":4,\"image\":null,\"name\":\"B2B\",\"productsCount\":12118,\"slug\":\"b2b\",\"type\":\"PUBLIC\"}],\"variants\":null},\"time\":0}],\"handlingDays\":0,\"isBulk\":false,\"storeId\":\"309\",\"amounts\":{\"orderAmount\":1.95,\"shippingAmount\":0,\"clickAndCollectCharge\":0,\"totalCouponDiscount\":0},\"orderType\":\"OFFLINE\"}}}"
var dummyJson  = dummyStringJson.replace("\"","")