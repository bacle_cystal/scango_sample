package ntuc.fairprice.omni.scango.ui.screen.qrcode

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import ntuc.fairprice.omni.common.qrcode.VisionProcessorBase
import ntuc.fairprice.omni.common.qrcode.common.CameraImageGraphic
import ntuc.fairprice.omni.common.qrcode.common.FrameMetadata
import ntuc.fairprice.omni.common.qrcode.common.GraphicOverlay
import ntuc.fairprice.omni.scango.utils.BarcodeQRCodeUtils
import ntuc.fairprice.omni.scango.utils.ScanData
import java.io.IOException

/**
 * Created by Binh.TH on 1/30/2019.
 */

class ScangoBarcodeProcessor(
    val context: Context,
    val scanType: ScanType,
    private val onDetechListener: OnDetechListener
) :
    VisionProcessorBase<List<FirebaseVisionBarcode>>() {

    private val detector: FirebaseVisionBarcodeDetector by lazy {
        val options: FirebaseVisionBarcodeDetectorOptions = when (scanType) {
            ScanType.QR_CODE -> FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(
                    FirebaseVisionBarcode.FORMAT_QR_CODE
                )
                .build()
            ScanType.BAR_CODE -> FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(
                    FirebaseVisionBarcode.FORMAT_ALL_FORMATS
                )
                .build()
        }
        FirebaseVision.getInstance().getVisionBarcodeDetector(options)
    }

    override fun stop() {
        try {
            detector.close()
        } catch (e: IOException) {
            Log.e(CodeScannerActivity.TAG, "Exception thrown while trying to close Barcode Detector: $e")
        }
    }

    override fun detectInImage(image: FirebaseVisionImage): Task<List<FirebaseVisionBarcode>> {
        return detector.detectInImage(image)
    }

    override fun onSuccess(
        originalCameraImage: Bitmap?,
        barcodes: List<FirebaseVisionBarcode>,
        frameMetadata: FrameMetadata,
        graphicOverlay: GraphicOverlay
    ) {
        graphicOverlay.clear()

        originalCameraImage?.let {
            val imageGraphic = CameraImageGraphic(graphicOverlay, it)
            graphicOverlay.add(imageGraphic)
        }

        if(barcodes.isNotEmpty()){


            val firebaseVisionBarcode = barcodes[0]
            onDetechListener.onDetechSuccess(
                BarcodeQRCodeUtils.getScanData(
                    firebaseVisionBarcode.displayValue ?: "8990800348056",
                    firebaseVisionBarcode.format))
            stop()
        }

        graphicOverlay.postInvalidate()
    }

    override fun onFailure(e: Exception) {
        Log.d(CodeScannerActivity.TAG, "onFailure $e")
        onDetechListener.onDetechFail(e)
    }

    interface OnDetechListener {
        fun onDetechSuccess(scanData: ScanData)
        fun onDetechFail(e: Exception)
    }
}