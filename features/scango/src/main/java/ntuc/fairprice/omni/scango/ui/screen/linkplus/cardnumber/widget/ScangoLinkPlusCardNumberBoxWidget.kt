package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.widget

import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScangoLinkPlusCardNumberBoxWidget: LayoutContainer {

    val isValidCardNumber: Boolean

    val cardNumber: Pair<String, Boolean>

    fun initWidget(view: View)

    fun displayInvalidState()

    fun displayDefaultState()

    fun displayDefaultFocussedState()
}