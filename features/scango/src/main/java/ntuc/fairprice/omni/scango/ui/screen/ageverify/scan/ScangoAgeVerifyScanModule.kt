package ntuc.fairprice.omni.scango.ui.screen.ageverify.scan

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.widget.ScangoAgeScannerWidget
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.widget.ScangoAgeScannerWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope


@Module
class ScangoAgeVerifyScanProvidesModule {

    @Provides
    @ActivityScope
    fun provideAgeVerifyScanViewModel(
        activity: ScangoAgeVerifyScanActivity,
        factory: ViewModelProvider.Factory
    ): ScangoAgeVerifyScanViewModel =
        ViewModelProviders.of(activity, factory).get(ScangoAgeVerifyScanViewModel::class.java)
}

@Module
abstract class ScangoAgeVerifyScanActivityBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoAgeVerifyScanActivity): Activity

    @Binds
    @ActivityScope
    abstract fun provideAgeScannerWidget(widget: ScangoAgeScannerWidgetImpl): ScangoAgeScannerWidget
}
