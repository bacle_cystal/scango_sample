package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoLayoutDealDetailsBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.OfferDiscount
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import thor.zopsmart.com.thor.base.configurations.Config
import thor.zopsmart.com.thor.base.extensions.toFormattedString

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoCartDealDetail @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    val binding: ScangoLayoutDealDetailsBinding

    var offer: ScangoOffer? = null

    init {
        val inflater = LayoutInflater.from(context)
        binding = ScangoLayoutDealDetailsBinding.inflate(inflater, this, true)
    }

    @ModelProp
    fun offer(offer: ScangoOffer) {
        this.offer = offer
        binding.tvOfferDescription.text = offer.desc
    }

    @ModelProp
    fun offerProductMap(items: ArrayList<ScangoCartItem>) {
        binding.container.removeAllViews()
        var counter = 0
        val totalCartItems = if (getTotalItems(items) == 0) 1 else getTotalItems(items)

        // Show offer discount
        val discountText = "-${Config.CURRENCY}${(getTotalDiscount(items, offer)).toFormattedString()}"

        binding.tvOfferDiscount.text = discountText

        // If there is a minimum cart amount, change the Because you bought text accordingly
        if ((offer?.minCartPrice ?: 0.0) != 0.0) {
            binding.tvBecauseYouBought.text = resources.getString(
                if (offer?.offerDiscount == OfferDiscount.PRODUCT)
                    R.string.because_you_bought_these_items
                else
                    R.string.because_you_bought_min_cart,
                "${Config.CURRENCY}${offer?.minCartPrice ?: 0.0}"
            )
        } else {
            binding.tvBecauseYouBought.text = resources.getString(R.string.label_because_you_bought)
        }

        items.forEachIndexed { _, item ->
            for (i in 1..item.quantity) {
                counter++
                val view = ScangoCartOfferView(context)
                view.layoutParams =
                    ConstraintLayout.LayoutParams(
                        ConstraintLayout.LayoutParams.WRAP_CONTENT,
                        ConstraintLayout.LayoutParams.MATCH_PARENT
                    )
                view.setImage(item.image, counter == totalCartItems)
                binding.container.addView(view)
            }
        }
    }

    private fun getTotalDiscount(items: ArrayList<ScangoCartItem>, offer: ScangoOffer?): Double =
        offer?.let {
            if (getTotalItems(items) == 0) {
                return it.discount
            } else {
                var discount = 0.0
                items.forEach { item ->
                    item.offer?.forEach { itemOffer ->
                        if (itemOffer == offer) {
                            //Check calculation offer value by discount type
                            discount += itemOffer.offerRule?.discountType?.getDiscountAmount(
                                itemOffer.mrp,
                                itemOffer.discount
                            ) ?: itemOffer.discount * (item.quantity-(item.refundQuantity?:0.0))
                        }
                    }
                }
                return discount
            }
        } ?: 0.0


    private fun getTotalItems(cartItems: ArrayList<ScangoCartItem>): Int {
        var count = 0
        cartItems.forEach {
            count += it.quantity
        }
        return count
    }
}