package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderDetailData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import thor.zopsmart.com.thor.model.orders.OrderData

interface ScangoRetrofitService {
    @GET("order/v0.2/orders/{ref_no}")
    fun getOrderDetails(@Path("ref_no") referenceNo: String): Call<ScangoOrderDetailData>

    @GET("order/v0.2/orders")
    fun getOrders(@Query("page") page : Int = 0): Call<ScangoOrderData>
}