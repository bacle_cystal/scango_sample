package ntuc.fairprice.omni.scango.utils

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import ntuc.fairprice.omni.scango.Constants

/**
 * Created by Binh.TH on 4/22/2019.
 */

class SoundService : Service(), MediaPlayer.OnPreparedListener {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private var mediaPlayer: MediaPlayer? = null

    override fun onPrepared(mp: MediaPlayer) {
        mp.start()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val action: String = intent.action
        val fileName: String = intent.extras.getString(Constants.INTENT_SOUNDSERVICE_FILE_NAME)
        when (action) {
            Constants.INTENT_SOUNDSERVICE_ACTION -> {
                playSoundWithFileName(this.applicationContext, fileName)
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun playSoundWithFileName(context: Context, fileName: String) {
        mediaPlayer = MediaPlayer()
        mediaPlayer?.apply {
            try {
                if (isPlaying) {
                    stop()
                    release()
                }

                val descriptor = context.assets.openFd(fileName)
                setDataSource(descriptor.fileDescriptor, descriptor.startOffset, descriptor.length)
                descriptor.close()
                setVolume(1f, 1f)
                isLooping = false
                setOnPreparedListener(this@SoundService)
                prepareAsync()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}