package ntuc.fairprice.omni.scango.ui.screen.orders.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoOrderRepository
import javax.inject.Inject

class ScangoOrderListDataFactory @Inject constructor(private val orderRepository: ScangoOrderRepository) : DataSource.Factory<Int, ScangoOrderData.Data.Order>() {

    private val _dataSourceLiveData = MutableLiveData<ScangoOrderListDataSource>()
    val dataSourceLiveData: LiveData<ScangoOrderListDataSource>
        get() = _dataSourceLiveData

    override fun create(): ScangoOrderListDataSource {
        val source = ScangoOrderListDataSource(orderRepository)
        _dataSourceLiveData.postValue(source)
        return source
    }

}
