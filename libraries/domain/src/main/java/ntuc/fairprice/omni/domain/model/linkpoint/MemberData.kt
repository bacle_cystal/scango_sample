package ntuc.fairprice.omni.domain.model.linkpoint

import com.google.gson.annotations.SerializedName

data class MemberData(
    var code: Int?,
    var `data`: Data?,
    var message: String?
) {
    data class Data(
        var status: Int?,
        var count: Int?,
        var limit: Int?,
        var offset: Int?
    )
}