package ntuc.fairprice.omni.scango.ui.screen.productdetail.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import ntuc.fairprice.omni.scango.epoxy.*
import ntuc.fairprice.omni.scango.epoxy.IncrementStatus
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.toScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import thor.zopsmart.com.thor.OpenForTesting
import thor.zopsmart.com.thor.epoxy.*
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.Offer
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.entity.ShoppingList
import thor.zopsmart.com.thor.repository.db.room.repo.MainRepository
import thor.zopsmart.com.thor.repository.db.room.repo.WishlistRepository
import thor.zopsmart.com.thor.repository.webservice.model.ApiResponse
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import javax.inject.Inject
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoMainRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.base.customview.filter.FilterGroupDataHolder
import thor.zopsmart.com.thor.model.OfferDetail


inline fun <T> sgDependantLiveData(
    vararg dependencies: LiveData<*>,
    crossinline mapper: () -> T?
): MediatorLiveData<T> {
    return MediatorLiveData<T>().also { mediatorLiveData ->
        val observer = Observer<Any> { mediatorLiveData.value = mapper() }
        dependencies.forEach { dependencyLiveData ->
            mediatorLiveData.addSource(dependencyLiveData, observer)
        }
    }
}

@OpenForTesting
class ScangoProductDetailViewModel @Inject constructor(
    application: Application,
    private val mainRepository: MainRepository,
    private val scangoMainRepository: ScangoMainRepository,
    private val cartRepository: ScangoCartRepository,
    private val wishlistRepository: WishlistRepository
) : BaseViewModel(application) {

    var status = MutableLiveData<ProductDetailLoadStatus>()
    private val cartItems = cartRepository.getCartItemsLiveData()
    val productId = MutableLiveData<Long>()

    private val wishlistItemData = MutableLiveData<List<Long>>()

    private val pageDataInternal = MutableLiveData<List<SGComponent<*>>>()

    val scangoCartItem = MediatorLiveData<ScangoCartItem?>().apply {
        addSource(productId) {
            value = cartRepository.getCartItems().find { it.id == productId.value }
        }
    }.default(null)

    val pageData: LiveData<List<SGComponent<*>>> = sgDependantLiveData(cartItems, wishlistItemData, pageDataInternal) {
        pageDataInternal.value?.let { data ->

            val cartMap = cartItems.value?.associate { it.id to it.quantity }
            val wishList = wishlistItemData.value

            data.filter { it is SGProductGrid }
                .flatMap { (it as SGProductGrid).items }
                .forEach { item ->
                    item.isShoppingListed = wishList?.contains(item.id) ?: false
                    item.quantity = cartMap?.getOrElse(item.id) { 0 } ?: 0
                }

            data.filter { it is SGProductScroller }
                .flatMap { (it as SGProductScroller).items }
                .forEach { item ->
                    item.isShoppingListed = wishList?.contains(item.id) ?: false
                    item.quantity = cartMap?.getOrElse(item.id) { 0 } ?: 0
                }

            data.filter { it is SGProductDetail }
                .flatMap { listOf((it as SGProductDetail).item) }
                .forEach { item ->
                    item.isShoppingListed = wishList?.contains(item.id) ?: false
                    item.quantity = cartMap?.getOrElse(item.id) { 0 } ?: 0
                }

            data
        }
    }

    fun cartChange() = cartItems

    fun isCartEmpty() = cartItems.value.isNullOrEmpty()

    fun loadProduct(widget: SGProductGrid) {
        val index = pageDataInternal.value?.let { it.indexOf(widget) }
        if (widget.isLoading || widget.hasLoadedAll) {
            return
        }
        launch {
            widget.isLoading = true
            mainRepository.loadProducts(widget.associatedData, widget.page)
                .onSuccess {
                    index?.let { i ->
                        updateFilters(it.filterList, i)
                    }
                    val items = it.itemList
                    if (widget.page <= 1) {
                        widget.items = items
                    } else {
                        widget.items = widget.items.plus(items)
                    }
                    widget.isLoading = false
                    widget.hasLoadedAll = (items.isEmpty()) // When no more products are loaded then we stop pagination
                    widget.page = widget.page + 1
                    // Notify data changes
                    pageDataInternal.value = pageDataInternal.value
                }
                .onError {
                    // TODO error handling
                    widget.isLoading = false
                }
        }
    }

    private fun updateFilters(filterList: ArrayList<FilterGroupDataHolder>, index: Int) {
        pageDataInternal.value = pageDataInternal.value?.toMutableList()?.let { list ->
            var filterIndex: Int? = null
            for (i in index downTo 0) {
                if (list[i] is SGFilterSort) {
                    filterIndex = i
                    break
                }
            }
            filterIndex?.let {
                (list[it] as SGFilterSort).filters = filterList
            }
            list
        }
    }

    fun loadProductDetail(url: String) {
        status.value = ProductDetailLoading
        launch {
            loadProductDetails(url)
                .onSuccess {
                    if (!it.isNullOrEmpty()) {
                        pageDataInternal.value = it
                        status.value = ProductDetailLoadSuccess
                    } else {
                        status.value = ProductDetailLoadGeneralFail
                    }

                }
                .onError {
                    // Make a page for error and add it to the FPComponent
                    if (it is ScangoHttpServiceException && it.isConnectionError) {
                        status.value = ProductDetailLoadNetworkFail
                    } else {
                        status.value = ProductDetailLoadGeneralFail
                    }
                }
        }
    }

    val incrementStatus = MutableLiveData<IncrementStatus>()

    fun addToCart(cartItem: CartItem) = launch {
        cartRepository.increment(cartItem.toScangoCartItem())
    }

    fun removeFromCart(cartItem: CartItem) = launch { cartRepository.decrement(cartItem.toScangoCartItem()) }

    fun setQuantity(quantity: Int, cartItem: CartItem) =
        launch { cartRepository.setQuantity(quantity, cartItem.toScangoCartItem()) }

    private suspend fun loadProductDetails(url: String) = scangoMainRepository.loadProductDetails(url)

    fun loadOfferDetail(index: Int, offer: Offer) = launch {
        mainRepository.loadOfferDetail(offer.id)
            .onSuccess {
                updateOfferView(it, offer, index)
            }
            .onError {
                // Handle
            }
    }

    private fun updateOfferView(offerDetail: OfferDetail, offer: Offer, index: Int) {
        val components = pageDataInternal.value as MutableList
        components[index] = SGOfferDetailPDP(offer, offerDetail)
        pageDataInternal.value = components
    }

    //FelixV: FPOn added this param in FPOnDynamicPage
    // We need to clone this feature later cause current implementation does not call this function yet
    var isWishListsEmpty: Boolean = false

    fun addToShoppingList(item: Item, shoppingLists: List<ShoppingList>) {
        launch {
            wishlistRepository.addToShoppinglist(item, shoppingLists)
            loadWishlistedItems()
        }
    }

    fun loadWishlistedItems() = launch {
        wishlistRepository.getWishlistedIds()
            .onSuccess {
                wishlistItemData.value = it.first
                isWishListsEmpty = it.second
            }
    }

    fun createNewShoppingList(name: String) {
        launch {
            wishlistRepository.createShoppingList(name)
            loadWishlistedItems()
        }
    }

    fun createNewShoppingList(name: String, item: Item, shoppingLists: MutableList<ShoppingList>, from: FPScreen) {
        launch {
            wishlistRepository.createShoppingList(name)
                .onSuccess {
                    shoppingLists.add(it)
                    addToShoppingList(item, shoppingLists)
                }
        }
    }
}

sealed class ProductDetailLoadStatus
object ProductDetailLoading : ProductDetailLoadStatus()
object ProductDetailLoadSuccess : ProductDetailLoadStatus()
object ProductDetailLoadGeneralFail : ProductDetailLoadStatus()
object ProductDetailLoadNetworkFail : ProductDetailLoadStatus()
