package ntuc.fairprice.omni.scango.ui.screen.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoItemTutorialBinding

class ScangoTutorialAdapter(val mContext: Context, val onItemClickListener: OnItemClickListener) : RecyclerView.Adapter<ScangoTutorialAdapter.TutorialHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: TutorialGuideItem)
    }

    var items: List<TutorialGuideItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TutorialHolder {
        val binding: ScangoItemTutorialBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.scango_item_tutorial, parent, false)
        return TutorialHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TutorialHolder, position: Int) = holder.bind(items[position])

    inner class TutorialHolder(val binding: ScangoItemTutorialBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: TutorialGuideItem) {
            binding.apply {
                textTutorialNumber.text = (layoutPosition + 1).toString()
                imageTutorial.setImageDrawable(ContextCompat.getDrawable(mContext, item.imageId))
                textTutorial.text = item.textGuide

                binding.root.setOnClickListener{
                    onItemClickListener.onItemClick(item)
                }
            }.executePendingBindings()
        }
    }
}