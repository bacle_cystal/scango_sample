package ntuc.fairprice.omni.scango.ui.screen.ageverify.manually

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.widget.ScangoAgeVerifyManuallyWidget
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.widget.ScangoAgeVerifyManuallyWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
class ScangoAgePincodeProvidesModule {


    @Provides
    @ActivityScope
    fun provideAgePincodeViewModel(
        activity: ScangoAgeVerifyManuallyActivity,
        factory: ViewModelProvider.Factory
    ): ScangoAgeVerifyManuallyViewModel = ViewModelProviders.of(activity, factory).get()

}

@Module
abstract class ScangoAgePincodeBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideScangoAgePincodeWidget(widget: ScangoAgeVerifyManuallyWidgetImpl): ScangoAgeVerifyManuallyWidget

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoAgeVerifyManuallyActivity): Activity

}