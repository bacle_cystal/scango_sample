package ntuc.fairprice.omni.scango.ui.screen.cart.widget

import android.view.View
import android.view.animation.Animation
import kotlinx.android.synthetic.main.scango_activity_cart.*
import ntuc.fairprice.omni.scango.utils.AnimationUtils
import javax.inject.Inject

class ScangoAgeRestrictionWidgetImpl @Inject constructor() : ScangoAgeRestrictionWidget {
    override lateinit var containerView: View

    override fun initWidget(view: View) {
        containerView = view
    }

    override fun displayAgeRestrictionWarning() {
        AnimationUtils.startCartWarningAnim(text_barcode_18plus_warning, object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
    }
}