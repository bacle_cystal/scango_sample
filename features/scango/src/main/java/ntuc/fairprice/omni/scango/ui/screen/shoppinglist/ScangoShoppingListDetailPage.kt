package ntuc.fairprice.omni.scango.ui.screen.shoppinglist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.EpoxyVisibilityTracker
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.scango_fragment_shopping_list_detail_page.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.epoxy.SGComponent
import ntuc.fairprice.omni.scango.epoxy.SGProductGridModel
import org.json.JSONObject
import ntuc.fairprice.omni.scango.ProductCellBindingModelBuilder
import ntuc.fairprice.omni.scango.ProductCellBindingModel_
import ntuc.fairprice.omni.scango.epoxy.SGProductDelegate
import ntuc.fairprice.omni.scango.epoxy.SGProductGrid
import ntuc.fairprice.omni.scango.ui.screen.productdetail.ui.ScangoProductDetailActivity
import thor.zopsmart.com.thor.base.BaseFragment
import thor.zopsmart.com.thor.R as RFP
import ntuc.fairprice.omni.scango.R as RSG
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.epoxy.*
import thor.zopsmart.com.thor.interfaces.ToastColor
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import javax.inject.Inject


class ScangoShoppingListDetailPage : BaseFragment(), SGProductDelegate, FPViewAllDelegate {
    private val spanCount = 10

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val vm: ScangoShoppingListDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoShoppingListDetailViewModel::class.java)
    }

    override val mLayout: Int
        get() = RSG.layout.scango_fragment_shopping_list_detail_page

    override fun onCreated() {

        val gridLayoutManager = GridLayoutManager(mContext, spanCount)

        cl_loader.setOnClickListener {
            // do nothing
        }

        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(epoxy_rv)

        epoxy_rv.layoutManager = gridLayoutManager

        epoxy_rv.buildModelsWith { controller ->

            controller.spanCount = spanCount

            vm.pageData.value?.forEach {
                (it as SGComponent<FPDelegate>).render(this@ScangoShoppingListDetailPage).forEach {
                    it.addTo(controller)
                }
            }
        }

        vm.pageData.observe(this, Observer {
            epoxy_rv.requestModelBuild()
        })

        vm.status.observe(this, Observer {
            cl_loader.gone()
            cl_empty_list.gone()
            cl_shopping_list.visible()
            when(it) {
                is ShoppingListLoading -> cl_loader.visible()
                is ShoppingListLoadGeneralFail -> {
                    cl_shopping_list.gone()
                    cl_empty_list.visible()
                } // no shopping list item page
                is RemovalFail -> {
                    showConfirmation(
                        message = "Something went wrong. Try again",
                        yesText = "Okay"
                    )
                }
                is RemovalSuccess -> {
                    showFPToast(
                        toastMessage = "Item removed",
                        action = {
                            
                        },
                        color = ToastColor.WHITE
                    )
                }
            }
        })

        btn_add_all_to_cart.setOnClickListener {
            vm.addAllToCart()
            showFPToast("All items added to cart")
        }

    }

    override fun onResume() {
        super.onResume()
        vm.loadProducts(shoppingListId)
    }

    override fun onAddToCart(cartItem: CartItem) {
        vm.increment(cartItem)
    }

    override fun onRemoveFromCart(cartItem: CartItem) {
        vm.decrement(cartItem)
    }

    override fun addToShoppingList(item: Item) {
        vm.removeFromShoppingList(item, shoppingListId)
    }

    override fun onProductClick(item: Item) {
        if(item.url.isNullOrEmpty())
            return
        val intent = Intent(mContext, ScangoProductDetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_URL, item.url)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_ID, item.id)
        changePage(intent)
    }

    override fun onQuantitySet(quantity: Int, cartItem: CartItem) {
        vm.setQuantity(quantity, cartItem)
    }

    override fun loadMoreProduct(sgProductGrid: SGProductGrid) {

    }

    override fun onViewAllClick(associatedData: JSONObject) {
        // do nothing
    }

    private var shoppingListId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            shoppingListId = it.getLong("shoppingListId")
        }
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    inline fun productGridCell(
        count: Int,
        modelInitializer: ProductCellBindingModelBuilder.() -> Unit
    ): SGProductGridModel {
        return SGProductGridModel(count).apply {
            modelInitializer()
        }
    }
}
