package ntuc.fairprice.omni.scango.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ntuc.fairprice.omni.scango.Constants.RUNTIME_PERMISSION_REQUEST

object PermissionUtils {

    fun hasCameraPermission(context: Context) = isPermissionGranted(context, Manifest.permission.CAMERA)

    fun requestCameraPermission(activity: Activity) = ActivityCompat.requestPermissions(
        activity, arrayOf(Manifest.permission.CAMERA), RUNTIME_PERMISSION_REQUEST
    )

    private fun isPermissionGranted(context: Context, permission: String): Boolean =
        ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
}