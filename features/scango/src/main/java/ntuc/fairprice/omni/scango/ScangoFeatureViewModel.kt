package ntuc.fairprice.omni.scango

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidator
import ntuc.fairprice.omni.scango.domain.restrictionconfig.ScangoSyncRestrictionsConfigUseCase
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired.ScangoSessionExpiredFragment
import ntuc.fairprice.omni.scango.utils.DeviceUtils
import ntuc.fairprice.omni.scango.utils.isLoggedIn
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import java.lang.Exception
import javax.inject.Inject


class ScangoFeatureViewModel @Inject constructor(
    application: Application,
    val cartRepository: ScangoCartRepository,
    val accountRepository: AccountRepository,
    val scangoStoreRepository: ScangoStoreRepository,
    private val deviceUtils: DeviceUtils,
    scangoSyncRestrictionsConfigUseCase: ScangoSyncRestrictionsConfigUseCase,
    private val restrictionsValidator: RestrictionsValidator
) : BaseViewModel(application) {

    val loginStatusLiveData = MediatorLiveData<Boolean>()

    init {
        scangoSyncRestrictionsConfigUseCase.execute()
        loginStatusLiveData.addSource(accountRepository.getCustomerLiveData()) {
            loginStatusLiveData.value = accountRepository.isLoggedIn
        }
    }

    var isHasItemInCart = MutableLiveData<Boolean>().default(false)
    var isShowToggleUI = MutableLiveData<Boolean>().default(true)
    var isShowSearchUI = MutableLiveData<Boolean>().default(true)

    val currentTab = MutableLiveData<Int>()
    val _storeName = MutableLiveData<String>()
    val checkCheckInStatusResult = MutableLiveData<CheckCheckInStatusResult>()
    val checkInResult = MutableLiveData<CheckInResult>()
    val _timeStamp = MutableLiveData<String>()
    val exitStoreLiveData = MutableLiveData<ExitStoreStatus>()
    val cachedApiDelayTime get() = scangoStoreRepository.getAPIDelayTimeCached()

    val dateTimeRestriction
        get() = restrictionsValidator.getTimeWarningInMilis()

    val serverGapTime get() = scangoStoreRepository.getServerGapTimeCached()

    fun isCheckedIn() = scangoStoreRepository.isCheckedIn()

    val isVisiableBarcode = MediatorLiveData<Boolean>().apply {
        addSource(_storeName) {
            value = !it.isNullOrEmpty()
        }
    }.default(false)

    val isLoggedIn: Boolean
        get() = accountRepository.isLoggedIn

    var selectedTabBeforeLogin: Int? = null

    fun getStoreID() = scangoStoreRepository.getStoreIdCached() ?: 0

    fun changeUIScanCode(storeName: String) {
        _storeName.value = storeName
    }

    fun updateIndicatorIcon(hasItemInCard: Boolean = false) {
        isHasItemInCart.value = hasItemInCard
    }

    fun changeUIToggle(showToggle: Boolean, showSearch: Boolean) {
        isShowToggleUI.value = showToggle
        isShowSearchUI.value = showSearch
    }

    fun exitStore() = launch {
        exitStoreLiveData.value = ExitStoreStatus.Loading
        scangoStoreRepository.getCheckoutStore().onSuccess {
            exitStoreLiveData.value = ExitStoreStatus.Success
        }.onError {
            //FelixV: this API is currently get problem in case user logged out => FPOn clear all the credentials
            // => this API always return UnAuth error => we should let user exit the store even API success or fail
//            if (it is ScangoHttpServiceException && it.isConnectionError) {
//                exitStoreLiveData.value = ExitStoreStatus.NetworkError
//            } else {
//                exitStoreLiveData.value = ExitStoreStatus.GeneralError
//            }
            exitStoreLiveData.value = ExitStoreStatus.Success
        }
    }

    fun checkCheckInVerify() = launch {
        scangoStoreRepository.getCheckinStoreVerify()
            .onSuccess {
                checkCheckInStatusResult.value = CheckCheckInStatusResult.CheckCheckInStatusSuccess
            }.onError {
                //FelixV: in case user does not have network, we're better to let them use the app
                if (it is ScangoHttpServiceException && it.isConnectionError) {
                    checkCheckInStatusResult.value = CheckCheckInStatusResult.CheckCheckInStatusSuccess
                } else if (it is ScangoHttpServiceException && it.isSessionExpired) {
                    checkCheckInStatusResult.value = CheckCheckInStatusResult.CheckCheckInStatusSessionExpired
                } else {
                    checkCheckInStatusResult.value = CheckCheckInStatusResult.CheckCheckInStatusFail
                }
            }
    }

    fun getCheckIn(storeId: String) = launch {
        checkInResult.value = CheckInResult.Loading
        try {
            scangoStoreRepository.getCheckinStore(storeId.toInt(), deviceUtils.deviceId).onSuccess {
                checkInResult.value = CheckInResult.CheckInSuccess
            }.onError {
                if (it is ScangoHttpServiceException && it.isForbidden) {
                    checkInResult.value = CheckInResult.CheckInUserBlocked
                } else {
                    checkInResult.value = CheckInResult.CheckInFail
                }
            }
        } catch (e: Exception) {
            checkInResult.value = CheckInResult.CheckInFail
        }
    }

    fun checkStoreCached() {
        _storeName.value = scangoStoreRepository.getStoreNameCached()
    }

    fun checkTimeStampCached() {
        _timeStamp.value = scangoStoreRepository.getTimeStampCached()
    }

    fun clearCredentialWhenUnAuthAccessToken() {
        accountRepository.deleteCustomer()
        accountRepository.clearLocalStorage()
        cartRepository.clearCart()
    }

    fun clearDataStoreWhenInvalidCheckin() {
        cartRepository.clearCart()
        scangoStoreRepository.clearStoreInfo()
    }
}

sealed class CheckCheckInStatusResult {
    object CheckCheckInStatusSuccess : CheckCheckInStatusResult()
    object CheckCheckInStatusSessionExpired : CheckCheckInStatusResult()
    object CheckCheckInStatusFail : CheckCheckInStatusResult()
}

sealed class CheckInResult {
    object Loading : CheckInResult()
    object CheckInUserBlocked : CheckInResult()
    object CheckInSuccess : CheckInResult()
    object CheckInFail : CheckInResult()
}

sealed class ExitStoreStatus {
    object Loading : ExitStoreStatus()
    object Success : ExitStoreStatus()
    object NetworkError : ExitStoreStatus()
    object GeneralError : ExitStoreStatus()
}