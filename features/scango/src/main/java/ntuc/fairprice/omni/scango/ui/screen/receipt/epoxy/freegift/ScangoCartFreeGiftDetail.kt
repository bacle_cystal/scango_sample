package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import ntuc.fairprice.omni.scango.databinding.ScangoLayoutFreeGiftBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import thor.zopsmart.com.thor.base.configurations.Config

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoCartFreeGiftDetail @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    val binding: ScangoLayoutFreeGiftBinding

    init {
        val inflater = LayoutInflater.from(context)
        binding = ScangoLayoutFreeGiftBinding.inflate(inflater, this, true)
    }

    @ModelProp
    fun item(item: ScangoCartItem) {
        binding.item = item
        val offer = item.offer?.getOrNull(0)

        if(offer != null && offer.minCartPrice != 0.0)  {
            binding.tvBecauseMessage.text = "Because you bought ${Config.CURRENCY}${offer?.minCartPrice} worth of items"
        } else if(offer?.appliedPromoCode != null) {
            binding.tvBecauseMessage.text = "Because you applied promo code : ${offer.appliedPromoCode}"
        } else {
            binding.tvBecauseMessage.text = "Because you bought ${Config.CURRENCY}0.0 worth of items"
        }
    }

    @ModelProp
    fun offerProductMap(cartItems: ArrayList<ScangoCartItem>) {
        binding.container.removeAllViews()
        var counter = 0
        val totalCartItems = getTotalItems(cartItems)

        cartItems.forEachIndexed { index, cartItem ->
            for (i in 1..cartItem.quantity) {
                counter++
                val view = ScangoCartOfferView(context)
                view.layoutParams =
                    LayoutParams(
                        LayoutParams.WRAP_CONTENT,
                        LayoutParams.MATCH_PARENT
                    )

                view.setImage(cartItem.image, counter == totalCartItems)
                binding.container.addView(view)
            }
        }
    }

    private fun getTotalItems(cartItems: ArrayList<ScangoCartItem>): Int {
        var count = 0
        cartItems.forEach {
            count += it.quantity
        }
        return count
    }
}