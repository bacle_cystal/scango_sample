package ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget

import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScangoLinkPlusPhoneNumberBoxWidget: LayoutContainer {

    val isValidPhoneNumber: Boolean

    val phoneNumber: String

    fun initWidget(view: View)

    fun displayInvalidState()

    fun displayDefaultState()

    fun displayDefaultFocussedState()
}