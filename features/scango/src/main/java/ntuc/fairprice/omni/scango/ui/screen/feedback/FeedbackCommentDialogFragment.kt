package ntuc.fairprice.omni.scango.ui.screen.feedback

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_feedback_comment_dialog_fragment.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFeedbackCommentDialogFragmentBinding
import javax.inject.Inject
import ntuc.fairprice.omni.scango.repository.db.room.entity.FeedbackOption
import org.json.JSONArray

class FeedbackCommentDialogFragment : ScangoBaseBottomSheetFragment<ScangoFeedbackCommentDialogFragmentBinding, FeedbackCommentDialogViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "FeedbackCommentDialogFragment"

        const val ARG_NUM_STARS = "num_stars"

        fun newInstance(rating: Float): FeedbackCommentDialogFragment =
            FeedbackCommentDialogFragment().apply {
                arguments = Bundle().apply {
                    putFloat(ARG_NUM_STARS, rating)
                }
            }
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: FeedbackCommentDialogViewModel by lazy {
        viewModelFactory.create(FeedbackCommentDialogViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_feedback_comment_dialog_fragment

    lateinit var listFeedback: List<FeedbackOption>

    var actionAfterDismiss: () -> Unit = {}

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val rating = arguments?.getFloat(ARG_NUM_STARS)

        ratingBarComment.rating = rating!!

        recycler_reason.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = scangoFeedbackAdapter
        }

        initAddressList()

        btnSubmitComment.setOnClickListener {
            viewModel._postFeedbackOptions("14AA0EFE-065C-43B3-B315-1E6FE5045E18", ratingBarComment.numStars, JSONArray(), edtComment.text.toString())
        }

        viewModel.apply {
            feedbackOptions.observe(this@FeedbackCommentDialogFragment, Observer {
                if (it != null) {
                    scangoFeedbackAdapter.options = it
                    scangoFeedbackAdapter.notifyDataSetChanged()
                }
            })

            apiResponse.observe(this@FeedbackCommentDialogFragment, Observer {
                if (it != null) {
                    dismiss()
                }
            })

            apiResponse.observe(this@FeedbackCommentDialogFragment, Observer {
                if (it != null) {
                    dismiss()
                }
            })
        }

        viewModel._getFeedbackOptions()
    }

    fun initAddressList() {
        context?.let {
            listFeedback = arrayListOf(
                FeedbackOption(
                    4,
                    "Others",
                    1,
                    0,
                    true,
                    false
                )
            )
        }
        scangoFeedbackAdapter.options = listFeedback
        scangoFeedbackAdapter.notifyDataSetChanged()
    }

    private val scangoFeedbackAdapter: ScangoFeedbackAdapter by lazy {
        ScangoFeedbackAdapter()
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        actionAfterDismiss.invoke()
    }
}
