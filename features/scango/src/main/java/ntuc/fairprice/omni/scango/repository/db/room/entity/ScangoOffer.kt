package ntuc.fairprice.omni.scango.repository.db.room.entity

import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderDetailData
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.InvalidIdentifierException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.Offer

data class ScangoOffer(
    val id: Long,
    val alertMessage: String,
    val desc: String,
    val imageUrl: String?,
    val onPack: Boolean,
    val price: Double,
    val shortDesc: String,
    val type: String,
    val validFrom: String,
    val validTill: String,
    var mrp: Double = 0.0,
    var discount: Double = 0.0,
    var appliedCount: Double? = 1.0,
    var isJwc: Boolean = false,
    var minCartPrice: Double = 0.0,
    var offerDiscount: OfferDiscount = OfferDiscount.PRODUCT,
    var shippingDiscount: Double? = null,
    var appliedPromoCode: String? = null,
    var offerRule: OfferRule? = null
) {
    fun isPwP() = offerRule.isPwpOffer()

    override fun equals(other: Any?): Boolean {
        return ((other != null) && (other is ScangoOffer) && (other.id == id))
    }
}

data class OfferRule(val discountType: DiscountType, val discountVal: Double, val offerType: OfferType?)

fun OfferRule?.isPwpOffer() = this?.offerType == OfferType.PWP

fun List<ScangoOrderDetailData.Data.Order.Item.OfferData>.toOffer() =

    map {
        ScangoOffer(
            id = it.id ?: throw InvalidIdentifierException("id not found"),
            alertMessage = "",
            desc = it.description ?: "",
            imageUrl = it.imageUrl,
            onPack = false,
            price = 0.0,
            shortDesc = "",
            type = it.offerType ?: "",
            validFrom = it.offerValidFrom ?: "",
            validTill = it.offerValidTill ?: "",
            mrp = 0.0,
            discount = it.discount ?: 0.0,
            isJwc = false,
            offerRule = it.rule?.toOfferRule()
        )
    }


fun List<ScangoOrderDetailData.Data.Order.Offers>.toScangoOffer() =
    map {
        ScangoOffer(
            id = it.id?.toLong() ?: throw InvalidIdentifierException("id not found"),
            alertMessage = "",
            desc = it.description ?: "",
            imageUrl = it.imageUrl,
            onPack = false,
            price = 0.0,
            shortDesc = "",
            type = it.offerType ?: "",
            validFrom = it.offerValidFrom ?: "",
            validTill = it.offerValidTill ?: "",
            mrp = 0.0,
            discount = it.discount?.toDouble() ?: 0.0,
            minCartPrice = it.rule?.cartPrice?.toDouble() ?: 0.0,
            isJwc = false
        )
    }

fun JSONObject.toScangoOffer(): ScangoOffer {
    return ScangoOffer(
        long("id") ?: throw InvalidIdentifierException("id not found"),
        string("alertMessage") ?: "",
        string("description") ?: "",
        string("imageUrl"),
        boolean("onPack") ?: false,
        double("price") ?: 0.0,
        string("shortDescription") ?: "",
        string("type") ?: "",
        string("validFrom") ?: "",
        string("validTill") ?: "",
        mrp = double("mrp") ?: 0.0,
        discount = double("discount") ?: 0.0,
        isJwc = isTagPresent("jwc"),
        minCartPrice = getMinCartPrice(),
        offerRule = getOfferRule()
    )

}

private fun JSONObject.getMinCartPrice(): Double {
    val buy =
        try {
            jsonR(listOf("ruleDetail", "buy"))
        } catch (e: Exception) {
            return 0.0
        }
    return buy double "minCartPrice" ?: 0.0
}

fun JSONArray.toOfferList(): ArrayList<ScangoOffer> {
    val offerList = arrayListOf<ScangoOffer>()
    forEach {
        try {
            offerList.add(it.toScangoOffer())
        } catch (e: Exception) {

        }
    }
    return offerList
}

fun Offer.toScangoOffer() = ScangoOffer(
    id = id,
    alertMessage = alertMessage,
    desc = desc,
    imageUrl = imageUrl,
    onPack = onPack,
    price = price,
    shortDesc = shortDesc,
    type = type,
    validFrom = validFrom,
    validTill = validTill,
    mrp = mrp,
    discount = discount,
    isJwc = isJwc
)

fun ScangoOffer.toOffer() = Offer(
    id = id,
    alertMessage = alertMessage,
    desc = desc,
    imageUrl = imageUrl,
    onPack = onPack,
    price = price,
    shortDesc = shortDesc,
    type = type,
    validFrom = validFrom,
    validTill = validTill,
    mrp = mrp,
    discount = discount,
    isJwc = isJwc
)

fun List<Offer>.toListScangoOffer() = this.map {
    it.toScangoOffer()
}

fun List<ScangoOffer>.toListOffer() = this.map {
    it.toOffer()
}


fun JSONObject.getOfferRule(): OfferRule? {
    try {
        val rule = safeJson("rule")
        val getObject = rule safeJson "get"
        val discountVal = getObject double "v" ?: return null
        val discountType = getObject string "t" ?: return null
        return OfferRule(DiscountType.valueOf(discountType), discountVal = discountVal, offerType = getOfferType())
    } catch (e: Exception) {
        return null
    }
}


fun ScangoOrderDetailData.Data.Order.Item.OfferData.Rule.toOfferRule(): OfferRule? {
    var discountVal = this.total?.v
    var discountType = this.total?.t
    if (discountVal == null) {
        discountVal = this.get?.v
        discountType = this.get?.t
    }
    if (discountType == null || discountVal == null) return null
    return OfferRule(DiscountType.valueOf(discountType), discountVal = discountVal, offerType = null)
}

fun JSONObject.getOfferType(): OfferType =
    if (isPwp()) {
        OfferType.PWP
    } else if (isTagPresent("jwc")) {
        OfferType.JWC
    } else {
        OfferType.OTHER
    }

fun JSONObject.isPwp() = isTagPresent("pwp", true) || string("type").equals("PWP", true)

fun JSONObject.isTagPresent(tag: String, isExcluding: Boolean = false): Boolean {
    var isTagPresent = false
    try {
        val tags = jsonArrayR(listOf("ruleDetail", if (isExcluding) "buyExcluding" else "buy", "tags")) ?: JSONArray()
        tags.forEach {
            if (it.string("name")?.toLowerCase() == tag)
                isTagPresent = true
        }
    } catch (e: Exception) {
    }
    return isTagPresent
}

data class ScangoOfferDetail(
    var buyItems: List<Item>,
    val getItems: List<ScangoOfferItem>,
    val isFree: Boolean,
    val discountType: String,
    val discountValue: Double,
    val detailType: OfferDetailType = OfferDetailType.PRODUCT,
    val slugList: ArrayList<String> = arrayListOf()
)

enum class OfferDiscount {
    PRODUCT, CART, SHIPPING
}

enum class OfferType(val offerType: String) {
    JWC("JWC"), PWP("PWP"), OTHER("OTHER")
}

enum class DiscountType(val discountType: String) {
    ABSOLUTE_OFF("ABSOLUTE"),
    FIXED("FIXED"),
    PERCENT_OFF("PERCENT_OFF");

    fun getDiscountAmount(mrp: Double, discountVal: Double) =
        when (this) {
            ABSOLUTE_OFF -> {
                discountVal
            }
            FIXED -> {
                mrp - discountVal
            }
            PERCENT_OFF -> {
                discountVal
            }
        }
}

enum class OfferDetailType {
    TAG, CATEGORY, PRODUCT, BRAND
}

data class ScangoOfferItem(val item: Item, val discountVal: Double, val discountType: String, val isFree: Boolean)

