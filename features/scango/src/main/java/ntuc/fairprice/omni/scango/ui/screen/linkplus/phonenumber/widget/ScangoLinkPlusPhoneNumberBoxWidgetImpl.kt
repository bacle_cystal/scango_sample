package ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_activity_link_plus_phone_number.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberViewModel
import ntuc.fairprice.omni.scango.utils.removeSpaces
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

class ScangoLinkPlusPhoneNumberBoxWidgetImpl @Inject constructor() : ScangoLinkPlusPhoneNumberBoxWidget {

    companion object {
        private const val SINGAPORE_CODE = "+65"
    }

    @Inject
    lateinit var viewModel: ScangoLinkPlusPhoneNumberViewModel

    private val phoneNumberRegex = Regex(pattern = "^(\\+65)?([98])\\d{7}$")

    private val isAlreadyInDefaultState
        get() = txt_link_plus.text == containerView.context.getString(R.string.btn_link_plus)

    override lateinit var containerView: View

    override val isValidPhoneNumber
        get() = phoneNumberRegex.containsMatchIn(edt_link_plus_mobile.text.toString().removeSpaces())

    override val phoneNumber: String
        get() = edt_link_plus_mobile.text.toString().removeSpaces().removePrefix(SINGAPORE_CODE)

    override fun initWidget(view: View) {
        containerView = view
        edt_link_plus_mobile.setText(viewModel.userPhoneNumber)

        //Move the cursor to the last index ..
        edt_link_plus_mobile?.apply {
            setSelection(text.toString().length)
        }

        initListener()
    }

    private fun initListener() {
        edt_link_plus_mobile.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                card_plus_mobile.setBackgroundResource(R.drawable.sg_feedback_rectangle_blue)
            } else {
                card_plus_mobile.setBackgroundResource(R.drawable.sg_feedback_rectangle_grey)
            }
        }
        edt_link_plus_mobile.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(!isAlreadyInDefaultState) {
                    displayDefaultFocussedState()
                }
                txt_link_plus.isEnabled = isValidPhoneNumber
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

    }

    override fun displayInvalidState() {
        img_invalid_number_alert.visibility = View.VISIBLE
        card_plus_mobile.setBackgroundResource(R.drawable.sg_feedback_rectangle_red)
        txt_link_plus_mobile_wrong.visibility = View.VISIBLE
        btn_link_plus_contact_email.visibility = View.VISIBLE
        btn_link_plus_contact_phone.visibility = View.VISIBLE
        txt_link_plus.text = containerView.context.getString(R.string.btn_link_plus_try)
        txt_link_plus_mobile.setTextColor(ContextCompat.getColor(containerView.context, R.color.pinkish_red))
        if(viewModel.isPartiallyLinked) {
            txt_link_plus_try_with_card.visibility = View.GONE
        } else {
            txt_link_plus_try_with_card.visibility = View.VISIBLE
        }
    }

    override fun displayDefaultState() {
        img_invalid_number_alert.visibility = View.GONE
        card_plus_mobile.setBackgroundResource(R.drawable.sg_feedback_rectangle_grey)
        txt_link_plus_mobile_wrong.visibility = View.GONE
        btn_link_plus_contact_email.visibility = View.GONE
        btn_link_plus_contact_phone.visibility = View.GONE
        txt_link_plus.text = containerView.context.getString(R.string.btn_link_plus)
        txt_link_plus_mobile.setTextColor(ContextCompat.getColor(containerView.context, R.color.blue_blue))
        txt_link_plus_try_with_card.visibility = View.GONE
        txt_link_plus.isEnabled = isValidPhoneNumber
    }

    override fun displayDefaultFocussedState() {
        img_invalid_number_alert.visibility = View.GONE
        card_plus_mobile.setBackgroundResource(R.drawable.sg_feedback_rectangle_blue)
        txt_link_plus_mobile_wrong.visibility = View.GONE
        btn_link_plus_contact_email.visibility = View.GONE
        btn_link_plus_contact_phone.visibility = View.GONE
        txt_link_plus.text = containerView.context.getString(R.string.btn_link_plus)
        txt_link_plus_mobile.setTextColor(ContextCompat.getColor(containerView.context, R.color.blue_blue))
        txt_link_plus_try_with_card.visibility = View.GONE
        txt_link_plus.isEnabled = isValidPhoneNumber
    }
}