package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import ntuc.fairprice.omni.scango.utils.DateTimeUtils
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.json
import thor.zopsmart.com.thor.base.extensions.string
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


/*
Tempalte response
{
  "data": {
    "session": {
      "sessionID": "7dcf8b53-f0fb-4a1a-a140-c9f89ec32de8",
      "store": {
        "id": 5,
        "address": "#01,blk 808, French Rd, 01, Singapore 200808",
        "businessHours": null,
        "clientId": null,
        "hasClickCollect": true,
        "hasDeliveryHub": true,
        "hasPicking": true,
        "latitude": 1.3105912652884,
        "longitude": 103.8626452762,
        "metaData": {
          "Fax": "",
          "Phone": "",
          "SAP Code": "",
          "Store Format": "FairPrice",
          "WCS Id": ""
        },
        "name": "Fairprice French Road",
        "pickingStoreId": null
      },
      "deviceID": "76bcad07-cae8-4089-9b3f-4a3f96c53aa9"
    },
    "timestamp": "2019-05-09T06:54:06.234067136Z"
  }
}
*/
@Entity
data class ScangoStore(
    var id: String,
    var sessionId: String,
    var address: String,
    var name: String,
    var timeStamp: String
)

fun JSONObject.toScangoStore(): ScangoStore {

    val data = this json "data"
    val session = data json "session"
    val store = session json "store"
    val timeStamp = data string "timestamp"
    val parsedTimeStamp = DateTimeUtils.parseRFC3339DateTime(timeStamp!!) ?: System.currentTimeMillis()

    return ScangoStore(
        id = store string "id" ?: "",
        sessionId = session string "sessionID" ?: "",
        address = store string "address" ?: "",
        name = store string "name" ?: "",
        timeStamp = parsedTimeStamp.toString()
    )
}