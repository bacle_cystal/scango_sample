package ntuc.fairprice.omni.scango.ui.screen.checkout

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_checkout.*
import kotlinx.android.synthetic.main.scango_checkout_payment_selection.*
import kotlinx.android.synthetic.main.scango_layout_stylish_toolbar.*
import kotlinx.android.synthetic.main.scango_place_order_button.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.dialog.ScangoPaymentErrorDialog
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.ScangoCardSelection
import ntuc.fairprice.omni.scango.base.ScangoComponent
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.getTotalPromoDiscount
import ntuc.fairprice.omni.scango.repository.db.room.typeconverter.ScangoConverters
import ntuc.fairprice.omni.scango.ui.screen.ageverify.AgeVerifyType
import ntuc.fairprice.omni.scango.ui.screen.ageverify.ScangoAgeNoticeFragment
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyActivity
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanActivity
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorRemoveItemFragment
import ntuc.fairprice.omni.scango.ui.screen.cart.*
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import ntuc.fairprice.omni.scango.ui.screen.checkout.view.ScangoAddPaymentSheet
import ntuc.fairprice.omni.scango.ui.screen.checkout.view.ScangoSelectionSheet
import ntuc.fairprice.omni.scango.ui.screen.checkout.view.ScangoSwitchLinkpointListener
import ntuc.fairprice.omni.scango.ui.screen.promo.view.ScangoPromoRemoveListener
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivity
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview.ScangoPaymentSummary
import ntuc.fairprice.omni.scango.utils.observe
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.visible
import javax.inject.Inject
import thor.zopsmart.com.thor.R as RFP

class ScangoCheckoutActivity : ScangoBaseActivity(), HasSupportFragmentInjector {
    private lateinit var viewBinding: ViewDataBinding

    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog

    @Inject
    lateinit var viewModel: ScangoCheckoutViewModel
    @Inject
    lateinit var scangoPaymentErrorDialog: ScangoPaymentErrorDialog

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    private val paymentSheet: ScangoAddPaymentSheet by lazy {
        ScangoAddPaymentSheet(mContext).apply {
            addCard = { params, isDefault ->
                viewModel.saveCard(params, isDefault)
            }
        }
    }

    private val selectionSheet: ScangoSelectionSheet by lazy {
        ScangoSelectionSheet(mContext)
    }

    private var isFirstTimeFlow: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this@ScangoCheckoutActivity, R.layout.scango_activity_checkout)
        viewBinding.lifecycleOwner = this
        viewBinding.apply {
            setVariable(BR.vm, viewModel)
            executePendingBindings()
        }

        showEmptyUI()

        viewModel.loadCards()
        viewModel.loadCart()
        viewModel.loadLinkPoint()

        // Uncomment for test coupon
        // viewModel.loadPromotions()

        observe(viewModel.cardLd, ::onCardsStateChange)
        observe(viewModel.cartSyncStatus, ::onCartSyncStatusChange)
        observe(viewModel.orderPlacementStatus, ::onOrderPlacementStatusChange)

//        observe(viewModel.linkPointAndCartLd, ::onLinkPointAndCartStatusChange)
        observe(viewModel.timeStampStatus, ::onTimeStampStatusChange)

        place_order_button_layout.setOnClickListener {
            viewModel.checkTimeStamp()
        }

        iv_close_back.setOnClickListener {
            onBackPressed()
        }

        with(order_details_layout) {
            addPromoClick = View.OnClickListener {
                gotoScangoPromoPage("Checkout", paymentSummary?.youPay ?: 0.0, viewModel.selectedCouponList)
            }
            promoRemoveListener = object : ScangoPromoRemoveListener {
                override fun onPromoRemoved(coupon: ScangoCoupon) {
                    viewModel.removeCoupon(coupon)
                }
            }
            swithButtonListener = object : ScangoSwitchLinkpointListener {
                override fun onSwitchLinkpoint(isActive: Boolean, totalValue: Double) {
                    viewModel.changeLinkPointStatus(isActive)
                    place_order_button_layout.total = totalValue
                }
            }
            isCheckoutScreen = true
        }
    }

    private fun openCardSelectionDialog(cardId: Long) {

        val views = listOf<ScangoComponent<*>>(ScangoCardSelection(viewModel.cards, cardId))

        selectionSheet.addClickListener = {
            openAddCardSheet()
        }

        selectionSheet.selectionListener = {
            viewModel.changeCard(it)
        }
        selectionSheet.show(views)
    }

    private fun openAddCardSheet() {
        paymentSheet.show()
    }

    private fun onCardsStateChange(cardState: CardState) {

        when (cardState) {
            is CardState.Success -> {
                payment_layout.apply {
                    enableLayouts(this)
                    changeCardAction = { id ->
                        openCardSelectionDialog(id)
                    }
                    renderCard(cardState.card)
                }
                paymentSheet.clearViews()
            }
            /**
             * Added this Card status check here.
             */
            is CardState.NoCardFound -> {
                payment_layout.apply {
                    enableLayouts(this)
                    showForm()
                    cardAddAction = { param, isDefault ->
                        viewModel.saveCard(param, isDefault)
                    }
                }
            }
        }
        updateUi()
    }


    /**
     * This method is responsible to ensure that the ui state is consistent at any given time.
     * Takes care of showing/hiding place order button, loader ui.
     * This method can be called any time during the course of checkout
     */
    private fun updateUi() {
        cl_empty_checkout.gone()

        val cards = viewModel.cardLd.value
        val cart = viewModel.cartSyncStatus.value

        // Check if any of the API calls are in progress.
        run {
            when (cart) {
                is ScangoCartSuccess -> {
                    //item_cart_dropdown_view.visible()
                    order_details_layout.visible()
                }
                else -> {
                    place_order_button_layout.gone()
                    return
                }
            }
            when (cards) {
                is CardState.Loading -> {
                    scanGoProgressDialog.show()
                }
                is CardState.Success -> {
                    hideCardDialog()
                    order_details_layout.visible()
                    payment_layout.visible()
                    //item_cart_dropdown_view.visible()
                }
                is CardState.LoadCardErrorNetwork -> {
                    hideCardDialog()
                    scangoNetworkErrorDialog.apply {
                        onActionClick = {
                            viewModel.loadCards()
                        }
                        show()
                    }
                }
                is CardState.SaveCardErrorNetwork -> {
                    hideCardDialog()
                    scangoNetworkErrorDialog.apply {
                        onActionClick = {
                            viewModel.saveCard(cards.param, cards.default)
                        }
                        show()
                    }
                }
                is CardState.NoCardFound -> {
                    place_order_button_layout.gone()
                    return
                }
                else -> {
                    hideCardDialog()
                    place_order_button_layout.gone()
                    return
                }
            }
        }

        // We reached here(without returning), that means button has to be visible.
        place_order_button_layout.visible()
        // Next, set the state of the button(active, inactive)
        place_order_button_layout.isActive = true
    }

    //Just hide loading dialog when cart response and linkpoint complete
    fun hideCardDialog() {
        if (viewModel.linkPointAndCartLd.value is LoadCartAndLinkpointSuccess) {
            scanGoProgressDialog.dismiss()
        }
    }


    private fun onOrderPlacementStatusChange(orderPlacementStatus: OrderPlacementStatus?) {
        scanGoProgressDialog.dismiss()
        when (orderPlacementStatus) {
            is OrderPlacing -> scanGoProgressDialog.show()
            is OrderNeedAgeVerify -> {
                ScangoAgeNoticeFragment.newInstance().show(
                    supportFragmentManager,
                    ScangoAgeNoticeFragment.TAG
                )
            }
            is OrderSuccess -> gotoReceiptScreen(orderPlacementStatus.scangoCheckoutResult.orderId)
            is OrderCardError -> {
                scangoGeneralErrorDialog.show()
            }
            is OrderFailure -> {
                onOrderFail(orderPlacementStatus.e)
                enableViews(place_order_button_layout)
            }
            is OrderErrorNetwork -> {
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        viewModel.placeOrder()
                    }
                    show()
                }
            }
        }
    }

    private fun onTimeStampStatusChange(timeStampStatus: ScangoGetTimeStatus?) {
        scanGoProgressDialog.dismiss()
        when (timeStampStatus) {
            is ScangoGetTimeSuccess -> {
                viewModel.placeOrder()
            }
            is ScangoGetTimeIsViolated -> {
                if (!cartViewModel.getListTimeRestriction().isNullOrEmpty()) {
                    val liquorRemoveItem = LiquorRemoveItemFragment.newInstance()
                    liquorRemoveItem.setCallbackListener(
                        object : LiquorRemoveItemFragment.LiquorRemoveListener {
                            override fun onRemovedItemLiquor() {
                                finish()
                            }
                        }
                    )
                    liquorRemoveItem.show(
                        supportFragmentManager,
                        LiquorRemoveItemFragment::class.java.name
                    )
                }
            }
            is ScangoGetTimeLoading -> {
                scanGoProgressDialog.show()
            }
            is ScangoGetTimeErrorNetwork -> {
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        viewModel.checkTimeStamp()
                    }
                    show()
                }
            }
        }
    }

    private fun onLinkPointAndCartStatusChange(loadCartAndLinkpointStates: LoadCartAndLinkpointStates?) {
        scanGoProgressDialog.dismiss()
        when (loadCartAndLinkpointStates) {
            is LoadCartAndLinkpointLoading -> {
                scanGoProgressDialog.show()
            }
            is LoadCartAndLinkpointSuccess -> {
                onLoadAllSuccess(
                    loadCartAndLinkpointStates.cartSyncData,
                    loadCartAndLinkpointStates.scangoPaymentSummary
                )
            }
        }
    }

    private fun onOrderFail(e: Throwable) {
        scangoPaymentErrorDialog.apply {
            onActionClick = {
                place_order_button_layout.performClick()
            }
            onReviewActionClick = {
                dismiss()
            }
            show()
        }
    }

    private fun onCartSyncStatusChange(scangoCartResponse: ScangoCartResponse?) {
        when (scangoCartResponse) {
            is ScangoCartFailGeneral -> {
                scanGoProgressDialog.dismiss()
                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        viewModel.loadCards()
                        viewModel.loadCart()
                        viewModel.loadLinkPoint()
                    }
                    show()
                }
            }
            is ScangoCartFailNetwork -> {
                scanGoProgressDialog.dismiss()
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        viewModel.loadCards()
                        viewModel.loadCart()
                        viewModel.loadLinkPoint()
                    }
                    show()
                }
            }
        }
        updateUi()
    }

    private fun onLoadAllSuccess(data: ScangoCartSyncData, paymentSummary: ScangoPaymentSummary) {
        //item_cart_dropdown_view.updateListItemCart(data.cartSyncItems)
        val promoDiscount = viewModel.selectedCouponList.getTotalPromoDiscount()
        place_order_button_layout.total = data.sellingPrice - promoDiscount
        order_details_layout.paymentSummary = paymentSummary
        //When response content linkpoint value default switch true
        /*paymentSummary.linkPointStatus?.let {
            if (it.isFullyLinked && it.dollarBalance != 0.0) {
                viewModel.changeLinkPointStatus(true)
            }
        }*/
    }

    private fun gotoReceiptScreen(orderId: Long) {
        startActivity(ScangoReceiptActivity.getIntent(this, orderId))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data?.let {
            if (requestCode == Constants.REQUEST_PROMO) {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.setSelectedCoupons(
                        ScangoConverters.toCouponList(it.getStringExtra("selectedCoupons")) ?: listOf()
                    )
                }
            }
            if (requestCode == Constants.REQUEST_AGE_VERIFY) {
                if (resultCode == Activity.RESULT_OK) {
                    showScangoToast("Age verified")
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    when (it.getIntExtra(ScangoAgeVerifyScanActivity.INTENT_SCANGO_TYPE_SCAN, 0)) {
                        AgeVerifyType.MANUALLY_INPUT.value -> {
                            startActivityForResult(
                                Intent(this@ScangoCheckoutActivity, ScangoAgeVerifyScanActivity::class.java),
                                Constants.REQUEST_AGE_VERIFY
                            )
                        }
                        AgeVerifyType.QR_CODE.value -> {
                            startActivityForResult(
                                Intent(this@ScangoCheckoutActivity, ScangoAgeVerifyManuallyActivity::class.java),
                                Constants.REQUEST_AGE_VERIFY
                            )
                        }
                    }

                }
            }
        }
    }

    override fun reloadData() {
        //TODO handle when remove liquor item reload checkout
    }

    private fun showEmptyUI() {
        cl_empty_checkout.visible()
        //item_cart_dropdown_view
        order_details_layout.gone()
        place_order_button_layout.gone()
    }

    override fun onBackPressed() {
        onBackFromLiquor()
        super.onBackPressed()
    }

    private fun onBackFromLiquor() {
        val fragments = supportFragmentManager.fragments
        for (f in fragments) {
            if (f != null && f is LiquorRemoveItemFragment)
                f.onBack()
        }
    }

    /* override fun handleBackPress(): Boolean {
         viewModel.apply {
             if (isAgeVerified) {
                 showAgeCancelDialog { clearAgeVerifyCached() }
             } else {
                 return false
             }
         }
         return true
     }*/
}
