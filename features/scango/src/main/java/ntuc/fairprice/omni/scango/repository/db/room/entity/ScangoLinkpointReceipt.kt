package ntuc.fairprice.omni.scango.repository.db.room.entity

import java.io.Serializable

data class ScangoLinkpointReceipt(
    val pointUsed: Double?,
    val dollarUsed: Double?,
    val pointExtra: Double?
) : Serializable
