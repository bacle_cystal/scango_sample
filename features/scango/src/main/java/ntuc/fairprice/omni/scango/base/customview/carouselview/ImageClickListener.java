package ntuc.fairprice.omni.scango.base.customview.carouselview;

/**
 * Created by leonardo on 06/11/16.
 */

public interface ImageClickListener {
    void onClick(int position);
}
