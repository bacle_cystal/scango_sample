package ntuc.fairprice.omni.scango.ui.screen.cart

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_cart.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoCartFullErrorDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.customview.swipeview.SwipeLayout
import ntuc.fairprice.omni.scango.epoxy.SGCartPageDelegate
import ntuc.fairprice.omni.scango.epoxy.SGComponent
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorRemoveItemFragment
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import ntuc.fairprice.omni.scango.ui.screen.cart.data.toCartItems
import ntuc.fairprice.omni.scango.ui.screen.cart.widget.ScangoAgeRestrictionWidget
import ntuc.fairprice.omni.scango.ui.screen.productdetail.ui.ScangoProductDetailActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.utils.RemoteConfigUtils
import ntuc.fairprice.omni.scango.utils.observe
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.setCurrencyText
import thor.zopsmart.com.thor.epoxy.FPDelegate
import thor.zopsmart.com.thor.epoxy.FPViewAllDelegate
import javax.inject.Inject
import thor.zopsmart.com.thor.R as RFP

class ScangoCartActivity : ScangoBaseActivity() {

    companion object {

        const val LATEST_PRODUCT_ID = "latestProductID"

//        fun getCartIntentSingle(context: Context) = Intent(context, ScangoCartActivity::class.java).apply {
//            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
//        }
//
//        fun getCartIntentWithLatestProductID(context: Context, productID: Long) =
//            Intent(context, ScangoCartActivity::class.java).apply {
//                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
//                putExtra(LATEST_PRODUCT_ID, productID)
//            }
    }

//    private lateinit var viewBinding: ViewDataBinding

//    @Inject
//    lateinit var scanGoProgressDialog: ScanGoProgressDialog
//    @Inject
//    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
//    @Inject
//    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
//    @Inject
//    lateinit var viewModel: ScangoCartViewModel
    @Inject
    lateinit var scangoAgeRestrictionWidget: ScangoAgeRestrictionWidget
    @Inject
    lateinit var scangoCartFullErrorDialog: ScangoCartFullErrorDialog

    private var lastProductID = -1L
    private var isShownAgeWarning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_cart)
//        viewBinding = DataBindingUtil.setContentView(this@ScangoCartActivity, R.layout.scango_activity_cart)
//
//        viewBinding.lifecycleOwner = this
//
//        viewBinding.apply {
//            setVariable(BR.vm, viewModel)
//            executePendingBindings()
//        }
//
//        intent.extras?.let {
//            lastProductID = it.getLong(LATEST_PRODUCT_ID, -1L)
//        }



        initView()

//        val responseJson  = JSONObject(dummyJson)
//
//        val cartItem =responseJson.toCartItems()
//        val data = ScangoCartSyncData(0.0,99.0, listOf(), listOf(),0.0)
//
//        updateCartText(data)
//        handleCartChangeVisibility(null)
//        viewModel.loadPage(data)

    }

//    @SuppressLint("SetTextI18n")
    private fun initView() {
        cvScan.setOnClickListener {
            startActivity(CodeScannerActivity.getScanBarcodeIntentSingle(this))
            finish()
        }

        cvCheckOut.setOnClickListener {
            launchActivity(Constants.checkoutClassname)
        }

        cl_scan_items.setOnClickListener {
            startActivity(CodeScannerActivity.getScanBarcodeIntentSingle(this))
            finish()
        }

        iv_close_back.setOnClickListener {
            onBackPressed()
        }


    }

    override fun onResume() {
        super.onResume()
//        viewModel.syncCart()
    }

    private fun initWidget() {
//        scangoAgeRestrictionWidget.initWidget(cl_main_scroll)
    }

    fun updateCartText(data: ScangoCartSyncData) {
//        data.getCartQuantity().let {
            tvCartNumberItems.text = "1"
//        }
        tv_total_price.setCurrencyText(19.0)
    }

    private fun createObservers() {

        //FelixV we don't need to load Cart from local now anymore
        //Only load from API Response

        //Observes changes in cart (adding or removing items from cart)
//        viewModel.getCartItemsLiveData()?.observe(this, Observer { handleCartChange(it) })

//            observe(syncCartEvent) {


//            }

//            observe(isNoHasLiquodItem10h30pm) {
//                when (it) {
//                    true -> {
//                        //gotoReceiptFragment()
//                        //Binh.TH 13/04
//                        //TODO show checkout flow
//                        launchActivity(Constants.checkoutClassname)
//                    }
//                    false -> gotoLiquidRemove()
//                }
//            }
//            observe(cartSyncStatus) {
//                when (it) {
//                    is ScangoCartLoading -> {
//                        scanGoProgressDialog.show()
//                    }
//                    is ScangoCartSuccess -> {
//                        scanGoProgressDialog.dismiss()
//                    }
//                    is ScangoCartFailGeneral -> {
//
//                        scanGoProgressDialog.dismiss()
//
//                        scangoGeneralErrorDialog.apply {
//                            onActionClick = {
//                                viewModel.syncCart()
//                            }
//                            show()
//                        }
//                    }
////                    is ScangoCartFailNetwork -> {
////                        scanGoProgressDialog.dismiss()
////
////                        scangoNetworkErrorDialog.apply {
////                            onActionClick = {
////                                viewModel.syncCart()
////                            }
////                            show()
////                        }
////                    }
//                }
//            }

    }


    private fun gotoLiquidRemove() {
        supportFragmentManager?.let {
            val fragment = LiquorRemoveItemFragment.newInstance()
            fragment.show(
                it,
                LiquorRemoveItemFragment.TAG
            )
        }
    }

    /**
     * Checks the cartItems and displays empty cart layout/ cart layout as required.
     * It also notifies the adapter when the data set changes
     */
    private fun handleCartChangeVisibility(cartItems: List<ScangoCartItem>?) {
//        if (cartItems != null && cartItems.isNotEmpty()) {
            cvCheckOut.visibility = View.VISIBLE
            cvScan.visibility = View.VISIBLE
            cl_cart.visibility = View.VISIBLE
            cl_empty_cart.visibility = View.GONE
            cl_scan_items.visibility = View.GONE
            //adapter.cartItems = cartItems
            //adapter.notifyDataSetChanged()
//        } else showEmptyCart()
    }

    private fun showEmptyCart() {
        cl_cart.visibility = View.GONE
        cvCheckOut.visibility = View.GONE
        cvScan.visibility = View.GONE
        cl_empty_cart.visibility = View.VISIBLE
        cl_scan_items.visibility = View.VISIBLE
    }

    private fun showEmptyScreen() {
        cl_cart.visibility = View.GONE
        cvCheckOut.visibility = View.GONE
        cvScan.visibility = View.GONE
        cl_empty_cart.visibility = View.GONE
        cl_scan_items.visibility = View.GONE
    }

//    override fun supportFragmentInjector() = dispatchingAndroidInjector


    //This logic clone from FPON FPCartPageDelegate
//    override fun addToCart(cartItem: ScangoCartItem) {
//        if (viewModel.getCartItemsQuantity() >= RemoteConfigUtils.maxCartSize) {
//            scangoCartFullErrorDialog.show()
//        } else {
//            viewModel.increment(cartItem)
//        }
//    }

//
//    var dummyStringJson = "{\"code\":200,\"status\":\"SUCCESS\",\"data\":{\"cart\":{\"youPay\":1.95,\"savings\":0,\"changes\":null,\"items\":[{\"id\":\"1140960\",\"q\":\"1\",\"p\":1.95,\"mrp\":1.95,\"priceOverridden\":false,\"discount\":0,\"isFree\":false,\"product\":{\"barcodes\":[\"8990800348056\"],\"brand\":{\"clientId\":null,\"description\":null,\"id\":23605,\"image\":null,\"logo\":null,\"name\":\"VAN MELLE\",\"productsCount\":2,\"slug\":\"van-melle\",\"status\":\"ENABLED\"},\"bulkOrderThreshold\":51,\"clientItemId\":\"248759\",\"createdAt\":\"2019-04-27T03:04:08+08:00\",\"description\":\"Approx 50 pieces\",\"handlingDays\":0,\"hasVariants\":0,\"id\":1140960,\"images\":[\"https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/XL/248759_XL1.jpg\",\"https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/XL/248759_LXL1.jpg\"],\"imagesExtra\":null,\"metaData\":{\"LinkPoint Eligible\":true,\"SAP SubClass\":140380103},\"name\":\"Mentos Chewy Mints - Fruit (Share-A-Bowl)\",\"organizationId\":\"2\",\"primaryCategory\":{\"clientId\":\"13493\",\"description\":null,\"id\":344,\"image\":null,\"name\":\"Chewy \\u0026 Marshmallow\",\"parentCategory\":{\"clientId\":\"12960\",\"description\":null,\"id\":690,\"image\":\"http://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/uat8/media/images/banners/web/category/Sweets_L2.jpg\",\"name\":\"Sweets\",\"parentCategory\":{\"clientId\":\"12704\",\"description\":null,\"id\":615,\"image\":\"banners/web/category/DrinksSnacks_L1.jpg\",\"name\":\"Drinks \\u0026 Snacks\",\"organizationId\":\"2\",\"parentCategory\":null,\"productsCount\":0,\"slug\":\"drinks--snacks-3\",\"status\":\"ENABLED\"},\"productsCount\":193,\"slug\":\"sweets\",\"status\":\"ENABLED\"},\"productsCount\":35,\"slug\":\"chewy--marshmallow\",\"status\":\"ENABLED\"},\"secondaryCategoryIds\":[154],\"slug\":\"mentos-chewy-mints-fruit-share-a-bowl-248759\",\"\t\":0,\"status\":\"ENABLED\",\"stockOverride\":{\"maxPurchasableStock\":50,\"stockBuffer\":null},\"storeSpecificData\":{\"discount\":\"0.000000\",\"mrp\":\"1.950000\",\"stock\":0,\"unlimitedStock\":false},\"tagIds\":[1,4],\"tags\":[{\"description\":null,\"filterable\":false,\"id\":4,\"image\":null,\"name\":\"B2B\",\"productsCount\":12118,\"slug\":\"b2b\",\"type\":\"PUBLIC\"}],\"variants\":null},\"time\":0}],\"handlingDays\":0,\"isBulk\":false,\"storeId\":\"309\",\"amounts\":{\"orderAmount\":1.95,\"shippingAmount\":0,\"clickAndCollectCharge\":0,\"totalCouponDiscount\":0},\"orderType\":\"OFFLINE\"}}}"
//    var dummyJson  = dummyStringJson.replace("\"","")
}
