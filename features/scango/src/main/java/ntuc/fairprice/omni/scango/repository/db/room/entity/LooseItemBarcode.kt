package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity

@Entity
data class LooseItemBarcode(
    val EANCode: String,
    val weightOrQuantity: Double,
    val price: Double
)