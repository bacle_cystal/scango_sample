package ntuc.fairprice.omni.scango.ui.screen.receipt

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_receipt_confirmation_fragment.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.base.ScangoPaymentEventDelegate
import ntuc.fairprice.omni.scango.databinding.ScangoReceiptConfirmationFragmentBinding
import ntuc.fairprice.omni.scango.utils.observe
import ntuc.fairprice.omni.scango.utils.setCardText
import ntuc.fairprice.omni.scango.utils.setCreaditCardImage
import ntuc.fairprice.omni.scango.utils.toBitmap
import thor.zopsmart.com.thor.base.extensions.*
import javax.inject.Inject

class ScangoReceiptConfirmationFragment :
    ScangoBaseBottomSheetFragment<ScangoReceiptConfirmationFragmentBinding, ScangoReceiptConfirmationViewModel>(),
    ScangoPaymentEventDelegate, HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    companion object {
        fun newInstance(totalAmount: Double, cardNumber: String, cardType: String, byteArrayQr: ByteArray) =
            ScangoReceiptConfirmationFragment().apply {
                arguments = Bundle().apply {
                    putDouble(Constants.EXTRA_RECEIPT_TOTAL_AMOUNT, totalAmount)
                    putString(Constants.EXTRA_RECEIPT_CARD_NUMBER, cardNumber)
                    putString(Constants.EXTRA_RECEIPT_CARD_TYPE, cardType)
                    putByteArray(Constants.EXTRA_BYTE_ARRAY_QR, byteArrayQr)
                }
            }

        val TAG = "ReceiptConfirmationFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val layoutId: Int
        get() = R.layout.scango_receipt_confirmation_fragment

    override val viewModel: ScangoReceiptConfirmationViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoReceiptConfirmationViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
            executePendingBindings()
        }

        btn_receipt_detail.setOnClickListener {
            dismiss()
        }
        txt_price.setCurrencyText(arguments?.getDouble(Constants.EXTRA_RECEIPT_TOTAL_AMOUNT) ?: 0.0)
        txt_card_number.setCardText(arguments?.getString(Constants.EXTRA_RECEIPT_CARD_NUMBER) ?: "")


        val cardNumber = arguments?.getString(Constants.EXTRA_RECEIPT_CARD_NUMBER)
        if (!cardNumber.isNullOrEmpty()) {
            txt_card_number.visible()
            ic_card.visible()
            txt_card_number.setCardText(cardNumber)
        } else {
            txt_card_number.invisible()
            ic_card.invisible()
        }


        ic_card.setCreaditCardImage(arguments?.getString(Constants.EXTRA_RECEIPT_CARD_TYPE) ?: "")
        img_receipt_qrcode.setImageBitmap(arguments?.getByteArray(Constants.EXTRA_BYTE_ARRAY_QR)?.toBitmap())

        ic_receipt_step_1.setOnClickListener {
            ic_receipt_step_2.setImageResource(R.drawable.sg_ic_step_current)
            ic_receipt_step_3.setImageResource(R.drawable.sg_ic_step_current)
            line_step_1.setBackgroundColor(resources.getColor(R.color.white_two, resources.newTheme()))
            line_step_2.setBackgroundColor(resources.getColor(R.color.white_two, resources.newTheme()))
        }

        ic_receipt_step_2.setOnClickListener {
            ic_receipt_step_2.setImageResource(R.drawable.sg_ic_success)
            line_step_1.setBackgroundColor(resources.getColor(R.color.blue_blue, resources.newTheme()))
        }

        ic_receipt_step_3.setOnClickListener {
            ic_receipt_step_2.setImageResource(R.drawable.sg_ic_success)
            ic_receipt_step_3.setImageResource(R.drawable.sg_ic_success)
            line_step_1.setBackgroundColor(resources.getColor(R.color.blue_blue, resources.newTheme()))
            line_step_2.setBackgroundColor(resources.getColor(R.color.blue_blue, resources.newTheme()))
        }

        viewModel.apply {
            updateReceiptStatus()
            observe(receiptStatus, ::onReceiptConfirmStatusChanged)
        }
    }

    private fun onReceiptConfirmStatusChanged(receiptConfirmStatus: ReceiptConfirmStatus?) {
        when (receiptConfirmStatus) {
            is ReceiptPaymentComplete -> {
                ic_receipt_step_2.performClick()
            }
        }
    }

    override fun onVerifyStatusChange() {
    }
}

