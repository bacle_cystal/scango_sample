package ntuc.fairprice.omni.util

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 * Created by Binh.TH on 4/9/2019.
 */

class ForegroundManager : LifecycleObserver {
    var isAppInForeground: Boolean = false
        private set

    fun isInAppForeground(): Boolean {
        return isAppInForeground
    }

    private fun updateForegroundState(isForeground: Boolean) {
        this.isAppInForeground = isForeground
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        updateForegroundState(false)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        updateForegroundState(true)
    }
}
