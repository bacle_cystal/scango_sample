package ntuc.fairprice.omni.scango.repository.webservice.websocket

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.gson.Gson
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivityViewModel
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import org.json.JSONObject
import java.lang.Exception
import java.net.URI

class ReceiptSocketService constructor(val vm: ScangoReceiptActivityViewModel): LifecycleObserver {

    private var mWebSocketClient: WebSocketClient? = null
    private var isConnected = false

    init {
        // FelixV: URI and action model are still pending from server side
        val uri = URI("")
        mWebSocketClient = object : WebSocketClient(uri) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                isConnected = true
                subscribeToActionChannel()
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                isConnected = false
            }

            override fun onMessage(message: String?) {
                message?.let {
                    val jsonObject = JSONObject(it)
                    vm.isNeedToShowFeedback.value = true
                }

            }

            override fun onError(ex: Exception?) {
                isConnected = false
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun openWebSocket() {
        Log.d("WebSocket", "Receipt WebSocket Open")
//        mWebSocketClient?.connect()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun closeWebSocket() {
        Log.d("WebSocket", "Receipt WebSocket Close")
//        mWebSocketClient?.close()
    }

    private fun subscribeToActionChannel() {
        if (isConnected) {
            val socketChannel = ReceiptActionChannel(
                ReceiptActionChannel.COMMAND_SUBSCRIBE,
                ReceiptActionChannel.ACTION_CHANNEL,
                "",
                ""
            )
            mWebSocketClient!!.send(Gson().toJson(socketChannel))
        }
    }
}