package ntuc.fairprice.omni.scango.ui.screen.shoppinglist

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.scango_layout_fragment_shopping_list_page.*
import ntuc.fairprice.omni.scango.TitleBindingModel_
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import thor.zopsmart.com.thor.AddShoppingListBindingModel_
import thor.zopsmart.com.thor.GapBindingModel_
import thor.zopsmart.com.thor.R as RFP
import ntuc.fairprice.omni.scango.R as RSG
import thor.zopsmart.com.thor.ShoppingListBindingModel_
import thor.zopsmart.com.thor.base.BaseFragment
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.epoxy.views.HorizontalGapModel_
import thor.zopsmart.com.thor.features.shoppinglist.ui.CreateShoppingListViewModel_
import thor.zopsmart.com.thor.repository.db.room.entity.ShoppingList
import thor.zopsmart.com.thor.repository.db.room.repo.WishlistRepository
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import javax.inject.Inject


class ScangoShoppingListPage : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var keyboardManager: KeyboardManager

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override val mLayout: Int
        get() = RSG.layout.scango_layout_fragment_shopping_list_page

    private val viewModel: ScangoShoppingListPageViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoShoppingListPageViewModel::class.java)
    }

    private val cartViewModel: ScangoCartViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoCartViewModel::class.java)
    }

    private var isCreatingNewList = false

    override fun onCreated() {

        epoxy_recycler_view.buildModelsWith {

            GapBindingModel_().id(hashCode()).gap(24F).addTo(it)

            // Adds the list of shopping list
            viewModel.shoppingLists.value?.forEach { shoppingList ->
                ShoppingListBindingModel_()
                    .id("shoppinglist_${shoppingList.id}")
                    .shoppingList(shoppingList)
                    .onClick { _ ->
                        changePage(
                            ScangoShoppingListDetailActivity.getIntent(
                                mContext,
                                shoppingList.id,
                                shoppingList.name
                            )
                        )
                    }
                    .editClickListener { _ ->
                        shoppingListEditDialog(actionRename = {
                            shoppingListRenameDialog {
                                viewModel.renameShoppingList(shoppingList, it)
                            }
                        }, actionDelete = {
                            shoppingListConfirmDeleteDialog(shoppingList.name, action = {
                                viewModel.deleteShoppingList(shoppingList)
                            })
                        })
                    }
                    .addTo(it)
            }

            // Adds a view to create a new shopping list
            AddShoppingListBindingModel_()
                .id("add_shopping_list")
                .addClick { _ ->
                    isCreatingNewList = !isCreatingNewList
                    epoxy_recycler_view.requestModelBuild()
                }
                .addIf(!isCreatingNewList, it)

            CreateShoppingListViewModel_()
                .id("create_shopping_list")
                .cancelClick { _ ->
                    isCreatingNewList = !isCreatingNewList
                    epoxy_recycler_view.requestModelBuild()
                    keyboardManager.hide(epoxy_recycler_view)
                }
                .onDone {
                    isCreatingNewList = !isCreatingNewList
                    viewModel.createShoppingList(it)
                    keyboardManager.hide(epoxy_recycler_view)
                }
                .addIf(isCreatingNewList, it)
        }

        viewModel.shoppingLists.observe(this, Observer {
            epoxy_recycler_view.requestModelBuild()
        })

        viewModel.cartItems.observe(this, Observer { listItem ->
            icon_cart.update(listItem.sumBy { it.quantity })
        })

        viewModel.wishlistStatus.observe(this,
            Observer {
                cl_loader.gone()
                when (it) {
                    is WishlistLoading -> cl_loader.visible()
                    is WishlistFail ->
                        showConfirmation(
                            message = it.e.message ?: resources.getString(RFP.string.generic_error),
                            yesText = resources.getString(RFP.string.btn_ok),
                            noText = resources.getString(RFP.string.btn_cancel)
                        )
                }
            }
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadData()
    }
}


class ScangoShoppingListPageViewModel @Inject constructor(
    mApplication: Application,
    private val wishlistRepository: WishlistRepository,
    private val scangoCartRepository: ScangoCartRepository
) : BaseViewModel(mApplication) {

    var shoppingLists: LiveData<List<ShoppingList>> = wishlistRepository.getShoppingListsLiveData()

    var cartItems: LiveData<List<ScangoCartItem>> = scangoCartRepository.getCartItemsLiveData()

    val wishlistStatus = MutableLiveData<WishlistStatus>()


    fun loadData() {
        launch {
            wishlistStatus.value = WishlistLoading
            wishlistRepository.loadShoppingList()
                .onSuccess {
                    wishlistStatus.value = WishlistSuccess
                }
                .onError {
                    wishlistStatus.value = WishlistFail(it)
                }
        }
    }

    fun deleteShoppingList(shoppingList: ShoppingList) {
        launch {
            wishlistStatus.value = WishlistLoading
            wishlistRepository.removeShoppingList(shoppingList)
                .onSuccess {
                    wishlistStatus.value = WishlistSuccess
                }
                .onError {
                    wishlistStatus.value = WishlistFail(it)
                }
        }
    }

    fun createShoppingList(name: String) {
        launch {
            wishlistStatus.value = WishlistLoading
            wishlistRepository.createShoppingList(name)
                .onSuccess {
                    wishlistStatus.value = WishlistSuccess
                }
                .onError {
                    wishlistStatus.value = WishlistFail(it)
                }
        }
    }

    fun renameShoppingList(shoppingList: ShoppingList, newName: String) {
        launch {
            wishlistStatus.value = WishlistLoading
            wishlistRepository.renameShoppingList(shoppingList, newName)
                .onSuccess {
                    wishlistStatus.value = WishlistSuccess
                }
                .onError {
                    wishlistStatus.value = WishlistFail(it)
                }
        }
    }
}

sealed class WishlistStatus
object WishlistLoading : WishlistStatus()
object WishlistSuccess : WishlistStatus()
class WishlistFail(val e: Throwable) : WishlistStatus()