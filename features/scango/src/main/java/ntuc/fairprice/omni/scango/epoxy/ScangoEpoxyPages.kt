@file:Suppress("unused")

package ntuc.fairprice.omni.scango.epoxy

import ntuc.fairprice.omni.scango.StylishToolbarBindingModel_
import ntuc.fairprice.omni.scango.base.ScangoComponent
import ntuc.fairprice.omni.scango.base.ScangoImageSlideShow
import ntuc.fairprice.omni.scango.base.ScangoTitle
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.epoxy.getRows
import thor.zopsmart.com.thor.features.widget.data.WidgetData
import thor.zopsmart.com.thor.features.widget.data.getSortKey
import thor.zopsmart.com.thor.features.widget.imageslideshow.data.imageSlideShowList
import thor.zopsmart.com.thor.features.widget.productcollection.data.getFilters
import thor.zopsmart.com.thor.features.widget.productcollection.data.productScroller
import thor.zopsmart.com.thor.features.widget.productdetails.data.productDetail
import thor.zopsmart.com.thor.model.*
import thor.zopsmart.com.thor.repository.webservice.model.ApiResponse
import thor.zopsmart.com.thor.repository.webservice.model.toSearchResults


// 03/04 Binh.TH
// Use this for homepage scango display image slide show only.
fun ApiResponse.toScangoEpoxyPage(): List<ScangoComponent<*>> {

    val data = this jsonNullable "data" ?: JSONObject()
    val page = data jsonNullable "page" ?: JSONObject()

    val entity = page jsonNullable "entity" ?: JSONObject()
    val pageName = page string "name"

    when (pageName) {

        /*"BRAND" -> {
            return toBrandPage(entity)
        }

        "CATEGORY" -> {
            return toCategoryPage(entity)
        }*/

        "HOME" -> {
            return toScangoEpoxyViews()
        }
        /*"PRODUCT" -> {
            return toProductPage()
        }*/

    }
    return toScangoEpoxyViews(true)
}


fun ApiResponse.toScangoEpoxyViews(
    filterNeeded: Boolean = false,
    isProductPage: Boolean = false
): List<ScangoComponent<*>> {
    val sgComponnents: MutableList<ScangoComponent<*>> = mutableListOf()
    val layouts = this jsonArrayR listOf("data", "page", "layouts") ?: JSONArray()
    val sgComponnents_new: MutableList<SGComponent<*>> = mutableListOf()

    layouts.forEachIndexed { index, it ->
        val widgetData = WidgetData(it)
        when (widgetData.name) {

            "ImageSlideShow" -> {
                val slides = widgetData.imageSlideShowList()
                if (slides.isNotEmpty()) {
                    sgComponnents.add(ScangoTitle("Store Campaign & Events"))
                    sgComponnents.add(ScangoImageSlideShow(widgetData.imageSlideShowList()))
                }
            }
            /*
            "CategoryCollection" -> {
                val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""

                if (title.isNotEmpty()) {
                    fpComponnents.add(FPHeader(title))
                }

                val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
                if (subtitle.isNotEmpty()) {
                    fpComponnents.add(FPSubtitle(subtitle))
                }

                val layoutType = widgetData.metaData string "layoutType"

                fpComponnents.add(
                    if (layoutType == "SCROLLER")
                        FPCategoryScroller(widgetData.categoryCollection())
                    else
                        FPCategoryGrid(widgetData.categoryCollection())
                )

            }
            "ProductCollection" -> {

                val products = widgetData.productScroller()

                // Add layouts only if the collection is not empty
                if (!products.isNullOrEmpty()) {
                    // Change product collection to scroller if the layout is not the last layout
                    val layoutType = widgetData.metaData string "layoutType" ?: "GRID"

                    // Add filter and sorting only for the Product Grid
                    if (layoutType == "GRID" && filterNeeded) {
                        //TODO : TUAN
                        sgComponnents_new.add(SGFilterSort(sortKey = widgetData.getSortKey(), filters = widgetData.getFilters()))
                    }

                    // Add header if present
                    val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""
                    if (title.isNotEmpty()) {

                        sgComponnents_new.add(
                            SGHeader(
                                title,
                                seeAll = true,
                                associatedData = widgetData.metaData
                            )
                        )
                    }

                    val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
                    if (subtitle.isNotEmpty()) {
                        sgComponnents_new.add(SGSubtitle(subtitle))
                    }

                    // Add the collection
                    sgComponnents_new.add(
                        if (layoutType == "SCROLLER")
                            SGProductScroller(products as ArrayList<Item>, widgetData.metaData)
                        else {
                            if(isProductPage)
                                SGProductScroller(products as ArrayList<Item>, widgetData.metaData)
                            else
                                SGProductGrid(products as ArrayList<Item>, widgetData.metaData)
                        }
                    )
                }
            }
            "BrandCollection" -> {
                val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""
                val layoutType = widgetData.metaData string "layoutType" ?: "GRID"

                if (title.isNotEmpty()) {
                    fpComponnents.add(FPHeader(title))
                }

                val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
                if (subtitle.isNotEmpty()) {
                    fpComponnents.add(FPSubtitle(subtitle))
                }

                fpComponnents.add(
                    if (layoutType == "SCROLLER")
                        FPBrandScroller(widgetData.brandCollection())
                    else {
                        FPBrandGrid(widgetData.brandCollection())
                    }
                )
            }
            "BannerWithText" -> {
                try {
                    fpComponnents.add(FPBannerWithText(widgetData.bannerWithText()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }
            }
            "BannerWithButton" -> {
                try {
                    fpComponnents.add(FPBannerWithButton(widgetData.bannerWithButton()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }

            }
            "BannerWithMultipleButtons" -> {
                try {
                    fpComponnents.add(FPBannerWithMultipleButtons(widgetData.bannerWithMultipleButtons()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }
            }
            "ProductDetail" -> {
                try {
                    val item = widgetData.productDetail()

                    fpComponnents.add(FPProductDetail(item))

                    val offerList = item.offerList
                    offerList.forEach {
                        fpComponnents.add(FPVerticalGap(4F))
                        fpComponnents.add(FPOfferDetailPDP(it, null))
                    }

                    val metadataJSON = JSONObject(item.metadata)

                    // Add description
                    if(!item.description.isNullOrEmpty()) {
                        fpComponnents.add(FPProductMetadata("DESCRIPTION", item.description!!))
                    }

                    // Add metadata layouts
                    val keys = metadataJSON.keys()
                    while(keys.hasNext()) {
                        val key = keys.next()
                        val metadata = item.getMetadataItem(key)
                        if(metadata.isNotEmpty() && key != DISPLAY_UNITS) {
                            fpComponnents.add(FPProductMetadata(key, metadata))
                        }
                    }
                    // TODO add dietary layout
                } catch (ignore: Exception) {
                    // Don't add layout if there is an error
                }
            }*/
        }
    }

    return sgComponnents
}


fun ApiResponse.toSGEpoxyPage(query: String? = null): List<SGComponent<*>> {

    val data = this jsonNullable "data" ?: JSONObject()
    val page = data jsonNullable "page" ?: JSONObject()

    val entity = page jsonNullable "entity" ?: JSONObject()
    val pageName = page string "name"

    when (pageName) {

        /*"BRAND" -> {
            return toBrandPage(entity)
        }

        "CATEGORY" -> {
            return toCategoryPage(entity)
        }
        */
        "HOME" -> {
            return toSGEpoxyViews()
        }
        "PRODUCT" -> {
            return toProductPage()
        }
        "SEARCH" -> {
            return toSearchPage(query)
        }
//        "TAG" -> {
//            return toTagPage(entity)
//        }

    }
    return toSGEpoxyViews(true)
}

// ID = 4
fun ApiResponse.toProductPage(): List<SGComponent<*>> {
    val views: MutableList<SGComponent<*>> = arrayListOf()
    views.addAll(toSGEpoxyViews(filterNeeded = false, isProductPage = true))
    return views
}

fun ApiResponse.toSGEpoxyViews(
    filterNeeded: Boolean = false,
    isProductPage: Boolean = false,
    fpHeader: SGHeader? = null,
    fpSubHeader: SGSubtitle? = null
): List<SGComponent<*>> {
    val layouts = this jsonArrayR listOf("data", "page", "layouts") ?: JSONArray()
    val sgComponnents_new: MutableList<SGComponent<*>> = mutableListOf()

    layouts.forEachIndexed { index, it ->
        val widgetData = WidgetData(it)
        when (widgetData.name) {

            /*
           "ImageSlideShow" -> {
               val slides = widgetData.imageSlideShowList()
               if (slides.isNotEmpty()) {
                   sgComponnents.add(ScangoTitle("Store Campaign & Events"))
                   sgComponnents.add(ScangoImageSlideShow(widgetData.imageSlideShowList()))
               }
           }
           "CategoryCollection" -> {
               val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""

               if (title.isNotEmpty()) {
                   fpComponnents.add(FPHeader(title))
               }

               val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
               if (subtitle.isNotEmpty()) {
                   fpComponnents.add(FPSubtitle(subtitle))
               }

               val layoutType = widgetData.metaData string "layoutType"

               fpComponnents.add(
                   if (layoutType == "SCROLLER")
                       FPCategoryScroller(widgetData.categoryCollection())
                   else
                       FPCategoryGrid(widgetData.categoryCollection())
               )

           }
           */
            "ProductCollection" -> {

                val products = widgetData.productScroller()

                // Add layouts only if the collection is not empty
                if (!products.isNullOrEmpty()) {
                    // Change product collection to scroller if the layout is not the last layout
                    val layoutType = widgetData.metaData string "layoutType" ?: "GRID"

                    // Add filter and sorting only for the Product Grid
                    if (layoutType == "GRID" && filterNeeded) {
                        //TODO : TUAN
                        sgComponnents_new.add(
                            SGFilterSort(
                                sortKey = widgetData.getSortKey(),
                                filters = widgetData.getFilters()
                            )
                        )
                    }

                    // Add header if present
                    val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""
                    if (title.isNotEmpty()) {

                        sgComponnents_new.add(
                            SGHeader(
                                title,
                                seeAll = false,
                                associatedData = widgetData.metaData
                            )
                        )
                    }

                    val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
                    if (subtitle.isNotEmpty()) {
                        sgComponnents_new.add(SGSubtitle(subtitle))
                    }

                    // Add the collection
                    sgComponnents_new.add(
                        if (layoutType == "SCROLLER")
                            SGProductScroller(products as ArrayList<Item>, widgetData.metaData)
                        else {
                            if (isProductPage)
                                SGProductScroller(products as ArrayList<Item>, widgetData.metaData)
                            else
                                SGProductGrid(products as ArrayList<Item>, widgetData.metaData)
                        }
                    )
                }
            }
            /*
            "BrandCollection" -> {
                val title = widgetData.metaData string "title" ?: widgetData.value string "title" ?: ""
                val layoutType = widgetData.metaData string "layoutType" ?: "GRID"

                if (title.isNotEmpty()) {
                    fpComponnents.add(FPHeader(title))
                }

                val subtitle = widgetData.metaData string "subtitle" ?: widgetData.value string "title" ?: ""
                if (subtitle.isNotEmpty()) {
                    fpComponnents.add(FPSubtitle(subtitle))
                }

                fpComponnents.add(
                    if (layoutType == "SCROLLER")
                        FPBrandScroller(widgetData.brandCollection())
                    else {
                        FPBrandGrid(widgetData.brandCollection())
                    }
                )
            }
            "BannerWithText" -> {
                try {
                    fpComponnents.add(FPBannerWithText(widgetData.bannerWithText()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }
            }
            "BannerWithButton" -> {
                try {
                    fpComponnents.add(FPBannerWithButton(widgetData.bannerWithButton()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }

            }
            "BannerWithMultipleButtons" -> {
                try {
                    fpComponnents.add(FPBannerWithMultipleButtons(widgetData.bannerWithMultipleButtons()))
                } catch (ignore: Exception) {
                    // Don't add the view if there is a parsing error
                }
            }
            */
            "ProductDetail" -> {
                try {
                    val item = widgetData.productDetail()

                    sgComponnents_new.add(SGProductDetail(item))

                    val offerList = item.offerList
                    offerList.forEach {
                        sgComponnents_new.add(SGVerticalGap(4F))
                        sgComponnents_new.add(SGOfferDetailPDP(it, null))
                    }
                    // Add metadata layouts
                    metaDataList.forEach { metaData ->
                        if (metaData == DESCRIPTION && !item.description.isNullOrEmpty()) {
                            sgComponnents_new.add(SGProductMetadata("DESCRIPTION", item.description!!))
                        } else if (metaData == DIETARY) {
                            if (!item.dietaryAttributes.isNullOrEmpty()) {
                                sgComponnents_new.add(SGVerticalGap(4F))
                                sgComponnents_new.add(SGDietaryDataNew("Dietary", item.dietaryAttributes))
                            }
                        } else {
                            val metadataVal = item.getMetadataItem(metaData)
                            if (metadataVal.isNotEmpty()) {
                                viewMetaDataList[metaData]?.let { header ->
                                    if (metaData == NUTRITIONAL) {
                                        sgComponnents_new.add(SGNutrionalData(header, getRows(metadataVal)))
                                    } else {
                                        sgComponnents_new.add(SGProductMetadata(header, metadataVal))
                                    }
                                }
                            }
                        }
                    }
                } catch (ignore: Exception) {
                    // Don't add layout if there is an error
                }
            }
        }
    }

    return sgComponnents_new
}

fun ApiResponse.toSearchPage(searchText: String?): List<SGComponent<*>> {
    val views: MutableList<SGComponent<*>> = arrayListOf()
    val header = searchText?.let { "Results for \"$searchText\"" } ?: "Search results"
    views.addAll(toSGEpoxyViews(filterNeeded = true, fpHeader = SGHeader(header)))
    return views
}

fun ApiResponse.toSGProductListPage(
    associatedData: JSONObject,
    isPromo: Boolean = false,
    title: String?
): List<SGComponent<*>> {
    val views: MutableList<SGComponent<*>> = arrayListOf()
    val products = toSearchResults()

    val data = safeJson("data")
    val productsCount = data long "count" ?: 0L

    // Add header if present
    val title = if (isPromo) "Promotions" else associatedData string "title" ?: title ?: ""
    val subTitle = if (isPromo) "" else "$productsCount products"
    if (title.isNotEmpty()) {
        views.add(SGTitleBar(title, false,true))
        if (subTitle.isNotEmpty()) views.add(SGSubtitle(subTitle))
    }

    val filters = (this safeJsonR listOf("data", "filters")).getFilters()

    if (isPromo) {
        filters.firstOrNull { it.keyName.equals("tag", true) }?.run {
            filterKeys.mapIndexed { id, filterKeyDataHolder ->
                Promo(id.toLong() + 2, filterKeyDataHolder.slug, filterKeyDataHolder.name)
            }.let {
                // Add the hardcoded All promo
                views.add(SGPromo(it.toMutableList().apply { add(0, Promo(1, null, "All")) }))
            }
            isHidden = true
        }
    }

    views.add(SGFilterSort(sortKey = associatedData string "sorting" ?: "POPULARITY", filters = filters))

    views.add(
        SGProductGrid(products, associatedData)
    )
    return views
}