package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber

import android.text.InputFilter
import android.text.Spanned

/**
 * Input filter to format any 16 digit card number to 0000 0000 0000 0000
 */
class CardNumberFormatInputFilter : InputFilter {

    companion object {
        private const val MAX_INPUT_LENGTH = 19

    }
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        if ((dest != null) and (dest!!.toString().trim { it <= ' ' }.length > MAX_INPUT_LENGTH)) return null
        return if (source.length == 1 && (dstart == 4 || dstart == 9 || dstart == 14)) " $source" else null
    }
}