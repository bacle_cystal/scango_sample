package ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.widget

import android.app.Activity
import android.view.View
import kotlinx.android.synthetic.main.scango_activity_scan_age_verify.*
import ntuc.fairprice.omni.common.qrcode.common.CameraSource
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScangoBarcodeProcessor
import ntuc.fairprice.omni.scango.utils.ScanData
import javax.inject.Inject

class ScangoAgeScannerWidgetImpl @Inject constructor(
    private val context: Activity,
    private val viewModel: ScangoAgeVerifyScanViewModel
) : ScangoAgeScannerWidget {

    private var scanType = ScanType.QR_CODE.value

    private var cameraSource: CameraSource? = null

    private var scanGoBarCodeProcessor: ScangoBarcodeProcessor? = null

    override lateinit var containerView: View

    override fun initWidget(view: View, scanType: Int) {
        containerView = view
        this.scanType = scanType
    }

    override fun start(activity: Activity) {
        try {
            if (cameraSource == null) {
                cameraSource = CameraSource(context, fireFaceOverlay)
            }
            firePreview?.start(cameraSource, fireFaceOverlay)
            initQrcodeProcessor()
        } catch (e: Exception) {
            cameraSource?.release()
            cameraSource = null
        }
    }

    override fun stop() {
        firePreview?.stop()
    }

    override fun destroy() {
        firePreview?.stop()
        scanGoBarCodeProcessor?.stop()
        cameraSource?.release()

    }

    override fun updateFacing(isChecked: Boolean) {
        cameraSource?.let {
            if (isChecked) {
                it.setFacing(CameraSource.CAMERA_FACING_FRONT)
            } else {
                it.setFacing(CameraSource.CAMERA_FACING_BACK)
            }
        }
        stop()
    }

    private fun initQrcodeProcessor() {

        if (scanGoBarCodeProcessor != null)
            return

        scanGoBarCodeProcessor = ScangoBarcodeProcessor(
            context, scanType = ScanType.QR_CODE
            , onDetechListener = object : ScangoBarcodeProcessor.OnDetechListener {
                override fun onDetechSuccess(scanData: ScanData) {
                    if(scanData is ScanData.QrCode) {
                        viewModel.handleDetectQrResult(scanData.code)
                        stop()
                    }
                }

                override fun onDetechFail(e: Exception) {
                }
            })
        cameraSource?.setMachineLearningFrameProcessor(scanGoBarCodeProcessor)
    }
}