package ntuc.fairprice.omni.scango.ui.screen.cart

import dagger.Binds
import dagger.Module
import ntuc.fairprice.omni.scango.ui.screen.cart.widget.ScangoAgeRestrictionWidget
import ntuc.fairprice.omni.scango.ui.screen.cart.widget.ScangoAgeRestrictionWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoRestrictionBindsModule {
    @Binds
    @ActivityScope
    abstract fun provideScangoRestrictionWidget(widget: ScangoAgeRestrictionWidgetImpl): ScangoAgeRestrictionWidget
}