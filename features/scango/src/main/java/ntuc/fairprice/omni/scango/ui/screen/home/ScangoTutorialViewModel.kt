package ntuc.fairprice.omni.scango.ui.screen.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.repository.db.room.entity.Customer
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

class ScangoTutorialViewModel @Inject constructor(
    application: Application,
    private val accountRepository: AccountRepository,
    private val scangoLinkPointRepository: ScangoLinkPointRepository
) : ScangoBaseViewModel(application) {

    val shouldDisplayLpBannerLiveData = MutableLiveData<Boolean>().default(false)

    fun checkIfLpLinked() = launch {
        getCustomer()?.let { customer ->
            scangoLinkPointRepository.getCustomers(customer.id.toString())
                .onSuccess {
                    onSuccess()
                    scangoLinkPointRepository.linkPointStatus = it
                    shouldDisplayLpBannerLiveData.value = (it.isNotLinked || it.isPartiallyLinked)
                            && getCustomer() != null
                            && !scangoLinkPointRepository.userDismissedForSession

                }
                .onError {
                    shouldDisplayLpBannerLiveData.value = false
                }
        }
    }

    fun userDismissedLpBanner()  {
        scangoLinkPointRepository.userDismissedForSession = true
    }

    private fun getCustomer(): Customer? =
        accountRepository.getCustomer()
}