package ntuc.fairprice.omni.scango.repository.db.room.entity

import android.os.Build
import androidx.room.Entity
import androidx.room.TypeConverters
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.InvalidItemException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.features.widget.categorycollection.data.Category
import thor.zopsmart.com.thor.features.widget.categorycollection.data.UrlType
import thor.zopsmart.com.thor.features.widget.categorycollection.data.toCategory
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.typeconverter.Converters
import java.util.*

@Entity
data class Product(
    var id: Long? = 1140960,
    var itemId: Long? = 248759,
    var name: String? = "Mentos Chewy Mints - Fruit",
    var fullName: String? = "Mentos Chewy Mints - Fruit",
    var hasVariants: Boolean? = false,
    var item_type: ProductType? = ProductType.PRODUCT,
    var images: List<String>? = listOf("https://s3-ap-southeast-1.amazonaws.com/www8.fairprice.com.sg/fpol/media/images/product/XL/248759_XL1.jpg"),
    var url: String? = null,
    var mrp: Double? = null,
    var discount: Double? = null,
    var sellingPrice: Double? = 1.95,
    var productId: Int? = 12,
    var description: String? = null,
    var brand: String? = null,
    var categories: List<Category>? = null,
    var data: JSONObject? = null,
    var parentData: JSONObject? = null,
    var stock: Int = 100,
    var quantity: Int = 0,
    var selectedVariantId: Long = 0,
    var variants: List<Product>? = mutableListOf(),
    var isShoppingListed: Boolean = false,
    var countryName: String? = null,
    var primaryCategory: Category? = null,
    var soldByWeight: Int = 0,
    var weight: Double = 0.0
) {

    @field:TypeConverters(Converters::class)
    var parentProduct: Product? = null

    fun getImage(): String {
        return images?.let {
            if (it.isNotEmpty())
                it[0]
            else
                ""
        }?:""
    }

    /**
     * TODO : Currently this always returns false
     */
    override fun equals(other: Any?): Boolean {
        val isEqual = (quantity == (other as Product).quantity &&
                id == other.id
                )
        return false
    }

    override fun hashCode(): Int {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val hash = Objects.hash(id, quantity, isShoppingListed)
            return hash
        } else {
            return 0
//            TODO("VERSION.SDK_INT < KITKAT")
        }
    }
}

enum class ProductType(val code: String) {
    PRODUCT("PRODUCT"), VARIANT("VARIANT")
}

fun Product.toCartItem(): ScangoCartItem {
    val metadata = data?.optJSONObject("metaData")
    return ScangoCartItem(
        id ?: 0,
        itemId ?: 1,
        quantity,
        name ?: "",
        fullName ?: "",
        images,
        url,
        mrp ?: 0.0,
        discount ?: 0.0,
        sellingPrice ?: 0.0,
        brand,
        getImage(),
//        categoryId = primaryCategory?.id,
        categoryId = metadata?.optInt("SAP SubClass")?:-1,
        soldByWeight = soldByWeight,
        weight = weight,
        displayUnit = metadata?.optString("DisplayUnit")?:"",
        _unitOfMeasurement = metadata?.optString("Unit Of Measurement")?:""
    )
}



fun Product.getUOM() = data?.optJSONObject("metaData")?.optString("Unit Of Measurement")?:""


//TODO 28/03 call product details
/*fun Product.toItem(): Item =
    Item(
        id!!,
        itemId!!,
        name!!,
        fullName!!,
        hasVariants,
        thor.zopsmart.com.thor.model.ItemType.PRODUCT,
        images,
        url,
        mrp!!,
        discount!!,
        sellingPrice!!,
        productId!!,
        description,
        brand,
        categories!!,
        data,
        parentData,
        stock,
        quantity,
        selectedVariantId,
        null,
        isShoppingListed,
        countryName
    )*/

fun Product.hasStockLeft(): Boolean {
    return stock > quantity
}

fun JSONObject.toProduct(): Product {
    val name = this string "name" ?: throw InvalidItemException("Item must have a name")
    val fullName = this string "fullName" ?: name
    val description = this string "description"

    val catArray = this jsonArray "categories" ?: JSONArray()
    val categoryList = mutableListOf<Category>()
    catArray.forEach {
        categoryList.add(it.toCategory())
    }

    var countryName: String? = "Singapore"
    try {
        val jsonCountry = this json "metaData"
        countryName = jsonCountry string "Country of Origin"
    } catch (e: Exception) {

    }

    val id = this long "id" ?: throw InvalidItemException("Item must have an id")

    val productId = this int "id" ?: 0

    val hasVariants = this int "hasVariants" == 1

    /**
     * Item json won't be present in an Item with variants. It will be found in the variants instead.
     */
    var itemType = ProductType.PRODUCT

    var itemId = 0L

    if (!hasVariants) {
        val itemJSON = this json "item"

        itemId = itemJSON long "id" ?: throw InvalidItemException("Item must have an itemId")

//        val itemTypeString = itemJSON string "entityType" ?: "PRODUCT"
//
//        if (itemTypeString == "VARIANT")
//            itemType = ProductType.VARIANT
    }

    val storeSpecificDataArray = this jsonArray "storeSpecificData" ?: JSONArray() //Store specific data array

    /**
     * An item can have variants eg: Pilsbury Flour 5 kg, Pilsbury Flour 10 kg are variants of Pilsbury Flour
     * If the item does have variants, then the store specific data will be in each of the variants.
     * For the above case set options mrp, discount and stock as 0
     * Otherwise the store specific data must be there in the main item itself.
     */
    var mrp = 0.0

    var discount = 0.0

    var stock = 100

    if (storeSpecificDataArray.length() == 0) {
        // TODO : Handle this
        /*if (!hasVariants)
            throw InvalidItemException("Item must contain store specific data with MRP, Selling Price and Discount")*/
    } else {
        val storeSpecificData =
            storeSpecificDataArray.getJSONObject(0) //TODO change to selected store (Store specific data)

        mrp = storeSpecificData double "mrp" ?: 0.0

        discount = storeSpecificData double "discount" ?: 0.0

        stock = 100 // storeSpecificData int "stock" ?: 0
    }

    //TODO (val sellingPrice = ssData.extract(SELLING_PRICE, 0.0))
    val sellingPrice = mrp - discount

    val url = this string "slug"

    val imageList = mutableListOf<String>()
    val jsonImages = this jsonArray "images" ?: JSONArray()
    jsonImages.forEachString {
        imageList.add(it)
    }

    var brand: String? = null
    try {
        val brandObj = this json "brand"
        brand = brandObj string "name"
    } catch (e: Exception) {

    }

    val category = this jsonNullable  "primaryCategory"
    val primaryCategory = category?.toCategory()?:null


    //TODO API for category id not ready. We fix categoryId to 23 to test time restriction. Remove when api ready for category id
//    val primaryCategoryFake = Category(23,"","","",0, UrlType.CATEGORY, primaryCategory)

    val soldByWeight = this int "soldByWeight" ?: 0
    //If variant is not present, then add the item itself as a variant
    val item = Product(
        id,
        itemId,
        name,
        fullName,
        false,
        itemType,
        imageList,
        url,
        mrp,
        0.0,
        sellingPrice,
        productId,
        description,
        brand,
        categoryList,
        this,
        this,
        stock,
        countryName = countryName,
        primaryCategory = primaryCategory,
        soldByWeight = soldByWeight
    )

//    val variants = mutableListOf<Product>()
//    if (!hasVariants) {
//        variants.add(item)
//    } else {
//        variants.addAll(
//            getVariantItems(
//                this jsonArray "variants" ?: JSONArray(),
//                brand,
//                this,
//                categoryList
//            )
//        )
//    }
//
//    //TODO change later to read selected variant from response if it exists
//    val selectedVariantId = variants[0].id
//
//    //Set variant data to item
//    item.selectedVariantId = selectedVariantId
//    item.variants = variants
//
//    //Set parent product
//    item.variants?.let {
//        val itemVariants = mutableListOf<Product>()
//        it.forEach {
//            it.parentProduct = item
//            itemVariants.add(it)
//        }
//        item.variants = itemVariants
//    }

    return item
}

private fun getVariantItems(
    jArray: JSONArray,
    brand: String?,
    parentData: JSONObject,
    categories: MutableList<Category>
): MutableList<Product> {
    val variants = mutableListOf<Product>()

    jArray.forEach {
        try {
            variants.add(it.toProduct().apply {
                this.brand = brand
                this.categories = categories
                this.parentData = parentData
            })
        } catch (e: Exception) {
        }
    }
    return variants
}