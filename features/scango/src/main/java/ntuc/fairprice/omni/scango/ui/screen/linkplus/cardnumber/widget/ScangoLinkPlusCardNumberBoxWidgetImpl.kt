package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.widget

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_activity_link_plus_card_number.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.removeSpaces
import ntuc.fairprice.omni.scango.utils.toSHA512
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

class ScangoLinkPlusCardNumberBoxWidgetImpl @Inject constructor(
    val accountRepository: AccountRepository
) : ScangoLinkPlusCardNumberBoxWidget {

    companion object {
        private const val CARD_NUMBER_LENGTH = 16
        private val NON_HASHING_CARD_SUFFIX = arrayOf("8108", "8103", "810500", "810501")
    }

    private val isAlreadyInDefaultState
        get() = txt_link_plus_card_link_account.text == containerView.context.getString(R.string.btn_link_plus)

    override lateinit var containerView: View

    override val isValidCardNumber
        get() = edt_link_plus_card_number.text.toString().removeSpaces().length == CARD_NUMBER_LENGTH

    override val cardNumber: Pair<String, Boolean>
        get() {
            val inputCardNumber = edt_link_plus_card_number.text.toString().removeSpaces()
            if (NON_HASHING_CARD_SUFFIX.none { inputCardNumber.startsWith(it) }) {
                return Pair(inputCardNumber.toSHA512(), true)
            }
            return Pair(inputCardNumber, false)
        }

    override fun initWidget(view: View) {
        containerView = view
        initListener()
    }

    private fun initListener() {
        edt_link_plus_card_number.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                cl_link_plus_card_number_container.setBackgroundResource(R.drawable.sg_feedback_rectangle_blue)
            } else {
                cl_link_plus_card_number_container.setBackgroundResource(R.drawable.sg_feedback_rectangle_grey)
            }
        }
        edt_link_plus_card_number.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(!isAlreadyInDefaultState) {
                    displayDefaultFocussedState()
                }
                txt_link_plus_card_link_account.isEnabled = isValidCardNumber
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    override fun displayInvalidState() {
        img_invalid_number_alert.visibility = View.VISIBLE
        cl_link_plus_card_number_container.setBackgroundResource(R.drawable.sg_feedback_rectangle_red)
        txt_link_plus_mobile_wrong.visibility = View.VISIBLE
        cl_link_plus_card_help_container.visibility = View.VISIBLE
        txt_link_plus_card_link_account.text = containerView.context.getString(R.string.btn_link_plus_try)
        txt_link_plus_card_number_title.setTextColor(ContextCompat.getColor(containerView.context, R.color.pinkish_red))
        btn_link_plus_try_with_number.visibility = View.VISIBLE
        cl_link_plus_bottom_container.visibility = View.GONE
    }

    override fun displayDefaultState() {
        img_invalid_number_alert.visibility = View.GONE
        cl_link_plus_card_number_container.setBackgroundResource(R.drawable.sg_feedback_rectangle_grey)
        txt_link_plus_mobile_wrong.visibility = View.GONE
        cl_link_plus_card_help_container.visibility = View.GONE
        txt_link_plus_card_link_account.text = containerView.context.getString(R.string.btn_link_plus)
        txt_link_plus_card_number_title.setTextColor(ContextCompat.getColor(containerView.context, R.color.blue_blue))
        btn_link_plus_try_with_number.visibility = View.GONE
        cl_link_plus_bottom_container.visibility = View.VISIBLE
        txt_link_plus_card_link_account.isEnabled = isValidCardNumber
    }

    override fun displayDefaultFocussedState() {
        img_invalid_number_alert.visibility = View.GONE
        cl_link_plus_card_number_container.setBackgroundResource(R.drawable.sg_feedback_rectangle_blue)
        txt_link_plus_mobile_wrong.visibility = View.GONE
        cl_link_plus_card_help_container.visibility = View.GONE
        txt_link_plus_card_link_account.text = containerView.context.getString(R.string.btn_link_plus)
        txt_link_plus_card_number_title.setTextColor(ContextCompat.getColor(containerView.context, R.color.blue_blue))
        btn_link_plus_try_with_number.visibility = View.GONE
        cl_link_plus_bottom_container.visibility = View.VISIBLE
        txt_link_plus_card_link_account.isEnabled = isValidCardNumber
    }
}