package ntuc.fairprice.omni.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import ntuc.fairprice.omni.R

class ScanGoProgressDialog(context: Context) {

    private var dialog: Dialog

    init {
        val builder = AlertDialog.Builder(context)
        builder.setView(R.layout.scango_component_loading_spinner)
        dialog = builder.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }


}
