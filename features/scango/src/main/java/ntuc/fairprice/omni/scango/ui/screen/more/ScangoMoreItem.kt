package ntuc.fairprice.omni.scango.ui.screen.more

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_view_more_item.view.*
import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.R as ZR
import thor.zopsmart.com.thor.base.extensions.setFont


class ScangoMoreItem :
    ConstraintLayout {

    private var icon: Int = 0
    private var item: String? = null

    constructor(context: Context) : super(context) {
        initialise(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialise(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialise(attrs)
    }

    private fun initialise(attrs: AttributeSet?) {

        View.inflate(context, R.layout.scango_view_more_item, this)

        if (attrs == null) return

        val typedArray = context!!.obtainStyledAttributes(attrs, R.styleable.ScangoMoreItem)
        icon = typedArray.getResourceId(R.styleable.ScangoMoreItem_item_icon, 0)
        item = typedArray.getString(R.styleable.ScangoMoreItem_item_name)
        typedArray.recycle()

        tv_nav_item.text = item

        setIcon(icon)
    }

    fun updateIcon(@DrawableRes res: Int) {
        setIcon(res)
        postInvalidate()
    }

    fun boldText(bold: Boolean) {
        if(bold) {
            tv_nav_item.setFont(ZR.font.lato_bold)
        } else {
            tv_nav_item.setFont(ZR.font.lato)
        }
    }

    fun setText(text:String){
        tv_nav_item.text = text
    }

    fun setIcon(@DrawableRes res: Int){
        if(res != 0){
            val drawable = ContextCompat.getDrawable(context,res)

            val size = resources.displayMetrics.density * 24
            drawable?.let {
                it.setBounds(0, 0, size.toInt(), size.toInt())
                tv_nav_item.setCompoundDrawables(drawable, null, null, null)
            }
        }
    }
}