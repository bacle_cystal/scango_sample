package ntuc.fairprice.omni.scango.base.keyboard

import android.view.View
import android.view.inputmethod.InputMethodManager
import javax.inject.Inject


class KeyboardManagerImpl @Inject constructor(
    private val inputMethodManager: InputMethodManager
) : KeyboardManager {

    override fun hide(view: View) {
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

}