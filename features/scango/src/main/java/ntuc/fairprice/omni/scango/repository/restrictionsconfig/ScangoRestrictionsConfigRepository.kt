package ntuc.fairprice.omni.scango.repository.restrictionsconfig

import ntuc.fairprice.omni.scango.repository.db.room.entity.RestrictionsConfig
import ntuc.fairprice.omni.scango.repository.webservice.model.Result

interface ScangoRestrictionsConfigRepository {

    fun getConfig(): RestrictionsConfig?

    fun saveConfig(config: String)

    suspend fun syncConfig(): Result<String>
}