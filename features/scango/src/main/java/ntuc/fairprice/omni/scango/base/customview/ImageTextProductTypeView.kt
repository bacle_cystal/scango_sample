package ntuc.fairprice.omni.scango.base.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.scango_item_product_type.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ui.screen.qrcode.product.ProductType
import thor.zopsmart.com.thor.base.extensions.image

/**
 * Contains an image and a text in the left to right order.
 */
class ImageTextProductTypeView @JvmOverloads constructor(
    mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(mContext, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.scango_item_product_type, this)
        setPadding(0, 0, 8, 0)
    }

    /**
     * Call this function to change the values.
     */
    fun update(productType: ProductType) {
        product_item_type_name.text = productType.name
        if (productType.id == null) {
            product_item_type_image.visibility = View.INVISIBLE
        } else {
            product_item_type_image.image(productType.id)
            product_item_type_image.visibility = View.VISIBLE
        }
        postInvalidate()
    }
}