package ntuc.fairprice.omni.scango.ui.screen.feedback

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.android.synthetic.main.scango_feedback_comment_dialog_fragment.*
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.FeedbackOption
import ntuc.fairprice.omni.scango.repository.db.room.repo.FeedbackRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ApiResponse
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import org.json.JSONArray
import org.json.JSONObject
import javax.inject.Inject

class FeedbackCommentDialogViewModel @Inject constructor(
    application: Application,
    private val feedbackRepository: FeedbackRepository
) : ScangoBaseViewModel(application) {

    var feedbackOptions = MutableLiveData<List<FeedbackOption>>()

    var apiResponse = MutableLiveData<ApiResponse>()

    fun _getFeedbackOptions() = launch {
        getFeedbackOptions().onSuccess {
            Log.d(FeedbackCommentDialogFragment.TAG, "Model _getFeedbackOptions onError $it")
            feedbackOptions.value = it
        }.onError {
            feedbackOptions.value = null
            onLoadFail(it)
        }
    }

    fun _postFeedbackOptions(session_id: String, rating: Int, option_ids: JSONArray = JSONArray(), custom: String) = launch {
        var json = JSONObject()
        json.put("session_id", session_id)
        json.put("rating", rating)
        json.put("option_ids", option_ids)
        json.put("custom", custom)
        postFeedbackOptions(json).onSuccess {
            apiResponse.value = it
        }.onError {
            onLoadFail(it)
            apiResponse.value = null
        }
    }

    // Network call operations getFeedbackOptions
    suspend fun getFeedbackOptions() = feedbackRepository.getFeedbackOptions()

    suspend fun postFeedbackOptions(requestBody: JSONObject) = feedbackRepository.postFeedbackOptions(requestBody)

}
