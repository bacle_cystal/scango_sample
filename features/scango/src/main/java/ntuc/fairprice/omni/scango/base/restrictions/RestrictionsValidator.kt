package ntuc.fairprice.omni.scango.base.restrictions

import ntuc.fairprice.omni.scango.repository.db.room.entity.Rule
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import java.util.*

interface RestrictionsValidator {

    fun isTimeViolated(cartItem: ScangoCartItem): Boolean

    fun getTimeWarningText(cartItem: ScangoCartItem?): String

    fun isAgeViolated(cartItem: ScangoCartItem): Boolean

    fun getTimeWarningInMilis(): Long

    fun getAssociatedRules(cartItem: ScangoCartItem): List<Rule>
}