package ntuc.fairprice.omni.scango.base.customview.tagview

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.base.extensions.setFont
import android.graphics.drawable.Drawable
import ntuc.fairprice.omni.scango.utils.setTextAppearanceV2


class TagView<T> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    private val tags = arrayListOf<TextView>()
    private var lineHeight = 0
    private val horizontalSpacing: Int
    private val verticalSpacing: Int
    private val textAppearanceId: Int
    private val textColorId: Int
    private val textFontSize: Int
    private val backgroundColorId: Int
    private val cornerRadius: Float
    private var leftDrawableId: Int
    private val leftDrawablePadding: Int
    private val sortType: Int
    private val strokeWidth: Int
    private val strokeColor: Int

    init {
        val styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.TagView)
        horizontalSpacing = styledAttrs.getDimension(R.styleable.TagView_horizontal_spacing, 1f).toInt()
        verticalSpacing = styledAttrs.getDimension(R.styleable.TagView_vertical_spacing, 1f).toInt()
        textAppearanceId = styledAttrs.getResourceId(R.styleable.TagView_text_style, -1)
        textColorId =
            styledAttrs.getColor(R.styleable.TagView_text_color, ContextCompat.getColor(context, R.color.white))
        textFontSize = styledAttrs.getDimension(R.styleable.TagView_text_font_size, 1f).toInt()
        backgroundColorId =
            styledAttrs.getColor(R.styleable.TagView_background_color, ContextCompat.getColor(context, R.color.white))
        cornerRadius = styledAttrs.getDimension(R.styleable.TagView_corner_radius, 60f)
        leftDrawableId = styledAttrs.getResourceId(R.styleable.TagView_left_drawable, -1)
        leftDrawablePadding = styledAttrs.getDimension(R.styleable.TagView_left_drawable_padding, 1f).toInt()
        sortType = styledAttrs.getInt(R.styleable.TagView_sort_type, -1)
        strokeWidth = styledAttrs.getDimension(R.styleable.TagView_stroke_width, 1f).toInt()
        strokeColor =
            styledAttrs.getColor(R.styleable.TagView_stroke_color, ContextCompat.getColor(context, R.color.white))

        styledAttrs.recycle()
    }

    override fun generateDefaultLayoutParams(): LayoutParams {
        return TagLayoutParams(horizontalSpacing, verticalSpacing)
    }

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): LayoutParams {
        return TagLayoutParams(horizontalSpacing, verticalSpacing)
    }

    override fun checkLayoutParams(p: ViewGroup.LayoutParams) = p is TagLayoutParams

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val count = childCount
        val width = r - l
        var xPos = paddingLeft
        var yPos = paddingTop

        tags.forEach { child ->
            if (child.visibility != View.GONE) {
                val childWidth = child.measuredWidth
                val childHeight = child.measuredHeight

                val lp = child.layoutParams as TagLayoutParams
                if (xPos + childWidth > width) {
                    xPos = paddingLeft
                    yPos += lineHeight
                }

                child.layout(xPos, yPos, xPos + childWidth, yPos + childHeight)
                xPos += childWidth + lp.horizontalSpacing
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        var height = MeasureSpec.getSize(heightMeasureSpec) - paddingTop - paddingBottom
        var lineHeight = 0

        var xPos = paddingLeft
        var yPos = paddingTop

        val childHeightMeasureSpec: Int
        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST)
        } else {
            childHeightMeasureSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        }

        tags.forEach { child ->
            if (child.visibility != View.GONE) {
                val lp = child.layoutParams as TagLayoutParams
                child.measure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST), childHeightMeasureSpec)
                val childWidth = child.measuredWidth
                lineHeight = Math.max(lineHeight, child.measuredHeight + lp.verticalSpacing)

                if (xPos + childWidth > width) {
                    xPos = paddingLeft
                    yPos += lineHeight
                }
                xPos += childWidth + lp.horizontalSpacing
            }
        }

        this.lineHeight = lineHeight
        if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.UNSPECIFIED) {
            height = yPos + lineHeight

        } else if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST) {
            if (yPos + lineHeight < height) {
                height = yPos + lineHeight
            }
        }
        setMeasuredDimension(width, height)
    }

    fun setTags(items: List<T>, transform: DataTransform<T>) {
        val newItems = when (sortType) {
            Annotations.SORT_TYPE_ASC -> items.sortedBy { transform.transfer(it).length }
            Annotations.SORT_TYPE_DESC -> items.sortedByDescending { transform.transfer(it).length }
            else -> items
        }

        newItems.forEach { setTag(it, transform) }
        requestLayout()
    }

    fun setTag(item: T, transform: DataTransform<T>) {
        val chips = (ContextCompat.getDrawable(context, R.drawable.sg_bg_dierary) as GradientDrawable).also {
            it.cornerRadius = cornerRadius
            it.setColor(backgroundColorId)
            it.setStroke(strokeWidth, strokeColor)
        }

        // val typeface = ResourcesCompat.getFont(context, R.font.lato_bold)

        leftDrawableId = transform.transferIcon(item)

        val tag = TextView(context).apply {
            setTextColor(textColorId)
            background = chips
            setFont(thor.zopsmart.com.thor.R.font.lato_bold)
            text = transform.transfer(item)
            textSize = textFontSize.toFloat()
            gravity = Gravity.CENTER_VERTICAL
            setOnTouchListener { _, _ -> false }
            if (textAppearanceId != -1) {
                setTextAppearanceV2(context, textAppearanceId)
            }

            val drawable = ContextCompat.getDrawable(context, leftDrawableId)

            val pixelDrawableSize = lineHeight // Or the percentage you like (0.8, 0.9, etc.)
            drawable?.setBounds(0, 0, pixelDrawableSize, pixelDrawableSize)

            if (leftDrawableId != -1) {
                setCompoundDrawables(drawable, null, null, null)
            }

            if (leftDrawablePadding != 1) {
                compoundDrawablePadding = leftDrawablePadding
            }
        }
        tags.add(tag)
        addView(tag)
    }

    fun clear() {
        tags.clear()
        removeAllViews()
    }
}