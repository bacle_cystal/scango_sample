package ntuc.fairprice.omni.scango.ui.screen.productdetail.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.EpoxyVisibilityTracker
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_view_offer_detail.view.*
import ntuc.fairprice.omni.scango.epoxy.SGComponent
import ntuc.fairprice.omni.scango.epoxy.SGProductDelegate
import ntuc.fairprice.omni.scango.epoxy.SGProductGrid
import ntuc.fairprice.omni.scango.epoxy.SGProductScroller
import ntuc.fairprice.omni.scango.utils.loadImageOptimize
import org.json.JSONArray
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.epoxy.FPDelegate
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.Offer
import thor.zopsmart.com.thor.model.OfferDetail
import thor.zopsmart.com.thor.model.getitemList
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.webservice.model.ApiResponse
import ntuc.fairprice.omni.scango.R as RSG

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class OfferDetailView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), SGProductDelegate {

    override fun onQuantitySet(quantity: Int, cartItem: CartItem) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadMoreProduct(fpProductGrid: SGProductGrid) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var addToCart: (CartItem) -> Unit = {}

    var removeFromCart: ((CartItem) -> Unit) = {}

    var productClick: ((Item) -> Unit) = {}

    var addToShoppingListAction: ((Item) -> Unit) = {}

    var components = mutableListOf<SGComponent<*>>()

    init {
        View.inflate(context, RSG.layout.scango_view_offer_detail, this)
    }

    @ModelProp
    fun offer(offer: Offer) {
        tv_offer_name.text = offer.desc
        iv_offer_image.setVisibility(offer.imageUrl?.let {
            iv_offer_image.loadImageOptimize(it)
            it.isNotEmpty()
        } ?: false)

        val gridLayoutManager = GridLayoutManager(context, 30)
        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(epoxy_offer_products)
        epoxy_offer_products.layoutManager = gridLayoutManager
        epoxy_offer_products.buildModelsWith { controller ->
            controller.spanCount = 30
            components.forEach {
                (it as? SGComponent<FPDelegate>)?.render(this@OfferDetailView)?.forEach { model ->
                    model.addTo(controller)
                }
            }
        }

    }

//    @ModelProp
//    fun offerDetail(@Nullable offerDetailResponse: ApiResponse?) {
//        offerDetailResponse?.let {
//            val data = it safeJson "data"
//            val offer = data safeJson "offers"
//            val offerDetailJson = offer safeJson "ruleDetail"
//            val buy = offerDetailJson safeJson "buy"
//            val get = offerDetailJson safeJson "get"
//
//            val buyProducts = (buy jsonArray "products" ?: JSONArray()).getitemList()
//            val getProducts = (get jsonArray "products" ?: JSONArray()).getitemList()
//
//
//            val isFree = get boolean "isFree" ?: false
//            tv_view_free_gift.setVisibility(isFree)
//            cl_no_gift.setVisibility(!isFree)
//
//            val discountType = get string "discountType" ?: offerDetailJson string "discountType" ?: ""
//            val discountVal = get double "discountValue" ?: offerDetailJson double "discountVal" ?: 0.0
//
//            tv_discount_detail.text = "Total discount: $discountVal"
//
//            components.clear()
//            components.add(
//                SGProductScroller(
//                    items = buyProducts,
//                    hasLoadedAll = true
//                )
//            )
//            epoxy_offer_products.requestModelBuild()
//        }
//
//    }

    @SuppressLint("SetTextI18n")
    @ModelProp
    fun offerDetail(@Nullable offerDetail: OfferDetail?) {
        offerDetail?.let {
            tv_view_free_gift.setVisibility(it.isFree)
            cl_no_gift.setVisibility(!it.isFree)
            tv_discount_detail.text = "Total discount: ${it.discountValue}"
            components.clear()
            components.add(
                SGProductScroller(
                    items = it.buyItems,
                    hasLoadedAll = true
                )
            )
            epoxy_offer_products.requestModelBuild()
        }
    }

    @CallbackProp
    fun addToCart(action: ((CartItem) -> Unit)?) {
        action?.let {
            addToCart = it
        }
    }

    @CallbackProp
    fun removeFromCart(action: ((CartItem) -> Unit)?) {
        action?.let {
            removeFromCart = it
        }
    }

    @CallbackProp
    fun productClick(action: ((Item) -> Unit)?) {
        action?.let {
            productClick = it
        }
    }

    @CallbackProp
    fun addToShoppingList(action: ((Item) -> Unit)?) {
        action?.let {
            addToShoppingListAction = it
        }
    }

    override fun onRemoveFromCart(cartItem: CartItem) {
        removeFromCart(cartItem)
    }

    override fun onProductClick(item: Item) {
        productClick(item)
    }

    override fun onAddToCart(cartItem: CartItem) {
        addToCart(cartItem)
    }

    override fun addToShoppingList(item: Item) {
        addToShoppingListAction(item)
    }
}