package ntuc.fairprice.omni.scango.ui.screen.promo

import android.app.Activity
import dagger.Binds
import dagger.Module
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoPromoBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoPromoActivity): Activity
}
