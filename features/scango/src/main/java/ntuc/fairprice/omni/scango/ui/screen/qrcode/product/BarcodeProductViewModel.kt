package ntuc.fairprice.omni.scango.ui.screen.qrcode.product

import android.app.Application
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import javax.inject.Inject

class BarcodeProductViewModel @Inject constructor(application: Application) : ScangoBaseViewModel(application) {
    var productItem = MutableLiveData<Product>()
    var numberItemInCard = MutableLiveData<Int>()
    var isMaxItem = MutableLiveData<Boolean>(false)
    var isShowSubAdd = MutableLiveData<Boolean>(false)
    var isLiquor = MutableLiveData<Boolean>(false)
    var isShowScanNext = MutableLiveData<Boolean>(false)

    val flagName = MediatorLiveData<String>().apply {
        addSource(productItem) {
            value = listCodeFlags[listCodeFlagsFull.indexOf(productItem.value?.countryName)].toUpperCase()
        }
    }

    val isHasItemInCart = MediatorLiveData<Boolean>().apply {
        addSource(numberItemInCard) {
            value = when (numberItemInCard.value?.compareTo(0)) {
                0 -> false
                1 -> true
                else -> false
            }
        }
    }

    val flagDrawable = MediatorLiveData<String>().apply {
        addSource(productItem) {
            value = "sg_ic_flag_" + listCodeFlags[listCodeFlagsFull.indexOf(productItem.value?.countryName)]
        }
    }

    val currentNumberItem = MediatorLiveData<Int>().apply {
        addSource(productItem) {
            value = productItem.value?.quantity
        }
    }

    val imageProduct = MediatorLiveData<String>().apply {
        addSource(productItem) { product ->
            value = product.getImage()
        }
    }

    val listCodeFlags = listOf(
        "ar",
        "au",
        "ca",
        "cn",
        "de",
        "es",
        "fr",
        "id",
        "in",
        "it",
        "jp",
        "kr",
        "mx",
        "my",
        "nz",
        "se",
        "sg",
        "th",
        "tw",
        "uk",
        "us",
        "vn"
    )

    val listCodeFlagsFull = listOf(
        "Argentina",
        "Australia",
        "Canada",
        "China",
        "Germany",
        "Spain",
        "France",
        "Indonesia",
        "India",
        "Italy",
        "Japan",
        "Korea",
        "Mexico",
        "Malaysia",
        "New Zealand",
        "Sweden",
        "Singapore",
        "Thailand",
        "Taiwan",
        "United Kingdom",
        "United States",
        "Viet Nam"
    )

    fun showSubAddUI() {
        isShowSubAdd.value = true
        isMaxItem.value = false
        isShowScanNext.value = false
    }

    fun showAddButtonBlue() {
        isShowSubAdd.value = false
        isMaxItem.value = false
        isShowScanNext.value = false
    }

    fun showMaxItemUI() {
        isShowSubAdd.value = false
        isMaxItem.value = true
        isShowScanNext.value = false
    }

    fun showLiquorItemUI() {
        isShowSubAdd.value = false
        isLiquor.value = true
        isShowScanNext.value = true
    }

    fun checkNumberItemInShoppingCart(numberItem: Int) {
        numberItemInCard.value = numberItem
    }
}