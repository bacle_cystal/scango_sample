package ntuc.fairprice.omni.scango

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import ntuc.fairprice.omni.Constants
import ntuc.fairprice.omni.Constants.SCAN_AND_GO_KEY
import ntuc.fairprice.omni.scango.utils.observe
import sg.ntucenterprise.nlauncher.NLauncher
import thor.zopsmart.com.thor.features.onboarding.ui.SplashScreenActivity
import javax.inject.Inject

class SplashScreenActivity : SplashScreenActivity() {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    val viewModel: ScangoFeatureViewModel by lazy {
        viewModelFactory.create(ScangoFeatureViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initToggleCheck()
    }

    private fun initToggleCheck() {
        observe(NLauncher.getInstance().boolLiveDataWith(SCAN_AND_GO_KEY, true)) {
            when (it) {
                true -> {
                    viewModel.apply {
                        observe(checkCheckInStatusResult) { checkInResult ->
                            when (checkInResult) {
                                CheckCheckInStatusResult.CheckCheckInStatusSuccess -> goToStoreScanHome()
                                CheckCheckInStatusResult.CheckCheckInStatusFail -> {
                                    clearDataStoreWhenInvalidCheckin()
                                    goToFPOnHome()
                                }
                            }
                        }
                    }
                }
                false -> goToFPOnHome()
            }
        }
    }

    //FelixV: override from SplashScreen of FPON screen
    override fun readyToStart() {
        navigateToCorrectHomePage()

    }

    private fun navigateToCorrectHomePage() {
        if (viewModel.isCheckedIn()) {
            viewModel.checkCheckInVerify()
        } else {
            changePage(ScangoFeatureActivity.intentMoveToOrderList(context = baseContext))
        }
    }

    private fun goToFPOnHome() {
        gotoHomePage()
        finish()
    }

    private fun goToStoreScanHome() {
        startActivity(Intent().apply {
            setClassName(this@SplashScreenActivity, Constants.scanGoClassname)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        })
        finish()
    }

}
