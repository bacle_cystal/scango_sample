package ntuc.fairprice.omni.scango.utils

object HttpStatusCode {

    const val HTTP_OK = 200
    const val HTTP_CREATED = 201
    const val HTTP_ACCEPTED = 202

    const val HTTP_BAD_REQUEST = 400
    const val HTTP_UNAUTHORIZED = 401
    const val HTTP_FORBIDDEN = 403
    const val HTTP_NOT_FOUND = 404
    const val HTTP_SESSION_EXPIRED = 410

    const val HTTP_INTERNAL_SERVERERROR = 500

    const val HTTP_UNKNOWN = -1
}