package ntuc.fairprice.omni

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.preference.PreferenceManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.common.util.Strings
import com.google.firebase.FirebaseApp
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.instabug.library.Instabug
import com.instabug.library.invocation.InstabugInvocationEvent
import ntuc.fairprice.omni.Constants.CUSTOMER_ID
import ntuc.fairprice.omni.Constants.KEY_LP_IS_DISMISSED
import ntuc.fairprice.omni.Constants.LOGGED_IN_ACTION
import ntuc.fairprice.omni.Constants.SIGN_OUT_ACTION
import ntuc.fairprice.omni.Constants.USER_NAME
import ntuc.fairprice.omni.util.ForegroundManager
import sg.ntucenterprise.ncore.NCore
import sg.ntucenterprise.nlauncher.NLauncher
import thor.zopsmart.com.thor.BaseApplication
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.set

open class OmniApplication : BaseApplication(){

    val foregroundManager = ForegroundManager()

    private val localBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                SIGN_OUT_ACTION -> {
                    val customerID = intent.extras?.getLong(CUSTOMER_ID) ?: 0
                    try {
                        startService(
                            Intent(Constants.scanGoCheckOutService).apply {
                                setPackage(packageName)
                                putExtra(CUSTOMER_ID, customerID)
                            })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    NCore.getInstance().removeUserId()
                }

                LOGGED_IN_ACTION -> {
                    setUserIdentifier()
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        initApplication()
    }

    private fun initApplication() {
        NCore.configure(this)
        FirebaseApp.initializeApp(this)
        FirebaseRemoteConfig.getInstance().fetch()

        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastReceiver,
            IntentFilter(SIGN_OUT_ACTION))

        LocalBroadcastManager.getInstance(this).registerReceiver(localBroadcastReceiver,
            IntentFilter(LOGGED_IN_ACTION))

        ProcessLifecycleOwner.get().lifecycle.addObserver(foregroundManager)

        NLauncher.getInstance().boolLiveDataWith(Constants.INSTABUG_KEY, false).observe(ProcessLifecycleOwner.get(), Observer {
            if (it && !BuildConfig.DEBUG && !Instabug.isBuilt())
                Instabug.Builder(this, Constants.INSTABUG_TOKEN)
                    .setInvocationEvents(InstabugInvocationEvent.SCREENSHOT).build()
        })

        LocalStorage.init(this).set(KEY_LP_IS_DISMISSED, false)
    }

    private fun setUserIdentifier() {
        val userIdentifier = PreferenceManager.getDefaultSharedPreferences(baseContext)
            .getString(USER_NAME, "")
        if (!Strings.isEmptyOrWhitespace(userIdentifier) &&
            userIdentifier != NCore.getInstance().metadata.userId
        )
            NCore.getInstance().setUserId(userIdentifier)
    }
}