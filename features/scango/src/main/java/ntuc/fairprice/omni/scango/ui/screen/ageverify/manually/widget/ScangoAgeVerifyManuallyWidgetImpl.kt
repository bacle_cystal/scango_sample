package ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.widget

import android.view.View
import kotlinx.android.synthetic.main.scango_activity_age_verify_manually.*
import javax.inject.Inject
import ntuc.fairprice.omni.R as RB

class ScangoAgeVerifyManuallyWidgetImpl @Inject constructor() : ScangoAgeVerifyManuallyWidget {

    override lateinit var containerView: View

    override var onOtpInputComplete: (() -> Unit)? = null

    override fun initWidget(view: View) {
        containerView = view
    }

    override fun displayDefaultState() {
        reset(false)
        txt_age_pin_wrong.visibility = View.GONE
        txt_age_pin_wrong_image.visibility = View.GONE
    }

    override fun displayErrorState() {
        reset(true)
        txt_age_pin_wrong.visibility = View.VISIBLE
        txt_age_pin_wrong_image.visibility = View.VISIBLE
    }


    private fun reset(isSelected: Boolean) {
        et_staff_pin.clearFocus()
        et_staff_pin.isSelected = isSelected
        et_staff_id.clearFocus()
        et_staff_id.isSelected = isSelected
    }

    override fun getPincode(): String {
        return et_staff_pin.getString()
    }

    override fun getStaffId(): String {
        return et_staff_id.getString()
    }
}