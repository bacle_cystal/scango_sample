package ntuc.fairprice.omni.scango.ui.screen.promo

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.*
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCouponCodeList
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoOrderRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.db.sharedpref.setSGCartCoupons
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import thor.zopsmart.com.thor.viewmodel.dependantLiveData
import javax.inject.Inject

class ScangoPromoViewModel @Inject constructor(
    val mApplication: Application,
    private val scangoOrderRepository: ScangoOrderRepository,
    private val cartRepository: ScangoCartRepository,
    private val storeRepository: ScangoStoreRepository
) : BaseViewModel(mApplication) {

    private val pageLDInternal = MutableLiveData<List<ScangoComponent<*>>>()

    var selectedCoupons = mutableListOf<ScangoCoupon>()
    val couponStatus = MutableLiveData<ScangoCouponStatus>()

    val pageLD = dependantLiveData(pageLDInternal) {
        pageLDInternal.value?.let {
            it
        }
    }

    var cartAmount: Double = 0.0

    fun loadPromotions() = launch {
        couponStatus.value = ScangoCouponLoading
        scangoOrderRepository.loadPromotions()
            .onSuccess {
                couponStatus.value = ScangoCouponLoadSuccess
                loadPromoPage(it)
            }
            .onError {
                loadPromoPage(listOf())
                couponStatus.value = ScangoCouponLoadFail(it)
            }
    }


    fun applyCoupon(couponCode: String, isApply: Boolean) = launch {
        // Only make the add coupon api call if the coupon is not added
        if (!selectedCoupons.any { it.coupon == couponCode }) {
            couponStatus.value = ScangoCouponLoading
            val couponCodeList = selectedCoupons.toCouponCodeList()
            couponCodeList.add(couponCode)
            scangoOrderRepository.applyCoupon(
                couponCodeList,
                cartAmount,
                cartRepository.getCartItems(),
                storeRepository.getStoreIdCached().toString()
            )
                .onSuccess {
                    selectedCoupons = it.toMutableList()
                    couponStatus.value = ScangoCouponSuccess
                    loadPromotions()
                }
                .onError {
                    couponStatus.value = ScangoCouponFail(it, isApply)
                    if (isApply) updateEntryErrorView()
                }
        }
    }

    private fun updateEntryErrorView() {
        val components = pageLDInternal.value?.toMutableList()
        pageLDInternal.value = components?.apply {
            filter { it is ScangoCouponEditText }.map { it as ScangoCouponEditText }[0].isError = true
            /*val index = indexOfFirst { it is FPCouponEditText }
            this[index] = FPCouponEditText(true)*/
        }
    }

    fun removeCoupon(coupon: ScangoCoupon) {
        selectedCoupons.remove(coupon)
        loadPromotions()
    }

    private fun loadPromoPage(availableCoupons: List<ScangoCoupon>) {
        val component = mutableListOf<ScangoComponent<*>>()


        if (selectedCoupons.isNotEmpty()) {
            component.add(ScangoTitle("Applied promotions"))
            component.add(ScangoCouponList(selectedCoupons, true))
        }

        component.add(ScangoCouponEditText())

        component.add(ScangoVerticalGap(9f))
        component.add(ScangoTitle("Available promotions"))

        if (availableCoupons.isNotEmpty()) {
            component.add(ScangoCouponList(availableCoupons.filter { !selectedCoupons.contains(it) }))
        }

        pageLDInternal.value = component
    }

    fun setSelectedCoupons() {
        cartRepository.localStorage.setSGCartCoupons(selectedCoupons)
    }
}

sealed class ScangoCouponStatus
object ScangoCouponLoading : ScangoCouponStatus()
object ScangoCouponLoadSuccess : ScangoCouponStatus()
class ScangoCouponLoadFail(val e: Throwable) : ScangoCouponStatus()
object ScangoCouponSuccess : ScangoCouponStatus()
class ScangoCouponFail(val e: Throwable, val isApply: Boolean) : ScangoCouponStatus()