package ntuc.fairprice.omni.scango.interfaces

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.util.DisplayMetrics
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_dialog_cancel_age_verify.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.typeconverter.ScangoConverters
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyActivity
import ntuc.fairprice.omni.scango.ui.screen.checkout.ui.ScangoBaseCheckoutLayout
import ntuc.fairprice.omni.scango.ui.screen.home.Tab
import ntuc.fairprice.omni.scango.ui.screen.promo.ScangoPromoActivity
import thor.zopsmart.com.thor.interfaces.AppView
import thor.zopsmart.com.thor.interfaces.ToastColor


/**
 * Created by Binh.TH on 4/29/2019.
 */

interface ScangoAppView : AppView {
    /**
     * Enables the entire layout which was previously disabled and reverts the alpha to 0.3
     *
     * @param layout viewgroup of type BaseCheckoutLayout
     */
    fun enableLayouts(vararg layouts: ScangoBaseCheckoutLayout) {
        for (layout in layouts) {
            layout.alpha = 1f
            layout.blockAllTouchEvents(false)
        }
    }

    /**
     * Disables the entire layout which was previously disabled and reverts the alpha to 0.3
     *
     * @param layout viewgroup of type BaseCheckoutLayout
     */
    fun disableLayouts(vararg layouts: ScangoBaseCheckoutLayout, alpha: Float = 0.3f) {
        for (layout in layouts) {
            layout.alpha = alpha
            layout.blockAllTouchEvents(true)
        }
    }

    fun gotoScangoPromoPage(parent: String, cartPrice: Double, coupons: List<ScangoCoupon>) =
        (mContext as Activity)
            .startActivityForResult(
                ScangoPromoActivity.getIntent(mContext)
                    .putExtra("selectedCoupons", ScangoConverters.toStringCouponList(coupons))
                    .putExtra("parentActivity", parent)
                    .putExtra("totalCartAmount", cartPrice),
                Constants.REQUEST_PROMO
            )

    fun gotoHomePageScango() {
        changePage(ScangoFeatureActivity.intentWithDefaultTab(mContext, Tab.HOME))
    }

    fun gotoAgePincode() {
        changePage(Intent(mContext, ScangoAgeVerifyManuallyActivity::class.java))
    }


    fun showAgeCancelDialog(
        action: () -> Unit
    ) {
        val dialog = Dialog(mContext)

        dialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.scango_dialog_cancel_age_verify)

            window?.let {
                it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                it.setDimAmount(0.5F)
            }

            btn_no_keep.setOnClickListener {
                dismiss()
            }

            tv_yes_del.setOnClickListener {
                action()
                dismiss()
            }
            findViewById<ImageView>(R.id.img_close).visibility = View.GONE
        }.show()
    }

    fun showScangoToast(toastMessage: String, action: () -> Unit = { }, color: ToastColor = ToastColor.GREEN) {

        val handler = Handler()

        if ((mContext as Activity).isFinishing) {
            return
        }
        val dialog = Dialog(mContext)
        dialog.apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)

            setContentView(thor.zopsmart.com.thor.R.layout.layout_toast)

            window?.let {
                it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                it.setDimAmount(0.1F)
                val attr = it.attributes
                //attr.height = getDeviceMetrics(context).heightPixels * 0.8.toInt()
                val displayMetrics = context.resources.displayMetrics
                val dpHeight = displayMetrics.heightPixels / displayMetrics.density
                attr.y = dpHeight.times(0.8).toInt()
                it.attributes = attr
                it.setBackgroundDrawableResource(thor.zopsmart.com.thor.R.drawable.bg_transparent)
            }

            setOnKeyListener { _, keyCode, _ ->
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    handler.removeCallbacks(action)
                    dismiss()
                    handler.removeCallbacksAndMessages(null)
                    (mContext as Activity).finish()
                }
                return@setOnKeyListener true
            }

            when (color) {
                ToastColor.WHITE -> {
                    findViewById<ConstraintLayout>(thor.zopsmart.com.thor.R.id.toast_root).setBackgroundResource(thor.zopsmart.com.thor.R.drawable.bg_white_toast)
                    findViewById<TextView>(thor.zopsmart.com.thor.R.id.tv_message).setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            thor.zopsmart.com.thor.R.color.darkText
                        )
                    )
                }
                ToastColor.RED -> {
                    findViewById<ConstraintLayout>(thor.zopsmart.com.thor.R.id.toast_root).setBackgroundResource(thor.zopsmart.com.thor.R.drawable.bg_red_toast)
                    findViewById<TextView>(thor.zopsmart.com.thor.R.id.tv_message).setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            thor.zopsmart.com.thor.R.color.white
                        )
                    )
                }
                ToastColor.GREEN -> {
                    findViewById<ConstraintLayout>(thor.zopsmart.com.thor.R.id.toast_root).setBackgroundResource(thor.zopsmart.com.thor.R.drawable.bg_toast)
                    findViewById<TextView>(thor.zopsmart.com.thor.R.id.tv_message).setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            thor.zopsmart.com.thor.R.color.white
                        )
                    )
                }
            }

            findViewById<ImageView>(thor.zopsmart.com.thor.R.id.iv_close).setOnClickListener {
                dismiss()
                action()
            }
            findViewById<TextView>(thor.zopsmart.com.thor.R.id.tv_message).text = toastMessage
            setCancelable(false)
        }.show()
        // Show the toast for
        handler.postDelayed(
            {
                dialog.dismiss()
                action()
            }, 2000
        )
    }


    fun getDeviceMetrics(context: Context): DisplayMetrics {
        val metrics = DisplayMetrics()
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        display.getMetrics(metrics)
        return metrics
    }
}