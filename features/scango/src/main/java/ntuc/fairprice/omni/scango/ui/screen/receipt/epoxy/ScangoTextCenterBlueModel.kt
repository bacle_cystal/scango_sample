package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder

@EpoxyModelClass(layout = R.layout.scango_layout_text_blue_center)
abstract class ScangoTextCenterBlueModel : EpoxyModelWithHolder<ScangoTextCenterBlueModel.ScangoTextCenterBlueModelHolder>() {

    @EpoxyAttribute
    var contentText: String? = null

    @EpoxyAttribute
    var visibility = View.VISIBLE

    @EpoxyAttribute
    var onTextClick: View.OnClickListener? = null

    override fun bind(holder: ScangoTextCenterBlueModelHolder) {
        holder.apply {
            content.text = contentText
            content.visibility = visibility
            content.setOnClickListener(onTextClick)
        }
    }

    class ScangoTextCenterBlueModelHolder : ScangoBaseEpoxyHolder() {
        val content: TextView by bind(R.id.tvFooterCenterBlue)
    }
}
