@file:Suppress("unused", "UNUSED_PARAMETER", "NestedLambdaShadowedImplicitParameter")

package ntuc.fairprice.omni.scango.epoxy

import android.app.Application
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.Carousel
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyVisibilityTracker
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.scango_fragment_dynamic_page.*
import ntuc.fairprice.omni.domain.model.BaseViewModel
import ntuc.fairprice.omni.scango.Constants
import org.json.JSONObject
import javax.inject.Inject
import ntuc.fairprice.omni.scango.R as RSG
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoMainRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoProductService
import ntuc.fairprice.omni.scango.ui.screen.productdetail.ui.ScangoProductDetailActivity
import thor.zopsmart.com.thor.analytics.FILTER_INTERACTION
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.analytics.SORT_FILTER
import thor.zopsmart.com.thor.analytics.events.AddToCart
import thor.zopsmart.com.thor.analytics.events.RemoveFromCart
import thor.zopsmart.com.thor.analytics.events.Shortlist
import thor.zopsmart.com.thor.base.BaseFragment
import thor.zopsmart.com.thor.base.PageType
import thor.zopsmart.com.thor.base.SinglePageActivity.Companion.EXTRA_PAGE_TYPE
import thor.zopsmart.com.thor.base.SinglePageActivity.Companion.EXTRA_SLUG
import thor.zopsmart.com.thor.base.SinglePageActivity.Companion.EXTRA_URL
import thor.zopsmart.com.thor.base.customview.filter.FilterGroupDataHolder
import thor.zopsmart.com.thor.base.customview.filter.SideNavFilter
import thor.zopsmart.com.thor.base.exceptions.StoreNotSelectedException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.epoxy.*
import thor.zopsmart.com.thor.features.account.ui.LoginActivity
import thor.zopsmart.com.thor.features.brand.data.Brand
import thor.zopsmart.com.thor.features.shoppinglist.ui.CreateShoppingListBottomFragment
import thor.zopsmart.com.thor.features.shoppinglist.ui.ShoppingListBottomFragment
import thor.zopsmart.com.thor.features.sort.SortSheetDialog
import thor.zopsmart.com.thor.features.sort.getSortType
import thor.zopsmart.com.thor.features.widget.categorycollection.data.UrlType
import thor.zopsmart.com.thor.model.Category
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.Promo
import thor.zopsmart.com.thor.model.toCartItem
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.entity.ShoppingList
import thor.zopsmart.com.thor.repository.db.room.entity.concatNames
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import thor.zopsmart.com.thor.repository.db.room.repo.CartRepository
import thor.zopsmart.com.thor.repository.db.room.repo.WishlistRepository
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.*
import thor.zopsmart.com.thor.R as RFP

class ScangoDynamicPage : BaseFragment(), FPAbstractDelegates, SGProductDelegate, SGFPFilterSortDelegate {

    companion object {
        fun newPromoPage(promoTag: String? = null, storeID: Int, bannerId: String? = null): ScangoDynamicPage {
            return ScangoDynamicPage().apply {
                arguments = Bundle().apply{
                    putString("associatedData", JSONObject().put("hasOffers", true).put("storeId", storeID).toString())
                    putBoolean("isProductPage", true)
                    putBoolean("isPromoPage", true)
                    putString("promoTag", promoTag)
                    putString("bannerId", bannerId)
                }
            }
        }
    }

    var onScrollAction: (Int) -> Unit = {}

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ScangoDynamicPageViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoDynamicPageViewModel::class.java)
    }

    private val spanCount: Int = 30

    private lateinit var sortType: String
    private var filterMap: MutableMap<String,String> = mutableMapOf()

    private val sideNavFilter: SideNavFilter by lazy {
        SideNavFilter(mContext).apply {
            onSelectAction = {
                val filterKeys = sideNavFilter.getSelectionKeys()
                filterMap = getFiltersMap(filterKeys)
                viewModel.updateProductsWithFilter(filterKeys, isTagLayout = isTagLayout(), isPromo = isPromoPage)
            }
            onClearAction = {
                filterMap.clear()
                viewModel.updateProductsWithFilter(isTagLayout = isTagLayout(), isPromo = isPromoPage)
            }
        }
    }

    private val sortSheet: SortSheetDialog by lazy {
        SortSheetDialog(mContext)
    }

    private var extraQuery: String? = null
    private var slug: String = "home"
    private var isProductPage: Boolean = false
    private var isPromoPage: Boolean = false
    private var productAssociatedData: JSONObject = JSONObject()
    private fun isTagLayout() = slug.contains("tag?url=")
    private var bannerId: String? = null

    override val mLayout = RSG.layout.scango_fragment_dynamic_page

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        // address this issue https://github.com/airbnb/epoxy/issues/328
        Carousel.setDefaultGlobalSnapHelperFactory(null)

        EpoxyController.setGlobalDuplicateFilteringDefault(true)
    }

    override fun onCreated() {
        // Retrieve the slug from the bundle
        slug = arguments?.getString(EXTRA_SLUG) ?: slug
        viewModel.pageType = arguments?.getSerializable(EXTRA_PAGE_TYPE) as PageType?
        viewModel.pageUrl = arguments?.getString(EXTRA_URL)
        extraQuery = arguments?.getString("EXTRA_QUERY")
        isProductPage = arguments?.getBoolean("isProductPage") ?: false
        isPromoPage = arguments?.getBoolean("isPromoPage") ?: false
        productAssociatedData = JSONObject(arguments?.getString("associatedData") ?: "{}")
        arguments?.getString("promoTag")?.let {
            viewModel.selectPromo(it)
        }
        bannerId = arguments?.getString("bannerId", null)


        val gridLayoutManager = GridLayoutManager(mContext, spanCount)

        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(recycler_view)

        recycler_view.layoutManager = gridLayoutManager

        recycler_view.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                onScrollAction(recyclerView.computeVerticalScrollOffset())
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        recycler_view.buildModelsWith { controller ->
            controller.spanCount = spanCount
            val pageData = viewModel.pageData.value
            pageData?.forEach { it ->
                if (it is SGFilterSort) {
                    initFilterSortView(it)
                }

                (it as SGComponent<FPDelegate>).render(this@ScangoDynamicPage).forEach {
                    it.addTo(controller)
                }

                if(pageData.indexOf(it) == (pageData.size - 1) && it is SGProductGrid && !it.hasLoadedAll) {
                    SGLoader().render(this@ScangoDynamicPage).forEach {
                        it.addTo(controller)
                    }
                }
            }
        }

        viewModel.pageData.observe(this, Observer {
            if (it != null) {
                recycler_view.requestModelBuild()
            }
        })

        viewModel.incrementStatus.observe(this, Observer {
            when (it) {
                is IncrementFail -> {
                    if (it.e is StoreNotSelectedException) {
                        gotoDeliverySelection()
                    }
                }
            }
        })

        viewModel.wishListStatus.observe(this, Observer {
            when(it) {
                is WishListSuccess -> showFPToast(resources.getString(RFP.string.added_to_list))
                is WishListFail -> {
                    // Show error toast
                }
            }
        })

        viewModel.pageLoadStatus.observe(this, Observer {
            when(it) {
                is PageLoading -> loader.visible()
                is PageLoaded -> loader.gone()
            }
        })

        viewModel.trackEvent.observe(this, Observer { eventType ->
            when(eventType){
                is TrackAddToCart ->{
                    eventType.getContentIfNotHandled()?.run {
                        trackAddEvent(first, second)
                    }
                }
                is TrackRemoveFromCart ->{
                    eventType.getContentIfNotHandled()?.run {
                        trackRemoveEvent(first, second)
                    }
                }
                is TrackShortlist ->{
                    eventType.getContentIfNotHandled()?.run {
                        trackShortlistEvent(first, second)
                    }
                }
            }
        })

        loadData()

    }

    private fun initFilterSortView(fpFilterSort: SGFilterSort) {
        val sortingKey = fpFilterSort.sortKey
        sortType = getSortType(sortingKey)
        if (!sideNavFilter.isInitialised || !fpFilterSort.hasBeenConsumed) {
            sideNavFilter.populate(fpFilterSort.filters)
            fpFilterSort.hasBeenConsumed = true
        }

        sortSheet.apply {

            selectedKey = sortingKey

            sortClickListener = {
                fpFilterSort.sortKey = it
                sortType = getSortType(it)
                //TODO improve this, avoid calling for performance
                recycler_view.requestModelBuild()
                viewModel.updateProductGridWithSortingKey(it)
            }
        }
    }

    /**
     * Loads the data from the Api and assigns it to the data variable.
     */
    private fun loadData() {
        if(bannerId != null) {
            viewModel.loadProductForBanner(bannerId!!)
        } else {
            if (isProductPage) {
                viewModel.loadProduct(productAssociatedData, isPromoPage, isTagLayout())
            } else {
                viewModel.loadPage(slug, extraQuery)
            }
        }
        viewModel.loadWishlistedItems()
    }

    override fun loadMoreProduct(fpProductGrid: SGProductGrid) {
        // TODO enable only for the last component if needed?
        viewModel.loadProduct(fpProductGrid)
    }

    override fun onFilterClick() {
    }

    override fun onSortClick() {
        sortSheet.show()
    }

    override fun onSlideClick(url: String) {
    }

    override fun onBrandClick(brand: Brand) {
        gotoFponDynamicPage("brand?url=${brand.slug}", PageType.BRAND, brand.slug)
    }

    override fun onCategoryClick(category: Category) {
        gotoFponDynamicPage("category?url=${category.url}", PageType.CATEGORY, category.url ?: "")
    }

    override fun onCategoryClick(category: thor.zopsmart.com.thor.features.widget.categorycollection.data.Category) {
        when(category.urlType) {
            UrlType.CATEGORY -> gotoFponDynamicPage("category?url=${category.slug}", PageType.CATEGORY, category.slug ?: "")
            UrlType.TAG -> gotoFponDynamicPage("tag?url=${category.slug}", PageType.TAG, category.slug ?: "")
        }
    }

    override fun onAddToCart(cartItem: CartItem) {
        viewModel.increment(cartItem, FPScreen.LISTING_DETAIL)
    }

    private fun trackAddEvent(cartItem: CartItem, from: FPScreen) {
        AddToCart().also {
            when (from) {
                FPScreen.LISTING_DETAIL -> {
                    it.paramsMap.putAll(cartItem.toPLPAnalytics())
                    it.paramsMap.putAll(filterMap)
                    if(::sortType.isInitialized){
                        it.paramsMap[SORT_FILTER] = sortType
                    }
                }
                FPScreen.SIMILAR_PRODUCTS ->
                    it.paramsMap.putAll(cartItem.toSimProductsAnalytics())
                else -> { }
            }
            cartItem.parentItem?.toAnalytics()?.let { itemMap -> it.paramsMap.putAll(itemMap) }
        }
    }

    private fun trackRemoveEvent(cartItem: CartItem, from:FPScreen) {
        RemoveFromCart().also {
            when (from) {
                FPScreen.LISTING_DETAIL -> {
                    it.paramsMap.putAll(cartItem.toPLPAnalytics())
                    it.paramsMap.putAll(filterMap)
                    if(::sortType.isInitialized){
                        it.paramsMap[SORT_FILTER] = sortType
                    }
                }
                FPScreen.SIMILAR_PRODUCTS ->
                    it.paramsMap.putAll(cartItem.toSimProductsAnalytics())
                else -> { }
            }
            cartItem.parentItem?.toAnalytics()?.let { itemMap -> it.paramsMap.putAll(itemMap) }
        }
    }

    private fun trackShortlistEvent(cartItem: CartItem, from: FPScreen) {
        Shortlist().also {
            when (from) {
                FPScreen.LISTING_DETAIL -> {
                    it.paramsMap.putAll(cartItem.toPLPAnalytics())
                    it.paramsMap.putAll(filterMap)
                    if(::sortType.isInitialized){
                        it.paramsMap[SORT_FILTER] = sortType
                    }
                }
                FPScreen.SIMILAR_PRODUCTS ->
                    it.paramsMap.putAll(cartItem.toSimProductsAnalytics())
                else -> { }
            }
            cartItem.parentItem?.toAnalytics()?.let { itemMap -> it.paramsMap.putAll(itemMap) }
        }
    }

    override fun onRemoveFromCart(cartItem: CartItem) {
        viewModel.decrement(cartItem, FPScreen.LISTING_DETAIL)
    }

    override fun addToShoppingList(item: Item) {
        // Open the bottom sheet dialog
        if (viewModel.isLoggedIn())
            openShoppingListBottomSheet(item)
        else
            startActivity(LoginActivity.getIntent(mContext))
    }

    override fun onProductClick(item: Item) {
        if(item.url.isNullOrEmpty())
            return
        val intent = Intent(mContext, ScangoProductDetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_URL, item.url)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_ID, item.id)
        changePage(intent)
    }

    override fun onViewAllClick(associatedData: JSONObject) {
        openProductListingPage(associatedData)
    }

    override fun onPromoSelected(promo: Promo) {
        viewModel.selectPromo(promo.slug)
        // clear the filter, it will refresh the product again
        viewModel.loadProduct(productAssociatedData, true, isTagLayout())
    }

    override fun onQuantitySet(quantity: Int, cartItem: CartItem) {
        viewModel.setQuantity(quantity, cartItem)
    }

    private fun openShoppingListBottomSheet(item: Item) {
        if(viewModel.isWishListsEmpty) {
            openCreateShoppingListBottomSheet(item, mutableListOf())
        } else {
            ShoppingListBottomFragment.openShoppingListBottomSheet(
                item = item,
                addAction = { list ->
                    viewModel.addToShoppingList(item, list)
                },
                createAction = { list ->
                    openCreateShoppingListBottomSheet(item, list)
                },
                fragmentManager = fragmentManager
            )
        }
    }

    private fun openCreateShoppingListBottomSheet(item: Item, shoppingLists: List<ShoppingList>) {
        CreateShoppingListBottomFragment.openCreateListBottomFragment(
            createAction = {
                viewModel.createNewShoppingList(it, item, shoppingLists.toMutableList())
            },
            fragmentManager = fragmentManager
        )
    }

    private fun getFiltersMap(filterKeys: HashMap<String, List<String>>): MutableMap<String,String>{
        val keysMap: MutableMap<String, String> = mutableMapOf()
        var filterValue: String
        filterKeys.forEach {
            filterValue = ""
            it.value.forEachIndexed { index, key ->
                filterValue += key
                if (index != it.value.size - 1)
                    filterValue += ","
            }
            keysMap["${FILTER_INTERACTION}_${it.key}".toLowerCase()] = filterValue
        }

        return keysMap
    }
}

class ScangoDynamicPageViewModel @Inject constructor(
    val mApplication: Application,
    private val cartRepository: CartRepository,
    private val scangoMainRepository: ScangoMainRepository,
    private val wishlistRepository: WishlistRepository,
    private val accountRepository: AccountRepository,
    private val productService: ScangoProductService,
    val scangoStoreRepository: ScangoStoreRepository,
    private val gson: Gson
) : BaseViewModel(mApplication) {

    private val cartItems = cartRepository.getCartItemsLiveData()

    private val wishlistItemData = MutableLiveData<List<Long>>()
    val wishListStatus = MutableLiveData<WishListStatus>()

    val pageLoadStatus = MutableLiveData<PageLoadStatus>()

    private val pageDataInternal = MutableLiveData<List<SGComponent<*>>>()

    var pageType: PageType? = null
    var pageUrl: String? = null

    val selectedPromoSlug = MutableLiveData<String>()

    val pageData: LiveData<List<SGComponent<*>>> =
        dependantLiveData(selectedPromoSlug, cartItems, wishlistItemData, pageDataInternal) {
            pageDataInternal.value?.let { data ->
                val cartMap = cartItems.value?.associate { it.id to it.quantity }
                val wishList = wishlistItemData.value

                data.filter { it is SGProductGrid }
                    .flatMap { (it as SGProductGrid).items }
                    .forEach { item ->
                        item.isShoppingListed = wishList?.contains(item.id) ?: false
                        item.quantity = cartMap?.getOrElse(item.id) { 0 } ?: 0
                    }

                data.filter { it is SGProductScroller }
                    .flatMap { (it as SGProductScroller).items }
                    .forEach { item ->
                        item.isShoppingListed = wishList?.contains(item.id) ?: false
                        item.quantity = cartMap?.getOrElse(item.id) { 0 } ?: 0
                    }

                data.filter { it is SGPromo }
                    .flatMap { (it as SGPromo).promos }
                    .forEach { promo ->
                        promo.isSelected = promo.slug == selectedPromoSlug.value
                    }
                data
            }
        }

    var isWishListsEmpty: Boolean = false

    fun addToShoppingList(item: Item, shoppingLists: List<ShoppingList>) {
        launch {
            wishlistRepository.addToShoppinglist(item, shoppingLists)
                .onSuccess {
                    wishListStatus.value = WishListSuccess(shoppingLists.concatNames())
                    trackEvent.value = TrackShortlist(item.toCartItem(), FPScreen.LISTING_DETAIL)
                }
                .onError {
                    wishListStatus.value = WishListFail(it)
                }
            loadWishlistedItems()
        }
    }

    fun loadWishlistedItems() = launch {
        wishlistRepository.getWishlistedIds()
            .onSuccess {
                wishlistItemData.value = it.first
                isWishListsEmpty = it.second
            }
    }

    fun createNewShoppingList(name: String, item: Item, shoppingLists: MutableList<ShoppingList>) {
        launch {
            wishlistRepository.createShoppingList(name)
                .onSuccess {
                    shoppingLists.add(it)
                    addToShoppingList(item, shoppingLists)
                }
        }
    }

    val incrementStatus = MutableLiveData<IncrementStatus>()
    val trackEvent = MutableLiveData<TrackEvent>()

    fun increment(cartItem: CartItem, from: FPScreen) = launch {
        cartRepository.increment(cartItem)
            .onSuccess {
                trackEvent.value = TrackAddToCart(cartItem, from)
            }
            .onError {
                incrementStatus.value = IncrementFail(it)
            }
    }

    fun decrement(cartItem: CartItem, from: FPScreen) = launch {
        cartRepository.decrement(cartItem)
            .onSuccess {
                trackEvent.value = TrackRemoveFromCart(cartItem, from)
            }
    }

    fun JSONObject.setPromoTag() {
        if (selectedPromoSlug.value == null) {
            remove("tag")
        } else {
            put("tag", selectedPromoSlug.value)
        }
    }

    fun loadProductForBanner(bannerId: String) {
        launch {
            pageLoadStatus.value = PageLoading
            productService.getProductsByBarcodes(getProductIdsForCampaignId(bannerId), scangoStoreRepository.getStoreIdCached()?:0)
                .onSuccess {
                    pageLoadStatus.value = PageLoaded
                    val finalList = mutableListOf<SGComponent<*>>()
                    finalList.add(SGTitleBar("Promotions", false,true))
                    finalList.add(SGProductGrid(it).copy(hasLoadedAll = true))
                    pageDataInternal.value = finalList
                }
                .onError {
                    pageLoadStatus.value = PageLoaded
                    println(it.message)
                }
        }
    }

    private fun getProductIdsForCampaignId(campaignId: String): List<String> {
        try {
            val campaignProductsString = FirebaseRemoteConfig.getInstance().getString(campaignId)
            return campaignProductsString.split(",")
        } catch (e: Exception) {
            return emptyList()
        }
    }

    fun loadProduct(productAssociatedData: JSONObject, isPromo: Boolean, isTagLayout: Boolean) {
        productAssociatedData.apply {
            if(!isTagLayout && isPromo)
                setPromoTag()
        }
        launch {
            pageLoadStatus.value = PageLoading
            scangoMainRepository.loadProductsPage(productAssociatedData, isPromo)
                .onSuccess {
                    // this is hack of not overwriting tag if present already
                    val fpPromo = pageDataInternal.value?.first { it is SGPromo }
                    // TODO : TUAN
                    if (fpPromo == null) {
                        pageDataInternal.value = it
                    } else {
                        pageDataInternal.value = it.toMutableList().apply {
                            val index = indexOfFirst { it is SGPromo }
                            if (index > 0) {
                                removeAt(index)
                                add(index, fpPromo)
                            } else {
                                add(1, fpPromo)
                            }
                        }
                    }
                    pageLoadStatus.value = PageLoaded
                }
                .onError {
                    pageLoadStatus.value = PageLoaded
                }
        }
    }

    fun loadPage(slug: String, query: String?) = launch {
        pageLoadStatus.value = PageLoading
        scangoMainRepository.loadPage(slug, query)
            .onSuccess {
                // TODO : TUAN
                pageDataInternal.value = it
                pageLoadStatus.value = PageLoaded
            }
            .onError {
                pageLoadStatus.value = PageLoaded
            }
    }

    private fun withFPProductGrid(action: SGProductGrid.() -> Unit) {
        (pageDataInternal.value?.firstOrNull {
            it is SGProductGrid
        } as? SGProductGrid)?.let {
            action(it)
        }
    }

    // TODO should use viewid and criterial from filter sort view , and create a new one,
    // TODO after fetching can replace the componenet with a new one directly
    fun updateProductGridWithSortingKey(key: String) = launch {
        withFPProductGrid {
            hasLoadedAll = false
            page = 1
            associatedData.put("sorting", key)
            loadProduct(this)
        }
    }

    fun updateProductsWithFilter(filterKeys: HashMap<String, List<String>> = HashMap(), isTagLayout: Boolean, isPromo: Boolean) {
        var filterValue = ""
        filterKeys.forEach {
            if (filterValue.isNotEmpty())
                filterValue += "&"
            filterValue += "${it.key}:"

            it.value.forEachIndexed { index, key ->
                filterValue += key
                if (index != it.value.size - 1)
                    filterValue += ","
            }
        }

        filterValue = Uri.encode(filterValue)

        withFPProductGrid {
            hasLoadedAll = false
            page = 1
            associatedData.put("filter", filterValue).apply {
                if(!isTagLayout && isPromo)
                    setPromoTag()
            }
            loadProduct(this)
        }
    }

    fun loadProduct(widget: SGProductGrid) {
        val index = pageDataInternal.value?.let { it.indexOf(widget) }
        if (widget.isLoading || widget.hasLoadedAll) {
            return
        }
        launch {
            widget.isLoading = true
            scangoMainRepository.loadProducts(widget.associatedData, widget.page, pageType, pageUrl)
                .onSuccess {
                    index?.let{ i ->
                        updateFilters(it.filterList, i)
                    }
                    val items = it.itemList
                    if (widget.page <= 1) {
                        widget.items = items
                    } else {
                        widget.items = widget.items.plus(items)
                    }

                    widget.isLoading = false
                    widget.hasLoadedAll = (items.isEmpty()) // When no more products are loaded then we stop pagination
                    widget.page = widget.page + 1
                    // Notify data changes
                    pageDataInternal.value = pageDataInternal.value
                }
                .onError {
                    // TODO error handling
                    widget.isLoading = false
                }
        }
    }

    private fun updateFilters(filterList: ArrayList<FilterGroupDataHolder>, index: Int) {
        if(index <= 0) return
        pageDataInternal.value = pageDataInternal.value?.toMutableList()?.let { list ->
            var filterIndex: Int? = null
            for (i in index downTo 0) {
                if(list[i] is SGFilterSort) {
                    filterIndex = i
                    break
                }
            }
            filterIndex?.let {
                (list[it] as SGFilterSort).apply {
                    filters = filterList
                    hasBeenConsumed = false
                }
            }
            list
        }
    }

    fun selectPromo(promoSlug: String?) {
        if (selectedPromoSlug.value != promoSlug) {
            selectedPromoSlug.value = promoSlug
        }
    }

    fun setQuantity(quantity: Int, cartItem: CartItem) = launch {
        cartRepository.setQuantity(quantity, cartItem)
    }

    fun isLoggedIn() = accountRepository.isLoggedIn()
}

sealed class IncrementStatus
object IncrementSuccess : IncrementStatus()
class IncrementFail(val e: Throwable) : IncrementStatus()

sealed class WishListStatus
class WishListSuccess(val successMessage: String): WishListStatus()
class WishListFail(val e: Throwable): WishListStatus()

sealed class PageLoadStatus
object PageLoading : PageLoadStatus()
object PageLoaded : PageLoadStatus()