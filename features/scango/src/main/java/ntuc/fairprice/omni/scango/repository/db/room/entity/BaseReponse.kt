package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.json
import thor.zopsmart.com.thor.base.extensions.string

@Entity
data class BaseReponse(
    var code: Int,
    var status: String?,
    var message: String?
)

fun JSONObject.toBaseReponse(): BaseReponse {
    val code = int("code") ?: -1
    val status = string("status")
    val message = string("message")
    return BaseReponse(code, status, message)
}