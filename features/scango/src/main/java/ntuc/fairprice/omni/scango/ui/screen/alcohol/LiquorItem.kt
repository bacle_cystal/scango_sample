package ntuc.fairprice.omni.scango.ui.screen.alcohol

data class LiquorItem(
        val imageUrl: String,
        val name: String,
        val detail: String,
        val quantity: Int,
        val price: String
)

