package ntuc.fairprice.omni.scango.ui.screen.qrcode

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.gms.common.annotation.KeepName
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_scan_code.*
import kotlinx.android.synthetic.main.scango_icon_cart_view.*
import kotlinx.android.synthetic.main.scango_scan_tab_view.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.Constants.INTENT_SOUNDSERVICE_ACTION
import ntuc.fairprice.omni.scango.Constants.cartClassname
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScanFragmentRapidType
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.ScangoManuallyInputDelegate
import ntuc.fairprice.omni.scango.base.ScangoMaxItemDelegate
import ntuc.fairprice.omni.scango.base.customview.scannerwalkthrough.ScangoScannerWalkThroughDialog
import ntuc.fairprice.omni.scango.databinding.ScangoActivityScanCodeBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import ntuc.fairprice.omni.dialog.ScangoCartFullErrorDialog
import ntuc.fairprice.omni.dialog.ScangoUserBlockedErrorDialog
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartItem
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.BarcodeErrorFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.ErrorType
import ntuc.fairprice.omni.scango.ui.screen.qrcode.manually.ScannerCodeManuallyFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.product.BarcodeProductRapidFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.scannerwidget.ScannerWidget
import ntuc.fairprice.omni.scango.utils.*
import ntuc.fairprice.omni.scango.widget.WelcomeToast
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.base.withLogin
import javax.inject.Inject

@KeepName
class CodeScannerActivity : ScangoBaseActivity(), ActivityCompat.OnRequestPermissionsResultCallback,
    CompoundButton.OnCheckedChangeListener, ScangoManuallyInputDelegate, ScangoMaxItemDelegate,
    HasSupportFragmentInjector {

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    companion object {
        const val TAG = "CodeScannerActivity"
        const val SCAN_TYPE = "scan_type"

        fun getLoginIntent(context: Context): Intent {
            return Intent(context, CodeScannerActivity::class.java).withLogin(context)
        }

        fun getScanBarcodeIntentSingle(context: Context) = Intent(context, CodeScannerActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
            putExtra(SCAN_TYPE, ScanType.BAR_CODE.value)
        }
    }

    @Inject
    lateinit var viewModel: CodeScannerActivityViewModel
    @Inject
    lateinit var scannerWidget: ScannerWidget
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoCartFullErrorDialog: ScangoCartFullErrorDialog
    @Inject
    lateinit var scangoUserBlockedErrorDialog: ScangoUserBlockedErrorDialog

    private var walkThroughDialog: ScangoScannerWalkThroughDialog? = null
    private var scanType: Int = ScanType.QR_CODE.value

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        val binding: ScangoActivityScanCodeBinding =
            DataBindingUtil.setContentView(this, R.layout.scango_activity_scan_code, null)
        binding.vm = viewModel
        binding.productItem = viewModel.productItem.value
        binding.lifecycleOwner = this

        //Change UI by type for screen
        scanType = intent.getIntExtra(CodeScannerActivity.SCAN_TYPE, ScanType.QR_CODE.value)

        when (scanType) {
            ScanType.BAR_CODE.value -> viewModel.changeUIScanCode(
                ScanType.BAR_CODE,
                !cartViewModel.getCartItems().isNullOrEmpty()
            )
            ScanType.QR_CODE.value -> viewModel.changeUIScanCode(ScanType.QR_CODE)
        }

        initWidget()
        initListeners()
        initViewModel()
        scan_tab_view.updateInitState()
    }

    private fun initViewModel() {
        viewModel.apply {
            getRapidShowCached()

            //Observer
            observe(productItem, ::onProductItemChanged)
            observe(storeData, ::onStoreItemChanged)
            observe(maxItemEvent, ::onMaxItemStateChanged)
            observe(errorType, ::onError)
            observe(isCheckedInSuccess) {
                if (it == true) {
                    WelcomeToast.show(this@CodeScannerActivity,  "ScanGo store")
                }
            }
            observe(showLiquidRapidFragmentEvent, ::onShowLiquidRapidFragmentEventChanged)

            if (isVisiableGuide.value == true && scanType == ScanType.BAR_CODE.value) {
                displayWalkthroughDialog()
            }

            observe(isLoading) {
                if (it == true) scanGoProgressDialog.show()
                else scanGoProgressDialog.dismiss()
            }
        }
    }

    private fun initWidget() {
        scannerWidget.initWidget(container, scanType)
    }

    private fun onMaxItemStateChanged(maxItem: Boolean) {
        if (maxItem) {
            scangoCartFullErrorDialog.onActionClick = {
                scannerWidget.start(this@CodeScannerActivity)
            }
            scangoCartFullErrorDialog.show()
        }
    }

    private fun onError(errorType: Int?) {
        when (errorType) {
            ErrorType.USER_FORBIDDEN.value -> {
                scangoUserBlockedErrorDialog.show()
            }

            else -> {
                showErrorBarcodeFragment()
            }
        }
    }

    private fun onStoreItemChanged(scangoStore: ScangoStore?) {

            val resultIntent = Intent()
            resultIntent.putExtra(Constants.INTENT_RESULT_CITY_NAME, "ScanGo Store")
            setResult(Activity.RESULT_OK, resultIntent)
            finish()

    }

    private fun onProductItemChanged(productItem: Product?) {
        viewModel.apply {
            if (productItem != null) {
                addToCart(cartViewModel, productItem)
                if (maxItemEvent.value == false) {
                    //With mode barcode rapid
                    if (isBarcodeRapid.value == true) {
                        isShowAddProduct.value = true
                        //Check age violated show warning
                        if (checkAgeViolated(productItem)) {
                            isAddItem18plus.value = true
                        }
                        Handler().postDelayed({
                            cv_product.visible()
                            startAnimationAddProduct(object : Animation.AnimationListener {
                                override fun onAnimationRepeat(animation: Animation?) {
                                }

                                override fun onAnimationEnd(animation: Animation?) {
                                    scannerWidget.start(this@CodeScannerActivity)
                                    icon_cart.update(cartViewModel.getCartItemsQuantity())
                                    //Hide layout age restriction if showed when anim end
                                    if (isAddItem18plus.value == true) {
                                        isAddItem18plus.value = false
                                    }
                                }

                                override fun onAnimationStart(animation: Animation?) {
                                }
                            })
                        }, 500)
                    } else {
                        startActivity(Intent(applicationContext,ScangoCartActivity::class.java))

                    }
                }
            } else {
                showErrorBarcodeFragment()
            }
        }
    }

    private fun initListeners() {
        btnBack.setOnClickListener { finish() }

        ic_code_manually.setOnClickListener {
            showScannerCodeManuallyFragment()
        }

        text_code_bottom.setOnClickListener {
            showScannerCodeManuallyFragment()
        }

        icon_cart.setCustomOnClickListener(View.OnClickListener {
            supportFragmentManager.popBackStack()
        })

        scan_tab_view.scanIndividualTapped = {
            createSoundService(
                this@CodeScannerActivity,
                INTENT_SOUNDSERVICE_ACTION,
                "scan_individual_sound.wav"
            )
            viewModel.changeBarcodeRapid(false)
            if (viewModel.isGotit.value == true) {
                showImageToast(R.drawable.sg_scan_mode_one_by_one)
            }
            AnimationUtils.startDimAnimation(view_scanner_opacity)
        }
        scan_tab_view.scanRapidTapped = {
            createSoundService(
                this@CodeScannerActivity,
                INTENT_SOUNDSERVICE_ACTION,
                "rapid_scan_sound.wav"
            )
            viewModel.changeBarcodeRapid(true)
            if (viewModel.isGotit.value == true) {
                showImageToast(R.drawable.sg_scan_mode_rapid_scan)
            }
            AnimationUtils.startDimAnimation(view_scanner_opacity)
        }

        layout_qrcode_guideline.setOnClickListener {
            gotoHelpPage()
        }

        scan_tab_view.updateInitState()
    }

    private fun showScannerCodeManuallyFragment() {

        val qrcodeManuallyFragment = ScannerCodeManuallyFragment.newInstance(
            viewModel.isVisiableBarcode.value ?: true
        )
        qrcodeManuallyFragment.setCallbackListener(this)
        addFragment(
            fragment = qrcodeManuallyFragment,
            TAG = ScannerCodeManuallyFragment.TAG,
            transit = ScangoBaseActivity.TransitionFrom.RIGHT,
            addToBackStack = true
        )
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        scannerWidget.updateFacing(isChecked)
        scannerWidget.stop()
    }

    /**
     * Keeping this method to access from BarcodeProductFragment.
     * Need to find a better way to handle this scenario
     */
    fun startCamera() {
        scannerWidget.start(this)
    }

    override fun onResume() {
        super.onResume()
        //Don't start camera when tutorial is showing
        if (walkThroughDialog?.isShown != true) {
            scannerWidget.start(this)
        }
        icon_cart.update(cartViewModel.getCartItemsQuantity())
    }

    /** Stops the camera.  */
    override fun onPause() {
        super.onPause()
        scannerWidget.stop()
    }

    public override fun onDestroy() {
        super.onDestroy()
        scannerWidget.destroy()
    }

    override fun onInputBarcodeManually(productId: String) {
        val scanData = BarcodeQRCodeUtils.getScanData(productId, FirebaseVisionBarcode.FORMAT_UNKNOWN)
        viewModel.getProductByBarcode(scanData)
    }

    override fun onInputQrcodeManually(storeIdString: String) {
        viewModel.getCheckIn(storeIdString)
    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is BarcodeErrorFragment -> {
                fragment.onManuallyInputClick = { showManuallyInputFromError() }
            }
        }
    }

    private fun showErrorBarcodeFragment() {
        scannerWidget.stop()
        addFragment(
            fragment = BarcodeErrorFragment.newInstance(scanType, viewModel.errorType.value ?: 1),
            TAG = BarcodeErrorFragment.TAG,
            transit = ScangoBaseActivity.TransitionFrom.RIGHT,
            addToBackStack = true
        )
    }

    private fun showManuallyInputFromError() {
        supportFragmentManager.popBackStack()
        ic_code_manually.callOnClick()
    }

    private fun startAnimationAddProduct(animationListener: Animation.AnimationListener) {
        AnimationUtils.startRedJsonCartAnimation(lottie_view)
        AnimationUtils.startDimAnimation(view_scanner_opacity)

        //Anim of add to cart view flow
        //0->0.4(display opacity change 0->100)
        //0.4->1.4(keep display 1s)
        //1.4->1.8(change position of imageview + start red json)
        //1.6->1.8(change position of imageview)
        //1.7->1.8(opacity change 100->10)
        val isShowWarning = viewModel.isShow18PlusLayout

        AnimationUtils.startAlphaAnim(cv_product, text_barcode_18plus_warning, animationListener, isShowWarning)
        AnimationUtils.startAlphaAnimCart(text_number_item)
    }

    private fun showImageToast(imageId: Int) {
        val toastImage =
            LayoutInflater.from(this).inflate(R.layout.scango_barcode_scan_toast, null) as AppCompatImageView
        toastImage.setImageResource(imageId)
        with(Toast(this)) {
            setGravity(Gravity.CENTER, 0, 0)
            view = toastImage
            show()
            Handler().postDelayed({
                cancel()
            }, 800)
        }
    }

    override fun onOpenCartToEdit() {
        onBackPressed()
        launchActivity(cartClassname)
    }

    override fun reloadData() {
        scannerWidget.start(this)
        icon_cart.update(cartViewModel.getCartItemsQuantity())
    }

    override fun onStop() {
        super.onStop()
        stopSoundService(this@CodeScannerActivity)
    }

    private fun onShowLiquidRapidFragmentEventChanged(result: Product) {
        replaceFragment(
            fragment = BarcodeProductRapidFragment.newInstance(
                result,
                ScanFragmentRapidType.LIQUOR_ITEM.value,
                cartViewModel.getTextTimeWarning(result.toCartItem())
            ),
            TAG = BarcodeProductRapidFragment.TAG,
            transit = TransitionFrom.RIGHT,
            addToBackStack = true
        )
    }

    private fun displayWalkthroughDialog() {
        scannerWidget.stop()
        walkThroughDialog = ScangoScannerWalkThroughDialog(this@CodeScannerActivity)
        walkThroughDialog?.gotItLeftClickListener = {
            btn_scan_barcode_rapid?.touch()
        }
        walkThroughDialog?.gotItRightClickListener = {
            btn_scan_barcode_one?.touch()
            viewModel.setRapidShowed()
            scannerWidget.start(this@CodeScannerActivity)
        }
        walkThroughDialog?.show()
    }
}