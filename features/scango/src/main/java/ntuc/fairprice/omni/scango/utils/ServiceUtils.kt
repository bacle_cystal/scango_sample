package ntuc.fairprice.omni.scango.utils

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import ntuc.fairprice.omni.scango.AlarmBroadcastReceiver
import ntuc.fairprice.omni.scango.Constants.INTENT_ALARMSERVICE_REQUESTCODE
import ntuc.fairprice.omni.scango.Constants.INTENT_SOUNDSERVICE_FILE_NAME
import java.util.*


/**
 * Created by Binh.TH on 4/5/2019.
 */

/**
 *
 * @param activity Activity
 * @param  Server time gap (Localtime - server time)
 * @param calendarTrigger Calendar time to handle action
 * @param apiDelayTime Long Time delta before and after get API checkinVerify
 * @param requestCode Int
 */
fun createAlarmService(
    activity: Activity,
    serverTimeGap: Long,
    timeToTrigger: Long,
    apiDelayTime: Long,
    requestCode: Int
) {
    val alarmMgr = activity.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val broadcastIntent = Intent(activity, AlarmBroadcastReceiver::class.java)
    broadcastIntent.putExtra(INTENT_ALARMSERVICE_REQUESTCODE, requestCode)
    val pIntent = PendingIntent.getBroadcast(
        activity,
        requestCode, broadcastIntent, 0
    )

    // Calculation delta time with server to reject case fake time on local
    var timeToAlert = DateTimeUtils.getServerTimeMilis(timeToTrigger, serverTimeGap, apiDelayTime)

    // Just check for time not yet pass current time
    if (timeToAlert > System.currentTimeMillis()) {
        alarmMgr.setExact(
            AlarmManager.RTC_WAKEUP,
            timeToAlert,
            pIntent
        )
    }
}


fun createSoundService(activity: Activity, action: String, fileName: String) {
    val intent = Intent()
    intent.setClass(activity, SoundService::class.java)
    intent.action = action
    intent.putExtra(INTENT_SOUNDSERVICE_FILE_NAME, fileName)
    activity.startService(intent)
}

fun stopSoundService(activity: Activity) {
    val intent = Intent()
    intent.setClass(activity, SoundService::class.java)
    activity.stopService(intent)
}




