package ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.widget

import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScangoLinkPlusOtpWidget: LayoutContainer {

    fun initWidget(view: View)

    var onOtpInputComplete: (() -> Unit)?

    fun getOtp(): String

    fun displayErrorState()

    fun displayDefaultState()

    fun showTimer()

    fun hideTimer()
}