package ntuc.fairprice.omni.scango

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.appbar.AppBarLayout
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_feature.*
import kotlinx.android.synthetic.main.scango_layout_navigation_bottom.*
import kotlinx.android.synthetic.main.scango_search_edit_text.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.dialog.ScangoUserBlockedErrorDialog
import ntuc.fairprice.omni.scango.Constants.REQUEST_ALARM_NOTICE_10h20
import ntuc.fairprice.omni.scango.Constants.REQUEST_ALARM_REMOVE_LIQUOR_10h30
import ntuc.fairprice.omni.scango.Constants.REQUEST_NAME_PRODUCT
import ntuc.fairprice.omni.scango.Constants.REQUEST_NAME_STORE
import ntuc.fairprice.omni.scango.Constants.cartClassname
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.epoxy.ScangoDynamicPage
import ntuc.fairprice.omni.scango.ui.screen.deals.ScangoDealsBeforeCheckInFragment
import ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired.ScangoSessionExpiredFragment
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoHomeFragment
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoTutorialFragment
import ntuc.fairprice.omni.scango.ui.screen.home.Tab
import ntuc.fairprice.omni.scango.ui.screen.home.exitstore.ExitStoreFragment
import ntuc.fairprice.omni.scango.ui.screen.more.ScangoMoreFragment
import ntuc.fairprice.omni.scango.ui.screen.orders.ScangoOrderListActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListPage
import ntuc.fairprice.omni.scango.ui.screen.uicheck.UiCheckFragment
import ntuc.fairprice.omni.scango.utils.*
import ntuc.fairprice.omni.toggle.ToggleFragment
import thor.zopsmart.com.thor.features.account.ui.LoginActivity
import javax.inject.Inject

class ScangoFeatureActivity : ScangoBaseActivity(), HasSupportFragmentInjector, ToggleFragment.CallBackListener {

    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
    @Inject
    lateinit var scangoUserBlockedErrorDialog: ScangoUserBlockedErrorDialog

    private var pendingCheckInStoreID = ""

    private var selectedBannerId: String? = null

    companion object {
        const val FRAGMENT_TAG = "FRAGMENT_TAG"

        const val DEFAULT_TAB_POSITION = "DEFAULT_TAB_POSITION"
        fun intentWithDefaultTab(context: Context, tab: Tab): Intent {
            val intent = Intent(context, ScangoFeatureActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(DEFAULT_TAB_POSITION, tab.position)
            return intent
        }

        const val IS_MOVING_TO_ORDERS_LIST = "IS_MOVING_TO_ORDERS_LIST"
        fun intentMoveToOrderList(context: Context): Intent {
            val intent = intentWithDefaultTab(context, Tab.MORE)
            intent.putExtra(IS_MOVING_TO_ORDERS_LIST, true)
            return intent
        }
    }

    val viewModel: ScangoFeatureViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoFeatureViewModel::class.java)
    }

    private lateinit var viewBinding: ViewDataBinding

    private val toggleFragment: ToggleFragment by lazy {
        //First init toggle with state isOnStore = true
        ToggleFragment.newInstance(true)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.scango_activity_feature)

        viewBinding = DataBindingUtil.setContentView(this@ScangoFeatureActivity, R.layout.scango_activity_feature)

        viewBinding.lifecycleOwner = this

        viewBinding.apply {
            setVariable(BR.viewModel, viewModel)
            executePendingBindings()
        }

        if (savedInstanceState == null) {
            //Hide search bar
            viewModel.isShowSearchUI.value = false
            replaceFragment(
                fragment = ScangoTutorialFragment(),
                TAG = ScangoTutorialFragment.TAG,
                transit = TransitionFrom.RIGHT
            )

            addFragmentToggle(
                fragment = toggleFragment,
                TAG = ToggleFragment.TAG,
                transit = TransitionFrom.RIGHT
            )

            toggleFragment.setCallbackListener(this)
        }

        viewModel.apply {
            observe(loginStatusLiveData) {
                if (!isLoggedIn) {
                    exitStore()
                }
            }
            observe(_storeName, ::onStoreNameChanged)
            createObserverTimeService()
            createObserverCheckinStatus()
            observe(exitStoreLiveData, ::onExitStoreStatusChanged)
            observe(checkInResult, ::onCheckInStatusChange)
        }

        layout_tab_home.setOnClickListener {
            prepareForBottomNavigationItemClick(Tab.HOME.position)
        }
        layout_tab_promotions.setOnClickListener {
            prepareForBottomNavigationItemClick(Tab.PROMOTIONS.position)
        }
        layout_tab_scan.setOnClickListener {
            if (PermissionUtils.hasCameraPermission(this)) {
                prepareForBottomNavigationItemClick(Tab.SCAN.position)
            } else {
                PermissionUtils.requestCameraPermission(this)
            }
        }

        layout_tab_favorite.setOnClickListener {
            prepareForBottomNavigationItemClick(Tab.FAVORITE.position)
        }

        layout_tab_more.setOnClickListener {
            prepareForBottomNavigationItemClick(Tab.MORE.position)
        }

        ic_cart.setOnClickListener {
            launchActivity(cartClassname)
        }

        custom_search_field.setOnFocusChangeListener { _, focused ->
            if (focused) {
                launchActivity(Constants.searchClassname)
                custom_search_field.clearFocus()
            }
        }

        tooltip_checkin.setOnClickListener {
            supportFragmentManager?.let {
                val fragment = UiCheckFragment.newInstance()
                fragment.show(
                    it,
                    UiCheckFragment.TAG
                )
            }
        }

        //Check name store cached and change layout
        viewModel.checkStoreCached()

        intent?.extras?.let {
            handleDeeplink(it)
        }
    }

    private fun createObserverCheckinStatus() {
        viewModel.apply {
            checkCheckInStatusResult.observe(this@ScangoFeatureActivity, Observer {
                when (it) {
                    CheckCheckInStatusResult.CheckCheckInStatusFail -> {
                        viewModel.exitStoreLiveData.value = ExitStoreStatus.Success
                        toggleFragment.navigateToFPOnHome()
                    }
                    CheckCheckInStatusResult.CheckCheckInStatusSuccess -> {
                        if (isCheckedIn()) {
                            checkTimeStampCached()
                        }
                    }
                    CheckCheckInStatusResult.CheckCheckInStatusSessionExpired -> {
                        ScangoSessionExpiredFragment.newInstance().show(
                            supportFragmentManager,
                            ScangoSessionExpiredFragment.TAG
                        )
                    }
                }
            })
        }
    }

    private fun createObserverTimeService() {
        viewModel.apply {
            _timeStamp.observe(this@ScangoFeatureActivity, Observer {
                //Create 2 alarm service : notice 10h20 and auto remove liquor 10h30
                //Notice before 10min = 10* 60sec* 1000
                //All time need ensure *1000 in milis(1sec= 1000milis)
                createAlarmService(
                    this@ScangoFeatureActivity,
                    serverGapTime,
                    dateTimeRestriction - 10 * 60 * 1000,
                    cachedApiDelayTime,
                    REQUEST_ALARM_NOTICE_10h20
                )
                createAlarmService(
                    this@ScangoFeatureActivity,
                    serverGapTime,
                    dateTimeRestriction,
                    cachedApiDelayTime,
                    REQUEST_ALARM_REMOVE_LIQUOR_10h30
                )
            })
        }
    }

    private fun onClickBottomNavigationItem(position: Int): Boolean {
        val currentTag = getTabFragmentTag(viewModel.currentTab.value ?: Tab.HOME.position)
        val newTag = getTabFragmentTag(position)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val currentFragment = fragmentManager.findFragmentByTag(currentTag)
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment)
        }

        var newFragment: Fragment? = fragmentManager.findFragmentByTag(newTag)
        if (newFragment == null) {
            newFragment = newFragmentInstance(position)
            if (newFragment.isAdded) {
                fragmentTransaction.show(newFragment)
            } else {
                fragmentTransaction.replace(R.id.container, newFragment, newTag)
            }
        } else {
            fragmentTransaction.show(newFragment)
        }
        viewModel.currentTab.value = position
        fragmentTransaction.commit()

        return true
    }

    private fun getTabFragmentTag(position: Int) = FRAGMENT_TAG + position

    private fun handleDeeplink(bundle: Bundle?) {
        try {
            bundle?.let {
                it.getString("storeID")?.let { storeID ->
                    viewModel.clearDataStoreWhenInvalidCheckin()
                    if (checkIfLoggedIn()) {
                        pendingCheckInStoreID = storeID
                        return
                    }
                    viewModel.getCheckIn(storeID)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun newFragmentInstance(position: Int): Fragment {
        return when (position) {
            Tab.HOME.position -> {
                when (viewModel.isCheckedIn()) {
                    true -> ScangoHomeFragment()
                    else -> ScangoTutorialFragment()

                }
            }
            Tab.PROMOTIONS.position -> {
                if (viewModel.isCheckedIn()) {
                    if (selectedBannerId == null) {
                        ScangoDynamicPage.newPromoPage(storeID = viewModel.getStoreID(), bannerId = "campaign_all")
                    } else {
                        ScangoDynamicPage.newPromoPage(storeID = viewModel.getStoreID(), bannerId = selectedBannerId)
                            .also {
                                selectedBannerId = null
                            }
                    }

                } else {
                    ScangoDealsBeforeCheckInFragment.newInstance()
                }
            }
            Tab.FAVORITE.position -> ScangoShoppingListPage()
            Tab.MORE.position -> ScangoMoreFragment()
            else -> Fragment()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.apply {
            if (isCheckedIn()) {
                checkCheckInVerify()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.apply {
            updateIndicatorIcon(!cartViewModel.getCartItems().isNullOrEmpty())
        }
        container?.hideKeyBoard()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val extras = intent?.extras
        if (extras == null) {
            goToSelectedFeatureOnLoginSuccess()
        } else {
            navigateToCorrectDestination(extras)
        }
        intent?.extras?.let {
            handleDeeplink(it)
        }
        if (pendingCheckInStoreID.isNotEmpty()) {
            viewModel.getCheckIn(pendingCheckInStoreID)
            pendingCheckInStoreID = ""
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_NAME_STORE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val storeName = data?.extras?.getString(Constants.INTENT_RESULT_CITY_NAME) ?: ""
                    viewModel.changeUIScanCode(storeName)
                }
            }
            //TODO: handle REQUEST_NAME_PRODUCT
        }
    }


    private fun navigateToCorrectDestination(extras: Bundle) {
        extras.apply {
            val defaultTab = getInt(DEFAULT_TAB_POSITION, -1)
            viewModel.selectedTabBeforeLogin = when (defaultTab) {
                -1 -> viewModel.selectedTabBeforeLogin // Nothing change here
                else -> {
                    defaultTab
                }
            }
            goToSelectedFeatureOnLoginSuccess()

            if (getBoolean(IS_MOVING_TO_ORDERS_LIST, false)) {
                startActivity(ScangoOrderListActivity.getIntent(this@ScangoFeatureActivity))
            }
        }
    }

    fun changeToggleVisiable(isShowToggleUI: Boolean, isShowSearch: Boolean = true) {
        val params = container.layoutParams as CoordinatorLayout.LayoutParams

        if (isShowToggleUI) {
            params.behavior = AppBarLayout.ScrollingViewBehavior()
            viewModel.changeUIToggle(isShowToggleUI, isShowSearch)
        } else {
            params.behavior = null
            viewModel.changeUIToggle(isShowToggleUI, isShowSearch)
        }
        container.requestLayout()
    }

    override fun onStoreExitClicked(storeName: String) {
        val nextFrag = ExitStoreFragment.newInstance(storeName)
        nextFrag.setCallbackListener(object : ExitStoreFragment.ExitStoreCallBackListener {
            override fun onStoreExitClicked() {
                viewModel.exitStore()
                onBackPressed()
            }

            override fun onStoreCancelClicked() {
                onBackPressed()
            }

        })
        addFragmentFullScreen(
            fragment = nextFrag,
            TAG = ExitStoreFragment.TAG,
            transit = ScangoBaseActivity.TransitionFrom.RIGHT,
            addToBackStack = true
        )
    }

    private fun prepareForBottomNavigationItemClick(position: Int) {
        when (position) {
            Tab.HOME.position -> {
                viewModel.selectedTabBeforeLogin = null
                changeToggleVisiable(true, false)
                onClickBottomNavigationItem(position)
            }

            Tab.PROMOTIONS.position -> onClickPromotions()

            Tab.SCAN.position -> onClickScanButton()

            Tab.FAVORITE.position -> onClickFavouritesTab()

            Tab.MORE.position -> {
                viewModel.selectedTabBeforeLogin = null
                changeToggleVisiable(false, false)
                onClickBottomNavigationItem(position)
            }
        }
    }

    private fun onClickPromotions() {
        if (!viewModel.isLoggedIn) {
            viewModel.selectedTabBeforeLogin = Tab.PROMOTIONS.position
        } else {
            viewModel.selectedTabBeforeLogin = null
        }
        changeToggleVisiable(false, true)
        onClickBottomNavigationItem(Tab.PROMOTIONS.position)
    }

    private fun onClickFavouritesTab() {
        if (checkIfLoggedIn()) {
            viewModel.selectedTabBeforeLogin = Tab.FAVORITE.position
            return
        }
        viewModel.selectedTabBeforeLogin = null
        changeToggleVisiable(false)
        onClickBottomNavigationItem(Tab.FAVORITE.position)
    }

    private fun onClickScanButton() {
        if (checkIfLoggedIn()) {
            viewModel.selectedTabBeforeLogin = Tab.SCAN.position
            return
        }
        viewModel.selectedTabBeforeLogin = null
        if (!toggleFragment.viewModel.companyName.value.isNullOrEmpty()) {
            startActivityForResult(
                Intent(this, CodeScannerActivity::class.java).putExtra(
                    CodeScannerActivity.SCAN_TYPE,
                    ScanType.BAR_CODE.value
                ), REQUEST_NAME_PRODUCT
            )
        } else {
            startActivityForResult(
                Intent(this, CodeScannerActivity::class.java).putExtra(
                    CodeScannerActivity.SCAN_TYPE,
                    ScanType.QR_CODE.value
                ), REQUEST_NAME_STORE
            )
        }
    }

    private fun checkIfLoggedIn(): Boolean {
//        if (!viewModel.isLoggedIn) {
//            val intent = intent.apply {
//                addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
//            }
//            startActivity(LoginActivity.getIntent(this, intent))
//            return true
//        }
        return false
    }

    private fun goToSelectedFeatureOnLoginSuccess() {
        if (viewModel.isLoggedIn) {
            viewModel.selectedTabBeforeLogin?.let {
                if (it == Tab.SCAN.position) {
                    prepareForBottomNavigationItemClick(Tab.HOME.position)
                }
                prepareForBottomNavigationItemClick(it)
            }
        }
    }

    override fun reloadData() {
        viewModel.checkStoreCached()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (PermissionUtils.hasCameraPermission(this)) {
            prepareForBottomNavigationItemClick(Tab.SCAN.position)
        } else {
            DialogUtils.showCameraPermissionDialog(this)
        }
    }

    private fun onStoreNameChanged(storeName: String) {
        toggleFragment.updateData(storeName)
        when {
            storeName.isEmpty() -> {
                layout_tab_home.performClick()
                replaceFragment(
                    fragment = ScangoTutorialFragment(),
                    TAG = ScangoTutorialFragment.TAG,
                    transit = TransitionFrom.RIGHT
                )
            }
            else -> {
                layout_tab_home.performClick()
                replaceFragment(
                    fragment = ScangoHomeFragment(),
                    TAG = getTabFragmentTag(Tab.HOME.position),
                    transit = TransitionFrom.RIGHT
                )
            }
        }
    }

    private fun onExitStoreStatusChanged(state: ExitStoreStatus) {
        when (state) {
            is ExitStoreStatus.Loading -> {
                scanGoProgressDialog.show()
            }
            is ExitStoreStatus.Success -> {
                scanGoProgressDialog.dismiss()
                onExitStore()
            }
            is ExitStoreStatus.GeneralError -> {
                scanGoProgressDialog.dismiss()
                scangoGeneralErrorDialog.show()
            }
            is ExitStoreStatus.NetworkError -> {
                scanGoProgressDialog.dismiss()
                scangoNetworkErrorDialog.show()
            }
        }
    }

    private fun onCheckInStatusChange(state: CheckInResult) {
        when (state) {
            is CheckInResult.Loading -> {
                scanGoProgressDialog.show()
            }
            is CheckInResult.CheckInSuccess -> {
                scanGoProgressDialog.dismiss()
                reloadData()

            }
            is CheckInResult.CheckInUserBlocked -> {
                scanGoProgressDialog.dismiss()
                scangoUserBlockedErrorDialog.show()
            }
            is CheckInResult.CheckInFail -> {
                scanGoProgressDialog.dismiss()
            }
        }
    }

    private fun onExitStore() {
        viewModel.changeUIScanCode("")
        viewModel.clearDataStoreWhenInvalidCheckin()
    }

    fun onBannerClicked(bannerId: String) {
        selectedBannerId = bannerId
        prepareForBottomNavigationItemClick(Tab.PROMOTIONS.position)
    }
}
