package ntuc.fairprice.omni.ui.widget

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.content.ContextCompat
import ntuc.fairprice.omni.ui.R
import ntuc.fairprice.omni.ui.utils.dpToPx
import ntuc.fairprice.omni.ui.utils.spToPx

/**
 * Created by Binh.TH on 2019/01/18
 * Custom UI for toggle
 * Draw a textLeft and textRight
 *
 * Draw 2 icon in-appstore and onapp
 * Draw a slider white color move on UI follow value of Animation's moveAnim property
 */
class ToggleButtonView : View {

    companion object {
        const val BUTTON_STATE_LEFT = true
        const val BUTTON_STATE_RIGHT = false
        const val WIDTH_SIZE_FIX = 328F
    }

    private var leftTextOn = resources.getString(R.string.toggle_onstore_on)
    private var leftTextOff = resources.getString(R.string.toggle_onstore_off)
    private var rightTextOn = resources.getString(R.string.toggle_instore_on)
    private var rightTextOff = resources.getString(R.string.toggle_instore_off)

    private var selectTextColor = ContextCompat.getColor(context, R.color.black)
    private var unSelectTextColor = ContextCompat.getColor(context, R.color.black)

    private val selectTextSize = getFloatDimentionValue(R.dimen.text_size_toggle_selected).spToPx(context)
    private val unSelectTextSize = getFloatDimentionValue(R.dimen.text_size_toggle_unselected).spToPx(context)

    private lateinit var fontMetrics: Paint.FontMetricsInt
    private var baseline = 0f

    private var buttonBackgroundColor = ContextCompat.getColor(context, R.color.white30)
    private var sliderColor = ContextCompat.getColor(context, R.color.white)
    private var buttonPaint = Paint()
    private var sliderPaint = Paint()
    private var textPaint = Paint()
    private lateinit var buttonRectF: RectF
    private lateinit var sliderRectF: RectF
    private lateinit var textRectF: RectF

    //Text left dimension text
    private var xLeftTextOn = 0f
    private var lengthLeftTextOn = 0f
    private var xLeftTextOff = 0f
    private var lengthLeftTextOff = 0f

    //Text right dimension text
    private var xRightTextOn = 0f
    private var lengthRightTextOn = 0f
    private var xRightTextOff = 0f
    private var lengthRightTextOff = 0f

    private val radiusCorner = getFloatDimentionValue(R.dimen.radius_corner_toggle).dpToPx(context)

    //Distance from left slider to background layout
    //private val distanceToRight = getFloatDimentionValue(R.dimen.slider_toggle_distance_to_right).dpToPx(context)
    //private val sliderWidth = 270F.dpToPx(context)
    private val sliderHeight = getFloatDimentionValue(R.dimen.slider_height).dpToPx(context)

    private var mWidth = 0
    private var mHeight = 0

    private var moveAnim = 0f
    private var sliderLength = 0f
    private var distanceToRight = 0f
    private var rightMoveDis = 0f
    private var textAlpha = 255

    private val textAnimDuration = 800L
    private val animDuration = 500L

    private var isSelect = false
    private var onSelectedChangeListener: OnSelectedChangeListener? = null

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(attrs, defStyleAttr, defStyleRes)
    }

    fun init(attrs: AttributeSet?, defStyleAttr: Int = 0, defStyleRes: Int = 0) {
        var typedArray = context.obtainStyledAttributes(attrs, R.styleable.SwitchButton, defStyleAttr, defStyleRes)
        buttonBackgroundColor =
            typedArray.getColor(R.styleable.SwitchButton_btn_buttonBackgroundColor, buttonBackgroundColor)
        sliderColor = typedArray.getColor(R.styleable.SwitchButton_btn_sliderColor, sliderColor)
        selectTextColor = typedArray.getColor(R.styleable.SwitchButton_btn_selectTxtColor, selectTextColor)
        unSelectTextColor = typedArray.getColor(R.styleable.SwitchButton_btn_unSelectTxtColor, unSelectTextColor)

        initPaint()

        typedArray.recycle()
    }

    private fun initPaint() {
        buttonPaint.isAntiAlias = true
        buttonPaint.color = buttonBackgroundColor

        sliderPaint.isAntiAlias = true
        sliderPaint.color = sliderColor

        textPaint.apply {
            strokeWidth = 3f
            textAlign = Paint.Align.CENTER
            typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        mWidth = w
        mHeight = h
        buttonRectF = RectF(0f, 0f, mWidth.toFloat(), mHeight.toFloat())
        textRectF = RectF(0f, 0f, mWidth.toFloat(), mHeight.toFloat())
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var widthSpecSize = MeasureSpec.getSize(widthMeasureSpec)
        var heightSpecSize = MeasureSpec.getSize(heightMeasureSpec)

        widthSpecSize += paddingLeft + paddingRight
        heightSpecSize += paddingTop + paddingBottom
        val measuredWidth = resolveSizeAndState(Math.min(widthSpecSize, heightSpecSize), widthMeasureSpec, 0)
        val measuredHeight = resolveSizeAndState(Math.min(widthSpecSize, heightSpecSize), heightMeasureSpec, 0)
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        sliderLength = mWidth.times(getFloatDimentionValue(R.dimen.slider_width)).div(WIDTH_SIZE_FIX)
        distanceToRight =
            mWidth.times(getFloatDimentionValue(R.dimen.slider_toggle_distance_to_right)).div(WIDTH_SIZE_FIX)

        xLeftTextOn = mWidth.times(getFloatDimentionValue(R.dimen.text_left_on_toggle_margin)).div(WIDTH_SIZE_FIX)
        lengthLeftTextOn = mWidth.times(getFloatDimentionValue(R.dimen.text_left_on_toggle_width)).div(WIDTH_SIZE_FIX)
        xLeftTextOff = mWidth.times(getFloatDimentionValue(R.dimen.text_left_off_toggle_margin)).div(WIDTH_SIZE_FIX)
        lengthLeftTextOff = mWidth.times(getFloatDimentionValue(R.dimen.text_left_off_toggle_width)).div(WIDTH_SIZE_FIX)

        xRightTextOn = mWidth.times(getFloatDimentionValue(R.dimen.text_right_on_toggle_margin)).div(WIDTH_SIZE_FIX)
        lengthRightTextOn = mWidth.times(getFloatDimentionValue(R.dimen.text_right_on_toggle_width)).div(WIDTH_SIZE_FIX)
        xRightTextOff = mWidth.times(getFloatDimentionValue(R.dimen.text_right_off_toggle_margin)).div(WIDTH_SIZE_FIX)
        lengthRightTextOff =
            mWidth.times(getFloatDimentionValue(R.dimen.text_right_off_toggle_width)).div(WIDTH_SIZE_FIX)

        fontMetrics = textPaint.fontMetricsInt

        canvas?.drawRoundRect(
            buttonRectF,
            getFloatDimentionValue(R.dimen.radius_corner_toggle).dpToPx(context),
            getFloatDimentionValue(R.dimen.radius_corner_toggle).dpToPx(context),
            buttonPaint
        )

        drawSlider(canvas)

        //When nearly completely anim draw icon and text
        if ((moveAnim < 0.2f && !isSelect) || (moveAnim > 0.5f && isSelect)) {
            drawIconShop(canvas)
            drawLeftText(canvas)
            drawRightText(canvas)
        }
    }

    private fun drawIconShop(canvas: Canvas?) {
        canvas?.let {
            val d = if (isSelect) resources.getDrawable(
                R.drawable.sg_ic_graphic_in_store_shopping,
                null
            ) else resources.getDrawable(R.drawable.sg_ic_graphic_online_shopping, null)
            val left = if (isSelect) mWidth.times(
                getFloatDimentionValue(R.dimen.icon_bound_left_on)
            ).div(WIDTH_SIZE_FIX).toInt() else mWidth.times(getFloatDimentionValue(R.dimen.icon_bound_left_off)).div(
                WIDTH_SIZE_FIX
            ).toInt()
            val top = 0
            val right =
                left + mWidth.times(getFloatDimentionValue(R.dimen.icon_bound_right)).div(WIDTH_SIZE_FIX).toInt()
            val bottom = top + getFloatDimentionValue(R.dimen.icon_bound_bottom).dpToPx(context).toInt()

            d.setBounds(left, top, right, bottom)
            d.draw(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (moveAnim == 0f || moveAnim == 1f) {
            isSelect = !isSelect
            startAnimator(isSelect)
            leftTextAlphaAnimator()
            onSelectedChangeListener?.onSelectedChange(if (isSelect) BUTTON_STATE_RIGHT else BUTTON_STATE_LEFT)
        }
        return super.onTouchEvent(event)
    }

    private fun drawSlider(canvas: Canvas?) {
        canvas?.save()

        //Draw a RectF move from margin -> sliderWidth+ rightMoveDis (Value of rightMoveDis gain by moveAnim)

        rightMoveDis = distanceToRight * Math.min(1f, moveAnim * 2)

        sliderRectF = RectF(rightMoveDis, 0F, sliderLength + rightMoveDis, sliderHeight)

        canvas?.drawRoundRect(sliderRectF, radiusCorner, radiusCorner, sliderPaint)

        canvas?.restore()
    }

    private fun drawLeftText(canvas: Canvas?) {
        canvas?.save()
        val leftText = if (isSelect) leftTextOff else leftTextOn
        val xLeftText = if (isSelect) xLeftTextOff else xLeftTextOn
        textRectF.left = xLeftText
        textRectF.right = if (isSelect) xLeftText.plus(lengthLeftTextOff) else xLeftText.plus(lengthLeftTextOn)
        canvas?.clipRect(textRectF)

        textPaint.apply {
            textSize = if (isSelect) unSelectTextSize else selectTextSize
            color = if (isSelect) unSelectTextColor else selectTextColor
            alpha = textAlpha
        }
        fontMetrics = textPaint.fontMetricsInt

        baseline = (textRectF.bottom + textRectF.top - fontMetrics.bottom - fontMetrics.top) / 2
        canvas?.drawText(leftText, textRectF.centerX(), baseline, textPaint)
        canvas?.restore()
    }

    private fun drawRightText(canvas: Canvas?) {
        canvas?.save()
        val rightText = if (isSelect) rightTextOn else rightTextOff
        val xRightText = if (isSelect) xRightTextOn else xRightTextOff

        textRectF.left = xRightText
        textRectF.right = if (isSelect) xRightText.plus(lengthRightTextOn) else xRightText.plus(lengthRightTextOff)
        canvas?.clipRect(textRectF)

        textPaint.apply {
            textSize = if (isSelect) selectTextSize else unSelectTextSize
            color = if (isSelect) selectTextColor else unSelectTextColor
            alpha = textAlpha
        }
        fontMetrics = textPaint.fontMetricsInt

        baseline = (textRectF.bottom + textRectF.top - fontMetrics.bottom - fontMetrics.top) / 2
        canvas?.drawText(rightText, textRectF.centerX(), baseline, textPaint)
    }

    private fun startAnimator(selectState: Boolean) {
        var anim = ValueAnimator.ofFloat(moveAnim, (if (selectState) 1f else 0f))
        anim.duration = animDuration
        anim.interpolator = AccelerateDecelerateInterpolator()
        anim.addUpdateListener {
            moveAnim = it.animatedValue as Float
            invalidate()
        }
        anim.start()
    }

    private fun leftTextAlphaAnimator() {
        val animator = ValueAnimator.ofInt(10, 255)
        animator.duration = textAnimDuration
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener {
            textAlpha = (it.animatedValue as Int)
            invalidate()
        }
        animator.start()
    }

    interface OnSelectedChangeListener {
        fun onSelectedChange(state: Boolean)
    }

    fun setOnSelectedChangeListener(listener: OnSelectedChangeListener) {
        onSelectedChangeListener = listener
    }

    private fun getFloatDimentionValue(dimentionId: Int): Float {
        val outValue = TypedValue()
        resources.getValue(dimentionId, outValue, true)
        return outValue.float
    }

    fun updateInitState() {
        moveAnim = 1f
        isSelect = true
        this.invalidate()
    }
}