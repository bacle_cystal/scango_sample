package ntuc.fairprice.omni.scango.ui.screen.feedback

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoItemFeedbackListBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.FeedbackOption

class ScangoFeedbackAdapter : RecyclerView.Adapter<ScangoFeedbackAdapter.AddressHolder>() {
    var options: List<FeedbackOption> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val binding: ScangoItemFeedbackListBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.scango_item_feedback_list, parent, false)
        return AddressHolder(binding)
    }

    override fun getItemCount(): Int = options.size

    override fun onBindViewHolder(holder: AddressHolder, position: Int) = holder.bind(options[position])

    inner class AddressHolder(val binding: ScangoItemFeedbackListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(option: FeedbackOption) {
            binding.apply {
                textItemStoreName.text = option.option_text
                cbFeedback.isChecked = option.isChecked
            }.executePendingBindings()
        }
    }
}