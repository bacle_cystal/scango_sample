package ntuc.fairprice.omni.scango.repository.db.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import java.util.*

@Dao
interface ScangoCartItemDao {
    @Insert(onConflict = REPLACE)
    fun insert(cartItem: ScangoCartItem): Long

    @Update
    fun update(cartItem: ScangoCartItem)

    @Delete
    fun delete(cartItem: ScangoCartItem)

    @Query("DELETE FROM ScangoCartItem")
    fun clearCart()

    @Insert(onConflict = REPLACE)
    fun insertAll(cartItems: ArrayList<ScangoCartItem>)

    @Query("SELECT * FROM ScangoCartItem")
    fun getCartItemsLiveData(): LiveData<List<ScangoCartItem>>

    @Query("SELECT * FROM ScangoCartItem")
    fun getCartItems(): List<ScangoCartItem>

    @Query("SELECT * FROM ScangoCartItem WHERE itemId = :itemId")
    fun getCartItem(itemId: Long): ScangoCartItem

    @Query("select SUM((mrp - discount) * quantity) from ScangoCartItem")
    fun getFinalPriceLiveData(): LiveData<Double>

    @Query("SELECT * FROM ScangoCartItem WHERE itemId = :itemId")
    fun getCartItemLiveData(itemId: Long): LiveData<ScangoCartItem>

    @Query("SELECT quantity FROM ScangoCartItem WHERE itemId = :itemId")
    fun getQuantityLiveData(itemId: Long): LiveData<Int>

    @Query("SELECT SUM(quantity) FROM ScangoCartItem")
    fun getTotalItemsLiveData(): LiveData<Int>

}