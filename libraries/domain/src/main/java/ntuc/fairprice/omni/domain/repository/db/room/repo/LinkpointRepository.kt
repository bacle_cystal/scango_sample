package ntuc.fairprice.omni.domain.repository.db.room.repo

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.domain.BuildConfig
import ntuc.fairprice.omni.domain.model.linkpoint.LinkPointStatus
import ntuc.fairprice.omni.domain.model.linkpoint.MemberData
import ntuc.fairprice.omni.domain.repository.webservice.service.LinkpointService
import ntuc.fairprice.omni.domain.model.linkpoint.MemberDetail
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import thor.zopsmart.com.thor.repository.db.room.repo.Repository
import thor.zopsmart.com.thor.repository.webservice.model.Result
import thor.zopsmart.com.thor.repository.webservice.model.getRetrofitResult

class LinkpointRepository constructor(mApp: Application, val mLinkpointService: LinkpointService) : Repository(mApp) {

    companion object {

        val TAG = "LinkpointRepository"
        const val BEARER_TOKEN_PREFIX = "Bearer "

        private val REST_ADAPTER = Retrofit.Builder()
            .baseUrl(BuildConfig.host_scango_lp)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        private val LINK_POINT_SERVICE = REST_ADAPTER.create(LinkpointService::class.java)

        fun getLinkpointService(): LinkpointService {
            return LINK_POINT_SERVICE
        }

        fun newInstance(mApp: Application): LinkpointRepository {

            return LinkpointRepository(mApp, getLinkpointService())

        }
    }

    suspend fun getCustomerLPStatus(id : String, accessToken: String): Result<LinkPointStatus> = withContext(Dispatchers.IO) {
        val authToken = BEARER_TOKEN_PREFIX + accessToken

        mLinkpointService.getCustomerLPStatus(id, authToken).getRetrofitResult {
            LinkPointStatus(
                it.data.linkStatus,
                it.data.pointBalance,
                it.data.dollarBalance
            )
        }

    }

    suspend fun unlinkLPAccount(id : String, accessToken: String): Result<MemberData> = withContext(Dispatchers.IO) {
        val authToken = BEARER_TOKEN_PREFIX + accessToken
        mLinkpointService.unlinkLPAccount(id, authToken).getRetrofitResult {
            it
        }
    }

//    suspend fun sendOTP(): Result<MemberDetail> = withContext(Dispatchers.IO) {
//        mLinkpointService.sendOTP().getRetrofitResult {
//            if (it.data != null) {
//                try {
//                    Success(MemberDetail(it.data!!))
//                } catch(throwable: Throwable) {
//                    Failure(Throwable("Member could not be parsed", throwable))
//                }
//            } else {
//                Failure(Exception("Member is empty"))
//            }
//        }
//
//    }
//
//    suspend fun validateOTP(otp: String): Result<MemberDetail> = withContext(Dispatchers.IO) {
//        mLinkpointService.validateOTP(otp).getRetrofitResult {
//            if (it.data != null) {
//                try {
//                    Success(MemberDetail(it.data!!))
//                } catch(throwable: Throwable) {
//                    Failure(Throwable("Member could not be parsed", throwable))
//                }
//            } else {
//                Failure(Exception("Member is empty"))
//            }
//        }
//
//    }
//
//    suspend fun putCustomers(id : String, link_type: Int, link_id: String): Result<MemberDetail> = withContext(Dispatchers.IO) {
//        mLinkpointService.putCustomers(id, link_type, link_id).getRetrofitResult {
//            if (it.data != null) {
//                try {
//                    Success(MemberDetail(it.data!!))
//                } catch(throwable: Throwable) {
//                    Failure(Throwable("Member could not be parsed", throwable))
//                }
//            } else {
//                Failure(Exception("Member is empty"))
//            }
//        }
//
//    }

}