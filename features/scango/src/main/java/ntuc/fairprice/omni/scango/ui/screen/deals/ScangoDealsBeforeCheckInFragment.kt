package ntuc.fairprice.omni.scango.ui.screen.deals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ntuc.fairprice.omni.scango.R

class ScangoDealsBeforeCheckInFragment : Fragment() {

    companion object {
        fun newInstance() = ScangoDealsBeforeCheckInFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.scango_fragment_deals_before_checkin, container, false)

}
