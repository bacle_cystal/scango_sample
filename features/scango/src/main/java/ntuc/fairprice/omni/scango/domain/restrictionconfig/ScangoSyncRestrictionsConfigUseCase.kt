package ntuc.fairprice.omni.scango.domain.restrictionconfig

interface ScangoSyncRestrictionsConfigUseCase {

    fun execute()
}