package ntuc.fairprice.omni.scango.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.RatingBar
import kotlin.math.roundToInt

class CeilRatingBar: RatingBar {

    private var publicOnRatingBarChangeListener: OnRatingBarChangeListener? = null

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    )
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?) : super(context)

    init {
        stepSize = 0.01f
        super.setOnRatingBarChangeListener {ratingBar, rating, fromUser ->
            if (fromUser) setRating(rating.roundToInt().toFloat())
            else publicOnRatingBarChangeListener?.onRatingChanged(ratingBar, rating, fromUser)
        }
    }

    override fun getOnRatingBarChangeListener() = publicOnRatingBarChangeListener

    override fun setOnRatingBarChangeListener(listener: OnRatingBarChangeListener?) {
        publicOnRatingBarChangeListener = listener
    }

}