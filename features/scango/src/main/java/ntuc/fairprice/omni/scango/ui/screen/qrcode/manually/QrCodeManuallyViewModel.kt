package ntuc.fairprice.omni.scango.ui.screen.qrcode.manually

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import javax.inject.Inject

class QrCodeManuallyViewModel @Inject constructor(
    application: Application
) : ScangoBaseViewModel(application) {

    var storeItem = MutableLiveData<ScangoStore>()
    var isBarcode = MutableLiveData<Boolean>()
}