package ntuc.fairprice.omni.scango.utils

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import ntuc.fairprice.omni.ui.R

fun View.touch() {
    this.dispatchTouchEvent(
        MotionEvent.obtain(
            0, 0,
            MotionEvent.ACTION_DOWN, 100f, 100f, 0.5f, 5f, 0, 1f, 1f, 0, 0
        )
    )
}

@BindingAdapter("refreshBottomNavStyleOnSelected")
fun refreshBottomNavStyleOnSelected(textView: TextView, isTabSelected: Boolean) {
    textView.apply {
        typeface = if (isTabSelected) {
            ResourcesCompat.getFont(
                context,
                R.font.lato_bold
            )
        } else {
            ResourcesCompat.getFont(context, R.font.lato)
        }
    }
}

fun dpToPx(context: Context, dp: Int): Int {
    return (context.resources.displayMetrics.density * dp).toInt()
}