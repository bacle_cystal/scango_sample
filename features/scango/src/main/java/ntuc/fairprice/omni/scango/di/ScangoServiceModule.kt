package ntuc.fairprice.omni.scango.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ntuc.fairprice.omni.scango.repository.webservice.service.StoreCheckoutService

@Module
abstract class ScangoServiceModule {

    @ContributesAndroidInjector
    abstract fun bindScangoCheckoutServiceInjector(): StoreCheckoutService
}