package ntuc.fairprice.omni.scango.ui.screen.receipt

import android.app.Activity
import dagger.Binds
import dagger.Module
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoReceiptActivityBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoReceiptActivity): Activity
}