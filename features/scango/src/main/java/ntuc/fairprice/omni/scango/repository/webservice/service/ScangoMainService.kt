package ntuc.fairprice.omni.scango.repository.webservice.service

import android.app.Application
import ntuc.fairprice.omni.scango.epoxy.toSGEpoxyPage
import ntuc.fairprice.omni.scango.epoxy.toSGProductListPage
import ntuc.fairprice.omni.scango.epoxy.toScangoEpoxyPage
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.jsonR
import thor.zopsmart.com.thor.base.extensions.safeJson
import thor.zopsmart.com.thor.epoxy.toEpoxyPage
import thor.zopsmart.com.thor.epoxy.toProductListPage
import thor.zopsmart.com.thor.features.widget.productcollection.data.ProductsWithFilter
import thor.zopsmart.com.thor.features.widget.productcollection.data.getFilters
import thor.zopsmart.com.thor.repository.webservice.model.Result
import thor.zopsmart.com.thor.repository.webservice.model.toSearchResults
import thor.zopsmart.com.thor.repository.webservice.service.BaseService
import thor.zopsmart.com.thor.repository.webservice.serviceclient.ZSRequest
import javax.inject.Inject

class ScangoMainService @Inject constructor(val application: Application) : BaseService(application) {

    suspend fun loadHomePage(storeId: String) = apiCall(
        "/api/layout/home?storeId=$storeId",
        ZSRequest.Method.GET
    ) {
        it.toScangoEpoxyPage()
    }

    suspend fun loadProductDetails(url: String) =
        apiCall("/api/layout/product?url=$url", ZSRequest.Method.GET) {
            it.toSGEpoxyPage()
        }

    suspend fun loadPage(slug: String, query: String?) = apiCall("/api/layout/$slug", ZSRequest.Method.GET) {
        it.toSGEpoxyPage(query)
    }

    suspend fun loadProducts(slug: String): Result<ProductsWithFilter> = apiCall(slug, ZSRequest.Method.GET) {
        val res = it.toSearchResults()
        val data = it safeJson  "data"
        val count = data int  "count" ?: 0
        val filters = (it jsonR listOf("data", "filters")).getFilters()
        val productWithFilter = ProductsWithFilter(res, filters, count)
        productWithFilter
    }

    suspend fun loadProductsPage(associatedData: JSONObject, slug: String, isPromo: Boolean = false, title: String? = null) = apiCall("/api/product?$slug", ZSRequest.Method.GET) {
        it.toSGProductListPage(associatedData, isPromo, title)
    }

}