package ntuc.fairprice.omni.scango.utils

import android.content.Context
import ntuc.fairprice.omni.scango.Constants
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.get
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Binh.TH on 3/21/2019.
 */

//getting current date

object DateTimeUtils {

    private val RFC_3339_FORMATS = arrayOf(
        "yyyy-MM-dd'T'HH:mm:ss'Z'",
        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
        "yyyy-MM-dd'T'HH:mm:ssXXX",
        "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
    )

    @JvmStatic
    fun getCurrentTime(): Date {
        val cal = Calendar.getInstance()
        val date = cal.time
        return date
    }

    fun getCurrentDayString(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return sdf.format(getCurrentTime())
    }

    fun getDayEEEddyyyyFromString(date: Date?): String {
        val sdf = SimpleDateFormat("MMM dd yyyy", Locale.US)
        return sdf.format(date)
    }

    fun getDayyyyyMMddFromString(date: Date?): String {
        val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.US)
        return sdf.format(date)
    }

    fun getHourFromString(date: Date?): String {
        val sdf = SimpleDateFormat("hh:mm a", Locale.US)
        return sdf.format(date)
    }

    //compare the dates
    @JvmStatic
    fun isBeforeDate10h30(date: Date): Boolean {
        val c1 = Calendar.getInstance()
        c1.set(Calendar.HOUR, 10)
        c1.set(Calendar.AM_PM, 1)
        c1.set(Calendar.MINUTE, 30)
        return date.before(c1.time)
    }

    //Get calendar by hour and minutes value, am:ampm=0, pm"ampm=1

    /**
     * Create to get calendar by hour and minutes value
     * @param hour Int
     * @param minutes Int
     * @param ampm Int am;0 pm:1
     * @return Calendar
     */
    fun getCalendarByValue(hour: Int, minutes: Int, ampm: Int): Calendar {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR, hour)
        cal.set(Calendar.AM_PM, ampm)
        cal.set(Calendar.MINUTE, minutes)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal
    }


    /**
     * Get calendar from time milis
     * @param millis Long
     * @return Long
     */
    fun getCalendarFromMillis(millis: Long): Calendar {
        val cal = Calendar.getInstance()
        cal.timeInMillis = millis
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal
    }


    /**
     * Create to get real time with timestamp
     * @param timeGet Long : Time need to get after delta time
     * @param timeStamp Long
     * @param apiDelayTime Long
     * @return time in milis
     */
    fun getServerTimeMilis(timeGet: Long, serverTimeGap: Long, apiDelayTime: Long): Long {
        return (timeGet + serverTimeGap + apiDelayTime)
    }

    fun getCurrentServerTimeMillis(context: Context): Long {
        val currentTime = System.currentTimeMillis()
        val serverTimeGap = LocalStorage.init(context).get(Constants.SHARE_PRE_SERVERGAP_TIME, 0L) ?: 0L
        return currentTime + serverTimeGap
    }

    fun getSecOfDay(currentTimeMillis: Long): Int {
        val c = Calendar.getInstance().apply {
            timeInMillis = currentTimeMillis
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        val passed = currentTimeMillis - c.timeInMillis
        return (passed / 1000).toInt()
    }

    fun getDateFromDistancetime(distanceTime: Int): String {
        val c = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        val timeNew = c.timeInMillis.plus(distanceTime * 1000)
        c.timeInMillis = timeNew
        val sdf = SimpleDateFormat("hh:mm a", Locale.US)
        return sdf.format(c.time)
    }

    fun getDateFromTimeBound(distanceTime: Int): Long {
        val c = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, 0)
            set(Calendar.MINUTE, 0)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }
        val timeNew = c.timeInMillis.plus(distanceTime * 1000)
        c.timeInMillis = timeNew
        return c.timeInMillis
    }

    fun checkTimeViolated(context: Context, inboundServerStart: Int, inboundServerEnd: Int) =
        if (inboundServerStart > inboundServerEnd) {
            //Next day
            (getSecOfDay(getCurrentServerTimeMillis(context)) > inboundServerStart || getSecOfDay(
                getCurrentServerTimeMillis(context)
            ) < inboundServerEnd)
        } else (getSecOfDay(getCurrentServerTimeMillis(context)) !in inboundServerStart..inboundServerEnd)

    /**
     * Parse the DataTime given in format RFC3339 to Milliseconds
     */
    fun parseRFC3339DateTime(dateTime: String): Long? {
        var timeStamp: Long? = null
        for (it in RFC_3339_FORMATS) {
            try {
                //This parse need change to UTC timezone
                val simpleDateFormat = SimpleDateFormat(it, Locale("GMT"))
                simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
                timeStamp = simpleDateFormat.parse(dateTime).time
                break
            } catch (e: Exception) {
                continue
            }
        }
        return timeStamp
    }
}


