package ntuc.fairprice.omni.fairpriceon

import android.content.Intent
import android.os.Bundle
import ntuc.fairprice.omni.BaseSplitActivity
import thor.zopsmart.com.thor.features.home.ui.HomeActivity

class FairpriceOnFeatureActivity : BaseSplitActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}
