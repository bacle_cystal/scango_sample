package ntuc.fairprice.omni.scango.ui.screen.promo.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.scango_layout_added_promo_item.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.R as RB

import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.utils.toFormattedString
import thor.zopsmart.com.thor.base.configurations.Config
import thor.zopsmart.com.thor.base.extensions.setVisibility
import thor.zopsmart.com.thor.model.Offer
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.scango_layout_added_promo_linkpoint.view.*
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.databinding.ScangoLayoutAddedPromoItemBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoLinkpointReceipt
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.LinkPointStatus


class ScangoAddedPromoViewList @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    var isEditable: Boolean = false
    var isReceipt: Boolean = false

    init {
        orientation = VERTICAL
        attrs?.let {
            val a = context.obtainStyledAttributes(it, R.styleable.ScangoCouponAddedList)
            with(a) {
                isEditable = getBoolean(R.styleable.ScangoCouponAddedList_isEditable, false)
                isReceipt = getBoolean(R.styleable.ScangoCouponAddedList_isReceipt, false)
                recycle()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun addPromos(
        coupons: List<ScangoCoupon>,
        promoRemoveListener: ScangoPromoRemoveListener? = null,
        jwcOffers: List<ScangoOffer>,
        linkpoint: ScangoLinkpointReceipt? = null
    ) {
        removeAllViews()

        jwcOffers.forEach {
            val addedPromoItemBinding: ScangoLayoutAddedPromoItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.scango_layout_added_promo_item,
                this,
                false
            )
            addedPromoItemBinding.setVariable(BR.isReceipt, isReceipt)
            addView(addedPromoItemBinding.root.apply {
                tv_promo_name.text = resources.getString(thor.zopsmart.com.thor.R.string.label_nav_jwc)
                tv_discount.text = "-${Config.CURRENCY} ${it.discount.toFormattedString()}"
                iv_remove.setVisibility(false)
            })
        }

        coupons.forEach { coupon ->
            val addedPromoItemBinding: ScangoLayoutAddedPromoItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.scango_layout_added_promo_item,
                this,
                false
            )
            addedPromoItemBinding.setVariable(BR.isReceipt, isReceipt)
            addView(addedPromoItemBinding.root.apply {
                tv_promo_name.text = coupon.coupon
                tv_discount.text = "-${Config.CURRENCY} ${coupon.finalDiscount.toFormattedString()}"
                iv_remove.setVisibility(isEditable)
                iv_remove.setOnClickListener {
                    promoRemoveListener?.onPromoRemoved(coupon)
                }
            })
        }


        linkpoint?.let {
            if (it.pointUsed != 0.0) {
                val addedLinkpointItemBinding: ntuc.fairprice.omni.scango.databinding.ScangoLayoutAddedPromoLinkpointBinding =
                    DataBindingUtil.inflate(
                        LayoutInflater.from(context),
                        R.layout.scango_layout_added_promo_linkpoint,
                        this,
                        false
                    )
                addedLinkpointItemBinding.setVariable(BR.isReceipt, isReceipt)
                addView(addedLinkpointItemBinding.root.apply {
                    tv_lp_point_number.text = "-" + context.getString(RB.string.two_decimal_formatted_val, it.pointUsed)
                    tv_lp_point_dollar.text =
                        "-${Config.CURRENCY}" + context.getString(RB.string.two_decimal_formatted_val, it.dollarUsed)
                })
            }
        }
    }
}

interface ScangoPromoRemoveListener {
    fun onPromoRemoved(coupon: ScangoCoupon)
}