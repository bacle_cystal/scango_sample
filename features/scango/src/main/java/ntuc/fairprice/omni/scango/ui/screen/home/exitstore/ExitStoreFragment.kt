package ntuc.fairprice.omni.scango.ui.screen.home.exitstore

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_exit_store.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentExitStoreBinding
import javax.inject.Inject

class ExitStoreFragment : ScangoBaseFragment<ScangoFragmentExitStoreBinding, ExitStoreViewModel>(),
    HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override val layoutId: Int
        get() = R.layout.scango_fragment_exit_store

    companion object {
        fun newInstance() = ExitStoreFragment()
        val TAG = "ExitStoreFragment"

        const val STORE_NAME = "bundle-exit-store-name"
        fun newInstance(storeName: String) = ExitStoreFragment().apply {
            arguments = Bundle().apply {
                putString(STORE_NAME, storeName)
            }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ExitStoreViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ExitStoreViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.scango_fragment_exit_store, container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.apply {
            storeName.value = arguments?.getString(STORE_NAME)
        }

        btn_cancel.setOnClickListener { callBackListener?.onStoreCancelClicked() }
        btn_exit_confirm.setOnClickListener { callBackListener?.onStoreExitClicked() }
    }

    private var callBackListener: ExitStoreCallBackListener? = null

    fun setCallbackListener(callback: ExitStoreCallBackListener) {
        this.callBackListener = callback
    }

    interface ExitStoreCallBackListener {
        fun onStoreExitClicked()
        fun onStoreCancelClicked()
    }

}
