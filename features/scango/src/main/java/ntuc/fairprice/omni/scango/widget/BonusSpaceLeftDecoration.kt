package ntuc.fairprice.omni.scango.widget

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R

class BonusSpaceLeftDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val itemPosition = parent.getChildAdapterPosition(view)
        if (state.itemCount == 0) return
        when (itemPosition) {
            RecyclerView.NO_POSITION -> return
            0 -> outRect.set(view.context.resources.getDimension(R.dimen.dp_16).toInt(), 0, 0, 0)
        }
    }
}