package ntuc.fairprice.omni.scango.repository.webservice.model

data class Return(
    val returns: List<ReturnX>
) {
    data class ReturnX(
        val createdAt: String?,
        val items: List<Item>?,
        val total: Double?
    ) {
        data class Item(
            val discount: Double?,
            val id: Int?,
            val mrp: Double?,
            val quantity: Int?,
            val reason: String?
        )
    }
}