package ntuc.fairprice.omni.scango.utils

import android.graphics.Bitmap
import android.net.Uri
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder

sealed class ScanData {
    data class NormalItem(val barcode: String) : ScanData()
    data class WeightedItemWithWeight(val barcode: String, val price: Double, val weight: Double) : ScanData()
//    data class WeightedItemWithQuantity(val barcode: String, val price: Double, val quantity: Int) : ScanData()
    data class QrCode(val code: String) : ScanData()
}

fun ScanData.getBarcode(): String {
    return when (this) {
        is ScanData.NormalItem -> {
            this.barcode
        }
        is ScanData.WeightedItemWithWeight -> {
            this.barcode
        }

//        is ScanData.WeightedItemWithQuantity -> {
//            this.barcode
//        }
        else -> ""
    }
}

object BarcodeQRCodeUtils {

    private val String.barcodeSeriesIdentifier: String
        get() = this.substring(0, 2)

    private val String.isEAN13Series: Boolean
        get() = (this.length == 13 && this.barcodeSeriesIdentifier == "21")

    private val String.isITFSeries: Boolean
        get() = (this.length == 18 && this.barcodeSeriesIdentifier == "22")

    private val String.isITF25Series: Boolean
        get() = (this.length == 18 && this.barcodeSeriesIdentifier == "25")

    fun getScanData(scannedData: String, type: Int): ScanData {

        try {
            return when (type) {
                FirebaseVisionBarcode.FORMAT_UPC_E -> {
                    //Normal Item Barcode
                    getNormalItemFrom_UPCE(scannedData)
                }

                FirebaseVisionBarcode.FORMAT_ITF,
                FirebaseVisionBarcode.FORMAT_EAN_13 -> {
                    //Return normal item when Weighted item is unknown
                    getWeightedItem(scannedData) ?: ScanData.NormalItem(scannedData.removePrefix("0"))
                }

                FirebaseVisionBarcode.FORMAT_QR_CODE -> {
                    ScanData.QrCode(scannedData)
                }

                else -> {

                    getNormalItemFrom_UPCE("8990800348056")
                    //Get scan data for unknown scanned type or manually entered
//                    getScanDataForUnknownType(scannedData)
                }
            }
        } catch (e: Exception) {
            return ScanData.NormalItem(scannedData.removePrefix("0"))
        }
    }

    private fun getScanDataForUnknownType(scannedData: String): ScanData {

        return when {
            scannedData.isEAN13Series -> {
                getScanData(scannedData, FirebaseVisionBarcode.FORMAT_EAN_13)
            }

            scannedData.isITFSeries ||
                scannedData.isITF25Series -> {
                getScanData(scannedData, FirebaseVisionBarcode.FORMAT_ITF)
            }

            else -> {
                //Unknown Format
                ScanData.NormalItem(scannedData.removePrefix("0"))
            }
        }
    }

    private fun getNormalItemFrom_UPCE(barcode: String): ScanData.NormalItem {
        //Normal Item
        val convertedBarCode = when (barcode[6].toString().toInt()) {
            in 0..2 ->
                barcode.substring(0, 3) + barcode[6] + "0000" + barcode.substring(3, 6) + barcode[7]
            3 ->
                barcode.substring(0, 4) + "00000" + barcode.substring(4, 6) + barcode[7]
            4 ->
                barcode.substring(0, 5) + "00000" + barcode[5] + barcode[7]
            in 5..9 ->
                barcode.substring(0, 6) + "0000" + barcode[6] + barcode[7]
            else -> barcode
        }
        return ScanData.NormalItem(convertedBarCode.removePrefix("0"))
    }

    private fun getWeightedItem(barcode: String): ScanData? {

        if (!verifyCheckSum(barcode)) return null

        val scanData: ScanData.WeightedItemWithWeight? = when {
            barcode.isEAN13Series -> {
                val EANComponent = barcode.substring(2, 8)
                val price = barcode.substring(8, 12)

                val priceNumber = "${price.substring(0, 2)}.${price.substring(2, 4)}".toDouble()
                val weightNumber = 1.0
                ScanData.WeightedItemWithWeight("210000$EANComponent", priceNumber, weightNumber)
            }
            barcode.isITFSeries -> {
                val EANComponent = barcode.substring(2, 7)
                val weightOrQuantity = barcode.substring(7, 12)
                val price = barcode.substring(12, 17)

                val priceNumber = "${price.substring(0, 3)}.${price.substring(3, 5)}".toDouble()

//                if (weightOrQuantity.endsWith("000")) {
//                    val quantityNumber = weightOrQuantity.substring(0, 2).toInt()
//                    ScanData.WeightedItemWithQuantity("2200000$EANComponent", priceNumber, quantityNumber)
//                } else {
//
//                }
                val weightNumber = "${weightOrQuantity.substring(0, 2)}.${weightOrQuantity.substring(2, 5)}".toDouble()
                ScanData.WeightedItemWithWeight("2200000$EANComponent", priceNumber, weightNumber)
            }
            barcode.isITF25Series -> {
                val EANComponent = barcode.substring(2, 8)
                val price = barcode.substring(10, 17)

                val priceNumber = "${price.substring(0, 5)}.${price.substring(5, 7)}".toDouble()
                val weightNumber = 1.0
                ScanData.WeightedItemWithWeight("250000$EANComponent", priceNumber, weightNumber)
            }
            else -> {
                null
            }
        }

        if(scanData == null || scanData.price == 0.0 || scanData.weight == 0.0) {
            return null
        }
        return scanData
    }

    private fun verifyCheckSum(barcode: String): Boolean {
        val checkSum = barcode.last().toString().toInt()
        var sum = 0
        var shouldMultiply = true
        for (pos in barcode.length - 2 downTo 0) {
            var digit = barcode.get(pos).toString().toInt()
            if (shouldMultiply) {
                digit *= 3
            }
            shouldMultiply = !shouldMultiply
            sum += digit
        }
        return (sum + checkSum) % 10 == 0
    }

    fun getQRImageFromText(textInput: String, width: Int, height: Int): Bitmap {
        val multiFormatWriter = MultiFormatWriter()
        val bitMatrix = multiFormatWriter.encode(
            if (textInput.isEmpty()) "TEMP_INVALID_SESSION" else textInput,
            BarcodeFormat.QR_CODE, width,
            height
        )
        val barcodeEncoder = BarcodeEncoder()
        return barcodeEncoder.createBitmap(bitMatrix)
    }

    fun getStoreIDFromQRCode(urlInput: String): String {
        return try {
            //Here we handle 2 cases:
            // https://app.adjust.com/abcdxyz?deep_link=scango://storecheckin?storeid=5
            // and scango://storecheckin?storeid=5
            Uri.parse(Uri.parse(urlInput).getQueryParameter("deep_link")).getQueryParameter("storeid") ?: ""
        } catch (e: NullPointerException) {
            Uri.parse(urlInput).getQueryParameter("storeid") ?: ""
        } catch (e: Exception) {
            ""
        }
    }
}
