package ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import javax.inject.Inject

class ScangoSessionExpiredViewModel @Inject constructor(
    application: Application,
    val scangoStoreRepository: ScangoStoreRepository
) : ScangoBaseViewModel(application) {

    val _storeName = MutableLiveData<String>()

    fun checkStoreCached() {
        _storeName.value = scangoStoreRepository.getStoreNameCached()
    }
}