package ntuc.fairprice.omni.scango.ui.screen.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_turorial.*
import kotlinx.android.synthetic.main.scango_view_home_lp_banner.*
import ntuc.fairprice.omni.scango.Constants.INTENT_LINK_POINT_REFRESH
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentTurorialBinding
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import ntuc.fairprice.omni.scango.ui.screen.tutorial.HomeTutorialFragment
import javax.inject.Inject


class ScangoTutorialFragment : ScangoBaseFragment<ScangoFragmentTurorialBinding, ScangoTutorialViewModel>(),
    HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override val layoutId: Int = R.layout.scango_fragment_turorial

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ScangoTutorialViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoTutorialViewModel::class.java)
    }

    companion object {
        const val TYPESCREEN = "typescreen"
        fun newInstance(isOnStore: Boolean) = ScangoTutorialFragment().apply {
            arguments = Bundle().apply {
                putBoolean(TYPESCREEN, isOnStore)
            }
        }

        val TAG = "ScangoTutorialFragment"
    }

    lateinit var listTutorialItem: MutableList<TutorialGuideItem>
    lateinit var listStoreAddress: MutableList<StoreAddressItem>

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(activity as Context).unregisterReceiver(linkPointRefreshReceiver)
    }

    override fun onPause() {
        super.onPause()
        val intentFilter = IntentFilter(INTENT_LINK_POINT_REFRESH)
        LocalBroadcastManager.getInstance(activity as Context).registerReceiver(linkPointRefreshReceiver, intentFilter)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recycler_tutorial.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = scangoTutorialAdapter
        }

        recycler_store_address.apply {
            val itemDecorator: DividerItemDecoration
            context?.let {
                itemDecorator = DividerItemDecoration(it, DividerItemDecoration.VERTICAL)
                ContextCompat.getDrawable(it, R.drawable.divider)?.let { it1 -> itemDecorator.setDrawable(it1) }
                addItemDecoration(itemDecorator)
            }
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = scangoAddressAdapter
        }

        initTutorialList()
        initAddressList()

        txt_link_point_banner_detail.setOnClickListener {
            showLinkPlusScreen()
        }
        txt_link_point_banner_header.setOnClickListener {
            showLinkPlusScreen()
        }

        img_linkpoint_banner_close.setOnClickListener {
            vf_linkpoint_banner_container?.displayedChild = 1
            viewModel.userDismissedLpBanner()
            Handler().postDelayed({ vf_linkpoint_banner_container?.visibility = View.GONE }, 3000)
        }

        viewModel.shouldDisplayLpBannerLiveData.observe(this, Observer {
            vf_linkpoint_banner_container?.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.checkIfLpLinked()
    }

    // Adds animals to the empty animals ArrayList
    fun initTutorialList() {
        context?.let {
            listTutorialItem = mutableListOf(
                TutorialGuideItem(R.drawable.sg_tutorial_1_small, getString(R.string.text_tutorial_1_header), 0),
                TutorialGuideItem(R.drawable.sg_tutorial_2_small, getString(R.string.text_tutorial_2_header), 1),
                TutorialGuideItem(R.drawable.sg_tutorial_3_small, getString(R.string.text_tutorial_3_header), 2)
            )
        }
        scangoTutorialAdapter.items = listTutorialItem
        scangoTutorialAdapter.notifyDataSetChanged()
    }

    fun initAddressList() {
        context?.let {
            listStoreAddress = arrayListOf(
                StoreAddressItem(
                    "Funan Mall",
                    "109 North Bridge Rd, Singapore 179097",
                    true
                )
//                StoreAddressItem(
//                    "FairPrice, Singpost Paya Lebar",
//                    "10 Eunos Road 8 #B1-101 to 107 SingPost Centre, Singapore 408600",
//                    true
//                ),
//                StoreAddressItem(
//                    "FairPrice 888 Woodlands Dr 50",
//                    "100 Tras Street, #B1-01, 100Am, Singapore 079027",
//                    false
//                ),
//                StoreAddressItem(
//                    "FairPrice AMARA ",
//                    "109 North Bridge Rd, Singapore 179097",
//                    true
//                )
            )
        }
        scangoAddressAdapter.items = listStoreAddress
        scangoAddressAdapter.notifyDataSetChanged()
    }

    private val scangoAddressAdapter: ScangoAddressAdapter by lazy {
        ScangoAddressAdapter()
    }

    private val scangoTutorialAdapter: ScangoTutorialAdapter by lazy {
        ScangoTutorialAdapter(context!!, MyOnItemClickListener(this))
    }

    class MyOnItemClickListener(val fragment: Fragment) : ScangoTutorialAdapter.OnItemClickListener {
        override fun onItemClick(item: TutorialGuideItem) {
            fragment.fragmentManager?.let {
                val fragment = HomeTutorialFragment.newInstance(item.position, -1)
                fragment.show(
                    it,
                    HomeTutorialFragment.TAG
                )
            }
        }
    }

    private fun showLinkPlusScreen() {
        startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(activity as Context))
    }

    private val linkPointRefreshReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            vf_linkpoint_banner_container?.visibility = View.GONE
            viewModel.checkIfLpLinked()
        }
    }
}
