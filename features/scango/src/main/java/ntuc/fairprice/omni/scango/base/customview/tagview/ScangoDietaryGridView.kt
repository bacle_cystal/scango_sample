package ntuc.fairprice.omni.scango.base.customview.tagview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_layout_product_dietary.view.*
import ntuc.fairprice.omni.scango.R

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ScangoDietaryGridView @JvmOverloads constructor(
    mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(mContext, attrs, defStyleAttr) {
    init {
        View.inflate(context, R.layout.scango_layout_product_dietary, this)
    }

    @ModelProp
    fun gridData(gridData: List<String>) {
        (tagview as TagView<String>).clear()
        (tagview as TagView<String>).setTags(gridData, object : DataTransform<String> {
            override fun transfer(item: String): String {
                return item
            }

            override fun transferIcon(item: String): Int {
                return when (item) {
                    "Halal" -> R.drawable.sg_ic_product_type_halal
                    "Healthier-Choice", "Healthier Choice" -> R.drawable.sg_ic_product_type_heathier_choice
                    "Gluten-Free" -> R.drawable.sg_ic_product_type_gluten_free
                    "Lactose-Free" -> R.drawable.sg_ic_product_type_lactose_free
                    "Organic" -> R.drawable.sg_ic_product_type_organic
                    "Trans-Fat Free"-> R.drawable.sg_ic_product_type_trans_fat_free
                    "Vegetarian" -> R.drawable.sg_ic_product_type_vegetarian
                    else -> R.drawable.sg_ic_product_type_halal
                }
            }
        })
    }
}