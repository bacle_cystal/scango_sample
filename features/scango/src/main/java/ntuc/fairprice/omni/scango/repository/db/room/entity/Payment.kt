package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity

@Entity
data class Payment(
    var imageId: String?,
    var customerName: String?,
    var cardNumber: String?,
    var cardType: String?,
    var dayTime: String?,
    var hourTime: String?,
    var address: String?,
    var storeName: String?,
    var qrcode: String?,
    var moneyNumber: Double?,
    var listInvoice: List<ScangoItemInvoice>?
)