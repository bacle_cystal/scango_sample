package ntuc.fairprice.omni.scango.repository.restrictionsconfig

import android.app.Application
import android.content.SharedPreferences
import com.google.gson.Gson
import ntuc.fairprice.omni.scango.Constants.KEY_RESTRICTIONS_CONFIG
import ntuc.fairprice.omni.scango.repository.db.room.entity.RestrictionsConfig
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoRepository
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoVerificationService
import ntuc.fairprice.omni.scango.utils.loadJSONFromAsset
import thor.zopsmart.com.thor.repository.db.sharedpref.set
import javax.inject.Inject

class ScangoRestrictionsConfigRepositoryImpl @Inject constructor(
    private val application: Application,
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson,
    private val scangoVerificationService: ScangoVerificationService
) : ScangoRestrictionsConfigRepository, ScangoRepository(application) {

    /**
     * Return config from any one of the following
     * 1. Shared preference
     * 2. From local config file stored in assets folder (On returning from local file, store it in shared pref to avoid reading second time)
     */
    override fun getConfig(): RestrictionsConfig? {
        var config: RestrictionsConfig? = null
        try {
            //Check if the data is available in shared preference
            val restrictionsConfig = sharedPreferences.getString(KEY_RESTRICTIONS_CONFIG, null)
            if (restrictionsConfig == null) {
                //If not available in sharedpref then load it from local json
                val localRestrictionsConfig = getConfigFromLocal()
                if (localRestrictionsConfig != null) {
                    saveConfig(localRestrictionsConfig)
                    config = gson.fromJson(localRestrictionsConfig, RestrictionsConfig::class.java)
                }
            } else {
                config = gson.fromJson(restrictionsConfig, RestrictionsConfig::class.java)
            }
        } catch (e: Exception) {
            //Do nothing just return null
        }
        return config
    }

    override fun saveConfig(config: String) {
        sharedPreferences.set(KEY_RESTRICTIONS_CONFIG, config)
    }

    override suspend fun syncConfig() = scangoVerificationService.getRestrictionsConfig()

    private fun getConfigFromLocal(): String? = loadJSONFromAsset(application, "restrictions/config.json")
}