package ntuc.fairprice.omni.scango.ui.screen.uicheck

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_ui_check.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentUiCheckBinding
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorNoticeFragment
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackDialogFragment
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import javax.inject.Inject

class UiCheckFragment :
    ScangoBaseBottomSheetFragment<ScangoFragmentUiCheckBinding, UiCheckViewModel>(),
    HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override val layoutId: Int
        get() = R.layout.scango_fragment_ui_check

    companion object {
        fun newInstance() = UiCheckFragment()
        val TAG = "UiCheckFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: UiCheckViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(UiCheckViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.scango_fragment_ui_check, container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_feedback.setOnClickListener {
            fragmentManager?.let {
                val fragment = FeedbackDialogFragment.newInstance("")
                fragment.show(
                    it,
                    FeedbackDialogFragment.TAG
                )
            }
            dismiss()
        }

        btn_liquor.setOnClickListener {
            fragmentManager?.let {
                val fragment = LiquorNoticeFragment.newInstance()
                fragment.show(
                    it,
                    LiquorNoticeFragment.TAG
                )
            }
            dismiss()
        }


        /*btn_receipt_confirm.setOnClickListener {
            fragmentManager.let {
                val fragment = ScangoReceiptConfirmationFragment.newInstance()
                fragment.show(
                    it,
                    ScangoReceiptConfirmationFragment.TAG
                )
            }
            dismiss()
        }*/

        btn_link_plus_prefill.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(activity as Context))
            dismiss()
        }

        btn_link_plus_edit.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(activity as Context))
            dismiss()
        }
    }
}
