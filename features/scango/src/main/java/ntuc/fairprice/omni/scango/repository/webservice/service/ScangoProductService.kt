package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.toProduct
import ntuc.fairprice.omni.scango.repository.webservice.model.toSearchResults
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.model.Item
import javax.inject.Inject

@FeatureScope
class ScangoProductService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) : ScangoBaseService(scangoRequestProvider) {

    suspend fun getProductByBarcode(barcode: String, storeId: Int): Result<Product> {
        return apiCall(
            "/api/product?barcode=$barcode&storeId=$storeId",
            ScangoRequestProvider.Method.GET,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it.toProduct()
        }
    }

    suspend fun getProductsByBarcodes(barcodes: List<String>, storeId: Int): Result<List<Item>> {
        return apiCall(
            "/api/product?barcode=${barcodes.joinToString(",")}&storeId=$storeId",
            ScangoRequestProvider.Method.GET,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it.toSearchResults()
        }
    }
}