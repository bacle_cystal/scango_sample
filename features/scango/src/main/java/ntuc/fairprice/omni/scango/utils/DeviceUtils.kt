package ntuc.fairprice.omni.scango.utils

import android.app.Application
import android.provider.Settings
import thor.zopsmart.com.thor.di.ApplicationScope
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class DeviceUtils @Inject constructor(val application: Application) {

    val deviceId: String
        get() = Settings.Secure.getString(
            application.contentResolver,
            Settings.Secure.ANDROID_ID
        )
}