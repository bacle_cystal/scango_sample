package ntuc.fairprice.omni.scango.widget

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.*
import androidx.lifecycle.ViewModel
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoLayoutTitleEditTextBinding

/**
 * custom title edit text
 * sample useage
 *
<jp.cp.cosmetics.ui.widgets.TitleEditText
android:id="@+id/title_edit"
content="@={ viewModel.text }"
error="@{ viewModel.error }"
hint="@{ @string/menu_title }"
title="@{ @string/menu_title }"
android:layout_width="match_parent"
android:layout_height="wrap_content"
app:layout_constraintStart_toStartOf="parent"
app:layout_constraintTop_toTopOf="parent"
app:lines="1"
app:maxLines="1" />
 */

class TitleEditText(context: Context, attributeSet: AttributeSet) :
    ConstraintLayout(context, attributeSet), View.OnTouchListener {
    init {
        initView(context, attributeSet)
    }

    companion object {
        const val PHONE_DIGITS = "1234567890+"
    }

    val content = ObservableField<String>()

    lateinit var viewBinding: ScangoLayoutTitleEditTextBinding

    private fun initView(context: Context, attributeSet: AttributeSet) {
        viewBinding = DataBindingUtil.inflate<ScangoLayoutTitleEditTextBinding>(
            LayoutInflater.from(context),
            R.layout.scango_layout_title_edit_text,
            this,
            true
        ).apply {
            view = this@TitleEditText
        }
        val typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.TitleEditText)
        try {
            viewBinding.apply {
                edtContent.apply {
                    setOnTouchListener(this@TitleEditText)

                    val maxLength = typedArray.getInt(R.styleable.TitleEditText_maxLength, 0)
                    if (maxLength > 0) {
                        filters = arrayOf<InputFilter>(InputFilter.LengthFilter(maxLength))
                    }

                    val inputType = typedArray.getInt(
                        R.styleable.TitleEditText_android_inputType,
                        InputType.TYPE_CLASS_TEXT
                    )
                    setInputType(inputType)

                    val customDigits =
                        typedArray.getString(R.styleable.TitleEditText_android_digits)
                    if (customDigits.isNullOrEmpty().not()) {
                        keyListener = DigitsKeyListener.getInstance(customDigits!!)
                    }

                    /*when (inputType) {
                     InputType.TYPE_CLASS_PHONE -> {
                            keyListener = DigitsKeyListener.getInstance(PHONE_DIGITS)
                        }
                        else -> {

                        }
                    }*/
                    val imeOptions =
                        typedArray.getInt(R.styleable.TitleEditText_android_imeOptions, -1)
                    if (imeOptions != -1) {
                        setImeOptions(imeOptions)
                    }

                    val lines = typedArray.getInt(R.styleable.TitleEditText_lines, 1)
                    if (lines > 1) {
                        setLines(lines)
                    }

                    val maxLines = typedArray.getInt(R.styleable.TitleEditText_maxLines, 1)
                    if (maxLines > 1) {
                        setMaxLines(maxLines)
                        gravity = Gravity.TOP
                    }

                    val contentSize = typedArray.getDimensionPixelSize(R.styleable.TitleEditText_contentSize, 0)
                    if (contentSize > 0) {
                        setTextSize(TypedValue.COMPLEX_UNIT_PX, contentSize.toFloat())
                    }
                }
                textTitle.apply {
                    val titleSize = typedArray.getDimensionPixelSize(R.styleable.TitleEditText_titleSize, 0)
                    if (titleSize > 0) {
                        setTextSize(TypedValue.COMPLEX_UNIT_PX, titleSize.toFloat())
                    }
                }
            }
        } finally {
            typedArray.recycle()
        }
    }

    /*fun setError(text: String) {
        viewBinding.apply {
            textError.text = text
            if (text.isBlank()) {
                textError.visibility = View.GONE
                edtContent.background = context?.getDrawable(R.drawable.edit_text_selector)
            } else {
                textError.visibility = View.VISIBLE
                edtContent.background = context?.getDrawable(R.drawable.edit_text_error)
            }
        }
    }*/

    override fun onTouch(view: View?, event: MotionEvent?): Boolean {
        if (view is AppCompatEditText) if (view.lineCount < 4) return false
        view?.parent?.requestDisallowInterceptTouchEvent(true)
        if ((event?.action?.let { it }!! and MotionEvent.ACTION_UP) != 0 && (event.actionMasked and MotionEvent.ACTION_UP) != 0) {
            view?.parent?.requestDisallowInterceptTouchEvent(false)

        }
        return false
    }
}

@BindingAdapter(value = ["title", "hint", "viewModel"], requireAll = false)
fun customTitleEditText(
    view: TitleEditText,
    textTitle: String?,
    textHint: String?,
    viewModel: ViewModel?
) {
    view.viewBinding.let { binding ->
        binding.textTitle.apply {
            if (textTitle.isNullOrEmpty()) {
                visibility = View.GONE
            } else {
                visibility = View.VISIBLE
                text = textTitle
            }
        }
        binding.edtContent.hint = textHint
    }
}

@BindingAdapter(value = ["content", "contentAttrChanged"], requireAll = false)
fun bindContent(
    view: TitleEditText,
    value: String?,
    inverseBindingListener: InverseBindingListener?
) {
    view.content.apply {
        set(value)
        addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable, propertyId: Int) {
                inverseBindingListener?.onChange()
            }
        })
    }
}

@InverseBindingAdapter(attribute = "content", event = "contentAttrChanged")
fun bindContentInverseAdapter(view: TitleEditText): String? {
    return view.content.get()
}