package ntuc.fairprice.omni.scango.base.customview

import android.content.Context
import android.text.InputFilter
import android.text.method.DigitsKeyListener
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.scango_label_edit_text.view.*
import ntuc.fairprice.omni.scango.base.customview.inputfilter.CreditCardExpiryInputFilter
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.CardNumberFormatInputFilter
import ntuc.fairprice.omni.scango.utils.showKeyboard
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.base.extensions.*
import ntuc.fairprice.omni.scango.R as RSG

/**
 * Custom text input layout with floating label hint.
 */
class ScangoLabelEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : TextInputLayout(context, attrs, defStyleAttr) {

    var hint: String = ""
    var title: String = ""
    var isClearButtonVisible = true
    var isShowApply = false

    init {
        LayoutInflater.from(context).inflate(RSG.layout.scango_label_edit_text, this)
        if (attrs != null) {
            val ta = getContext().obtainStyledAttributes(attrs, RSG.styleable.ScangoLabelEditText)
            hint = ta.getString(RSG.styleable.ScangoLabelEditText_floating_hint) ?: ""
            title = ta.getString(RSG.styleable.ScangoLabelEditText_edittext_title) ?: ""
            isShowApply = ta.getBoolean(RSG.styleable.ScangoLabelEditText_is_show_apply, false)
            isClearButtonVisible = ta.getBoolean(RSG.styleable.ScangoLabelEditText_showClearButton, true)
            ta.recycle()
        }

        custom_edit_text.setOnFocusChangeListener { _, focus ->
            if (focus) {
                custom_edit_text.translationY = 0F
                setFocusState()
            } else {
                if (custom_edit_text.text.isNullOrEmpty()) {
                    text_hint.visibility = View.VISIBLE
                    text_card_title.visibility = View.GONE
                }
                iv_close.visibility = View.GONE
                custom_edit_text.translationY = if (isEmpty()) -3F else 0F
                custom_layout_edittext.setBackgroundResource(R.drawable.edit_text_border)
            }
        }

        custom_edit_text.textListener {
            textChangeAction()
        }

        iv_close.setOnClickListener {
            custom_edit_text.clearText()
        }


        tv_apply.setOnClickListener {
            applyClick?.onClick(it)
        }

        setDefaultState()
        text_hint.text = hint
        text_card_title.text = title
        text_hint.setOnClickListener {
            custom_edit_text.requestFocus()
            custom_edit_text.showKeyboard()
        }
    }

    override fun setError(errorText: CharSequence?) {
        super.setError(errorText)
        if (!errorText.isNullOrEmpty())
            custom_layout_edittext.setBackgroundResource(R.drawable.bg_label_edit_text_error)
        else
            custom_layout_edittext.setBackgroundResource(R.drawable.border_category_selected)
    }

    val textChangeAction = {
        custom_edit_text.translationY = if (isEmpty() && custom_edit_text.isFocused) -3F else 0F
        iv_close.visibility =
            if (!isEmpty() && custom_edit_text.isFocused && isClearButtonVisible) View.VISIBLE else View.GONE
        tv_apply.visibility =
            if (!isEmpty() && custom_edit_text.isFocused && isShowApply) View.VISIBLE else View.GONE
    }

    /**
     * Gets the string from the edit text
     */
    fun getString(): String = custom_edit_text.getString()

    /**
     * Allows user to implement a search functionality.
     */
    fun addTextListener(textChangeAction: (String) -> Unit) {
        custom_edit_text.textListener {
            textChangeAction()
            textChangeAction(it)
        }
    }

    /**
     * Clears the text in the edit text.
     */
    fun clearText() {
        custom_edit_text.clearText()
    }

    /**
     * Returns true if edit text is filled and vice versa
     */
    fun isEmpty() = custom_edit_text.getString().isEmpty()

    /**
     * Hides the password typed in the EditText field.
     * Shown as *****
     */
    fun hidePassword() {
        custom_edit_text.hidePassword()
    }

    /**
     * Reverts the changes done by [.hidePassword] and shows the text as it is
     */
    fun showPassword() {
        custom_edit_text.showPassword()
    }

    /**
     * Set text in the edit text.
     * @param text is the text to be added in the edit text
     * @param isEnabled allows the user to decide if it should be enabled or not after edit text is set
     */
    fun setText(text: String, isEnabled: Boolean, cursorEnd: Boolean = false) {
        custom_edit_text.setTextValue(text)
        if (!cursorEnd)
            custom_edit_text.setSelection(0)
        custom_edit_text.isEnabled = isEnabled
        custom_edit_text.clearFocus()
    }

    fun setNumberInput(lengthLimit: Int = -1) {
        custom_edit_text.keyListener = DigitsKeyListener.getInstance("0123456789")
        if (lengthLimit != -1)
            custom_edit_text.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(lengthLimit))
    }

    fun setNumberPasswordInput(lengthLimit: Int = -1) {
        custom_edit_text.keyListener = DigitsKeyListener.getInstance("0123456789")
        custom_edit_text.setSelection(custom_edit_text.text.length)
        custom_edit_text.hidePassword()
        if (lengthLimit != -1)
            custom_edit_text.filters =
                arrayOf(
                    InputFilter.LengthFilter(lengthLimit)
                )
    }

    fun setCardInput() {
        custom_edit_text.keyListener = DigitsKeyListener.getInstance("0123456789 ")
        custom_edit_text.filters = arrayOf(
            InputFilter.LengthFilter(19),
            CardNumberFormatInputFilter()
        )
    }

    fun setPincodeInput(lengthLimit: Int = -1) {
        if (lengthLimit != -1)
            custom_edit_text.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(lengthLimit))
    }

    fun setDateInput() {
        custom_edit_text.keyListener = DigitsKeyListener.getInstance("0123456789/")
        custom_edit_text.filters = arrayOf<InputFilter>(CreditCardExpiryInputFilter())
        custom_edit_text.imeOptions = EditorInfo.IME_ACTION_NEXT
    }

    fun enableEdit(enable: Boolean) {
        custom_edit_text.isEnabled = enable
    }

    fun setErrorState() {
        img_invalid_number_alert.visibility = View.VISIBLE
        custom_layout_edittext.setBackgroundResource(RSG.drawable.sg_feedback_rectangle_red)
        text_error.visibility = View.VISIBLE
        iv_close.visibility = View.GONE
        tv_apply.visibility = View.GONE
        text_card_title.setTextColor(ContextCompat.getColor(context, RSG.color.pinkish_red))
        text_hint.visibility = View.GONE
    }

    fun setFocusState() {
        custom_layout_edittext.setBackgroundResource(RSG.drawable.sg_feedback_rectangle_blue)
        text_card_title.visibility = View.VISIBLE
        if (custom_edit_text.text.isNotEmpty() && isClearButtonVisible)
            iv_close.visibility = View.VISIBLE
        else
            iv_close.visibility = View.GONE
        if (custom_edit_text.text.isNotEmpty() && isShowApply)
            tv_apply.visibility = View.VISIBLE
        else
            tv_apply.visibility = View.GONE
        img_invalid_number_alert.visibility = View.GONE
        text_error.visibility = View.GONE
        text_card_title.setTextColor(ContextCompat.getColor(context, RSG.color.blue_blue))
        text_hint.visibility = View.GONE
    }


    fun setDefaultState() {
        img_invalid_number_alert.visibility = View.GONE
        custom_layout_edittext.setBackgroundResource(RSG.drawable.sg_feedback_rectangle_grey)
        text_error.visibility = View.GONE
        iv_close.visibility = View.GONE
        tv_apply.visibility = View.GONE
        text_hint.visibility = View.VISIBLE
        text_card_title.visibility = View.GONE
        text_card_title.setTextColor(ContextCompat.getColor(context, RSG.color.blue_blue))
    }

    var applyClick: OnClickListener? = null
}