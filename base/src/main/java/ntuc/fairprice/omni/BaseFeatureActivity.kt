package ntuc.fairprice.omni

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.play.core.splitinstall.SplitInstallManager
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory
import com.google.android.play.core.splitinstall.SplitInstallRequest
import com.google.android.play.core.splitinstall.SplitInstallSessionState
import com.google.android.play.core.splitinstall.SplitInstallStateUpdatedListener
import com.google.android.play.core.splitinstall.model.SplitInstallSessionStatus
import android.app.ActivityOptions
import kotlinx.android.synthetic.main.base_feature_activity.*

/** Activity that displays buttons and handles loading of feature modules. */
class BaseFeatureActivity : BaseSplitActivity() {

    companion object {
        val TAG = "BaseFeatureActivity"
        val NUM_MODULES = 4
    }

    /** Listener used to handle changes in state for install requests. */
    private val listener = SplitInstallStateUpdatedListener { state ->
        val multiInstall = state.moduleNames().size > NUM_MODULES
        state.moduleNames().forEach { name ->
            // Handle changes in state.
            when (state.status()) {
                SplitInstallSessionStatus.DOWNLOADING -> {
                    //  In order to see this, the application has to be uploaded to the Play Store.
                    Log.d(TAG, "Downloading module $name $state")
                }
                SplitInstallSessionStatus.REQUIRES_USER_CONFIRMATION -> {
                    /*
                      This may occur when attempting to download a sufficiently large module.
                      In order to see this, the application has to be uploaded to the Play Store.
                      Then features can be requested until the confirmation path is triggered.
                     */
                    toastAndLog("Requires user confirmation module $name $state  ${state.moduleNames().size}")
                    startIntentSender(state.resolutionIntent()?.intentSender, null, 0, 0, 0)
                }
                SplitInstallSessionStatus.INSTALLED -> {
                    toastAndLog("Installed module $name ${state.moduleNames().size}")
                    onSuccessfulLoad(name, launch = true)
                }

                SplitInstallSessionStatus.INSTALLING -> toastAndLog("Installing module $name $state ${state.moduleNames().size}")
                SplitInstallSessionStatus.FAILED -> {
                    toastAndLog("Error: ${state.errorCode()} for module ${state.moduleNames()} ${state.moduleNames().size}")
                }
            }
        }
    }

    private val moduleScanGo by lazy { getString(R.string.title_scango) }
    private val moduleFairpriceOn by lazy { getString(R.string.title_fairpriceon) }
    private val moduleAssets by lazy { getString(R.string.title_assets) }
    private val instantModule by lazy { getString(R.string.title_instant) }
    private val instantModuleUrl by lazy { getString(R.string.instant_feature_url) }

    lateinit var manager: SplitInstallManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_feature_activity)
        manager = SplitInstallManagerFactory.create(this)

        pg_load_module.indeterminateDrawable.setColorFilter(resources.getColor(R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY)

        val modules = manager.installedModules

        toastAndLog("Module $modules Already Installed ${modules.size}")

        if(modules.size >= NUM_MODULES && modules.contains(moduleFairpriceOn)) {
            startDefaultSplashScreen()
        } else {
            installAllFeaturesNow()
        }
//                installAllFeaturesDeferred()
//                requestUninstall()
//                loadAndLaunchModule(instantModule)
//                openUrl(instantModuleUrl)
    }

    override fun onResume() {
        // Listener can be registered even without directly triggering a download.
        manager.registerListener(listener)
        super.onResume()
    }

    override fun onPause() {
        // Make sure to dispose of the listener once it's no longer needed.
        manager.unregisterListener(listener)
        super.onPause()
    }

    /**
     * Load a feature by module name.
     * @param name The name of the feature module to load.
     */
    fun loadAndLaunchModule(name: String) {
        // Skip loading if the module already is installed. Perform success action directly.
        if (manager.installedModules.contains(name)) {
            toastAndLog("loadAndLaunchModule $name ${manager.installedModules.size}")
            onSuccessfulLoad(name, launch = true)
            return
        }

        // Create request to install a feature module by name.
        val request = SplitInstallRequest.newBuilder()
            .addModule(name)
            .build()

        // Load and install the requested feature module.
        toastAndLog("Module $name wasn't Installed")
        manager.startInstall(request)
    }

    fun openUrl(url: String) {
        var intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        intent.setPackage(Constants.packageName)
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        startActivity(intent)
    }

    /** Display assets loaded from the assets feature module. */
    fun displayAssets() {
        // Get the asset manager with a refreshed context, to access content of newly installed apk.
        val assetManager = createPackageContext(packageName, 0).assets
        // Now treat it like any other asset file.
        val assets = assetManager.open("assets.txt")
        val assetContent = assets.bufferedReader()
            .use {
                it.readText()
            }

        AlertDialog.Builder(this)
            .setTitle("Asset content")
            .setMessage(assetContent)
            .show()
    }

    /** Install all features but do not launch any of them. */
    fun installAllFeaturesNow() {
        // Request all known modules to be downloaded in a single session.
        val modules = manager.installedModules
        var builder = SplitInstallRequest.newBuilder()

        if(!modules.contains(moduleScanGo)) {
            builder.addModule(moduleScanGo)
        }
        if(!modules.contains(moduleAssets)) {
            builder.addModule(moduleAssets)
        }
        if(!modules.contains(moduleFairpriceOn)) {
            builder.addModule(moduleFairpriceOn)
        }

        val request = builder.build()
        // Start the install with above request.
        manager.startInstall(request).addOnSuccessListener {
            toastAndLog("Start Install ${request.moduleNames}")
        }.addOnFailureListener {
            toastAndLog("Start Install Failure ${it.message}")
        }
    }

    /** Install all features deferred. */
    fun installAllFeaturesDeferred() {
        val modules = listOf(moduleScanGo, moduleFairpriceOn, moduleAssets)

        manager.deferredInstall(modules).addOnSuccessListener {
            toastAndLog("Deferred installation of $modules")
        }
    }

    /** Request uninstall of all features. */
    fun requestUninstall() {
        toastAndLog(
            "Requesting uninstall of all modules." +
                    "This will happen at some point in the future."
        )

        val installedModules = manager.installedModules.toList()
        manager.deferredUninstall(installedModules).addOnSuccessListener {
            toastAndLog("Uninstalling $installedModules")
        }
    }

    /**
     * Define what to do once a feature module is loaded successfully.
     * @param moduleName The name of the successfully loaded module.
     * @param launch `true` if the feature module should be launched, else `false`.
     */
    private fun onSuccessfulLoad(moduleName: String, launch: Boolean) {
        if (launch) {
            val modules = manager.installedModules
            if(modules.contains(moduleFairpriceOn) && modules.contains(moduleAssets)) {
                toastAndLog("launchActivity $moduleName ${manager.installedModules.size}")
                startDefaultSplashScreen()
            }
//            when (moduleName) {
//                moduleAssets -> displayAssets()
//                instantModule -> launchActivity(Constants.instantClassname)
//            }
        }
    }

    /** Launch an activity by its class name. */
    fun launchActivity(className: String) {
        Intent().setClassName(packageName, className)
            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            .also {
                startActivity(it)
                overridePendingTransition(0, 0)
                finish()
            }
    }

    fun toastAndLog(text: String) {
//        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        Log.d(TAG, text)
    }

    fun startDefaultSplashScreen() {
        val modules = manager.installedModules
        val targetSplash = if (modules.contains(moduleScanGo)) Constants.scanGoSplashScreenClassname else Constants.fpOnSplashScreenClassname
        launchActivity(targetSplash)
    }
}