package ntuc.fairprice.omni.scango.base.customview

import android.app.Dialog
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ntuc.fairprice.omni.scango.R

open class ScangoBottomSheetDialogFragment : BottomSheetDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetDialog)
        return super.onCreateDialog(savedInstanceState)
    }
}