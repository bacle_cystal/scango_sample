package ntuc.fairprice.omni.scango.ui.screen.ageverify

enum class AgeVerifyType(val value: Int) {
    QR_CODE(1),
    MANUALLY_INPUT(2)
}
