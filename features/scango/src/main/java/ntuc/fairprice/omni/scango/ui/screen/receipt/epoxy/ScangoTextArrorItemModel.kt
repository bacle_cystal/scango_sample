package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder

@EpoxyModelClass(layout = R.layout.scango_layout_item_text_arrow_blue)
abstract class ScangoTextArrorItemModel : EpoxyModelWithHolder<ScangoTextArrorItemModel.ScangoTextArrowHeaderHolder>() {
    @EpoxyAttribute
    var titleText: String? = null

    override fun bind(holder: ScangoTextArrowHeaderHolder) {
        //holder.parent.setOnClickListener(clickListener)
        holder.apply {
            title.text = titleText
        }
    }

    class ScangoTextArrowHeaderHolder : ScangoBaseEpoxyHolder() {
        val title: TextView by bind(R.id.tv_title)
    }
}
