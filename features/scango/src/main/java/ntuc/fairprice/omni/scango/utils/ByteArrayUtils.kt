package ntuc.fairprice.omni.scango.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.nio.charset.Charset

fun ByteArray.toUTF8String() = this.toString(Charset.forName("UTF-8"))

fun Bitmap.toByteArray(): ByteArray {
    val stream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.PNG, 90, stream)
    return stream.toByteArray()
}

fun ByteArray?.toBitmap(): Bitmap = BitmapFactory.decodeByteArray(this, 0, this?.size ?: 0)

