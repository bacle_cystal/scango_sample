/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ntuc.fairprice.omni.scango.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ntuc.fairprice.omni.scango.ScangoFeatureViewModel
import ntuc.fairprice.omni.scango.epoxy.ScangoDynamicPageViewModel
import ntuc.fairprice.omni.scango.ui.screen.ageverify.ScangoAgeNoticeViewModel
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyViewModel
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanViewModel
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorNoticeViewModel
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorRemoveItemViewModel
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import ntuc.fairprice.omni.scango.ui.screen.checkout.ScangoCheckoutViewModel
import ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired.ScangoSessionExpiredViewModel
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackCommentDialogViewModel
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackDialogViewModel
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoHomeFragmentViewModel
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoTutorialViewModel
import ntuc.fairprice.omni.scango.ui.screen.home.exitstore.ExitStoreViewModel
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberViewModel
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.ScangoLinkPlusOtpViewModel
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberViewModel
import ntuc.fairprice.omni.scango.ui.screen.orders.ScangoOrderListViewModel
import ntuc.fairprice.omni.scango.ui.screen.productdetail.viewmodel.ScangoProductDetailViewModel
import ntuc.fairprice.omni.scango.ui.screen.promo.ScangoPromoViewModel
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptActivityViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivityViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.BarcodeErrorViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.manually.QrCodeManuallyViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.product.BarcodeProductRapidViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.product.BarcodeProductViewModel
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptConfirmationViewModel
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListDetailViewModel
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListPageViewModel
import ntuc.fairprice.omni.scango.ui.screen.uicheck.UiCheckViewModel
import thor.zopsmart.com.thor.di.RepositoryBinder
import thor.zopsmart.com.thor.di.ViewModelKey
import thor.zopsmart.com.thor.epoxy.CategoryPageViewModel
import thor.zopsmart.com.thor.epoxy.FponDynamicPageViewModel
import thor.zopsmart.com.thor.features.account.viewmodel.MyAccountViewModel
import thor.zopsmart.com.thor.features.deliverymethod.viewmodel.DeliverySelectionViewModel
import thor.zopsmart.com.thor.features.home.viewmodel.HomePageViewModel
import thor.zopsmart.com.thor.features.onboarding.viewmodel.SplashViewModel
import thor.zopsmart.com.thor.features.order.OrderDetailViewModel
import thor.zopsmart.com.thor.features.orderhistory.ui.OrderHistoryViewModel
import thor.zopsmart.com.thor.features.productdetail.viewmodel.ProductDetailViewModel
import thor.zopsmart.com.thor.features.shoppinglist.ui.ShoppingListPageViewModel
import thor.zopsmart.com.thor.features.shoppinglist.ui.WishlistViewModel
import thor.zopsmart.com.thor.repository.db.room.repo.WishlistRepository
import thor.zopsmart.com.thor.viewmodel.*

@Suppress("unused")
@Module(includes = [RepositoryBinder::class])
abstract class ScangoViewModelModule {
    // FPOn
    @Binds
    @IntoMap
    @ViewModelKey(CartViewModel::class)
    abstract fun bindCartViewModel(cartViewModel: CartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ShoppingListPageViewModel::class)
    abstract fun bindShoppingListViewModel(shoppingListViewModel: ShoppingListPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(splashViewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(productDetailViewModel: ProductDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderHistoryViewModel::class)
    abstract fun bindOrderHistoryViewModel(viewModel: OrderHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderDetailViewModel::class)
    abstract fun bindOrderDetailViewModel(viewModel: OrderDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliverySelectionViewModel::class)
    abstract fun bindDeliverySelectionViewModel(deliverySelectionViewModel: DeliverySelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CategoryPageViewModel::class)
    abstract fun bindCategoryPageViewModel(homeViewModel: CategoryPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FponDynamicPageViewModel::class)
    abstract fun bindFponDynamicPageViewModel(homeViewModel: FponDynamicPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyAccountViewModel::class)
    abstract fun bindMyAccountVM(viewModel: MyAccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomePageViewModel::class)
    abstract fun bindHomePageViewModel(homePageViewModel: HomePageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WishlistViewModel::class)
    abstract fun bindWishlistViewModel(wishlistViewModel: WishlistViewModel): ViewModel

    // Scango
    @Binds
    @IntoMap
    @ViewModelKey(ScangoFeatureViewModel::class)
    abstract fun bindScangoViewModel(scangoFeatureViewModel: ScangoFeatureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CodeScannerActivityViewModel::class)
    abstract fun bindCodeScannerActivityViewModel(codeScannerActivityViewModel: CodeScannerActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeProductViewModel::class)
    abstract fun bindBarcodeProductViewModel(barcodeProductViewModel: BarcodeProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeProductRapidViewModel::class)
    abstract fun bindBarcodeProductRapidViewModel(barcodeProductRapidViewModel: BarcodeProductRapidViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QrCodeManuallyViewModel::class)
    abstract fun bindQrCodeManuallyViewModel(qrCodeManuallyViewModel: QrCodeManuallyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExitStoreViewModel::class)
    abstract fun bindExitStoreViewModel(exitStoreViewModel: ExitStoreViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LiquorNoticeViewModel::class)
    abstract fun bindLiquorNoticeViewModel(liquorNoticeViewModel: LiquorNoticeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LiquorRemoveItemViewModel::class)
    abstract fun bindLiquorRemoveItemViewModel(liquorRemoveItemViewModel: LiquorRemoveItemViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedbackDialogViewModel::class)
    abstract fun bindFeedbackDialogViewModel(feedbackDialogViewModel: FeedbackDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedbackCommentDialogViewModel::class)
    abstract fun bindFeedbackCommentDialogViewModel(feedbackCommentDialogViewModel: FeedbackCommentDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoReceiptActivityViewModel::class)
    abstract fun bindScangoReceiptActivityViewModel(scangoReceiptActivityViewModel: ScangoReceiptActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UiCheckViewModel::class)
    abstract fun bindUiCheckViewModel(uiCheckViewModel: UiCheckViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeErrorViewModel::class)
    abstract fun bindBarcodeErrorViewModel(barcodeErrorViewModel: BarcodeErrorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoCartViewModel::class)
    abstract fun bindScangoCartViewModel(scangoCartViewModel: ScangoCartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoProductDetailViewModel::class)
    abstract fun bindScangoProductDetailViewModel(scangoProductDetailViewModel: ScangoProductDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoCheckoutViewModel::class)
    abstract fun bindScangoCheckoutViewModel(scangoCheckoutViewModel: ScangoCheckoutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoHomeFragmentViewModel::class)
    abstract fun bindScangoHomeFragmentViewModel(scangoHomeFragmentViewModel: ScangoHomeFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoShoppingListPageViewModel::class)
    abstract fun bindScangoShoppingListPageViewModel(scangoShoppingListPageViewModel: ScangoShoppingListPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoShoppingListDetailViewModel::class)
    abstract fun bindScangoShoppingListDetailViewModel(scangoShoppingListDetailViewModel: ScangoShoppingListDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoTutorialViewModel::class)
    abstract fun bindScangoScangoTutorialViewModel(scangoTutorialViewModel: ScangoTutorialViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoLinkPlusPhoneNumberViewModel::class)
    abstract fun bindScangoLinkPlusPhoneNumberViewModel(scangoLinkPlusPhoneNumberViewModel: ScangoLinkPlusPhoneNumberViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoLinkPlusOtpViewModel::class)
    abstract fun bindScangoLinkPlusOtpViewModel(scangoLinkPlusOtpViewModel: ScangoLinkPlusOtpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoLinkPlusCardNumberViewModel::class)
    abstract fun bindScangoLinkPlusCardNumberViewModel(scangoLinkPlusCardNumberViewModel: ScangoLinkPlusCardNumberViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoPromoViewModel::class)
    abstract fun bindScangoPromoViewModel(scangoPromoViewModel: ScangoPromoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoReceiptConfirmationViewModel::class)
    abstract fun bindScangoReceiptConfirmationViewModel(scangoReceiptConfirmationViewModel: ScangoReceiptConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoOrderListViewModel::class)
    abstract fun bindScangoOrderListViewModel(scangoOrderListViewModel: ScangoOrderListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoDynamicPageViewModel::class)
    abstract fun bindScangoDynamicPageViewModel(scangoDynamicPageViewModel: ScangoDynamicPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoAgeNoticeViewModel::class)
    abstract fun bindScangoAgeNoticeViewModel(scangoAgeNoticeViewModel: ScangoAgeNoticeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoAgeVerifyManuallyViewModel::class)
    abstract fun bindScangoAgePincodeViewModel(scangoAgePincodeViewModel: ScangoAgeVerifyManuallyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoAgeVerifyScanViewModel::class)
    abstract fun bindScangoAgeVerifyScanViewModel(scangoAgeVerifyScanViewModel: ScangoAgeVerifyScanViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ScangoSessionExpiredViewModel::class)
    abstract fun bindScangoSessionExpiredViewModel(scangoSessionExpiredViewModel: ScangoSessionExpiredViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: FPViewModelFactory): ViewModelProvider.Factory
}

@Module
abstract class RepositoryBinder {
    @Binds
    abstract fun bindWishlistRepository(wishlistRepository: WishlistRepository): WishlistRepository
}



