package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.graphics.Bitmap
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Space
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.lottie.LottieAnimationView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.VerificationStatus
import ntuc.fairprice.omni.scango.utils.px
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.setCurrencyText

@EpoxyModelClass(layout = R.layout.scango_layout_item_header_receipt)
abstract class ScangoReceiptHeaderModel : EpoxyModelWithHolder<ScangoReceiptHeaderModel.ScangoReceiptHeaderHolder>() {
    @EpoxyAttribute
    var imageId: String? = null
    @EpoxyAttribute
    var cardNumber: String? = null
    @EpoxyAttribute
    var dayTime: String? = null
    @EpoxyAttribute
    var hourTime: String? = null
    @EpoxyAttribute
    var nameStore: String? = null
    @EpoxyAttribute
    var pointNumber: Double? = null
    @EpoxyAttribute
    var moneyNumber: Double? = null
    @EpoxyAttribute
    var orderId: String? = null
    @EpoxyAttribute
    var qrCodeBitmap: Bitmap? = null

    @JvmField
    @EpoxyAttribute
    var isVerified: VerificationStatus? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    override fun bind(holder: ScangoReceiptHeaderHolder) {
        //holder.parent.setOnClickListener(clickListener)

        holder.apply {
            /*cardType.setImageDrawable(R.drawable.visa)
            cardNumTv.setCardText(cardNumber?:"")*/
            dayTimeTv.text = dayTime
            hourTimeTv.text = hourTime

            if (pointNumber != 0.0) {
                numberPointTv.visibility = View.VISIBLE
                numberPointTv.text = "+${pointNumber.toString()}"
                lpPointIv.visibility = View.VISIBLE
            } else {
                numberPointTv.visibility = View.GONE
                lpPointIv.visibility = View.GONE
            }

            totalMoneyTv.setCurrencyText(moneyNumber)
            shopNameTv.text = nameStore
            orderIdTv.text = orderId
            qrcodeIv.setImageBitmap(qrCodeBitmap)
            when (isVerified) {
                VerificationStatus.VerificationStatusVerified -> {
                    successTv.visibility = View.VISIBLE
                    lpPointIv.visibility = View.GONE
                    numberPointTv.visibility = View.GONE
                    headerLinkpointCl.visibility = View.VISIBLE
                    headerStoreCl.visibility = View.GONE
                    spacer.visibility = View.GONE
                }
                VerificationStatus.VerificationStatusPending -> {
                    successTv.visibility = View.GONE
                    lpPointIv.visibility = View.VISIBLE
                    numberPointTv.visibility = View.VISIBLE
                    headerLinkpointCl.visibility = View.GONE
                    headerStoreCl.visibility = View.VISIBLE
                    spacer.visibility = View.VISIBLE
                }
                VerificationStatus.VerificationStatusUnVerified -> {
                    successTv.visibility = View.GONE
                    lpPointIv.visibility = View.VISIBLE
                    numberPointTv.visibility = View.VISIBLE
                    headerLinkpointCl.visibility = View.GONE
                    headerStoreCl.visibility = View.GONE
                    spacer.visibility = View.GONE
                }
            }

            //BinhTH: 16/05 TODO handle link point
            headerLinkpointCl.gone()
            lpPointIv.gone()
            numberPointTv.gone()
        }
    }

    override fun buildView(parent: ViewGroup): View {
        val view = super.buildView(parent)
        (view.layoutParams as? ViewGroup.MarginLayoutParams)?.run {
            leftMargin = 16.px
            rightMargin = 16.px
        }
        return view
    }

    class ScangoReceiptHeaderHolder : ScangoBaseEpoxyHolder() {
        val qrcodeIv: ImageView by bind(R.id.image_qrcode)
        /* val cardType: ImageView by bind(R.id.iv_card_type)
         val cardNumTv: TextView by bind(R.id.tv_card_num)*/
        val dayTimeTv: TextView by bind(R.id.tv_day_time)
        val shopNameTv: TextView by bind(R.id.text_shop_name)
        val orderIdTv: TextView by bind(R.id.text_order_id)
        val hourTimeTv: TextView by bind(R.id.tv_hour_time)
        val numberPointTv: TextView by bind(R.id.text_number_point)
        val totalMoneyTv: TextView by bind(R.id.text_total_money)
        val spacer: Space by bind(R.id.spacer)

        //        val successIv: LottieAnimationView by bind(R.id.ic_success)
        val successTv: TextView by bind(R.id.tv_verified)
        val lpPointIv: ImageView by bind(R.id.ic_lp_point)
        val headerLinkpointCl: ConstraintLayout by bind(R.id.cl_header_link_point)
        val headerStoreCl: ConstraintLayout by bind(R.id.cl_header_store)
    }
}
