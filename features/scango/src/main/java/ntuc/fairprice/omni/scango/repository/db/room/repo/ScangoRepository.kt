package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import ntuc.fairprice.omni.scango.Constants.SHARE_PRE_STORE_ID
import ntuc.fairprice.omni.scango.repository.db.room.ScangoAppDatabase
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.clear
import thor.zopsmart.com.thor.repository.db.sharedpref.get

@FeatureScope
open class ScangoRepository(mApplication: Application) {
    val appDatabase = ScangoAppDatabase.getInstance(mApplication)
    val localStorage = LocalStorage.init(mApplication)

    fun getPreferredStoreId() : Int {
        return localStorage.get(SHARE_PRE_STORE_ID, null as Int?) ?: -1
    }

    fun clearLocalStorage() {
        localStorage.clear()
    }
}
