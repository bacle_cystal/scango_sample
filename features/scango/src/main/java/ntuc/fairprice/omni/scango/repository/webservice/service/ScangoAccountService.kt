package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoPaymentType
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartArray
import ntuc.fairprice.omni.scango.repository.webservice.model.*
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.BuildConfig
import thor.zopsmart.com.thor.base.extensions.long
import thor.zopsmart.com.thor.base.extensions.safeJson
import thor.zopsmart.com.thor.base.extensions.string
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.model.Card
import javax.inject.Inject

@FeatureScope
class ScangoAccountService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) :
    ScangoBaseService(scangoRequestProvider) {

    suspend fun loadCards() = apiCall(
        url = "/api/card",
        method = ScangoRequestProvider.Method.GET,
        isGuestRequest = false,
        forWho = ScangoRequestProvider.RequestFor.ZOPSMART
    ) {
        it.toCards()
    }

    suspend fun saveCard(params: JSONObject): Result<List<Card>> {
        // Get key for tokenization from server
        val cardKeyResult = apiCall(
            "/api/card-registration-key",
            ScangoRequestProvider.Method.GET,
            params = params,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it.toJSONObject().safeJson("data") string "key" ?: ""
        }

        val key = when (cardKeyResult) {
            is Success -> cardKeyResult.data
            is Failure -> return cardKeyResult
        }

        val tokenParams = JSONObject()
        tokenParams.put("keyId", key)
        tokenParams.put("cardInfo", params)

        val tokenResult = apiCall(
            BuildConfig.host_cybersource,
            ScangoRequestProvider.Method.POST,
            params = tokenParams,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.OTHER
        ) {
            it
        }

        // Get tokenized card result
        val tokenziedCardResult =
            when (tokenResult) {
                is Success -> tokenResult.data
                is Failure -> return Failure(Exception("Unable to tokenize card"))
            }

        // Add the final params for saving in server
        val finalCardParams = JSONObject().apply {
            tokenziedCardResult.let {
                put("keyId", it.toJSONObject() string "keyId")
                put("token", it.toJSONObject() string "token")
                put("number", it.toJSONObject() string "maskedPan")
                put("type", it.toJSONObject() string "cardType")
                put("timestamp", it.toJSONObject() long "timestamp")
                put("signedFields", it.toJSONObject() string "signedFields")
                put("signature", it.toJSONObject() string "signature")
            }
            put("expiryYear", params string "cardExpirationYear")
            put("expiryMonth", params string "cardExpirationMonth")
        }

        // Api call to save the tokenized card to server
        return apiCall(
            url = "/api/card",
            method = ScangoRequestProvider.Method.POST,
            isGuestRequest = false,
            params = finalCardParams,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it.toCards()
        }
    }
}