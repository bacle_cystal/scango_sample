package ntuc.fairprice.omni.scango.repository.webservice.service

import android.app.Application
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.toAgeVerifyResponse
import ntuc.fairprice.omni.scango.repository.webservice.model.toScangoStore
import ntuc.fairprice.omni.scango.repository.webservice.model.toTimeStamp
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import ntuc.fairprice.omni.scango.ui.screen.ageverify.model.AgeVerifyResponse
import org.json.JSONObject
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoStoreService @Inject constructor(
    val application: Application,
    scangoRequestProvider: ScangoRequestProvider
) : ScangoBaseService(scangoRequestProvider) {

    suspend fun getCheckin(storeId: Int, deviceId: String): Result<ScangoStore> {
        val requestBody = JSONObject()
        val item = JSONObject()
        item.put("storeID", storeId)
        item.put("deviceID", deviceId)
        requestBody.put("params", item)
        return apiCall(
            "/store/v0.2/sessions",
            ScangoRequestProvider.Method.POST,
            params = requestBody,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
        ) {
            it.toScangoStore()
        }
    }

    suspend fun getCheckinVerify(sessionId: String): Result<ScangoStore> = apiCall(
        "/store/v0.2/sessions/$sessionId",
        ScangoRequestProvider.Method.GET,
        forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
    ) {
        it.toScangoStore()
    }

    suspend fun getCheckout(sessionId: String): Result<String> = apiCall(
        "/store/v0.2/sessions/$sessionId",
        ScangoRequestProvider.Method.DELETE,
        forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
    ) {
        it.toString()
    }

    suspend fun getStoreTimeStamp(): Result<Long> {
        val requestBody = JSONObject()
        return apiCall(
            "/store/v0.2/timestamp",
            ScangoRequestProvider.Method.GET,
            params = requestBody,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
        ) {
            it.toTimeStamp()
        }
    }

    suspend fun verifyAgePincode(staffId: String, pincode: String, sessionID: String): Result<AgeVerifyResponse> {
        val requestBody = JSONObject()
        requestBody.put("staffID", staffId)
        requestBody.put("sessionID", sessionID)
        requestBody.put("pin", pincode)
        return apiCall(
            "/order/v0.2/age-verifications",
            ScangoRequestProvider.Method.POST,
            params = JSONObject().put("params", requestBody),
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
        )
        {
            it.toAgeVerifyResponse()
        }
    }
}
