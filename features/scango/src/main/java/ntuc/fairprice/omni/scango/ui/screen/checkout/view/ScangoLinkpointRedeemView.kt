package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.OnClickListener
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_add_payment_view.view.*
import kotlinx.android.synthetic.main.scango_layout_redeem_linkpoint.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.setCurrencyTextNegative
import ntuc.fairprice.omni.scango.utils.setLinkPointText
import thor.zopsmart.com.thor.base.extensions.setCurrencyText

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoLinkpointRedeemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.scango_layout_redeem_linkpoint, this)

        switch_button.setOnCheckedChangeListener { view, isChecked ->
            run {
                switchButtonClick(isChecked)
            }
        }
    }

    fun setPointRedeem(dollarNumber: String, pointNumber: String) {
        tv_link_point_redeemed.setLinkPointText(dollarNumber, pointNumber)
        postInvalidate()
    }

    fun setDiscount(money: Double) {
        tv_discount.setCurrencyTextNegative(money)
        postInvalidate()
    }

    var switchButtonClick: (Boolean) -> Unit = { _ ->
    }
}


interface ScangoSwitchLinkpointListener {
    fun onSwitchLinkpoint(isActive: Boolean, totalValue:Double)
}