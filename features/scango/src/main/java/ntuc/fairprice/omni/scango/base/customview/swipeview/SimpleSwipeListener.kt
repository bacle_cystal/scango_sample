package ntuc.fairprice.omni.scango.base.customview.swipeview;

import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem

interface SimpleSwipeListener {
    fun onOpen(direction: Int, isContinuous: Boolean, scangoCartItem: ScangoCartItem)
}
