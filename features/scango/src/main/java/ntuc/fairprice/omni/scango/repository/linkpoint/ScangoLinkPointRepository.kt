package ntuc.fairprice.omni.scango.repository.linkpoint

import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.*

interface ScangoLinkPointRepository {

    suspend fun getCustomers(customerId : String): Result<LinkPointStatus>

    suspend fun updateCustomerRecordWithNumber(customerId : String, phoneNumber: String): Result<UpdateCustomerLinkPointResponse>

    suspend fun updateCustomerRecordWithCard(customerId : String, cardNumber: String, isHashed: Boolean): Result<UpdateCustomerLinkPointResponse>

    suspend fun sendOtp(resendOTP: Boolean): Result<SendOtpResponse>

    suspend fun verifyOtp(otp: String): Result<VerifyOtpResponse>

    suspend fun unLinkCustomer(customerId : String): Result<UnLinkCustomerResponse>

    var userDismissedForSession: Boolean

    var linkPointStatus: LinkPointStatus
}