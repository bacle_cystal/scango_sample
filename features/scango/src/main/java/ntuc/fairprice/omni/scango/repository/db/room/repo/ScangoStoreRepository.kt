package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.OpenForTesting
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoStoreService
import ntuc.fairprice.omni.scango.ui.screen.ageverify.model.AgeVerifyResponse
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.room.repo.Repository
import thor.zopsmart.com.thor.repository.db.sharedpref.get
import thor.zopsmart.com.thor.repository.db.sharedpref.set
import javax.inject.Inject

@OpenForTesting
@FeatureScope
class ScangoStoreRepository @Inject constructor(
    val mApplication: Application,
    private val scangoStoreService: ScangoStoreService
) : Repository(mApplication) {

    fun isCheckedIn(): Boolean = !getStoreNameCached().isNullOrBlank()

    // Network calls getCheckinStore
    suspend fun getCheckinStore(storeId: Int, deviceId: String): Result<ScangoStore> {
        return scangoStoreService.getCheckin(storeId, deviceId).let {
            it.onSuccess { store ->
                withContext(Dispatchers.IO) {
                    localStorage.apply {
                        set(Constants.SHARE_PRE_STORE_ID, store.id.toInt())
                        set(Constants.SHARE_PRE_STORE_SESSION_ID, store.sessionId)
                        set(Constants.SHARE_PRE_STORE_NAME, store.name)
                        set(Constants.SHARE_PRE_AGE_VERIFY_CODE, "")
                    }
                }
            }
        }
    }

    // Network calls getCheckOutStore
    suspend fun getCheckoutStore(): Result<String> {
        return scangoStoreService.getCheckout(getSessionId()).let {
            it.onSuccess {
                withContext(Dispatchers.IO) {
                    clearStoreInfo()
                }
            }
        }
    }

    suspend fun verifyAgePincode(staffId: String, pincode: String): Result<AgeVerifyResponse> {
        return scangoStoreService.verifyAgePincode(staffId, pincode, getSessionId()).let {
            it.onSuccess {
                withContext(Dispatchers.IO) {
                    localStorage.apply {
                        set(Constants.SHARE_PRE_AGE_VERIFY_CODE, it.ageVerificationCode)
                    }
                }
            }
        }
    }

    // Network calls getCheckinStore Verify
    suspend fun getCheckinStoreVerify(): Result<ScangoStore> {
        val timeAfterGetApi = System.currentTimeMillis()
        return scangoStoreService.getCheckinVerify(getSessionId()).let {
            it.onSuccess { store ->
                withContext(Dispatchers.IO) {
                    localStorage.set(Constants.SHARE_PRE_TIME_STAMP, store.timeStamp)
                    localStorage.set(Constants.SHARE_PRE_APIDELAY_TIME, System.currentTimeMillis() - timeAfterGetApi)
                    localStorage.set(
                        Constants.SHARE_PRE_SERVERGAP_TIME,
                        (store.timeStamp.toLong()).minus(System.currentTimeMillis())
                    )
                }
            }
        }
    }

    suspend fun getCurrentTimeStamp(): Result<Long> {
        return scangoStoreService.getStoreTimeStamp().let {
            it.onSuccess { timeStamp ->
                localStorage.set(Constants.SHARE_PRE_TIME_STAMP, timeStamp.toString())
                localStorage.set(
                    Constants.SHARE_PRE_SERVERGAP_TIME,
                    (timeStamp).minus(System.currentTimeMillis())
                )
            }
        }
    }

    //TODO handle exit store API
    fun clearStoreInfo() {
        localStorage.apply {
            set(Constants.SHARE_PRE_STORE_ID, 0)
            set(Constants.SHARE_PRE_STORE_NAME, "")
            set(Constants.SHARE_PRE_TIME_STAMP, "")
            set(Constants.SHARE_PRE_AGE_VERIFY_CODE, "")
        }
    }

    //TODO handle exit store API
    fun getStoreNameCached() = localStorage.get(Constants.SHARE_PRE_STORE_NAME, "")

    fun getStoreIdCached() = localStorage.get(Constants.SHARE_PRE_STORE_ID, 0)

    fun getAgeVerifyCached() = localStorage.get(Constants.SHARE_PRE_AGE_VERIFY_CODE, "") ?: ""

    fun getTimeStampCached() = localStorage.get(Constants.SHARE_PRE_TIME_STAMP, "")

    fun getSessionId() = localStorage.getString(Constants.SHARE_PRE_STORE_SESSION_ID, "")

    fun getAPIDelayTimeCached() = localStorage.get(Constants.SHARE_PRE_APIDELAY_TIME, 0L) ?: 0L

    fun getServerGapTimeCached() = localStorage.get(Constants.SHARE_PRE_SERVERGAP_TIME, 0L) ?: 0L

    fun setRapidShowed() {
        return localStorage.set(Constants.SHARE_PRE_SHOW_RAPID_GUIDE, true)
    }

    fun getRapidCached() = localStorage.get(Constants.SHARE_PRE_SHOW_RAPID_GUIDE, false)
}