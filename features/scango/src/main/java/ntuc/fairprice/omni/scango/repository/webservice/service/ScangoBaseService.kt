package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.webservice.model.*
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

open class ScangoBaseService @Inject constructor(private val scangoRequestProvider: ScangoRequestProvider) {

    /**
     * Common method to make API calls. Any class that is a child of BaseService can use this method.
     *
     * @param headers
     * @param method specifies if the call is POST, GET, PUT, or DELETE
     * @param converter is a lambda function which tell the function how to process the API response.
     * @param url the api endpoint
     * @param params api body in case of post or put
     * @forWho RequestFor object
     */
    suspend fun <T : Any>
            apiCall(
        url: String,
        method: ScangoRequestProvider.Method,
        headers: HashMap<String, String> = HashMap(),
        params: JSONObject = JSONObject(),
        isGuestRequest: Boolean = false,
        forWho: ScangoRequestProvider.RequestFor = ScangoRequestProvider.RequestFor.SCANGO,
        converter: ((ApiResponse) -> T)
    ): Result<T> {

        val call = scangoRequestProvider.get(
            method,
            url,
            headers,
            params,
            isGuestRequest,
            forWho
        )

        val result = call.getResult()

        return when (result) {
            is Success -> Success(
                converter(result.data)
            )
            is Failure -> Failure(
                result.error
            )
        }
    }
}