package ntuc.fairprice.omni.scango.ui.screen.more

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.scango_fragment_more.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ui.screen.orders.ScangoOrderListActivity
import ntuc.fairprice.omni.scango.utils.appendNavItem
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.R as ZR
import thor.zopsmart.com.thor.base.BaseFragment
import thor.zopsmart.com.thor.base.browser.WebConstants
import thor.zopsmart.com.thor.features.account.ui.MyAccountActivity
import thor.zopsmart.com.thor.features.storelocator.StoreLocatorMapActivity
import thor.zopsmart.com.thor.viewmodel.AccountViewModel

class ScangoMoreFragment : BaseFragment() {

    override val mLayout: Int = R.layout.scango_fragment_more

    private val accountViewModel: AccountViewModel by lazy {
        ViewModelProviders.of(this).get(AccountViewModel::class.java)
    }

    private lateinit var footers:Array<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        footers = resources.getStringArray(ZR.array.nav_footer)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreated() {
        // Append footer links
        footers.forEach { footer ->
            footer_links.appendNavItem(name = footer).setOnClickListener{
                when(footer) {
                    getString(ZR.string.label_nav_recipes) -> showInAppBrowser(WebConstants.FP_URL_RECIPES, footer, FPScreen.RECIPES)
                    getString(ZR.string.label_nav_tips) -> showInAppBrowser(WebConstants.FP_URL_TIPS, footer, FPScreen.TIPS)
                    getString(ZR.string.label_nav_weekly_ads) -> showInAppBrowser(WebConstants.FP_URL_WEEKLY_ADS, footer, FPScreen.WEEKLY_ADS)
                    getString(ZR.string.label_nav_events) -> showInAppBrowser(WebConstants.FP_URL_EVENTS, footer, FPScreen.EVENTS)
                    getString(ZR.string.label_nav_lucky_draw) -> showInAppBrowser(WebConstants.FP_URL_LUCKY_DRAW, footer, FPScreen.LUCKY_DRAW)
                    getString(ZR.string.label_nav_jwc) -> showInAppBrowser(WebConstants.FP_URL_JWC, footer, FPScreen.JWC)
                    getString(ZR.string.label_nav_shop_n_donate) -> showExternalBrowser(WebConstants.FP_URL_DONATE)
                    getString(ZR.string.label_nav_business) -> showExternalBrowser(WebConstants.FP_URL_BUSINESS)
                    getString(ZR.string.label_nav_tnc) -> showInAppBrowser(WebConstants.FP_URL_TNC, footer, FPScreen.TNC)
                    getString(ZR.string.label_nav_privacy) -> showInAppBrowser(WebConstants.FP_URL_PRIVACY, footer, FPScreen.PRIVACY)
                    getString(ZR.string.label_nav_store_locator) -> startActivity(StoreLocatorMapActivity.getIntent(context!!))
                    else -> {
                        showToast("Clicked $footer")
                    }
                }
            }
        }

        /**
         * If the user is already logged in then hide the login view
         * nav_login.visibility = View.Gone
         */
        accountViewModel.getCustomerLiveData().observe(this, Observer {
            val isLoggedIn = it != null
            nav_account.apply{
                updateIcon(if(isLoggedIn) ZR.drawable.ic_account_active else ZR.drawable.ic_account)
                boldText(isLoggedIn)
            }
        })

        // Click listeners for nav items

        nav_account.setOnClickListener {
            context?.let { startActivity(MyAccountActivity.getIntent(it)) }
        }

        nav_orders.setOnClickListener {
            context?.let { startActivity(ScangoOrderListActivity.getIntent(it)) }
        }

        nav_help.setOnClickListener {
            gotoHelpPage()
        }
    }

}
