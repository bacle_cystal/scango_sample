package ntuc.fairprice.omni.scango.base.customview

import android.graphics.Paint
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.TypefaceSpan
import android.widget.TextView

/**
 * CustomTypefaceSpan allows for the use of a non-framework font supplied in the
 * assets/fonts directory of the application. Use this class whenever the
 * framework does not contain a needed font.
 */
class CustomTypefaceSpan
/**
 * @param family Ignored since this uses a completely custom included font.
 * @param type Typeface, specified as: Typeface.createFromAsset(
 * context.getAssets(), "fonts/Roboto-Medium.ttf"),
 * @param size Desired font size; this should have already been converted
 * from a dimension.
 * @param color Desired font color; this should have already been converted
 * to an integer representation of a color.
 */
    (family: String, private val newType: Typeface, private val newColor: Int? = null) :
    TypefaceSpan(family) {
    override fun updateDrawState(ds: TextPaint) {
        applyCustomTypeFace(ds, newType, newColor)
    }

    override fun updateMeasureState(paint: TextPaint) {
        applyCustomTypeFace(paint, newType, newColor)
    }

    private fun applyCustomTypeFace(paint: Paint, tf: Typeface, newColor: Int?) {
        val oldStyle: Int
        val old = paint.typeface
        oldStyle = old?.style ?: 0
        val fake = oldStyle and tf.style.inv()
        if (fake and Typeface.BOLD != 0) {
            paint.isFakeBoldText = true
        }
        if (fake and Typeface.ITALIC != 0) {
            paint.textSkewX = -0.25f
        }
        newColor?.let {
            paint.color = it
        }
        paint.typeface = tf
    }
}