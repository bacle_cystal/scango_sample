package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.scango_view_item_image.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.loadImageOptimize
import thor.zopsmart.com.thor.base.extensions.setVisibility

class ScangoCartOfferView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.scango_view_item_image, this)
    }

    fun setImage(url: String?, isLast: Boolean = false) {
        iv_offer_image.loadImageOptimize(url)
        iv_plus_icon.setVisibility(!isLast)
    }
}