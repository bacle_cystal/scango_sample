package ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_session_expired_fragment.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoSearchEditTextBinding
import javax.inject.Inject

class ScangoSessionExpiredFragment :
    ScangoBaseBottomSheetFragment<ScangoSearchEditTextBinding, ScangoSessionExpiredViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "ScangoSessionExpiredFragment"

        fun newInstance() = ScangoSessionExpiredFragment()
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: ScangoSessionExpiredViewModel by lazy {
        viewModelFactory.create(ScangoSessionExpiredViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_session_expired_fragment

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.checkStoreCached()
        btn_got_it.setOnClickListener {
            dismiss()
        }
    }
}
