package ntuc.fairprice.omni.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import ntuc.fairprice.omni.R


abstract class ScangoBaseErrorDialog(val context: Context) {

    val title: TextView
    val message: TextView
    val buttonAction: TextView
    val erroImage: ImageView
    var onActionClick: () -> Unit = {}

    private var dialog: Dialog

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.scango_error_dialog_layout, null)

        title = view.findViewById(R.id.scango_dialog_error_textView_title)
        message = view.findViewById(R.id.scango_dialog_error_textView_message)
        buttonAction = view.findViewById(R.id.scango_dialog_error_textView_button)
        erroImage = view.findViewById(R.id.scango_dialog_error_image)
        buttonAction.setOnClickListener {
            onActionClick.invoke()
            dismiss()
        }

        dialog = AlertDialog.Builder(context).setView(view).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }
}