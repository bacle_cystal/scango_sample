package ntuc.fairprice.omni.scango.ui.screen.receipt

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.HasFragmentInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_receipt.*
import kotlinx.android.synthetic.main.scango_layout_stylish_toolbar.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.ScangoPaymentEventDelegate
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.VerificationStatus
import ntuc.fairprice.omni.scango.repository.webservice.websocket.ReceiptSocketService
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackDialogFragment
import ntuc.fairprice.omni.scango.ui.screen.home.Tab
import ntuc.fairprice.omni.scango.utils.observe
import ntuc.fairprice.omni.scango.utils.toByteArray
import javax.inject.Inject

class ScangoReceiptActivity : ScangoBaseActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    private val viewModel: ScangoReceiptActivityViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoReceiptActivityViewModel::class.java)
    }

    private lateinit var viewBinding: ViewDataBinding

    companion object {
        //        const val BUNDLE_VERIFY = "bundle-verified"
        const val BUNDLE_ORDER_ID = "bundle-order-id"

        fun getIntent(context: Context, orderID: Long): Intent {
            return Intent(context, ScangoReceiptActivity::class.java).apply {
                putExtra(BUNDLE_ORDER_ID, orderID)
            }
        }
    }

    private var orderID: Long = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_receipt)
        viewBinding = DataBindingUtil.setContentView(this@ScangoReceiptActivity, R.layout.scango_activity_receipt)
        viewBinding.apply {
            lifecycleOwner = this@ScangoReceiptActivity
            setVariable(BR.viewModel, viewModel)
            executePendingBindings()
        }

        //val controller = ScangoReceiptEpoxyController()
        //rv_receipt.setController(controller)
//        rv_receipt.apply {
//            val epoxyVisibilityTracker = com.airbnb.epoxy.EpoxyVisibilityTracker()
//            epoxyVisibilityTracker.attach(this)
//            layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
//            isNestedScrollingEnabled = false
//        }

//        viewModel.apply {
//            /*payment.observe(this@ScangoReceiptActivity, Observer {
//                controller.setData(it)
//            })*/
//            pageModel.observe(this@ScangoReceiptActivity, Observer {
//                if (it != null) {
//                    rv_receipt.setModels(it)
//                }
//            })
//        }

        orderID = intent?.extras?.getLong(BUNDLE_ORDER_ID) ?: 0

//        viewModel.getPaymentData(orderID.toString())

        iv_close_back.setOnClickListener {
            finish()
            startActivity(ScangoFeatureActivity.intentMoveToOrderList(this))
        }
        lifecycle.addObserver(ReceiptSocketService(viewModel))

//        showFeedback()
//        observe(viewModel.orderDetailStatus, ::onOrderDetailStatusChange)
//        observe(viewModel.paymentVerifyStatus, ::onPaymentStatusChange)
    }

    private fun navigateToScanGoHome() {
        finish()
        gotoHomePageScango()
    }

    private fun showFeedback() {
        Handler().postDelayed({
            val fragment = FeedbackDialogFragment.newInstance(viewModel.payment.customerName ?: "")
            fragment.show(
                supportFragmentManager,
                FeedbackDialogFragment.TAG
            )
        }, 3000)
    }

    private fun showReceiptConfirm() {
        viewModel.apply {
            val fragment = ScangoReceiptConfirmationFragment.newInstance(
                payment.moneyNumber ?: 0.0,
                payment.cardNumber ?: "",
                payment.cardType ?: "",
                qrcodeBitmap.toByteArray()
            )
            setCallbackListener(fragment)
            fragment.show(
                supportFragmentManager,
                ScangoReceiptConfirmationFragment.TAG
            )
        }
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        if (childFragment is FeedbackDialogFragment) childFragment.actionAfterDismiss = { navigateToScanGoHome() }
    }

    private fun onPaymentStatusChange(paymentVerifyStatus: VerificationStatus) {
        when (paymentVerifyStatus) {
            is VerificationStatus.VerificationStatusLoading -> scanGoProgressDialog.show()
            is VerificationStatus.VerificationStatusPending -> {
                showReceiptConfirm()
                //TODO Binh.TH show feedback when web socket ready
            }
            is VerificationStatus.VerificationStatusVerified -> {

            }
            is VerificationStatus.VerificationStatusUnVerified -> {
            }
        }
    }

    private fun onOrderDetailStatusChange(orderDetailStatus: OrderDetailStatus?) {
        scanGoProgressDialog.dismiss()
        when (orderDetailStatus) {
            is OrderDetailLoading -> scanGoProgressDialog.show()
            is OrderDetailFailure -> {
                orderDetailFailure(orderDetailStatus.e)
            }
            is OrderDetailSuccess -> {
                viewModel.getPaymentVerify(orderDetailStatus.orderDetail)
                /*Handler().postDelayed({
                    viewModel.getPaymentVerify(orderDetailStatus.orderDetail)
                }, 5000)*/
            }
        }
    }

    private fun orderDetailFailure(e: Throwable) {
        scangoGeneralErrorDialog.apply {
            onActionClick = {
                viewModel.getPaymentData(orderID.toString())
            }
            show()
        }
    }

    override fun handleBackPress(): Boolean {
        finish()
        startActivity(ScangoFeatureActivity.intentMoveToOrderList(this))
        return super.handleBackPress()
    }

    private var callBackListener: ScangoPaymentEventDelegate? = null

    fun setCallbackListener(callback: ScangoPaymentEventDelegate) {
        this.callBackListener = callback
    }
}
