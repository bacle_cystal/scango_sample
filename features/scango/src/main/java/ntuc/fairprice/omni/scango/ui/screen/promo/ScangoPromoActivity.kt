package ntuc.fairprice.omni.scango.ui.screen.promo

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyVisibilityTracker
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_add_promo.*
import kotlinx.android.synthetic.main.scango_layout_stylish_toolbar.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.ScangoComponent
import ntuc.fairprice.omni.scango.base.ScangoCouponPageDelegate
import ntuc.fairprice.omni.scango.base.ScangoDelegate
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.typeconverter.ScangoConverters
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.utils.observe
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.base.browser.WebConstants
import thor.zopsmart.com.thor.base.browser.WebViewActivity
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.interfaces.ToastColor
import javax.inject.Inject
import thor.zopsmart.com.thor.R as RFP

class ScangoPromoActivity : ScangoBaseActivity(), HasSupportFragmentInjector, ScangoCouponPageDelegate {

    companion object {
        fun getIntent(context: Context): Intent = Intent(context, ScangoPromoActivity::class.java)
    }

    private lateinit var viewBinding: ViewDataBinding

    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
    @Inject
    lateinit var viewModel: ScangoPromoViewModel

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this@ScangoPromoActivity, R.layout.scango_activity_add_promo)
        viewBinding.lifecycleOwner = this
        viewBinding.apply {
            setVariable(BR.vm, viewModel)
            executePendingBindings()
        }

        viewModel.loadPromotions()

        observe(viewModel.pageLD, ::onPromoLoadChange)

        viewModel.selectedCoupons =
            ScangoConverters.toCouponList(intent.getStringExtra("selectedCoupons"))?.toMutableList() ?: mutableListOf()


        main_scroll.isNestedScrollingEnabled = false
        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(promo_epoxy)
        promo_epoxy.isNestedScrollingEnabled = false
        promo_epoxy.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        promo_epoxy.buildModelsWith { controller ->

            viewModel.pageLD.value?.forEach {
                it.let { component ->
                    (component as? ScangoComponent<ScangoDelegate>)
                        ?.render(this@ScangoPromoActivity)
                        ?.forEach { model ->
                            model.addTo(controller)
                        }
                }
            }
        }

        observe(viewModel.couponStatus, ::onCouponStatusChange)

        button_back.setOnClickListener {
            backToParent()
        }
        iv_close_back.setOnClickListener {
            backToParent()
        }
    }

    private fun backToParent() {
        val intent = Intent()
        viewModel.setSelectedCoupons()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun onCouponStatusChange(couponStatus: ScangoCouponStatus){
        scanGoProgressDialog.dismiss()
        when(couponStatus) {
            is ScangoCouponLoading -> {
                scanGoProgressDialog.show()
            }
            is ScangoCouponSuccess -> {
                showFPToast(resources.getString(R.string.promo_applied))
            }
            is ScangoCouponFail -> {
                if (!couponStatus.isApply)
                    showFPToast(
                        toastMessage = couponStatus.e.message ?: resources.getString(R.string.promo_code_error),
                        color = ToastColor.RED
                    )
            }
            is ScangoCouponLoadFail -> {
                // Do nothing. Let user enter promo codes manually
            }
        }
    }

    private fun onPromoLoadChange(scangoComponents: List<ScangoComponent<*>>?) {
        scangoComponents?.let {
            promo_epoxy.requestModelBuild()
        }
        button_back.visible()
    }


    override fun applyAction(couponName: String) {
        hideKeyboard()
        viewModel.applyCoupon(couponName, true)
    }

    override fun selectAction(coupon: ScangoCoupon) {
        viewModel.applyCoupon(coupon.coupon, false)
    }

    override fun removeAction(coupon: ScangoCoupon) {
        viewModel.removeCoupon(coupon)
    }


    override fun tncAction() {
        this.startActivity(
            WebViewActivity.getIntent(
                mContext,
                WebConstants.FP_URL_TNC,
                "",
                screenDetails = FPScreen.TNC
            )
        )
    }
}
