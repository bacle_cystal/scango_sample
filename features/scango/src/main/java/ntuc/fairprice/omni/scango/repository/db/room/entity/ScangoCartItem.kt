package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import ntuc.fairprice.omni.scango.Constants.UOM_KG
import ntuc.fairprice.omni.scango.repository.db.room.typeconverter.ScangoConverters
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorItem
import ntuc.fairprice.omni.scango.utils.hasDecimalValue
import ntuc.fairprice.omni.scango.utils.removeRedundant0
import ntuc.fairprice.omni.scango.utils.round
import ntuc.fairprice.omni.scango.utils.toWeightString
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.repository.db.room.typeconverter.Converters
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.entity.getMetadataItem

@Entity
data class ScangoCartItem(
    @PrimaryKey
    var id: Long,
    var itemId: Long,
    var quantity: Int,
    var name: String,
    var fullName: String,
    @field:TypeConverters(Converters::class) var images: List<String>?,
    var url: String?,
    var mrp: Double,
    var discount: Double,
    var sellingPrice: Double,
    var brand: String? = null,
    var image: String? = null,
    var metadata: String? = null,
    @field:TypeConverters(ScangoConverters::class) var offer: List<ScangoOffer>? = null,
    var timestamp: Long = 0L,
    var categoryId: Int? = null,
    //Weight fake date
    var soldByWeight: Int = 0,
    var isFree: Boolean = false,
    var isPwp: Boolean = false,
    var weight: Double = 0.0,
    var displayUnit: String? = "",
    private var _unitOfMeasurement: String? = "",
    var refundQuantity: Double? = 0.0,
    var refundPrice: Double? = 0.0
) {
    fun isSoldByWeight(): Boolean = soldByWeight == 1

    var totalDiscount: Double = 0.0

    val unitOfMeasurement: String
        get() = _unitOfMeasurement ?: ""
//            when (_unitOfMeasurement) {
//            "CAR" -> "123"
//            else -> ""
//        }

    fun hasPwpOffer(): Boolean {
        var hasPwp = false
        offer?.forEach {
            if (it.offerRule?.offerType == OfferType.PWP) {
                hasPwp = true
            }
        }
        return hasPwp
    }

    fun isFreeItem(): Boolean {
        if (isFree || (mrp == 0.0 && discount == 0.0 && offer != null)) {
            isFree = true
        }
        return isFree
    }

    fun getWeightedItemInfo(): String {
        if (UOM_KG.equals(unitOfMeasurement, false)) {
            return "${weight.round(3)} Kg"
        } else {
            return "$quantity"
//            return if(weight.hasDecimalValue()) "${weight.toInt()}" else "$weight"
        }
    }
}

fun ScangoCartItem.isSame(item: Item): Boolean {
    return item.id == id
}

fun List<ScangoCartItem>.has(item: Item): Boolean {
    this.forEach {
        if (it.id == item.id) return true
    }
    return false
}

fun List<ScangoCartItem>.toJSONArray(): JSONArray {
    val cart = JSONArray()
    forEach {
        val jsonObject = JSONObject()
        jsonObject.put("id", it.itemId.toString())
        jsonObject.put("q", it.quantity.toString())
        cart.put(jsonObject)
    }
    return cart
}

fun List<ScangoCartItem>.toCartArray(): JSONArray {
    val cart = JSONArray()
    forEach {
        val jsonObject = JSONObject()
        jsonObject.put("id", "1140960")
        jsonObject.put("p",1.95)
        jsonObject.put("q", "1")
        jsonObject.put("t", it.timestamp)
        cart.put(jsonObject)
    }
    return cart
}

fun ScangoCartItem.toCartItem() = CartItem(
    id = id,
    itemId = itemId,
    quantity = quantity,
    stock = 100,
    name = name,
    fullName = fullName,
    images = images,
    url = url,
    mrp = mrp,
    discount = discount,
    sellingPrice = sellingPrice,
    brand = brand,
    image = image,
    metadata = metadata,
    offer = offer?.toListOffer(),
    timestamp = timestamp.toFloat()
)

fun CartItem.toScangoCartItem() = ScangoCartItem(
    id = id,
    itemId = itemId,
    quantity = quantity,
    name = name,
    fullName = fullName,
    images = images,
    url = url,
    mrp = mrp,
    discount = discount,
    sellingPrice = sellingPrice,
    brand = brand,
    image = image,
    metadata = metadata,
    offer = offer?.toListScangoOffer(),
    timestamp = timestamp.toLong(),
    soldByWeight = 0,
    displayUnit = getDisplayUnits(),
    _unitOfMeasurement = getMetadataItem("Unit Of Measurement")
)


fun ScangoCartItem.toLiquorItem(): LiquorItem =
    LiquorItem(
        imageUrl = images?.get(0) ?: "",
        name = name,
        detail = metadata ?: "",
        quantity = quantity,
        price = sellingPrice.toString()
    )

