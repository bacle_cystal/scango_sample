package ntuc.fairprice.omni.scango.ui.screen.ageverify.manually

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import javax.inject.Inject

class ScangoAgeVerifyManuallyViewModel @Inject constructor(
    application: Application,
    private val scangoStoreRepository: ScangoStoreRepository
) : ScangoBaseViewModel(application) {

    sealed class AgeVerifyState {
        object AgeVerifyFailed : AgeVerifyState()
        object AgeVerificationSuccess : AgeVerifyState()
        object AgeVerificationError : AgeVerifyState()
        object NetworkError : AgeVerifyState()
    }

    val stateLiveData = MutableLiveData<AgeVerifyState>()

    fun verifyPincode(staffId: String, pincode: String) {
        launch {
            scangoStoreRepository.verifyAgePincode(staffId, pincode)
                .onSuccess {
                    stateLiveData.value = AgeVerifyState.AgeVerificationSuccess
                }
                .onError {
                    if (it is ScangoHttpServiceException && it.isConnectionError) {
                        stateLiveData.value = AgeVerifyState.NetworkError
                    } else {
                        stateLiveData.value = AgeVerifyState.AgeVerificationError
                    }
                }
        }
    }
}