package ntuc.fairprice.omni.scango.base

import android.app.ActivityOptions
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_ERROR_TOKEN_EXPIRED
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_NOTICE_10h20
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_REMOVE_LIQUOR_10h30
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_REMOVE_LIQUOR_COMPLETE
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.ScangoFeatureViewModel
import ntuc.fairprice.omni.scango.interfaces.ScangoAppView
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorNoticeFragment
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorRemoveItemFragment
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.BarcodeErrorFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import thor.zopsmart.com.thor.features.account.ui.LoginActivity
import javax.inject.Inject


abstract class ScangoBaseActivity : AppCompatActivity(), ScangoAppView {
    // Override the AppView members
    override val mContext: Context
        get() = this

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    val cartViewModel: ScangoCartViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoCartViewModel::class.java)
    }

    val scangoFeatureViewModel: ScangoFeatureViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoFeatureViewModel::class.java)
    }

    fun replaceFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        supportFragmentManager.beginTransaction().apply {
            setTransactionAnimation(this, transit)
            replace(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun addFragmentFullScreen(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        supportFragmentManager.beginTransaction().apply {
            setTransactionAnimation(this, transit)
            add(R.id.containerFull, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun addFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        supportFragmentManager?.beginTransaction()?.apply {
            setTransactionAnimation(this, transit)
            add(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun addFragmentToggle(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        supportFragmentManager.beginTransaction().apply {
            setTransactionAnimation(this, transit)
            replace(R.id.toggle_include, fragment, TAG)
            commitTransaction(this, false)
        }
    }

    fun launchActivity(className: String) {
        Intent().setClassName(packageName, className)
            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            .also {
                val options = ActivityOptions.makeCustomAnimation(
                    this,
                    ntuc.fairprice.omni.R.anim.left_in,
                    ntuc.fairprice.omni.R.anim.right_out
                )
                startActivity(it)
                overridePendingTransition(0, 0)
            }
    }

    fun setTransactionAnimation(
        fragmentTransaction: FragmentTransaction,
        transitionFrom: TransitionFrom
    ) {
/*        fragmentTransaction.apply {
            when (transitionFrom) {
                TransitionFrom.BOTTOM -> setCustomAnimations(
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom,
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom
                )
                TransitionFrom.RIGHT -> setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_right,
                    R.anim.enter_from_right,
                    R.anim.exit_to_right
                )
                TransitionFrom.NONE -> { // do nothing
                }
                else -> { // do nothing
                }
            }
        }*/
    }

    private fun commitTransaction(
        transaction: FragmentTransaction,
        addToBackStack: Boolean = false
    ) {
        if (addToBackStack) transaction.addToBackStack(null)
        transaction.commit()
    }

    enum class TransitionFrom {
        RIGHT, BOTTOM, LEFT, TOP, NONE
    }

    /**
     * Safe Start activity for result with ActivityOptionsCompats
     *
     * @param a           current activity
     * @param i           intent to launch
     * @param requestCode requestCcde
     * @param options     ActivityOptionsCompat
     */
    fun startActivityForResult(
        a: AppCompatActivity?,
        i: Intent,
        requestCode: Int,
        options: ActivityOptionsCompat?
    ) {
        if (a == null) {
            return
        }
        if (options != null) {
            a.startActivityForResult(
                i,
                requestCode,
                options.toBundle()
            )
        } else {
            a.startActivityForResult(
                i,
                requestCode
            )
        }
    }

    public override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    public override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MessageEvent) {
        when (event.message) {
            MESSAGE_EVENT_NOTICE_10h20 -> {
                if (!cartViewModel.getListTimeRestriction().isNullOrEmpty()) {
                    LiquorNoticeFragment.newInstance().show(
                        supportFragmentManager,
                        LiquorNoticeFragment.TAG
                    )
                }
            }
            MESSAGE_EVENT_REMOVE_LIQUOR_10h30 -> {
                if (!cartViewModel.getListTimeRestriction().isNullOrEmpty()) {
                    LiquorRemoveItemFragment.newInstance().show(
                        supportFragmentManager,
                        LiquorRemoveItemFragment::class.java.name
                    )
                }
            }
            MESSAGE_EVENT_REMOVE_LIQUOR_COMPLETE -> {
                //Reload data after remove item liquor
                val currentActivity = this@ScangoBaseActivity
                currentActivity.reloadData()
            }
            MESSAGE_EVENT_ERROR_TOKEN_EXPIRED -> {
                if (scangoFeatureViewModel.isLoggedIn) {
                    scangoFeatureViewModel.clearCredentialWhenUnAuthAccessToken()
                    val intentOnBottom = Intent(this, ScangoFeatureActivity::class.java).apply {
                        addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    }
                    val intentOnTop = LoginActivity.getIntent(this, intentOnBottom.apply {
                        addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    })
                    TaskStackBuilder.create(this)
                        .addNextIntent(intentOnBottom)
                        .addNextIntentWithParentStack(intentOnTop)
                        .startActivities()
                }
            }
        }
    }


    open fun handleBackPress(): Boolean = false

    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.container)

        //Resume scanner when from barcode error
        if (currentFragment != null && currentFragment.childFragmentManager.backStackEntryCount >= 1)
            currentFragment.childFragmentManager.popBackStack()
        else
            if (currentFragment is BarcodeErrorFragment) {
                if (currentFragment.activity is CodeScannerActivity) {
                    (currentFragment.activity as CodeScannerActivity).reloadData()
                }
            }
        if (!handleBackPress()) {
            super.onBackPressed()
        }
    }

    open fun reloadData() {}
}
