package ntuc.fairprice.omni.scango.ui.screen.qrcode.error

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.scango_fragment_barcode_max.*
import ntuc.fairprice.omni.scango.R

class BarcodeMaxItemFragment : Fragment() {
    companion object {
        fun newInstance() = BarcodeMaxItemFragment()
        val TAG = "BarcodeMaxItemFragment"
    }

    lateinit var viewBinding: ViewDataBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.scango_fragment_barcode_max, container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }

        ic_close_blue.setOnClickListener {
            activity?.onBackPressed()
        }
    }
}
