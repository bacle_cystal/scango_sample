package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity

@Entity
data class ScangoItemInvoice(
    var cartType: String?,
    var date: String?,
    var nameStore: String?,
    var phoneNumber: String?,
    var addressStore: String?,
    var uenNumber: String?,
    var gstNumber: String?,
    var orderNumber: String?,
    var moneyTotal: Double?,
    var moneyGst: Double?,
    var cardNumber: String?,
    var supportName: String?,
    var supportAddress: String?,
    var isReturnItem: Boolean? = false
)