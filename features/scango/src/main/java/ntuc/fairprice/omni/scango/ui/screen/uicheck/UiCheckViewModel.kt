package ntuc.fairprice.omni.scango.ui.screen.uicheck

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import javax.inject.Inject

class UiCheckViewModel @Inject constructor(application: Application) : ScangoBaseViewModel(application)