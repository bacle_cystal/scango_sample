package ntuc.fairprice.omni.scango.ui.screen.tutorial

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.scango_fragment_tutorial_home.*
import ntuc.fairprice.omni.scango.R
import androidx.recyclerview.widget.PagerSnapHelper
import ntuc.fairprice.omni.scango.Constants.REQUEST_NAME_STORE
import ntuc.fairprice.omni.scango.ScangoFeatureActivity
import ntuc.fairprice.omni.scango.base.customview.ScangoBottomSheetDialogFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType

const val ARG_ITEM_POSITION = "item_position"

class HomeTutorialFragment : ScangoBottomSheetDialogFragment() {

    companion object {
        val TAG = "HomeTutorialFragment"

        fun newInstance(position: Int, scanType: Int): HomeTutorialFragment =
            HomeTutorialFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ITEM_POSITION, position)
                    putInt(CodeScannerActivity.SCAN_TYPE, scanType)
                }
            }
    }

    private lateinit var viewModel: HomeTutorialViewModel

    lateinit var listTutorialHome: MutableList<TutorialHomeItem>

    var position = 0
    var scanType = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(R.layout.scango_fragment_tutorial_home, container)
        return  rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeTutorialViewModel::class.java)

        if(arguments != null) {
            position = arguments!!.getInt(ARG_ITEM_POSITION, 0)
            scanType = arguments!!.getInt(CodeScannerActivity.SCAN_TYPE, 1)
        }

        recycler_tutorial_home.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = tutorialHomeAdapter
        }

        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(recycler_tutorial_home)

        indicator.attachToRecyclerView(recycler_tutorial_home, pagerSnapHelper)

        tutorialHomeAdapter.registerAdapterDataObserver(indicator.adapterDataObserver)

        initTutorialList()

        btn_tutorial_got_it.setOnClickListener {
            this.dismiss()
            if(scanType == 1) {
                (activity as ScangoFeatureActivity).startActivityForResult(
                    Intent((activity as ScangoFeatureActivity), CodeScannerActivity::class.java).putExtra(
                        CodeScannerActivity.SCAN_TYPE,
                        ScanType.QR_CODE.value
                    ), REQUEST_NAME_STORE
                )
            }
        }
        recycler_tutorial_home.scrollToPosition(position)
    }

    fun initTutorialList() {
        context?.let {
            listTutorialHome = arrayListOf(
                TutorialHomeItem(
                    getString(R.string.text_tutorial_1_header),
                    getString(R.string.text_tutorial_1_detail),
                    R.drawable.sg_tutorial_1
                ),
                TutorialHomeItem(
                    getString(R.string.text_tutorial_2_header),
                    getString(R.string.text_tutorial_2_detail),
                    R.drawable.sg_tutorial_2
                ),
                TutorialHomeItem(
                    getString(R.string.text_tutorial_3_header),
                    getString(R.string.text_tutorial_3_detail),
                    R.drawable.sg_tutorial_3
                )
            )
        }
        tutorialHomeAdapter.items = listTutorialHome
        tutorialHomeAdapter.notifyDataSetChanged()
        tutorialHomeAdapter.notifyItemChanged(position)
    }

    private val tutorialHomeAdapter: TutorialHomeAdapter by lazy {
        TutorialHomeAdapter(context!!)
    }

}