package ntuc.fairprice.omni.scango.repository.webservice.model

import ntuc.fairprice.omni.scango.repository.db.room.entity.*
import ntuc.fairprice.omni.scango.ui.screen.ageverify.model.AgeVerifyResponse
import ntuc.fairprice.omni.scango.utils.DateTimeUtils
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.features.widget.productcollection.data.getItem
import thor.zopsmart.com.thor.model.Card
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.toCardList
import kotlin.collections.ArrayList

// TODO change this class to hold JSONObject, JSONArray (Handle only specific responses from the Api)
/**
 * Removed extending json object to handle Parse exception when apiResponse is empty.
 * This occurs during the Delete 204 response
 */
data class ApiResponse(val apiResponse: String) {
    fun toJSONObject() = JSONObject(apiResponse)
}

fun ApiResponse.toProduct(): Product {
    return try {
        val data = this.toJSONObject() json "data"
        val productArray = data jsonArray "product"
        val productData = productArray?.getJSONObject(0)
        productData?.toProduct()!!
    } catch (e: Exception) {
        Product()
    }
}

fun ApiResponse.toStore(): Store {
    val data = this.toJSONObject() json "data"
    val storeArray = data jsonArray "store"
    val storeData = storeArray?.getJSONObject(0)
    return storeData?.toStore()!!
}

fun ApiResponse.toScangoStore(): ScangoStore {
    val jsonObj = JSONObject(this.apiResponse)
    return jsonObj.toScangoStore()
}

fun ApiResponse.toTimeStamp(): Long {
    val data = this.toJSONObject() json "data"
    val timeStamp = DateTimeUtils.parseRFC3339DateTime(data string "timestamp"?:"" ) ?: 0
    return timeStamp
}

fun ApiResponse.toListFeedbackOption(): List<FeedbackOption> {
    val jsonObj = JSONObject(this.apiResponse)
    val current_timestamp = jsonObj long "current_timestamp"
    val feedbackArray = jsonObj jsonArray "data" ?: return arrayListOf()
    return feedbackArray.toListFeedbackOption()
}

fun ApiResponse.toCouponList(): ArrayList<ScangoCoupon> {
    val coupon = this.toJSONObject() jsonArrayR arrayListOf("data", "couponWallet")
    val coupons: ArrayList<ScangoCoupon> = arrayListOf()

    coupon!!.forEach {
        coupons.add(it.toCoupon())
    }

    return coupons
}


fun ApiResponse.toAppliedCoupons(): List<ScangoCoupon> {
    val data = this.toJSONObject() json ("data")
    val couponArray = data jsonArray "couponDiscounts" ?: JSONArray()
    val couponList = mutableListOf<ScangoCoupon>()
    couponArray.forEach {
        couponList.add(it.toAppliedCoupon())
    }
    return couponList
}


fun ApiResponse.toCheckoutResult(
    storeId: Long
): ScangoCheckoutResult {
    val data = this.toJSONObject() safeJson "data"
//    val order = data safeJson "order"
    val id = data long "referenceNumber" ?: 0L
//    val discount = order double  "discount" ?: 0.0
    val amount = data string "amount" ?: "0.0"
    return ScangoCheckoutResult(id, amount, 0.0, storeId)
}


fun ApiResponse.toSearchResults(): List<Item> {
    val products = this.toJSONObject() jsonArrayR listOf("data", "product") ?: JSONArray()
    val searchResults: ArrayList<Item> = arrayListOf()
    products.forEach {
        try {
            searchResults.add(it.getItem())
        } catch (ignore: Exception) {
        }
    }
    return searchResults
}


fun ApiResponse.toCards(): List<Card> {
    val data = this.toJSONObject() safeJson "data"
    val cards = data jsonArray "card"
    return cards?.toCardList() ?: listOf()
}

fun ApiResponse.toAgeVerifyResponse(): AgeVerifyResponse {
    val data = this.toJSONObject() json "data"
    val ageVerificationCode = data string "code" ?: ""
    return AgeVerifyResponse(ageVerificationCode)
}