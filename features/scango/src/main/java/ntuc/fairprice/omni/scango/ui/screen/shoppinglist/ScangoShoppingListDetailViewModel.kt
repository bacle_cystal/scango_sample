package ntuc.fairprice.omni.scango.ui.screen.shoppinglist

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import ntuc.fairprice.omni.scango.epoxy.SGComponent
import ntuc.fairprice.omni.scango.epoxy.SGHeader
import ntuc.fairprice.omni.scango.epoxy.SGProductGrid
import ntuc.fairprice.omni.scango.epoxy.SGSubtitle
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.ui.screen.productdetail.viewmodel.ProductDetailLoadGeneralFail
import ntuc.fairprice.omni.scango.ui.screen.productdetail.viewmodel.ProductDetailLoadNetworkFail
import thor.zopsmart.com.thor.base.extensions.getCurrentTimestamp
import thor.zopsmart.com.thor.features.shoppinglist.data.ShoppingListDetail
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.toCartItem
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.repo.CartRepository
import thor.zopsmart.com.thor.repository.db.room.repo.WishlistRepository
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import javax.inject.Inject

class ScangoShoppingListDetailViewModel @Inject constructor(
    mApplication: Application,
    private val repository: WishlistRepository,
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository
) : BaseViewModel(mApplication) {

    var status = MutableLiveData<ShoppingListStatus>()

    private var shoppingListDetail: ShoppingListDetail? = null

    private val cartItems = cartRepository.getCartItemsLiveData()

    private val pageDataInternal = MutableLiveData<List<SGComponent<*>>>()

    val pageData: LiveData<List<SGComponent<*>>> = sgDependantLiveData(cartItems, pageDataInternal) {
        pageDataInternal.value?.let { data ->

            val cartMap = cartItems.value?.associate { it.id to it.quantity }

            data.filter { it is SGProductGrid }
                .flatMap { (it as SGProductGrid).items }
                .forEach { item ->
                    item.quantity = cartMap?.getOrElse(item.id) { 0 }?:0
                }

            data
        }
    }

    fun loadProducts(id: Long) {
        status.value = ShoppingListLoading
        launch {
            repository.loadProducts(id)
                .onSuccess {
                    if(it.items.isNullOrEmpty())
                        status.value = ShoppingListLoadGeneralFail
                    else {
                        status.value = ShoppingListSuccess
                        shoppingListDetail = it
                        loadPage()
                    }
                }
                .onError {
                    if(it is ScangoHttpServiceException && it.isConnectionError){
                        status.value = ShoppingListLoadNetworkFail
                    } else {
                        status.value = ShoppingListLoadGeneralFail
                    }

                }
        }
    }

    private fun loadPage() {
        shoppingListDetail?.let {
            val components = mutableListOf<SGComponent<*>>()

            components.add(SGHeader(it.name))
            components.add(SGSubtitle("${it.productsCount} items"))
            components.add(SGProductGrid(it.items))

            pageDataInternal.value = components
        }
    }

    fun increment(cartItem: CartItem) = launch {
        cartRepository.increment(cartItem)
    }

    fun decrement(cartItem: CartItem) = launch {
        cartRepository.decrement(cartItem)
    }

    fun setQuantity(quantity: Int, cartItem: CartItem) = launch {
        cartRepository.setQuantity(quantity, cartItem)
    }

    fun addAllToCart() = launch {
        shoppingListDetail?.let {
            cartRepository.insertAll(it.items.convertToCartItems())
            cartRepository.syncCart()
        }
    }

    fun removeFromShoppingList(item: Item, shoppingListId: Long) = launch {
        status.value = ShoppingListLoading
        wishlistRepository.getShoppingListsForItem(item)
            .onSuccess { sl ->
                val filteredList = sl.filter { it.id != shoppingListId }

                wishlistRepository.addToShoppinglist(item, filteredList)
                    .onSuccess {
                        status.value = RemovalSuccess
                        loadProducts(shoppingListId)
                    }

            }
            .onError {
                status.value = RemovalFail
            }
    }

}

inline fun <T> sgDependantLiveData(vararg dependencies: LiveData<*>, crossinline mapper: () -> T?): MediatorLiveData<T> {
    return MediatorLiveData<T>().also { mediatorLiveData ->
        val observer = Observer<Any> { mediatorLiveData.value = mapper() }
        dependencies.forEach { dependencyLiveData ->
            mediatorLiveData.addSource(dependencyLiveData, observer)
        }
    }
}

private fun List<Item>.convertToCartItems(): List<CartItem> =
    filter { it.quantity == 0 }.map {
        it.toCartItem().apply {
                quantity++
                timestamp = getCurrentTimestamp()
        }
    }

sealed class ShoppingListStatus
object ShoppingListLoading : ShoppingListStatus()
object ShoppingListSuccess : ShoppingListStatus()
object ShoppingListLoadGeneralFail  : ShoppingListStatus()
object ShoppingListLoadNetworkFail  : ShoppingListStatus()
object RemovalFail : ShoppingListStatus()
object RemovalSuccess : ShoppingListStatus()