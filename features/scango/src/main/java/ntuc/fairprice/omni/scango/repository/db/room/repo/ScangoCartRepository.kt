package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import ntuc.fairprice.omni.scango.repository.db.room.dao.ScangoCartItemDao
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.sharedpref.getSGCartCoupons
import ntuc.fairprice.omni.scango.repository.db.sharedpref.setSGCartCoupons
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoSyncCartService
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoCartRepository @Inject constructor(
    mApplication: Application,
    private val cartSyncService: ScangoSyncCartService,
    private val cartItemDao: ScangoCartItemDao
) : ScangoRepository(mApplication) {
    //Get LiveData
    fun getCartItemsLiveData(): LiveData<List<ScangoCartItem>> = cartItemDao.getCartItemsLiveData()

    fun getFinalPriceLiveData(): LiveData<Double>? = cartItemDao.getFinalPriceLiveData()

    fun getTotalItemsQuantity(): LiveData<Int> = cartItemDao.getTotalItemsLiveData()

    fun clearSelectedCoupons() = localStorage.setSGCartCoupons(mutableListOf())

    fun setCartCoupons(coupons: List<ScangoCoupon>) = localStorage.setSGCartCoupons(coupons)

    fun getCartCoupons() = localStorage.getSGCartCoupons()

    //Function definitions
    private fun insert(cartItem: ScangoCartItem) {
        cartItemDao.insert(cartItem)
    }

    private fun update(cartItem: ScangoCartItem) {
        if (cartItem.isSoldByWeight()) {
            cartItemDao.getCartItem(cartItem.itemId)?.let {
                cartItem.sellingPrice += it.sellingPrice
                cartItem.weight += it.weight
            }
        }
        cartItemDao.update(cartItem)
    }

    fun delete(cartItem: ScangoCartItem) {
        cartItemDao.delete(cartItem)
    }

    fun getCartItems() = cartItemDao.getCartItems().sortedByDescending { it.timestamp }

    fun clearCart() {
        cartItemDao.clearCart()
    }

    fun increment(cartItem: ScangoCartItem, isFromScanning: Boolean = false) {
        if (isFromScanning) {
            cartItem.timestamp = System.currentTimeMillis()
        }

        updateCartQuantity(cartItem, 1)
    }

    fun decrement(cartItem: ScangoCartItem) = updateCartQuantity(cartItem, -1)

    private fun updateCartQuantity(cartItem: ScangoCartItem, quantityChange: Int) {

        val currentQuantity: Int = cartItem.quantity
        val finalQuantity = currentQuantity + quantityChange

        if (finalQuantity <= 0) {
            return delete(cartItem)
        }

        cartItem.quantity = finalQuantity

        return if (currentQuantity == 0) {
            insert(cartItem)
        } else {
            update(cartItem)
        }
    }

    suspend fun setQuantity(quantity: Int, cartItem: ScangoCartItem) {
        return updateCartQuantity(cartItem, quantity - cartItem.quantity)
    }

    fun insertAll(cartItems: List<ScangoCartItem>) {
        cartItemDao.insertAll(ArrayList(cartItems))
    }

    suspend fun sync(): Result<ScangoCartSyncData> {
        return cartSyncService.sync(getCartItems(), getPreferredStoreId()).onSuccess {
            updateCartAfterSync(it.cartSyncItems)
        }
    }

    private fun updateCartAfterSync(
        serverResponseItems: List<ScangoCartItem>
    ) {
        val currentCart = cartItemDao.getCartItems()
        currentCart.forEach { localItem ->
            val currentItemID = localItem.id
            if (!localItem.isSoldByWeight()) {
                localItem.quantity = serverResponseItems.filter { it.id == currentItemID }.sumBy { it.quantity }
            }
            val listItem = serverResponseItems.filter { it.id == currentItemID }
            val hasPwp = listItem.any { it.hasPwpOffer() }

            val offer = serverResponseItems.filter { it.id == currentItemID && it.offer != null }.first().offer
            var discount = serverResponseItems.filter { it.id == currentItemID }.sumByDouble { it.discount }

            var totalPwP = 0.0
            for (item in listItem) {
                item.offer?.let {
                    for (currentOffer in it) {
                        if (currentOffer.isPwP()) totalPwP += (currentOffer.discount.times(item.quantity))
                    }
                }
            }

            localItem.offer = offer
            localItem.isPwp = hasPwp
            localItem.discount = discount
            localItem.totalDiscount =
                serverResponseItems.filter { it.id == currentItemID }.sumByDouble { it.discount.times(it.quantity) }
                    .plus(if (hasPwp) totalPwP else 0.0)
            cartItemDao.update(localItem)
        }
        /*
          val cartItems = arrayListOf<ScangoCartItem>()
         val ids = arrayListOf<Long>()
        serverResponseItems.forEach {
             if (it.mrp != 0.0 || it.discount != 0.0) {
                 if (ids.contains(it.id)) {
                     val item = cartItems.filter { c -> it.id == c.id }[0]
                     item.quantity = item.quantity + it.quantity
                 } else {
                     ids.add(it.id)
                     cartItems.add(it)
                 }
             }
         }
         clearCart()
         cartItemDao.insertAll(cartItems)*/
    }
}