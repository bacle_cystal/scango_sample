package ntuc.fairprice.omni.scango.ui.screen.cart.widget

import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScangoAgeRestrictionWidget: LayoutContainer {
    fun initWidget(view: View)

    fun displayAgeRestrictionWarning()
}