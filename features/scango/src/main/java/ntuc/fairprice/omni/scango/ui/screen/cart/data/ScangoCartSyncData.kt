package ntuc.fairprice.omni.scango.ui.screen.cart.data

import android.annotation.SuppressLint
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import ntuc.fairprice.omni.scango.repository.db.room.entity.isPwpOffer
import ntuc.fairprice.omni.scango.repository.db.room.entity.toScangoOffer
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.InvalidIdentifierException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.features.widget.categorycollection.data.toCategory

data class ScangoCartSyncData(
    val discount: Double,
    val sellingPrice: Double,
    val cartSyncItems: List<ScangoCartItem>,
    val cartOffers: List<ScangoOffer>,
    val totalCouponDiscount: Double
) {
    fun getCartQuantity() = cartSyncItems.filter { !it.isFree }.sumBy {
        if (it.isSoldByWeight()) 1 else it.quantity
    }
}

fun JSONObject.cartItem(): ScangoCartItem {

    val productJSON = this safeJson "product"

    val id = this long "id" ?: throw InvalidIdentifierException(
        "Cart item needs" +
                " an id"
    )
    val quantity = this int "q" ?: 0

    val name = productJSON string "name" ?: ""
    val fullName = productJSON string "fullName" ?: name

    val storeSpecificData = productJSON safeJson "storeSpecificData"
    var mrp = this double "mrp" ?: 0.0
    var cartItemDiscount = storeSpecificData double "discount" ?: 0.0

    val isFree = this boolean "isFree" ?: false
    if (isFree) mrp = 0.0

    val url = productJSON string "slug" ?: ""
    val brandJSON = productJSON safeJson "brand"
    val brand: String? = brandJSON string "name"
    val metadataJSON = productJSON safeJson "metaData"
    val displayUnit = metadataJSON string "DisplayUnit"?:""
    val unitMeasurement = metadataJSON string "Unit Of Measurement"?:""
    val sapID = metadataJSON int "SAP SubClass"?:-1

    val imageList = mutableListOf<String>()
    val jsonImages = productJSON jsonArray "images" ?: JSONArray()
    jsonImages.forEachString { imageList.add(it) }
    val image = if (imageList.isEmpty()) null else imageList[0]

    // Read necessary tags
    var isPwp = false
    val tagArray = productJSON jsonArray "tags" ?: JSONArray()
    tagArray.forEach {
        if (it string "name" == "pwp") {
            isPwp = true
        }
    }
    // Offer parsing
    val appliedOffers: ArrayList<ScangoOffer> = arrayListOf()
    try {
        val offers = this jsonArray "offers" ?: JSONArray()
        offers.forEach { offer ->
            val offerDiscount = offer double "discount" ?: 0.0
            val details = offer json "details"
            val appliedPromoCode = offer string "appliedPromoCode"
            val appliedCount = offer double "appliedCount" ?:1.0
            appliedOffers.add(
                details.toScangoOffer().apply {
                    //                    if (this.offerRule.isPwpOffer()) {
//                        cartItemDiscount += offerDiscount
//                    }
                    this.appliedCount = appliedCount
                    this.mrp = mrp
                    this.discount = offerDiscount
                    this.appliedPromoCode = appliedPromoCode
                }
            )
        }
    } catch (e: Exception) {
    }

    val category = productJSON jsonNullable "primaryCategory"
    val primaryCategory = category?.toCategory()
    val soldByWeight = productJSON int "soldByWeight" ?: 0

    return ScangoCartItem(
        id,
        id,
        quantity,
        name,
        fullName,
        imageList,
        url,
        mrp,
        cartItemDiscount,
        mrp - cartItemDiscount,
        brand,
        image,
        metadataJSON.toString(),
        appliedOffers,
        soldByWeight = soldByWeight,
        categoryId = if (sapID != -1) sapID else primaryCategory?.id ?: 0,
        isFree = isFree,
        isPwp = isPwp,
        displayUnit = displayUnit,
        _unitOfMeasurement = unitMeasurement
    )
}

fun JSONObject.toCartItems(): ArrayList<ScangoCartItem> {
    val cartSyncList = arrayListOf<ScangoCartItem>()
    val itemJSON = jsonArray("items") ?: JSONArray()
    itemJSON.forEach {
        try {
            cartSyncList.add(it.cartItem())
        } catch (e: Exception) {
        }
    }
    return cartSyncList
}

fun JSONObject.toCartSyncData(): ScangoCartSyncData {
    val cartSyncItems = toCartItems()
    val discount = double("savings") ?: 0.0
    val sellingPrice = double("youPay") ?: 0.0
    val cartOffers = toCartOffers()

    val cartJ = safeJson("cart")
    val amounts = cartJ.safeJson("amounts")
    val totalCouponDiscounts = amounts.double("totalCouponDiscount") ?: 0.0

    return ScangoCartSyncData(discount, sellingPrice, cartSyncItems, cartOffers, totalCouponDiscounts)
}


@SuppressLint("UseSparseArrays")
fun List<ScangoCartItem>.toViewData(): ScangoCartSyncViewData {
    val offers = mutableListOf<ScangoOffer>()
    val offerMap = HashMap<Long, ArrayList<ScangoCartItem>>()
    val freeItemMap = HashMap<Long, ArrayList<ScangoCartItem>>()
    val freeItems = mutableListOf<ScangoCartItem>()
    val freeOfferId = mutableListOf<Long>()
    val jwcOffers = mutableListOf<ScangoOffer>()
    val buyItems = mutableListOf<ScangoCartItem>()
    // Add all free items in a list. Free gift will only have
    forEach {
        if (it.isFreeItem()) {
            freeItems.add(it)
            freeOfferId.add(it.offer!![0].id)
        }
    }
    // Add all bought items in to a list. Create an offer to item Id mapping.
    forEach {
        if (it.mrp != 0.0) {
            buyItems.add(it)
            it.offer?.forEach { offer ->
                if (freeOfferId.contains(offer.id)) {
                    val list = freeItemMap[offer.id]
                    if (list != null) {
                        list.add(it)
                        freeItemMap[offer.id] = list
                    } else {
                        freeItemMap[offer.id] = arrayListOf(it)
                    }
                } else {
                    if (!offer.isJwc && !offer.offerRule.isPwpOffer()) {
                        offers.add(offer)
                        val list = offerMap[offer.id]
                        if (list != null) {
                            list.add(it)
                            offerMap[offer.id] = list
                        } else {
                            offerMap[offer.id] = arrayListOf(it)
                        }
                    } else {
                        if (offer.isJwc) {
                            offer.discount = offer.discount * it.quantity
                            jwcOffers.add(offer)
                        }
                    }
                }
            }
        }
    }
    return ScangoCartSyncViewData(offers, offerMap, freeItemMap, freeItems, buyItems, jwcOffers)
}


private fun JSONObject.toCartOffers(): ArrayList<ScangoOffer> {
    val cartOffers = arrayListOf<ScangoOffer>()
    (this jsonArray "offers" ?: JSONArray()).forEach {
        val cartDiscount = it double "cartDiscount" ?: 0.0
        val offer =
            (it safeJson "offer").toScangoOffer().apply {
                discount = cartDiscount
            }
        cartOffers.add(offer)
    }
    return cartOffers
}

data class ScangoCartSyncViewData(
    var offers: List<ScangoOffer>,
    val offerMap: HashMap<Long, ArrayList<ScangoCartItem>>,
    val freeItemMap: HashMap<Long, ArrayList<ScangoCartItem>>,
    val freeItems: List<ScangoCartItem>,
    val buyItems: List<ScangoCartItem>,
    var jwcOffer: List<ScangoOffer>
)