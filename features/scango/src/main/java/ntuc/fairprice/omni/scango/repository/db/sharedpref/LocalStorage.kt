package ntuc.fairprice.omni.scango.repository.db.sharedpref

import android.content.SharedPreferences
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.typeconverter.ScangoConverters
import thor.zopsmart.com.thor.repository.db.sharedpref.get
import thor.zopsmart.com.thor.repository.db.sharedpref.set

fun SharedPreferences.setSGCartCoupons(selectedCoupons: List<ScangoCoupon>) = set("cartCoupons", ScangoConverters.toStringCouponList(selectedCoupons))

fun SharedPreferences.getSGCartCoupons() = ScangoConverters.toCouponList(get("cartCoupons", null as String?)) ?: listOf()