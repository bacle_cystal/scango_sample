package ntuc.fairprice.omni.scango.repository.webservice.serviceclient

import okhttp3.Call
import org.json.JSONObject

interface ScangoRequestProvider {

    sealed class RequestFor {
        object OTHER : RequestFor()
        object ZOPSMART : RequestFor()
        object SCANGO : RequestFor()
        object SCANGO_V2 : RequestFor()
    }

    sealed class Method {
        object GET : Method()
        object POST : Method()
        object PUT : Method()
        object DELETE : Method()
    }

    fun get(
        method: ScangoRequestProvider.Method,
        url: String,
        headers: HashMap<String, String> = HashMap(),
        params: JSONObject = JSONObject(),
        isGuest: Boolean = false,
        forWho: RequestFor
    ): Call
}