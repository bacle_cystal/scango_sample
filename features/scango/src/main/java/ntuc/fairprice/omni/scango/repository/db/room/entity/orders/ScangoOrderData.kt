package ntuc.fairprice.omni.scango.repository.db.room.entity.orders

import com.google.gson.annotations.SerializedName

data class ScangoOrderData(
    var code: Int?,
    var `data`: Data?,
    var status: String?
) {
    data class Data(
        var count: Int?,
        var limit: Int?,
        var offset: Int?,
        var isLast: Boolean?,
        var orders: List<Order>
    ) {
        data class Order(
            var address: Address?,
            var addressId: String?,
            var amount: String?, //
            var clientId: Any?,
            var completedAt: Any?,
            var couponCode: Any?,
            var couponDiscount: String?,
            var createdAt: String?, //
            var customer: Customer?,
            var discount: String?,
            var externalOrderId: Any?,
            var extraCharges: String?,
            var id: Int?,
            var invoiceAmount: String?,
            var itemCount: Int?,
            var metaData: List<Any?>?,
            var notes: Any?,
            var payment: List<Payment?>?,
            var paymentStatus: String?,
            var pendingAmount: String?,
            var placedFrom: String?,
            var placedOn: String?,
            var preferredDate: String?,
            var rating: Any?,
            var referenceNumber: String?,
            var refund: List<Any?>?,
            var refundAmount: String?,
            var shipping: String?,
            var slotEndTime: String?,
            var slotStartTime: String?,
            var slotType: String?,
            var status: String?,
            var store: Store?,
            var type: Type?
        ) {
            data class Store(
                var address: String?,
                var clientStoreId: Any?,
                var id: Int?,
                var latitude: String?,
                var longitude: String?,
                var metaData: MetaData?,
                var name: String?
            ) {
                data class MetaData(
                    var type: String?
                )
            }

            data class Type(
                var id: Int?,
                var name: String?
            )

            data class Address(
                var address: String?,
                var city: String?,
                var clientId: Any?,
                var id: Int?,
                var landmark: String?,
                var latitude: String?,
                var longitude: String?,
                var metaData: Any?,
                var pincode: Int?
            )

            data class Payment(
                var amount: String?,
                var bankTransactionId: Any?,
                var completedAt: Any?,
                var createdAt: String?,
                var gatewayTransactionId: Any?,
                var id: Int?,
                var metaData: Any?,
                var mode: String?,
                var paymentServiceId: Any?,
                var status: String?,
                var transactionId: String?
            )

            data class Customer(
                var addresses: List<Addresse?>?,
                var clientId: Any?,
                var clientIds: List<Any?>?,
                var defaultAddress: DefaultAddress?,
                var emails: List<Email?>?,
                var id: Int?,
                var image: String?,
                var isRegistered: Boolean?,
                var joinedOn: String?,
                var joinedTime: String?,
                var lastLoginTime: String?,
                var lastOrderedOn: String?,
                var metaData: MetaData?,
                var name: String?,
                var orderSummary: OrderSummary?,
                var phones: List<Phone?>?,
                var totalAmount: String?,
                var totalOrders: Int?,
                var updatedAt: String?
            ) {
                data class Email(
                    var email: String?,
                    var id: Int?,
                    var status: String?
                )

                data class OrderSummary(
                    var monthlyPurchaseTrend: List<MonthlyPurchaseTrend?>?,
                    var totalAmount: String?
                ) {
                    data class MonthlyPurchaseTrend(
                        var month: String?,
                        var orderAmount: String?,
                        var orderCount: Int?
                    )
                }

                data class Phone(
                    var id: Int?,
                    var phone: String?,
                    var status: String?
                )

                data class MetaData(
                    var Gender: String?,
                    @SerializedName("JWC Member")
                    var JWCMember: Boolean?,
                    @SerializedName("Marital Status")
                    var MaritalStatus: String?,
                    var NRIC: String?
                )

                data class Addresse(
                    var address: String?,
                    var city: String?,
                    var clientId: Any?,
                    var id: Int?,
                    var landmark: String?,
                    var latitude: String?,
                    var longitude: String?,
                    var metaData: Any?,
                    var pincode: Int?
                )

                data class DefaultAddress(
                    var address: String?,
                    var city: String?,
                    var clientId: Any?,
                    var id: Int?,
                    var landmark: String?,
                    var latitude: String?,
                    var longitude: String?,
                    var metaData: Any?,
                    var pincode: Int?
                )
            }
        }
    }
}

enum class ScangoPaymentType(val value: String) {
    COD("COD"),
    ONLINE("ONLINE")
}