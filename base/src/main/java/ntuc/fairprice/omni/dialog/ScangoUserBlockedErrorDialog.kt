package ntuc.fairprice.omni.dialog

import android.content.Context
import ntuc.fairprice.omni.R

class ScangoUserBlockedErrorDialog(context: Context): ScangoBaseErrorDialog(context) {

    init {
        title.text = context.resources.getString(R.string.user_blocked_error_title)
        message.text = context.resources.getString(R.string.user_blocked_error_message)
        erroImage.setImageResource(R.drawable.sg_ic_user_blocked)
        buttonAction.text = context.resources.getString(R.string.btn_ok_got_it)
    }
}