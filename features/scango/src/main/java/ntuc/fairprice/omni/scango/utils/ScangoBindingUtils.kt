package ntuc.fairprice.omni.scango.utils

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.Spanned
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.R as FPR

import ntuc.fairprice.omni.scango.base.customview.CustomTypefaceSpan
import java.io.File
import android.text.SpannableStringBuilder
import android.util.TypedValue
import androidx.core.content.res.ResourcesCompat


/**
 * Created by Binh.TH on 2/13/2019.
 */


@SuppressLint("CheckResult")
@BindingAdapter(
    value = ["loadImage", "localBackground", "placeholder", "centerCrop", "fitCenter", "circleCrop",
        "cacheSource", "animation"],
    requireAll = false
)
fun ImageView.loadImage(
    imageUrl: String? = "",
    localImage: Drawable?,
    placeHolder: Drawable?,
    centerCrop: Boolean = false,
    fitCenter: Boolean = false,
    circleCrop: Boolean = false,
    isCacheSource: Boolean = false,
    animation: Boolean = false
) {
    if (TextUtils.isEmpty(imageUrl)) {
        setImageDrawable(placeHolder)
        return
    }

    val requestBuilder: RequestBuilder<Drawable>
    val localImageId =
        context.resources.getIdentifier(imageUrl, "drawable", "ntuc.fairprice.omni.beta.scango")
    requestBuilder = if (localImageId != 0) {
        // Load image from local
        localImage?.let { background = localImage }
        Glide.with(context).load(localImageId)
    } else {
        // image is not in local, but may be in server.
        Glide.with(context).load(imageUrl)
    }
    val diskCacheStrategy = if (isCacheSource) {
        DiskCacheStrategy.DATA
    } else {
        DiskCacheStrategy.RESOURCE
    }
    val requestOptions =
        RequestOptions().diskCacheStrategy(diskCacheStrategy).placeholder(placeHolder)
    if (!animation) {
        requestOptions.dontAnimate()
    }
    if (centerCrop) {
        requestOptions.centerCrop()
    }
    if (fitCenter) {
        requestOptions.fitCenter()
    }
    if (circleCrop) {
        requestOptions.circleCrop()
    }
    val file = File(imageUrl)
    if (file.exists()) {
        requestOptions.signature(ObjectKey(file.lastModified().toString()))
    }
    requestBuilder.apply(requestOptions).into(this)
}

@BindingAdapter("loadImageLocal")
fun ImageView.loadImage(imageName: String?) {
    if (!TextUtils.isEmpty(imageName)) {
        setImageResource(context.resources.getIdentifier(imageName, "drawable", "ntuc.fairprice.omni.beta.scango"))
    }
}

@BindingAdapter("android:layout_marginBottom")
fun setBottomMargin(view: View, bottomMargin: Float) {
    val layoutParams = view.layoutParams as MarginLayoutParams
    layoutParams.setMargins(
        layoutParams.leftMargin, layoutParams.topMargin,
        layoutParams.rightMargin, Math.round(bottomMargin)
    )
    view.layoutParams = layoutParams
}

@BindingAdapter("scangoCardNumber")
fun TextView.setCardText(number: String?) {
    number?.let {
        if (number.length < 4) {
            text = number
        } else {
            val fontRegular = ResourcesCompat.getFont(context, FPR.font.lato)
            val fontBold = ResourcesCompat.getFont(context, FPR.font.lato_bold)
            fontRegular?.let { fontRegular ->
                fontBold?.let { fontBold ->
                    val subString = number.substring(number.length - 4)
                    val textSub = resources.getString(R.string.label_card_format, subString)
                    val spanableText = SpannableStringBuilder(textSub)
                    spanableText.setSpan(CustomTypefaceSpan("", fontBold), 0, 4, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)
                    spanableText.setSpan(
                        CustomTypefaceSpan("", fontRegular),
                        4,
                        textSub.length,
                        Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                    )
                    text = spanableText
                }
            }
        }
    }
}

fun TextView.setLinkPointText(dollarNumber: String, pointNumber: String) {
    val fontRegular = ResourcesCompat.getFont(context, FPR.font.lato)
    val fontBold = ResourcesCompat.getFont(context, FPR.font.lato_bold)
    fontRegular?.let { fontRegular ->
        fontBold?.let { fontBold ->
            val textStart = pointNumber
            val textEnd = resources.getString(R.string.text_linkpoint_format, dollarNumber)
            val textAll = textStart.plus(" $textEnd")
            val spanableText = SpannableStringBuilder(textAll)
            spanableText.setSpan(
                CustomTypefaceSpan("", fontBold),
                0,
                textStart.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            spanableText.setSpan(
                CustomTypefaceSpan("", fontRegular),
                textStart.length,
                textAll.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            text = spanableText
        }
    }
}


fun TextView.setTextDirectFAQ() {
    val fontRegular = ResourcesCompat.getFont(context, FPR.font.lato)
    val fontBold = ResourcesCompat.getFont(context, FPR.font.lato_bold)
    fontRegular?.let { fontRegular ->
        fontBold?.let { fontBold ->
            val textStart = resources.getString(R.string.text_scango_get_help) + " "
            val textMiddle = resources.getString(R.string.text_scan_amp_go_faq) + " "
            val textEnd = resources.getString(R.string.text_scango_any_questions)
            val textAll = textStart.plus(textMiddle).plus(textEnd)
            val spanableText = SpannableStringBuilder(textAll)
            spanableText.setSpan(
                CustomTypefaceSpan("", fontRegular, resources.getColor(R.color.brownish_grey)),
                0,
                textStart.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            spanableText.setSpan(
                CustomTypefaceSpan("", fontBold, resources.getColor(R.color.nice_blue)),
                textStart.length,
                textStart.length + textMiddle.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )
            spanableText.setSpan(
                CustomTypefaceSpan(
                    "", fontRegular, resources.getColor(R.color.brownish_grey)),
                    textStart.length + textMiddle.length,
                    textAll.length,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                )
                        text = spanableText
        }
    }
}


@BindingAdapter("leftRight_padding")
fun View.bindPaddingLeftRight(padding: Float) {
    val paddingDp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, padding, resources.displayMetrics).toInt()
    setPadding(
        paddingDp,
        // these call getPaddingTop etc on the receiver View
        this.paddingTop,
        paddingDp,
        this.paddingBottom
    )
}
