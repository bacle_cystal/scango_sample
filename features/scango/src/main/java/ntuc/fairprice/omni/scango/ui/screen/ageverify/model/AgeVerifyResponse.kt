package ntuc.fairprice.omni.scango.ui.screen.ageverify.model

import androidx.room.Entity

@Entity
data class AgeVerifyResponse(
    var ageVerificationCode: String?
)