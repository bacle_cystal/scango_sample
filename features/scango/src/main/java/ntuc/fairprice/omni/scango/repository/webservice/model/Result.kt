package ntuc.fairprice.omni.scango.repository.webservice.model

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.base.MessageEvent
import ntuc.fairprice.omni.scango.utils.HttpStatusCode
import okhttp3.Call
import okhttp3.Response
import okhttp3.ResponseBody
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

sealed class Result<out T : Any>

const val TAG = "ScangoHttpResult"
const val defaultErrorMessage = "There was an error processing your request"

data class Success<out T : Any>(val data: T) : Result<T>()

data class Failure(val error: Throwable?) : Result<Nothing>()

inline fun <T : Any> Result<T>.onSuccess(action: (T) -> Unit): Result<T> {
    if (this is Success)
        action(data)
    return this
}

inline fun <T : Any> Result<T>.onError(action: (Throwable) -> Unit): Result<T> {
    if (this is Failure && error != null)
        action(error)
    return this
}

fun <T : Any> Result<ApiResponse>.parse(converter: ((ApiResponse) -> T)): Result<T> {
    return when (this) {
        is Success -> Success(converter(this.data))
        is Failure -> Failure(this.error)
    }
}

suspend fun retrofit2.Call<ResponseBody>.getRetrofitResult(): Result<ApiResponse> {

    return withContext(Dispatchers.IO) {
        val response = execute()
        if (response.isSuccessful) {

            response.body()?.let {
                return@withContext Success(ApiResponse(it.string()))
            }
        }
        return@withContext Failure(
            ScangoHttpServiceException(
                "Failed to call the API", response.code(), response.body()?.toString()
            )
        )
    }

}

// Extension for the Result
suspend fun Call.getResult(): Result<ApiResponse> {
    return withContext(Dispatchers.IO) {
        try {
            val response = execute()
            val body = response.body()
            if (!response.isSuccessful) {
                val errorMessage = getErrorMessageFromResponse(response)

                val httpException =
                    ScangoHttpServiceException(errorMessage, response.code(), response.body()?.string() ?: "")
                if (httpException.isUnauthorized) {
                    EventBus.getDefault().post(
                        MessageEvent(
                            Constants.MESSAGE_EVENT_ERROR_TOKEN_EXPIRED
                        )
                    )
                }
                throw httpException
            }


            val result = body!!.string()
            body.close()
            response.close()
            Success(
                ApiResponse(
                    result
                )
            )
        } catch (e: ScangoHttpServiceException) {
            Failure(e)
        } catch (e: Exception) {
            Failure(ScangoHttpServiceException(e.message.orEmpty(), HttpStatusCode.HTTP_UNKNOWN, ""))
        }
    }
}

private fun getErrorMessageFromResponse(response: Response): String = when (response.code()) {
    504 -> "Timeout Error"
    500 -> "Server Error"
    else -> getErrorMessageFromResult(response.body()?.toString())
}

fun getErrorMessageFromResult(result: String?): String {
    val separator = ":"
    val name = "message"
    var errorMessage = try {
        val error: JSONObject? = JSONObject(result)
        if (null != error && error.has(name) && !error.isNull(name)) error.getString(name) else defaultErrorMessage
    } catch (e: JSONException) {
        defaultErrorMessage
    }

    if (errorMessage.contains(separator)) {
        errorMessage = errorMessage.split(separator.toRegex(), 2).toTypedArray()[1]
    }
    return errorMessage
}
