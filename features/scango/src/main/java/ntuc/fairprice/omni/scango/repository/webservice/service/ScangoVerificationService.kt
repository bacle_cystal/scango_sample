package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoVerificationService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) :
    ScangoBaseService(scangoRequestProvider) {

    suspend fun getRestrictionsConfig(): Result<String> =
        apiCall(
            "/verification/v0.2/categories",
            ScangoRequestProvider.Method.GET,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
        ) {
            it.apiResponse
        }
}