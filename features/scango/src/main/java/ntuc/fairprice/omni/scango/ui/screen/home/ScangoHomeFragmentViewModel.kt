package ntuc.fairprice.omni.scango.ui.screen.home

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.base.ScangoComponent
import ntuc.fairprice.omni.scango.base.ScangoDelegate
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.repo.ProductRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoMainRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.dependantLiveData
import javax.inject.Inject

class ScangoHomeFragmentViewModel @Inject constructor(
    application: Application,
    val productRepository: ProductRepository,
    val cartRepository: ScangoCartRepository,
    val mainRepository: ScangoMainRepository,
    val scangoStoreRepository: ScangoStoreRepository
) : ScangoBaseViewModel(application) {
    val cartItemsMutable = MutableLiveData<List<ScangoCartItem>>()

    private val pageDataInternal = MutableLiveData<List<ScangoComponent<*>>>()

    val pageData: LiveData<List<ScangoComponent<ScangoDelegate>>> = dependantLiveData(pageDataInternal) {
        pageDataInternal.value?.let { data ->
            data as List<ScangoComponent<ScangoDelegate>>
        }
    }


    val isHasItemInCart = MediatorLiveData<Boolean>().apply {
        addSource(cartItemsMutable) {
            it?.let {
                value = !it.isNullOrEmpty()
            }
        }
    }.default(false)

    val numberItemInCart = MediatorLiveData<Int>().apply {
        addSource(cartItemsMutable) {
            it?.let { listCartItem ->
                value = listCartItem.sumBy { if (it.isSoldByWeight()) 1 else it.quantity }
            }
        }
    }.default(0)

    fun getCartItemsLiveData() {
        cartItemsMutable.value = cartRepository.getCartItems()
    }

    fun loadPage() {
        launch {
            mainRepository.loadHomePage(scangoStoreRepository.getStoreIdCached().toString())
                .onSuccess {
                    pageDataInternal.value = it
                    onSuccess()
                }
                .onError {
                    onLoadFail(it)
                }
        }
    }
}
