package ntuc.fairprice.omni.scango.repository.webservice.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ntuc.fairprice.omni.scango.di.DaggerScangoComponent
import ntuc.fairprice.omni.scango.repository.db.room.repo.ProductRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.BaseApplication
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

class StoreCheckoutService : Service() {

    @Inject
    lateinit var productRepository: ProductRepository
    @Inject
    lateinit var accountRepository: AccountRepository
    @Inject
    lateinit var scangoStoreRepository: ScangoStoreRepository

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        /**
         *  Very Bad way of injecting in service. But since we don't have a better way right now,
         *  we will leave it for now and take up on refactor
         */
        DaggerScangoComponent.builder()
            .coreComponent(
                (applicationContext as BaseApplication).coreComponent)
            .build()
            .inject(this)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        CoroutineScope(Dispatchers.IO).launch {
            scangoStoreRepository.getCheckoutStore().onSuccess {
                scangoStoreRepository.clearStoreInfo()
                stopSelf()
            }.onError {
                it.printStackTrace()
                stopSelf()
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }
}