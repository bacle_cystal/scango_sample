package ntuc.fairprice.omni.toggle

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.layout_under_header_instore.*
import kotlinx.android.synthetic.main.toggle.*
import ntuc.fairprice.omni.BR
import ntuc.fairprice.omni.Constants
import ntuc.fairprice.omni.R
import ntuc.fairprice.omni.ui.widget.ToggleButtonView
import sg.ntucenterprise.nlauncher.NLauncher

class ToggleFragment : Fragment(), ToggleButtonView.OnSelectedChangeListener {


    companion object {
        val TAG = "ToggleFragment"
        private const val INIT_TOGGLE_STATE = "init_toggle_state"
        fun newInstance(isOnStore: Boolean = false) = ToggleFragment().apply {
            arguments = Bundle().apply {
                putBoolean(INIT_TOGGLE_STATE, isOnStore)
            }
        }
    }

    private lateinit var viewBinding: ViewDataBinding

    val viewModel: ToggleFragmentViewModel by lazy {
        ViewModelProviders.of(this@ToggleFragment).get(ToggleFragmentViewModel::class.java)
    }

    private var callBackListener: CallBackListener? = null

    fun setCallbackListener(callback: CallBackListener) {
        this.callBackListener = callback
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.toggle, container, false)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewBinding.lifecycleOwner = this

        viewBinding.apply {
            setVariable(BR.viewModel, viewModel)
            executePendingBindings()
        }
        viewModel.apply {
            isOnStore.observe(this@ToggleFragment, Observer {
                this@ToggleFragment.run {
                    when (it) {
                        false -> {
                            header_layout.layoutParams.height =
                                resources.getDimensionPixelSize(R.dimen.main_fragment_header_layout_height)
                            header_layout.setBackgroundColor(
                                ContextCompat.getColor(
                                    activity!!,
                                    R.color.nice_blue
                                )
                            )
                        }
                        true -> {
                            header_layout.layoutParams.height =
                                resources.getDimensionPixelSize(R.dimen.main_fragment_header_layout_height_not_checkin)
                            header_layout.setBackgroundColor(
                                ContextCompat.getColor(
                                    activity!!,
                                    R.color.nice_blue
                                )
                            )
                        }
                    }
                }
            })
            changeScreen.observe(viewLifecycleOwner, Observer {
                when (isOnStore.value) {
                    true -> launchActivity(Constants.scanGoClassname)
                    false -> launchActivity(Constants.fairpriceOnClassname)
                }
            })

            isVisiableCompanyLayout.observe(viewLifecycleOwner, Observer {
                when (it) {
                    true -> header_layout.layoutParams.height =
                        resources.getDimensionPixelSize(R.dimen.main_fragment_header_layout_height_checked_in)
                    false -> if (viewModel.isOnStore.value == true) {
                        header_layout.layoutParams.height =
                            resources.getDimensionPixelSize(R.dimen.main_fragment_header_layout_height_not_checkin)
                    }
                }
            })
        }

        arguments?.getBoolean(INIT_TOGGLE_STATE)?.let {
            if (it) {
                (toggle_btn as ToggleButtonView).updateInitState()
                viewModel.isOnStore.value = true
            }
        }

        txExit.setOnClickListener { ic_exit_store.callOnClick() }
        ic_exit_store.setOnClickListener {
            callBackListener?.onStoreExitClicked(viewModel.companyName.value ?: "")
        }

        NLauncher.getInstance().boolLiveDataWith(Constants.SCAN_AND_GO_KEY,false).
            observe(this@ToggleFragment, Observer { viewModel.changeFeatureFlag(it) })

        toggle_btn.setOnSelectedChangeListener(this)
    }

    override fun onSelectedChange(state: Boolean) {
        viewModel.changeState(state)
    }

    fun navigateToFPOnHome() {
        viewModel.changeState(true)
    }

    fun launchActivity(className: String) {
        Intent().setClassName(activity, className)
            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            .also {
                startActivity(it)
                activity?.overridePendingTransition(0, 0)
                activity?.finish()
            }
    }

    fun updateData(cityName: String) {
        viewModel.changeStateCompanyLayout(cityName)
    }

    interface CallBackListener {
        fun onStoreExitClicked(storeName: String)
    }
}