package ntuc.fairprice.omni.scango.ui.screen.home

enum class Tab(val position: Int) {
    HOME(0),
    PROMOTIONS(1),
    SCAN(2),
    FAVORITE(3),
    MORE(4)
}
