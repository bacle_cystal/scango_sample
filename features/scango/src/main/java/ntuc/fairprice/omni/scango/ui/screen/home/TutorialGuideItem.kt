package ntuc.fairprice.omni.scango.ui.screen.home

data class TutorialGuideItem(
        val imageId: Int,
        val textGuide: String,
        val position: Int
)