package ntuc.fairprice.omni.scango

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ntuc.fairprice.omni.OmniApplication
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_NOTICE_10h20
import ntuc.fairprice.omni.scango.Constants.MESSAGE_EVENT_REMOVE_LIQUOR_10h30
import ntuc.fairprice.omni.scango.Constants.REQUEST_ALARM_NOTICE_10h20
import ntuc.fairprice.omni.scango.Constants.REQUEST_ALARM_REMOVE_LIQUOR_10h30
import ntuc.fairprice.omni.scango.base.MessageEvent
import org.greenrobot.eventbus.EventBus

class AlarmBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val requestcode = intent?.extras?.getInt(Constants.INTENT_ALARMSERVICE_REQUESTCODE)
        context?.let {
            if ((it.applicationContext as OmniApplication).foregroundManager.isInAppForeground()) {
                when (requestcode) {
                    REQUEST_ALARM_NOTICE_10h20 ->
                        EventBus.getDefault().post(
                            MessageEvent(
                                MESSAGE_EVENT_NOTICE_10h20
                            )
                        )

                    REQUEST_ALARM_REMOVE_LIQUOR_10h30 ->
                        EventBus.getDefault().post(
                            MessageEvent(MESSAGE_EVENT_REMOVE_LIQUOR_10h30)
                        )
                }
            }
        }
    }
    /*private fun handleNotice10h20(context: Context?) {
        // Create the notification to be shown
        val mBuilder = NotificationCompat.Builder(context!!, "scango")
            .setSmallIcon(R.mipmap.scango_ic_launcher)
            .setContentTitle("We’ve noticed that you have liquor items in your cart. Please make payment before 10:30pm.")
            .setContentText("FairPrice is not allowed to sell liquor items from 10:30 pm to 7:00 am according to the Sigaporean law of Liquor Control (Supply and Consumption) Act 2015.")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        // Get the Notification manager service
        val am = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Generate an Id for each notification
        val id = System.currentTimeMillis() / 1000

        // Show a notification
        am.notify(id.toInt(), mBuilder.build())
    }*/

}