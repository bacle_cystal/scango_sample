package ntuc.fairprice.omni.scango.ui.screen.linkplus.otp

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import javax.inject.Inject

class ScangoLinkPlusOtpViewModel @Inject constructor(
    application: Application,
    private val scangoLinkPointRepository: ScangoLinkPointRepository
) : ScangoBaseViewModel(application) {

    private var otpAlreadySent = false

    sealed class State {
        object OtpSuccess : State()
        object OtpFailed : State()

        object OtpVerificationSuccess : State()
        object OtpVerificationError : State()

        object NetworkError : State()
    }

    val stateLiveData = MutableLiveData<State>()

    fun sendOtp() {
        if (!otpAlreadySent) {
            otpAlreadySent = true
        }
        launch {
            scangoLinkPointRepository.sendOtp(otpAlreadySent)
                .onSuccess {
                    stateLiveData.value = State.OtpSuccess
                }
                .onError {
                    if(it is ScangoHttpServiceException && it.isConnectionError){
                        stateLiveData.value = State.NetworkError
                    } else {
                        stateLiveData.value = State.OtpFailed
                    }
                }
        }
    }

    fun verifyOtp(otp: String) {
        launch {
            scangoLinkPointRepository.verifyOtp(otp)
                .onSuccess {
                    stateLiveData.value = State.OtpVerificationSuccess
                }
                .onError {
                    if(it is ScangoHttpServiceException && it.isConnectionError){
                        stateLiveData.value = State.NetworkError
                    } else {
                        stateLiveData.value = State.OtpVerificationError
                    }
                }
        }
    }
}