package ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.widget

import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.scango_activity_link_plus_otp.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.R as RB
import javax.inject.Inject
import android.widget.EditText
import android.widget.TextView


class ScangoLinkPlusOtpWidgetImpl @Inject constructor() : ScangoLinkPlusOtpWidget {

    private var currentlyFocusedEditText: EditText? = null
    private var timer: CountDownTimer? = null


    override lateinit var containerView: View

    override var onOtpInputComplete: (() -> Unit)? = null

    override fun initWidget(view: View) {
        containerView = view
        initListener()
    }

    private val canSendOtp: Boolean
        get() = getOtp().length == 6

    override fun displayDefaultState() {
        reset(false)
        enableButton()
        txt_link_plus_otp_wrong.visibility = View.GONE
        txt_link_plus_otp_try_another_number.visibility = View.GONE
        txt_link_plus_otp_wrong_image.visibility = View.GONE
        txt_link_plus_otp_try_another_number.visibility = View.GONE
        txt_link_plus_otp_change_number.visibility = View.VISIBLE
    }

    override fun displayErrorState() {
        reset(true)
        txt_link_plus_otp_wrong.visibility = View.VISIBLE
        txt_link_plus_otp_try_another_number.visibility = View.VISIBLE
        txt_link_plus_otp_wrong_image.visibility = View.VISIBLE
        txt_link_plus_otp_try_another_number.visibility = View.VISIBLE
        txt_link_plus_otp_change_number.visibility = View.GONE
    }

    override fun showTimer() {
        timer?.cancel()
        timer = object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                val lapsedSecs = (millisUntilFinished / 1000) + 1
                txt_link_plus_otp_time.text = "${lapsedSecs}s"
            }

            override fun onFinish() {
                hideTimer()
            }
        }.start()
        txt_link_plus_otp_time.visibility = View.VISIBLE
        disableButton()
    }

    override fun hideTimer() {
        timer?.cancel()
        txt_link_plus_otp_time.visibility = View.GONE
        enableButton()
    }

    private fun initListener() {
        edt_link_plus_otp_1.addTextChangedListener(textWatcher)
        edt_link_plus_otp_2.addTextChangedListener(textWatcher)
        edt_link_plus_otp_3.addTextChangedListener(textWatcher)
        edt_link_plus_otp_4.addTextChangedListener(textWatcher)
        edt_link_plus_otp_5.addTextChangedListener(textWatcher)
        edt_link_plus_otp_6.addTextChangedListener(textWatcher)

        edt_link_plus_otp_1.onFocusChangeListener = focusChangeListener
        edt_link_plus_otp_2.onFocusChangeListener = focusChangeListener
        edt_link_plus_otp_3.onFocusChangeListener = focusChangeListener
        edt_link_plus_otp_4.onFocusChangeListener = focusChangeListener
        edt_link_plus_otp_5.onFocusChangeListener = focusChangeListener
        edt_link_plus_otp_6.onFocusChangeListener = focusChangeListener

        edt_link_plus_otp_1.setOnKeyListener(onKeyListener)
        edt_link_plus_otp_2.setOnKeyListener(onKeyListener)
        edt_link_plus_otp_3.setOnKeyListener(onKeyListener)
        edt_link_plus_otp_4.setOnKeyListener(onKeyListener)
        edt_link_plus_otp_5.setOnKeyListener(onKeyListener)
        edt_link_plus_otp_6.setOnKeyListener(onKeyListener)
    }


    private fun reset(isSelected: Boolean) {
        edt_link_plus_otp_1.clearFocus()
        edt_link_plus_otp_1.isSelected = isSelected
        edt_link_plus_otp_2.clearFocus()
        edt_link_plus_otp_2.isSelected = isSelected
        edt_link_plus_otp_3.clearFocus()
        edt_link_plus_otp_3.isSelected = isSelected
        edt_link_plus_otp_4.clearFocus()
        edt_link_plus_otp_4.isSelected = isSelected
        edt_link_plus_otp_5.clearFocus()
        edt_link_plus_otp_5.isSelected = isSelected
        edt_link_plus_otp_6.clearFocus()
        edt_link_plus_otp_6.isSelected = isSelected
    }

    override fun getOtp(): String {
        val otp1 = edt_link_plus_otp_1.text.toString()
        val otp2 = edt_link_plus_otp_2.text.toString()
        val otp3 = edt_link_plus_otp_3.text.toString()
        val otp4 = edt_link_plus_otp_4.text.toString()
        val otp5 = edt_link_plus_otp_5.text.toString()
        val otp6 = edt_link_plus_otp_6.text.toString()
        return "$otp1$otp2$otp3$otp4$otp5$otp6"
    }

    private fun enableButton() {
        btn_link_plus_otp_resend.isEnabled = true
        btn_link_plus_otp_resend.setBackgroundResource(RB.drawable.bg_round_corner_blue)
        txt_link_plus_otp_resend.setTextColor(ContextCompat.getColor(containerView.context, R.color.white))
    }

    private fun disableButton() {
        btn_link_plus_otp_resend.isEnabled = false
        btn_link_plus_otp_resend.setBackgroundResource(R.drawable.bg_round_corner_white_two)
        txt_link_plus_otp_resend.setTextColor(ContextCompat.getColor(containerView.context, R.color.brownish_grey))
    }

    private var textWatcher = object: TextWatcher {

        override fun afterTextChanged(s: Editable) {
            if(s.toString().trim().isNotEmpty()) {
                currentlyFocusedEditText?.focusSearch(View.FOCUS_RIGHT)?.requestFocus()
            }

            if(canSendOtp) {
                onOtpInputComplete?.invoke()
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }

    private val focusChangeListener = View.OnFocusChangeListener { v, _ ->
        currentlyFocusedEditText = v as EditText
        currentlyFocusedEditText?.setSelection(currentlyFocusedEditText?.text?.length ?: 0)
    }

    private val onKeyListener = View.OnKeyListener { v, keyCode, event ->
        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL && (v as TextView).text.toString().trim().isEmpty()) {
            currentlyFocusedEditText?.focusSearch(View.FOCUS_LEFT)?.requestFocus()
        }
        false
    }
}