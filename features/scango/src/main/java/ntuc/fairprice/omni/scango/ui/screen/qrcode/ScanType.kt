package ntuc.fairprice.omni.scango.ui.screen.qrcode

enum class ScanType(val value: Int) {
    QR_CODE(1),
    BAR_CODE(2)
}
