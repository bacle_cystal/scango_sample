package ntuc.fairprice.omni.scango.base.customview

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.scango_item_indicator_red.view.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R

/**
 * A customview cart with number indicator already use like a base icon cart
 */
class ScangoCartView @JvmOverloads constructor(
    val mContext: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(mContext, attrs, defStyleAttr) {

    private var customOnClickListener: OnClickListener? = null
    init {
        val view = View.inflate(context, R.layout.scango_icon_cart_view, this)
        setPadding(0, 0, 0, 0)
        view.setOnClickListener {
            customOnClickListener?.onClick(this)
            launchActivity(Constants.cartClassname)
        }
    }

    /**
     * Call this function to change the values.
     */
    fun update(number: Int) {
        if (number < 1) {
            text_number_item.visibility = View.INVISIBLE
        } else {
            text_number_item.visibility = View.VISIBLE
            text_number_item.text = number.toString()
        }
        postInvalidate()
    }

    fun setCustomOnClickListener(onClickListener: OnClickListener) {
        customOnClickListener = onClickListener
    }

    private fun launchActivity(className: String) {
        Intent().setClassName(mContext, className)
            .setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            .also {
                val options = ActivityOptions.makeCustomAnimation(
                    mContext,
                    ntuc.fairprice.omni.R.anim.left_in,
                    ntuc.fairprice.omni.R.anim.right_out
                )
                mContext.startActivity(it,options.toBundle())
            }
    }
}