package ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.scango_activity_link_plus_phone_number.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.ScangoLinkPlusOtpActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget.ScangoLinkPlusPhoneNumberBoxWidget
import ntuc.fairprice.omni.scango.utils.observe
import ntuc.fairprice.omni.util.CustomerHelpUtils
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.base.browser.WebConstants
import javax.inject.Inject

class ScangoLinkPlusPhoneNumberActivity : ScangoBaseActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, ScangoLinkPlusPhoneNumberActivity::class.java)
    }

    @Inject
    lateinit var viewModel: ScangoLinkPlusPhoneNumberViewModel
    @Inject
    lateinit var scangoLinkPlusPhoneNumberBoxWidget: ScangoLinkPlusPhoneNumberBoxWidget
    @Inject
    lateinit var keyboardManager: KeyboardManager
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog



    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_link_plus_phone_number)
        initWidget()
        initListeners()
        initViewModel()
    }

    private fun initViewModel() {
        observe(viewModel.stateLiveData, ::onViewModelStateChanged)
    }

    private fun initWidget() {
        scangoLinkPlusPhoneNumberBoxWidget.initWidget(card_link_plus)
        scangoLinkPlusPhoneNumberBoxWidget.displayDefaultState()
    }

    private fun initListeners() {
        link_plus_phone_number_toolbar.setOnActionClickListener {
            keyboardManager.hide(edt_link_plus_mobile)
            finish()
        }
        edt_link_plus_mobile.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateAndUpdateCustomer()
                true
            } else {
                false
            }
        }
        txt_link_plus.setOnClickListener {
            validateAndUpdateCustomer()
        }
        txt_link_plus_try_with_card.setOnClickListener {
            startActivity(Intent(this, ScangoLinkPlusCardNumberActivity::class.java))
            finish()
        }
        btn_link_plus_contact_phone.setOnClickListener {
            CustomerHelpUtils.dialSupport(this)
        }
        btn_link_plus_contact_email.setOnClickListener {
            CustomerHelpUtils.emailSupport(this)
        }
        txt_link_plus_app.setOnClickListener {
            CustomerHelpUtils.openPlusAppInStore(this)
        }
        txt_link_plus_term.setOnClickListener {
            showInAppBrowser(WebConstants.FP_URL_TNC, resources.getString(R.string.txt_link_plus_term), FPScreen.TNC)
        }

    }

    private fun validateAndUpdateCustomer() {
        keyboardManager.hide(edt_link_plus_mobile)
        edt_link_plus_mobile.clearFocus()
        scangoLinkPlusPhoneNumberBoxWidget.run {
            if (isValidPhoneNumber) {
                scanGoProgressDialog.show()
                viewModel.updateCustomerRecordWithPhoneNumber(phoneNumber)
            } else {
                displayInvalidState()
            }
        }
    }

    private fun onViewModelStateChanged(state: ScangoLinkPlusPhoneNumberViewModel.State) {
        when (state) {
            is ScangoLinkPlusPhoneNumberViewModel.State.UpdateCustomerSuccess -> {
                scanGoProgressDialog.dismiss()
                startActivity(ScangoLinkPlusOtpActivity.getIntent(this, scangoLinkPlusPhoneNumberBoxWidget.phoneNumber, state.lpStatus))
                finish()
            }

            is ScangoLinkPlusPhoneNumberViewModel.State.AccountNotFound -> {
                scanGoProgressDialog.dismiss()
                scangoLinkPlusPhoneNumberBoxWidget.displayInvalidState()

            }

            is ScangoLinkPlusPhoneNumberViewModel.State.NetworkError -> {
                scanGoProgressDialog.dismiss()
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        validateAndUpdateCustomer()
                    }
                    show()
                }
            }

            is ScangoLinkPlusPhoneNumberViewModel.State.GeneralError -> {
                scanGoProgressDialog.dismiss()
                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        validateAndUpdateCustomer()
                    }
                    show()
                }
            }


        }
    }
}