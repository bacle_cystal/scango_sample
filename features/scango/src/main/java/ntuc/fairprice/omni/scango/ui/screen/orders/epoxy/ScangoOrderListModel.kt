package ntuc.fairprice.omni.scango.ui.screen.orders.epoxy

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.R as ZR
import thor.zopsmart.com.thor.base.changeDateFormat
import thor.zopsmart.com.thor.model.orders.DeliveryMode
import thor.zopsmart.com.thor.model.orders.DeliveryMode.DELIVERY
import thor.zopsmart.com.thor.model.orders.DeliveryMode.PICKUP
import thor.zopsmart.com.thor.model.orders.OrderStatus
import thor.zopsmart.com.thor.model.orders.OrderStatus.*


@EpoxyModelClass(layout = R.layout.scango_order_list_item)
abstract class ScangoOrderListModel : EpoxyModelWithHolder<ScangoOrderListModel.OrderHistoryHolder>() {

    @EpoxyAttribute
    var orderId: Int? = null
    @EpoxyAttribute
    var orderDate: String? = null
    @EpoxyAttribute
    var storeName: String? = null
    @EpoxyAttribute
    var orderType: DeliveryMode = DeliveryMode.UNKNOWN
    @EpoxyAttribute
    var orderNum: String? = null
    @EpoxyAttribute
    var orderStatus: OrderStatus = OrderStatus.UNKNOWN
    @EpoxyAttribute
    var orderTotal: String? = null
    @EpoxyAttribute
    var orderPaid: Boolean = false
    @EpoxyAttribute
    var slotStartEnd: String? = null
    @EpoxyAttribute
    var slotEndTime: String? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var placeHolderItem: Boolean = false

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    override fun bind(holder: OrderHistoryHolder) {

        val ctx = holder.parent.context

        holder.parent.setOnClickListener(clickListener)
        holder.orderDateTv.text = getPlaceOnDateText(orderDate)
        holder.orderStoreTv.text = storeName
        holder.orderNumTv.text = if (!placeHolderItem) "Order No:               " + orderNum else ""
//        holder.orderTypeTv.setText(getOrderTypeText(ctx, orderType))
        holder.orderStatusTv.text = getOrderStatusText(ctx, orderStatus, orderType, slotStartEnd, slotEndTime)

        holder.orderTotalTv.text = if (!placeHolderItem) "Order Total:          $" + orderTotal else ""

        val visibility = if (orderPaid) View.VISIBLE else View.GONE
        holder.isPaidTv.visibility = visibility

        // Changes specific to Order Status
        @ColorInt var statusTextColor = ZR.color.darkText
        @DrawableRes var statusDrawable = ZR.drawable.blue_dot

        when (orderStatus) {
            // TODO: Remove all hardcoding.
            COMPLETED -> {
                statusTextColor = ZR.color.darkGreen
                statusDrawable = ZR.drawable.ic_success
            }
            CANCELLED -> {
                statusTextColor = ZR.color.lightText
                statusDrawable = 0
            }
            DELAYED -> { // We don't have such status yet. Let change the string later
                statusTextColor = ZR.color.textColorError
                statusDrawable = ZR.drawable.red_dot
            }
        }
        holder.orderStatusTv.apply {
            setTextColor(ContextCompat.getColor(context, statusTextColor))
            setCompoundDrawablesWithIntrinsicBounds(statusDrawable, 0, 0, 0)
        }
    }

    private fun getPlaceOnDateText(orderDate: String?): String? {

        if (orderDate.isNullOrBlank()) return null

        return changeDateFormat(orderDate, "yyyy-MM-dd", "d MMM yy")
    }

    private fun getOrderStatusText(
        context: Context, orderStatus: OrderStatus, deliveryMode: DeliveryMode, slotStartTime: String?,
        slotEndTime: String?
    ): String? {

        return when (orderStatus) {
            PENDING -> context.getString(ZR.string.txt_ordered_placed)
            PICKING -> context.getString(ZR.string.txt_picking)
            CANCELLED -> context.getString(ZR.string.txt_cancelled)
            UNDELIVERED -> {
                if (PICKUP == deliveryMode) context.getString(ZR.string.unable_to_collect) else context.getString(
                    ZR.string.delivery_unsuccessful
                )
            }
            DISPATCHED -> "On the way... Arriving At ${slotStartTime} - ${slotEndTime}"
            COMPLETED -> {
                if (PICKUP == deliveryMode) context.getString(ZR.string.txt_collected) else context.getString(
                    ZR.string.txt_delivered
                )
            }
            else -> orderStatus.status
        }
    }

    private fun getOrderTypeText(context: Context, deliveryMode: DeliveryMode): String? {
        return when (deliveryMode) {
            DELIVERY -> context.getString(ZR.string.txt_home_delivery)
            PICKUP -> context.getString(ZR.string.txt_click_and_collect)
            else -> deliveryMode.status
        }
    }

    class OrderHistoryHolder : ScangoBaseEpoxyHolder() {
        val parent: View by bind(R.id.orderHistoryParent)
        val orderStoreTv: TextView by bind(R.id.tv_scango_store)
        val orderDateTv: TextView by bind(R.id.orderDate)
        val orderNumTv: TextView by bind(R.id.orderNum)
        val orderStatusTv: TextView by bind(R.id.orderStatus)
        val orderTotalTv: TextView by bind(R.id.orderTotal)
        val isPaidTv: TextView by bind(R.id.paid)
    }

}