package ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.scango_activity_link_plus_phone_done.*
import ntuc.fairprice.omni.R as BR
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.LinkPointStatus

class ScangoLinkPlusPhoneDoneActivity : ScangoBaseActivity() {

    private var lpStatus: LinkPointStatus? = null

    companion object {
        private const val INTENT_EXTRA_LP_STATUS = "intent_extra_lp_status"

        fun getIntent(context: Context, linkPointStatus: LinkPointStatus) =
            Intent(context, ScangoLinkPlusPhoneDoneActivity::class.java).apply {
                putExtra(INTENT_EXTRA_LP_STATUS, linkPointStatus)
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_link_plus_phone_done)
        lpStatus = intent?.extras?.getParcelable(INTENT_EXTRA_LP_STATUS)
        lpStatus?.let {
            tv_link_plus_phone_done_lppoints.text = getString(BR.string.two_decimal_formatted_val, it.pointBalance)
            tv_link_plus_phone_done_dollar_bal.text = getString(BR.string.equal_dollar_value, it.dollarBalance)
        }
        txt_link_plus_done_back_to_journey.setOnClickListener { finish() }
        tb_link_plus_done.setOnActionClickListener { finish() }
    }
}
