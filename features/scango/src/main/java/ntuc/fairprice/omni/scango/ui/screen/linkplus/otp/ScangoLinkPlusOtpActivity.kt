package ntuc.fairprice.omni.scango.ui.screen.linkplus.otp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.SHOW_FORCED
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.scango_activity_link_plus_otp.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.Constants.EXTRA_OTP_NUMBER
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.Constants.EXTRA_LINK_POINT_STATUS
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.LinkPointStatus
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneDoneActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.widget.ScangoLinkPlusOtpWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import ntuc.fairprice.omni.scango.utils.observe
import javax.inject.Inject


class ScangoLinkPlusOtpActivity: ScangoBaseActivity() {

    companion object {
        fun getIntent(context: Context, phoneNumber: String, lpStatus: LinkPointStatus) = Intent(context, ScangoLinkPlusOtpActivity::class.java).apply {
            putExtra(EXTRA_OTP_NUMBER, phoneNumber)
            putExtra(EXTRA_LINK_POINT_STATUS, lpStatus)
        }
    }

    @Inject
    lateinit var viewModel: ScangoLinkPlusOtpViewModel
    @Inject
    lateinit var scangoLinkPlusOtpWidget: ScangoLinkPlusOtpWidget
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog

    @Inject
    lateinit var keyboardManager: KeyboardManager

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_link_plus_otp)
        initViews()
        initWidget()
        initListeners()
        initViewModel()
    }

    private fun initViews() {
        val phoneNumber = intent?.extras?.getString(EXTRA_OTP_NUMBER, "")
        txt_link_plus_otp_detail_child.text = getString(R.string.txt_link_plus_otp_detail_child, phoneNumber)
    }

    private fun initViewModel() {
        viewModel.run {
            observe(stateLiveData, ::onViewModelStateChanged)
            sendOtp()
        }
        scanGoProgressDialog.show()
    }

    private fun initWidget() {
        scangoLinkPlusOtpWidget.initWidget(cl_link_plus_otp_container)
        scangoLinkPlusOtpWidget.onOtpInputComplete = {
            keyboardManager.hide(card_link_plus_otp)
            scanGoProgressDialog.show()
            viewModel.verifyOtp(scangoLinkPlusOtpWidget.getOtp())
        }
        scangoLinkPlusOtpWidget.displayDefaultState()
    }

    private fun initListeners() {
        btn_link_plus_otp_resend.setOnClickListener {
            keyboardManager.hide(card_link_plus_otp)
            scanGoProgressDialog.show()
            viewModel.sendOtp()
        }
        tb_link_plus_otp.setOnActionClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(this))
            finish()
        }
        txt_link_plus_otp_change_number.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(this))
            finish()
        }
        txt_link_plus_otp_try_another_number.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(this))
            finish()
        }
    }

    private fun onViewModelStateChanged(state: ScangoLinkPlusOtpViewModel.State){
        when(state) {
            is ScangoLinkPlusOtpViewModel.State.OtpSuccess -> {
                scanGoProgressDialog.dismiss()
                scangoLinkPlusOtpWidget.showTimer()
                
                edt_link_plus_otp_1.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(SHOW_FORCED, 0)

            }

            is ScangoLinkPlusOtpViewModel.State.OtpFailed -> {
                scanGoProgressDialog.dismiss()
                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        scanGoProgressDialog.show()
                        viewModel.sendOtp()
                    }
                    show()
                }

            }

            is ScangoLinkPlusOtpViewModel.State.NetworkError -> {
                scanGoProgressDialog.dismiss()

                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        scanGoProgressDialog.show()
                        viewModel.sendOtp()
                    }
                    show()
                }
            }

            is ScangoLinkPlusOtpViewModel.State.OtpVerificationSuccess -> {
                val lpStatus = intent?.extras?.getParcelable(EXTRA_LINK_POINT_STATUS) ?: LinkPointStatus()
                scanGoProgressDialog.dismiss()
                startActivity(ScangoLinkPlusPhoneDoneActivity.getIntent(this, lpStatus))
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.INTENT_LINK_POINT_REFRESH))
                finish()
            }

            is ScangoLinkPlusOtpViewModel.State.OtpVerificationError -> {
                scanGoProgressDialog.dismiss()
                scangoLinkPlusOtpWidget.displayErrorState()
            }
        }
    }
}