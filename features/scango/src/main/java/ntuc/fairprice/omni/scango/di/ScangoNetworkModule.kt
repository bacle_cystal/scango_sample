package ntuc.fairprice.omni.scango.di

import android.app.Application
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.BuildConfig
import ntuc.fairprice.omni.scango.repository.webservice.network.ScangoNetworkInterceptor
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoRetrofitService
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProviderImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import thor.zopsmart.com.thor.di.FeatureScope
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
class ScangoNetworkModule {
    @FeatureScope
    @Provides
    @Named("okHttpClient")
    fun provideOkHttpClient(
        app: Application
    ): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .writeTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(
            HttpLoggingInterceptor().setLevel(
                HttpLoggingInterceptor.Level.BODY
            )
        )
        .build()

    @FeatureScope
    @Provides
    @Named("okHttpClientWithInterceptor")
    fun provideOkHttpClientWithInterceptor(
        app: Application,
        sgNetworkInterceptor: ScangoNetworkInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .writeTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(sgNetworkInterceptor)
        .build()

    @FeatureScope
    @Provides
    @Named("retrofit")
    fun provideRetrofit(@Named("okHttpClient") okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.host)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @FeatureScope
    @Provides
    @Named("retrofitWithInterceptor")
    fun provideRetrofitWithInterceptor(@Named("okHttpClientWithInterceptor") okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.host_scango_v2)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @FeatureScope
    fun provideApiService(@Named("retrofitWithInterceptor") retrofit: Retrofit): ScangoRetrofitService =
        retrofit.create(ScangoRetrofitService::class.java)

    @Provides
    @FeatureScope
    fun provideGson(): Gson = Gson()

    @Provides
    @FeatureScope
    fun provideScangoRequestProvider(provider: ScangoRequestProviderImpl): ScangoRequestProvider = provider
}