package ntuc.fairprice.omni.scango.repository.webservice.websocket

import java.util.*

class ReceiptActionChannel {

    companion object {
        const val ACTION_CHANNEL = "ActionChannel"
        const val COMMAND_SUBSCRIBE = "subscribe"
        const val COMMAND_MESSAGE = "message"
        const val ACTION = "action"
    }

    private var command: String? = null
    private var identifier: String? = null
    private var data: String? = null

    constructor(command: String,
                channel: String,
                action: String,
                content: String) {
        this.command = command
        this.identifier = "{\"channel\":\"$channel\"}"
        this.data = "{\"action\":\"" + action + "\",\"code\":\"" + Date().time +
                "\",\"content\":\"" + content + "\"}"
    }
}