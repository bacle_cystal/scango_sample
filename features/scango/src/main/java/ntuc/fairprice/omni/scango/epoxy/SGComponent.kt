package ntuc.fairprice.omni.scango.epoxy

import android.util.Log
import android.view.View
import com.airbnb.epoxy.CarouselModelBuilder
import com.airbnb.epoxy.CarouselModel_
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.VisibilityState
import ntuc.fairprice.omni.scango.*
import ntuc.fairprice.omni.scango.CartDetailsBindingModel_
import ntuc.fairprice.omni.scango.CartItemBindingModel_
import ntuc.fairprice.omni.scango.FreeItemBannerBindingModel_
import ntuc.fairprice.omni.scango.ProductCellBindingModelBuilder
import ntuc.fairprice.omni.scango.ProductCellBindingModel_
import ntuc.fairprice.omni.scango.ProductScrollerBindingModel_
import ntuc.fairprice.omni.scango.base.SGCartDetailDelegate
import ntuc.fairprice.omni.scango.base.customview.swipeview.SimpleSwipeListener
import ntuc.fairprice.omni.scango.base.customview.swipeview.SwipeLayout
import ntuc.fairprice.omni.scango.base.customview.tagview.ScangoDietaryGridViewModel_
import ntuc.fairprice.omni.scango.epoxy.model.KeyedListener
import ntuc.fairprice.omni.scango.epoxy.model.ScangoCartItemViewModel_
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import ntuc.fairprice.omni.scango.ui.screen.promo.view.ScangoPromoRemoveListener
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartDealDetailModel_
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.freegift.ScangoCartFreeGiftDetailModel_
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview.ScangoPaymentSummary
import org.json.JSONObject
import thor.zopsmart.com.thor.*
import thor.zopsmart.com.thor.OfferHeaderBindingModel_
import thor.zopsmart.com.thor.base.customview.edittext.QuantitySelectListener
import thor.zopsmart.com.thor.base.customview.filter.FilterGroupDataHolder
import thor.zopsmart.com.thor.base.extensions.getAutoGenId
import thor.zopsmart.com.thor.epoxy.*
import thor.zopsmart.com.thor.epoxy.views.*
import thor.zopsmart.com.thor.features.widget.imageslideshow.data.ImageSlideShow
import thor.zopsmart.com.thor.features.widget.productdetails.view.OfferClickListener
import thor.zopsmart.com.thor.model.*
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import ntuc.fairprice.omni.scango.ProductDetailBindingModel_ as SGProductDetailBindingModel_
import ntuc.fairprice.omni.scango.ui.screen.productdetail.view.OfferDetailViewModel_ as SGOfferDetailViewModel_

interface SGCartPageDelegate : FPPWPDelegate, FPFreeGiftDelegate, SGCartItemDelegate, SGCartDetailDelegate

interface SGCartItemDelegate : FPDelegate, SimpleSwipeListener {
    fun addToCart(cartItem: ScangoCartItem)
    fun removeFromCart(cartItem: ScangoCartItem, swipeLayout: SwipeLayout, isClosed: Boolean)
    fun deleteFromCart(cartItem: ScangoCartItem)
    fun onCartProductImageClick(cartItem: ScangoCartItem)
}

interface SGProductDelegate : FPDelegate {
    fun onAddToCart(cartItem: CartItem)

    fun onRemoveFromCart(cartItem: CartItem)

    fun addToShoppingList(item: Item)

    fun onQuantitySet(quantity: Int, cartItem: CartItem)

    fun onProductClick(item: Item)

    fun loadMoreProduct(fpProductGrid: SGProductGrid)
}

interface SGFPFilterSortDelegate : FPDelegate {
    fun onFilterClick()
    fun onSortClick()
}

interface SGProductDetailDelegate : FPDelegate {
    // Add functions for product detail click like view all products for promo etc
    fun onImageClick(item: Item, position: Int)

    fun onOfferSummaryClick(index: Int)
    fun viewAllOfferProducts(offer: Offer)
    fun viewFreeGift(itemId: Long)
}

/* TODO class clone from FPComponent */
sealed class SGComponent<in T : FPDelegate> {

    protected val viewId = getAutoGenId()

    abstract fun render(delegate: T): List<EpoxyModel<*>>

    inline fun sgProductGridCell(
        count: Int,
        modelInitializer: ProductCellBindingModelBuilder.() -> Unit
    ): SGProductGridModel {
        return SGProductGridModel(count).apply {
            modelInitializer()
        }
    }

    /**
     * For use in the buildModels method of EpoxyController. A shortcut for creating a Carousel model, initializing it, and adding it to the controller.
     */
    inline fun carousel(modelInitializer: CarouselModelBuilder.() -> Unit): CarouselModel_ {
        return CarouselModel_().apply {
            modelInitializer()
        }
    }

    inline fun carouselNoAutoScroll(modelInitializer: CarouselNoAutoScrollModelBuilder.() -> Unit): CarouselNoAutoScrollModel_ {
        return CarouselNoAutoScrollModel_().apply {
            modelInitializer()
        }
    }

    inline fun category(modelInitializer: CategoryBindingModelBuilder.() -> Unit): CategoryBindingModel_ {
        return CategoryBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun categoryCollectionCell(modelInitializer: CategoryCollectionCellBindingModelBuilder.() -> Unit): CategoryCollectionCellBindingModel_ {
        return CategoryCollectionCellBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun content(modelInitializer: ContentBindingModelBuilder.() -> Unit): ContentBindingModel_ {
        return ContentBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun header(modelInitializer: HeaderBindingModelBuilder.() -> Unit): HeaderBindingModel_ {
        return HeaderBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun productCell(modelInitializer: ProductCellBindingModelBuilder.() -> Unit): ProductCellBindingModel_ {
        return ProductCellBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun subHeader(modelInitializer: SubHeaderBindingModelBuilder.() -> Unit): SubHeaderBindingModel_ {
        return SubHeaderBindingModel_().apply {
            modelInitializer()
        }
    }

    inline fun titleBar(modelInitializer: StylishToolbarBindingModelBuilder.() -> Unit): StylishToolbarBindingModel_ {
        return StylishToolbarBindingModel_().apply {
            modelInitializer()
        }
    }
}

data class SGCartItemList(val cartItems: List<ScangoCartItem>, val showAddRemove: Boolean) :
    SGComponent<SGCartItemDelegate>() {
    override fun render(delegate: SGCartItemDelegate): List<EpoxyModel<*>> {
        return cartItems.map {
            val offer = it.offer?.getOrNull(0)
            ScangoCartItemViewModel_()
                .id("$viewId ${it.id} ${offer?.id ?: -1}")
                .cartItem(it)
                .deleteClick { _ ->
                    delegate.deleteFromCart(it)
                }
                .addToCart { _ ->
                    delegate.addToCart(it)
                }
                .removeFromCart { scangoCartItem, swipeLayout, isClosed ->
                    delegate.removeFromCart(scangoCartItem, swipeLayout, isClosed)
                }
                .productImageClick { _ ->
                    delegate.onCartProductImageClick(it)
                }
                .showAddRemove(it.isSoldByWeight())
                .allowToRemove(true)
                .spanSizeOverride { span, _, _ ->
                    span
                }.keyedOnClickListener(
                    KeyedListener.create(
                        it,
                        delegate
                    )
                )
        }
    }
}


data class SGCartItemListCheckout(val cartItems: List<ScangoCartItem>) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return cartItems.map {
            val offer = it.offer?.getOrNull(0)
            ItemCartReceiptBindingModel_()
                .id("$viewId ${it.id} ${offer?.id ?: -1}")
                .cartItem(it)
                .allowToRemove(true)
                .disableWeight(!it.isSoldByWeight())
                .spanSizeOverride { span, _, _ ->
                    span
                }
                .paddingValue(8F)
        }
    }
}


// TODO add see all
data class SGHeader(
    val header: String,
    val seeAll: Boolean = false,
    var associatedData: JSONObject = JSONObject()
) : SGComponent<FPViewAllDelegate>() {
    override fun render(delegate: FPViewAllDelegate): List<EpoxyModel<*>> {
        return listOf(
            header {
                id(viewId)
                title(header)
                seeAll(seeAll)
                seeAllClick(View.OnClickListener {
                    delegate.onViewAllClick(associatedData)
                })
                spanSizeOverride { totalSpanCount, position, itemCount -> totalSpanCount }
            })
    }
}

data class SGSubtitle(val subTitle: String) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            subHeader {
                id(viewId)
                subTitle(subTitle)
                spanSizeOverride { totalSpanCount, position, itemCount -> totalSpanCount }
            })
    }
}

data class SGTitleBar(val title: String, val isShowback: Boolean, val isHiddenButton: Boolean) :
    SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            titleBar {
                id(viewId)
                title(title)
                isShowBack(isShowback)
                isHideButton(isHiddenButton)
                spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
            })
    }
}

data class SGFreeGiftBanner(val count: Int) : SGComponent<FPFreeGiftDelegate>() {
    override fun render(delegate: FPFreeGiftDelegate): List<EpoxyModel<*>> {
        return listOf(
            FreeItemBannerBindingModel_()
                .id(viewId)
                .count(count)
                .clickAction { _ ->
                    delegate.freeItemClick()
                }
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGOfferHeader(val message: String, val quantity: Int) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            OfferHeaderBindingModel_()
                .id(viewId)
                .message(message)
                .count(quantity)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGFreeGiftList(
    val cartItems: List<ScangoCartItem>,
    val hashMap: HashMap<Long, ArrayList<ScangoCartItem>>
) :
    SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return cartItems.map {
            ScangoCartFreeGiftDetailModel_()
                .id("$viewId ${it.id}")
                .item(it)
                .offerProductMap(hashMap[it.id] ?: arrayListOf())
                .spanSizeOverride { span, _, _ ->
                    span
                }
        }
    }
}

data class SGOfferList(val offerList: List<ScangoOffer>, val hashMap: HashMap<Long, ArrayList<ScangoCartItem>>) :
    SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return offerList.map {
            ScangoCartDealDetailModel_()
                .id("$viewId ${it.id}")
                .offer(it)
                .offerProductMap(hashMap[it.id] ?: arrayListOf())
                .spanSizeOverride { span, _, _ ->
                    span
                }
        }
    }
}

data class SGCartDetails(val paymentSummary: ScangoPaymentSummary) :
    SGComponent<SGCartDetailDelegate>() {
    override fun render(delegate: SGCartDetailDelegate): List<EpoxyModel<*>> {
        return listOf(
            CartDetailsBindingModel_()
                .id(viewId)
                .paymentSummary(paymentSummary)
                .addPromoClick { _ ->
                    delegate.promoClick()
                }
                .removePromoClick(object : ScangoPromoRemoveListener {
                    override fun onPromoRemoved(coupon: ScangoCoupon) {
                        delegate.onPromoRemoved(coupon)
                    }
                })
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGProductGrid(
    var items: List<Item>,
    var associatedData: JSONObject = JSONObject(),
    var page: Int = 2,
    var isLoading: Boolean = false,
    var hasLoadedAll: Boolean = false
) : SGComponent<SGProductDelegate>() {
    override fun render(delegate: SGProductDelegate): List<EpoxyModel<*>> {
        return items.mapIndexed { index, item ->
            sgProductGridCell(2) {
                id("${viewId}_${item.id}")
                addClick { _ ->
                    delegate.onAddToCart(item.toCartItem())
                }
                removeClick { _ ->
                    delegate.onRemoveFromCart(item.toCartItem())
                }
                favouriteClick { _ ->
                    delegate.addToShoppingList(item)
                }
                onVisibilityStateChanged { _, view, visibilityState ->
                    if (visibilityState == VisibilityState.VISIBLE && index == (items.size - 1)) {
                        delegate.loadMoreProduct(this@SGProductGrid)
                    }
                }
                cellClick { _ ->
                    delegate.onProductClick(item)
                }
                quantitySelectListener(object : QuantitySelectListener {
                    override fun onQuantitySelected(quantity: Int) {
                        delegate.onQuantitySet(quantity, item.toCartItem())
                    }
                })
                item(item.copy())
            }
        }
    }
}

data class SGFilterSort(
    var sortKey: String,
    var filters: ArrayList<FilterGroupDataHolder>,
    var hasBeenConsumed: Boolean = false
) :
    SGComponent<SGFPFilterSortDelegate>() {
    override fun render(delegate: SGFPFilterSortDelegate): List<EpoxyModel<*>> {
        return listOf(
            FilterSortViewModel_()
                .id(viewId)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
                .sortingKey(sortKey)
                .onFilterClick(View.OnClickListener {
                    delegate.onFilterClick()
                })
                .onSortClick(View.OnClickListener {
                    delegate.onSortClick()
                })
        )
    }
}

data class SGProductScroller(
    var items: List<Item>,
    var associatedData: JSONObject = JSONObject(),
    var page: Int = 2,
    var isLoading: Boolean = false,
    var hasLoadedAll: Boolean = false
) : SGComponent<SGProductDelegate>() {
    override fun render(delegate: SGProductDelegate): List<EpoxyModel<*>> {
        return listOf(carouselNoAutoScroll {
            id(viewId)
            numViewsToShowOnScreen(1.2f)

            withModelsFrom(items) { item ->
                ProductScrollerBindingModel_()
                    .id("${viewId}_${item.id}")
                    .item(item.copy())
                    .addClick { _ ->
                        delegate.onAddToCart(item.toCartItem())
                    }
                    .removeClick { _ ->
                        delegate.onRemoveFromCart(item.toCartItem())
                    }
                    .cellClick { _ ->
                        delegate.onProductClick(item)
                    }
                    .favouriteClick { _ ->
                        delegate.addToShoppingList(item)
                    }
                    .quantitySelectListener(object : QuantitySelectListener {
                        override fun onQuantitySelected(quantity: Int) {
                            delegate.onQuantitySet(quantity, item.toCartItem())
                        }
                    })
            }
            spanSizeOverride { totalSpanCount, position, itemCount ->
                totalSpanCount
            }
        })
    }
}

data class SGProductDetail(val item: Item) : SGComponent<SGProductDetailDelegate>() {
    override fun render(delegate: SGProductDetailDelegate): List<EpoxyModel<*>> {
        return listOf(
            SGProductDetailBindingModel_()
                .id(viewId)
                .item(item)
                .offerListener(
                    object : OfferClickListener {
                        override fun onOfferClicked(index: Int) {
                            delegate.onOfferSummaryClick(index)
                        }

                    }
                )
                .imageClickListener(
                    object : BannerSlideShow.SlideShowClickHandler {
                        override fun onClick(slide: ImageSlideShow, position: Int) {
                            delegate.onImageClick(item, position)
                        }
                    }
                )
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGDietaryData(val metaDataKey: String, val gridData: List<String>) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            CustomGridViewModel_()
                .id(viewId)
                .metaDataName(metaDataKey)
                .gridData(gridData)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}


data class SGDietaryDataNew(val metaDataKey: String, val gridData: List<String>) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            ScangoDietaryGridViewModel_()
                .id(viewId)
                .gridData(gridData)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}


data class SGOfferDetailPDP(val offer: Offer, val offerDetail: OfferDetail?) : SGComponent<FPAbstractDelegates>() {
    override fun render(delegate: FPAbstractDelegates): List<EpoxyModel<*>> {
        return listOf(
            SGOfferDetailViewModel_()
                .id(viewId)
                .offer(offer)
                .offerDetail(offerDetail)
                .addToShoppingList {
                    delegate.addToShoppingList(it)
                }
                .removeFromCart {
                    delegate.onRemoveFromCart(it)
                }
                .addToCart {
                    delegate.onAddToCart(it)
                }
                .productClick {
                    delegate.onProductClick(it)
                }
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}


data class SGNutrionalData(val metaDataKey: String, val rowData: ArrayList<ArrayList<String>>) :
    SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            TableViewModel_()
                .id(viewId)
                .metaDataName(metaDataKey)
                .rowData(rowData)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}


data class SGProductMetadata(val metadataKey: String, val metaData: String) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            ProductMetaDataBindingModel_()
                .id(viewId)
                .metadata(metaData)
                .metadataName(metadataKey)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGVerticalGap(val margin: Float) : SGComponent<FPDelegate>() {
    override fun render(delegate: FPDelegate): List<EpoxyModel<*>> {
        return listOf(
            GapBindingModel_()
                .id(viewId)
                .gap(margin)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}

data class SGPromo(val promos: List<Promo>) : SGComponent<FPPromoDelegate>() {
    override fun render(delegate: FPPromoDelegate): List<EpoxyModel<*>> {
        return listOf(carousel {
            id(viewId)
            withModelsFrom(promos) { promo ->
                PromoBindingModel_()
                    .id("${viewId}_${promo.id}")
                    .title(promo.name)
                    .inverted(!promo.isSelected)
                    .onClick { model, _, _, _ ->
                        if (model.inverted()) {
                            delegate.onPromoSelected(promo)
                        }
                    }
            }
            spanSizeOverride { totalSpanCount, position, itemCount ->
                totalSpanCount
            }
        })
    }

}

class SGLoader() : SGComponent<FPAbstractDelegates>() {
    override fun render(delegate: FPAbstractDelegates): List<EpoxyModel<*>> {
        return listOf(
            LoaderBindingModel_()
                .id(viewId)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}