package ntuc.fairprice.omni.scango.repository.webservice.model

import ntuc.fairprice.omni.scango.utils.HttpStatusCode
import org.json.JSONObject


class ScangoHttpServiceException(message: String, val code: Int, val responseBody: String?) :
    Exception(message) {

    val isConnectionError: Boolean
        get() = code == HttpStatusCode.HTTP_UNKNOWN

    val isUnauthorized: Boolean
        get() = code == HttpStatusCode.HTTP_UNAUTHORIZED


    val isNotFound: Boolean
        get() = code == HttpStatusCode.HTTP_NOT_FOUND


    val isSessionExpired: Boolean
        get() = (code == HttpStatusCode.HTTP_SESSION_EXPIRED &&
                JSONObject(responseBody).getJSONObject("error").getInt("code") == 4101)

    val isForbidden: Boolean
        get() = code == HttpStatusCode.HTTP_FORBIDDEN

    val isBadRequest: Boolean
        get() = code == HttpStatusCode.HTTP_BAD_REQUEST

    val isUnknownError: Boolean
        get() = code >= HttpStatusCode.HTTP_INTERNAL_SERVERERROR

}