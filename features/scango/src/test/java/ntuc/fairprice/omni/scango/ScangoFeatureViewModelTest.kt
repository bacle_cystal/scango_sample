package ntuc.fairprice.omni.scango

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import ntuc.fairprice.omni.scango.repository.db.room.repo.ProductRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.Success
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import thor.zopsmart.com.thor.repository.db.room.entity.Customer
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository

@RunWith(MockitoJUnitRunner::class)
@Suppress("UNCHECKED_CAST")
class ScangoFeatureViewModelTest {

    @Mock
    private lateinit var mockApplication: Application
    @Mock
    private lateinit var mockObserver: Observer<Unit>
    @Mock
    private lateinit var mockAccountRepository: AccountRepository
    @Mock
    private lateinit var mockStoreRepository: ScangoStoreRepository
    @Mock
    private lateinit var mockCustomer: Customer
    @Mock
    private lateinit var mockScangoCartRepository: ScangoCartRepository

    private lateinit var subject: ScangoFeatureViewModel

    @ExperimentalCoroutinesApi
    @get:Rule
    val rule = RuleChain.outerRule(InstantTaskExecutorRule()).around(InstantDispatcherTestRule())

    companion object {
        private const val CUSTOMER_ID = 123456789L
        private const val SCAN_GO_STORE_ID = "12345"
        private const val ACCESS_TOKEN = "0-o-9-i-8-u-7-y-6-t-5-r-4-e-3"
    }

    @Before
    @Throws(Exception::class)
    fun prepare() {
        subject = ScangoFeatureViewModel(
            mockApplication,
            mockScangoCartRepository,
            mockAccountRepository,
            mockStoreRepository
        )
    }

    @Test
    fun `exit store called when update data change`() = runBlocking {
        //GIVEN
        given(mockCustomer.id).willReturn(CUSTOMER_ID)
        given(mockAccountRepository.getCustomer()).willReturn(mockCustomer)
        given(mockAccountRepository.getAccessToken()).willReturn(ACCESS_TOKEN)

        //WHEN
        subject.exitStore()

        //THEN
        then(mockObserver).should().onChanged(null)
    }

    @Test
    fun `test isUserLoggedIn flag when account isLoggedIn is false`() {
        //GIVEN
        given(mockAccountRepository.getCustomer()).willReturn(null)

        //THEN
        assertFalse(subject.isLoggedIn)
    }

    @Test
    fun `test isUserLoggedIn flag when account isLoggedIn is true`() {
        //GIVEN
        given(mockAccountRepository.getCustomer()).willReturn(mockCustomer)

        //THEN
        assertTrue(subject.isLoggedIn)
    }

}