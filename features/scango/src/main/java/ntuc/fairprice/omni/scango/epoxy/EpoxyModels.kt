package ntuc.fairprice.omni.scango.epoxy

import com.airbnb.epoxy.CarouselModelBuilder
import com.airbnb.epoxy.CarouselModel_
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import ntuc.fairprice.omni.scango.ProductCellBindingModelBuilder
import ntuc.fairprice.omni.scango.ProductCellBindingModel_
import thor.zopsmart.com.thor.epoxy.views.CarouselNoAutoScrollModelBuilder

class SGProductGridModel(private val count: Int) : ProductCellBindingModel_(), EpoxyModel.SpanSizeOverrideCallback {
    override fun getSpanSize(totalSpanCount: Int, position: Int, itemCount: Int): Int {
        return totalSpanCount / count
    }
}

inline fun EpoxyController.productGridCell(count: Int, modelInitializer: ProductCellBindingModelBuilder.() -> Unit) {
    SGProductGridModel(count).apply {
        modelInitializer()
    }
        .addTo(this)
}

/**
 * For use in the buildModels method of EpoxyController. A shortcut for creating a Carousel model, initializing it, and adding it to the controller.
 */
inline fun EpoxyController.carousel(modelInitializer: CarouselModelBuilder.() -> Unit) {
    CarouselModel_().apply {
        modelInitializer()
    }.addTo(this)
}

/** Add models to a CarouselModel_ by transforming a list of items into EpoxyModels.
 *
 * @param items The items to transform to models
 * @param modelBuilder A function that take an item and returns a new EpoxyModel for that item.
 */
inline fun <T> CarouselModelBuilder.withModelsFrom(
    items: List<T>,
    modelBuilder: (T) -> EpoxyModel<*>
) {
    models(items.map { modelBuilder(it) })
}

inline fun <T> CarouselNoAutoScrollModelBuilder.withModelsFrom(
    items: List<T>,
    modelBuilder: (T) -> EpoxyModel<*>
) {
    models(items.map { modelBuilder(it) })
}