package ntuc.fairprice.omni.domain.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading = MutableLiveData<Boolean>().apply { value = false }
    val errorMessage = MutableLiveData<String>()
    val error = MutableLiveData<Error>()

    protected fun launch(block: suspend () -> Unit): Job {
        showLoading()
        return viewModelScope.launch {
            block()
        }
    }

    open fun onSuccess() {
        isLoading.value = false
    }

    open fun showLoading() {
        isLoading.value = true
    }

    open fun onLoadFail(throwable: Throwable) {
        try {
            when (throwable.cause) {
                is UnknownHostException -> {
                    errorMessage.value = "No Internet Connection"
                }
                is SocketTimeoutException -> {
                    errorMessage.value = "Connect timeout, please retry"
                }
                else -> {
                    errorMessage.value = throwable.message
                }
            }
        } catch (e: Exception) {
            errorMessage.value = throwable.message
        }
        isLoading.value = false
    }
}
