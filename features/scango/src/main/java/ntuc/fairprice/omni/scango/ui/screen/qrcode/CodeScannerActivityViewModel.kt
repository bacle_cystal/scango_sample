package ntuc.fairprice.omni.scango.ui.screen.qrcode

import android.app.Application
import android.provider.Settings
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.Constants.MAX_CART_SIZE
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidator
import ntuc.fairprice.omni.scango.repository.db.room.entity.Product
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoStore
import ntuc.fairprice.omni.scango.repository.db.room.entity.getUOM
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartItem
import ntuc.fairprice.omni.scango.repository.db.room.repo.ProductRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.ErrorType
import ntuc.fairprice.omni.scango.utils.*
import ntuc.fairprice.omni.ui.utils.default
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import thor.zopsmart.com.thor.repository.db.room.repo.DeviceInfo
import java.lang.Exception
import javax.inject.Inject

class CodeScannerActivityViewModel @Inject constructor(
    application: Application,
    private val productRepository: ProductRepository,
    private val scangoStoreRepository: ScangoStoreRepository,
    private val restrictionsValidator: RestrictionsValidator,
    private val deviceUtils: DeviceUtils
) : ScangoBaseViewModel(application) {

    var isVisiableBarcode = MutableLiveData<Boolean>().default(false)
    var isVisiableGuide = MutableLiveData<Boolean>().default(false)

    var productItem = MutableLiveData<Product>()
    var storeData = MutableLiveData<ScangoStore>()
    var errorType = MutableLiveData<Int>()
    var isCheckedInSuccess = MutableLiveData<Boolean>().default(false)
    var isBarcodeRapid = MutableLiveData<Boolean>()
    var isGotit = MutableLiveData<Boolean>()


    //Add to card, check max logic
    var numberItemInCard = MutableLiveData<Int>()
    var maxItemEvent = MutableLiveData<Boolean>()

    var showLiquidRapidFragmentEvent = MutableLiveData<Product>()

    val isShow18PlusLayout
        get() = (isBarcodeRapid.value ?: false) && (isAddItem18plus.value ?: false)

    val isHasItemInCart = MediatorLiveData<Boolean>().apply {
        addSource(numberItemInCard) {
            value = when (numberItemInCard.value?.compareTo(0)) {
                0 -> false
                1 -> true
                else -> false
            }
        }
    }
    var isShowAddProduct = MutableLiveData<Boolean>().default(false)
    var isAddItem18plus = MutableLiveData<Boolean>().default(false)
    val serverGapTime get() = scangoStoreRepository.getServerGapTimeCached()


    val imageProduct = MediatorLiveData<String>().apply {
        addSource(productItem) { product ->
            value = product.getImage()
        }
    }

    fun changeUIScanCode(scanType: ScanType, hasItemInCart: Boolean = false) {
        when (scanType) {
            ScanType.QR_CODE -> isVisiableBarcode.value = false
            ScanType.BAR_CODE -> {
                isVisiableBarcode.value = true
                isHasItemInCart.value = hasItemInCart
            }
        }
    }

    fun changeBarcodeRapid(_barcodeRapid: Boolean) {
        isBarcodeRapid.value = _barcodeRapid
    }

    fun getProductByBarcode(scanData: ScanData) = launch {
        val product =  Product()
        onGetProductSuccess(product, scanData)
    }

    fun getCheckIn(storeId: String) = launch {
//        try {
//            scangoStoreRepository.getCheckinStore(storeId.toInt(), deviceUtils.deviceId).onSuccess {
                storeData.value = ScangoStore("111","123123","Hanoi","ScanGo store","")
                isCheckedInSuccess.value = true
                onSuccess()
//            }.onError {
//                super.onLoadFail(it)
//                if (it is ScangoHttpServiceException && it.isNotFound) {
//                    errorType.value = ErrorType.NOT_RECOGNISED_QR_CODE.value
//                } else if (it is ScangoHttpServiceException && it.isForbidden) {
//                    errorType.value = ErrorType.USER_FORBIDDEN.value
//                } else {
//                    onError(it)
//                }
//            }
//        } catch (e: Exception) {
//            super.onLoadFail(null)
//            errorType.value = ErrorType.NOT_RECOGNISED_QR_CODE.value
//        }
    }

    fun addToCart(cartViewModel: ScangoCartViewModel, product: Product) {
        val totalQuantity = cartViewModel.getCartItemsQuantity()
        if (totalQuantity >= RemoteConfigUtils.maxCartSize) {
            maxItemEvent.value = true
        } else {
            product.quantity = getCartItemQuantity(cartViewModel, product)
            cartViewModel.increment(product.toCartItem(), true, true)
            numberItemInCard.value = cartViewModel.getCartItemsQuantity()
            maxItemEvent.value = false
        }
    }

    fun setRapidShowed() {
        scangoStoreRepository.setRapidShowed()
        isGotit.value = true
    }

    fun getRapidShowCached() {
        isVisiableGuide.value = (scangoStoreRepository.getRapidCached() ?: false).not()
        isGotit.value = (scangoStoreRepository.getRapidCached() ?: false)
    }

    //With case age violated show a warning
    fun checkAgeViolated(product: Product): Boolean = restrictionsValidator.isAgeViolated(product.toCartItem())

    private fun getCartItemQuantity(cartViewModel: ScangoCartViewModel, product: Product): Int {
        val listItem = cartViewModel.getCartItems()
        listItem.forEach {
            if (product.id == it.id)
                return it.quantity
        }
        return product.quantity
    }

    /**
     * This needs to be refactored
     */
    private fun onGetProductSuccess(product: Product, scanData: ScanData) {
        onSuccess()
        val isTimeViolated = restrictionsValidator.isTimeViolated(product.toCartItem())
        if (isTimeViolated) {
            showLiquidRapidFragmentEvent.value = product
        } else {
            var updatedProduct = product
//            if (scanData is ScanData.WeightedItemWithQuantity) {
//                updatedProduct =
//                    product.copy(quantity = scanData.quantity, sellingPrice = scanData.price, mrp = scanData.price)
//            } else if (scanData is ScanData.WeightedItemWithWeight) {
//                updatedProduct =
//                    product.copy(weight = scanData.weight, sellingPrice = scanData.price, mrp = scanData.price)
//            }
            if (scanData is ScanData.WeightedItemWithWeight) {
                updatedProduct =
                    product.copy(
                        weight = 9.00,
                        sellingPrice = 7.00,
                        mrp = 7.10)
            }
            productItem.value = updatedProduct
        }
    }

    private fun onError(throwable: Throwable) {
        errorType.value = if (throwable is ScangoHttpServiceException && throwable.isConnectionError) {
            ErrorType.NETWORK_ERROR.value
        } else {
            ErrorType.SOMETHING_WRONG.value
        }
    }
}