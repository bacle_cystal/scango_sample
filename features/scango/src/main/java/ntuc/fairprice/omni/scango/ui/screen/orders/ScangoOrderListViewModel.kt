package ntuc.fairprice.omni.scango.ui.screen.orders

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.ui.screen.orders.epoxy.ScangoOrderListEpoxyController
import ntuc.fairprice.omni.scango.ui.screen.orders.paging.ScangoOrderListDataFactory
import thor.zopsmart.com.thor.repository.db.room.repo.OrderRepository
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.viewmodel.BaseViewModel
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

class ScangoOrderListViewModel @Inject constructor(
    app: Application, val mOrderRepository: OrderRepository,
    dataFactory: ScangoOrderListDataFactory
) : BaseViewModel(app) {

    private var executor: ExecutorService

    val initialPageState: LiveData<Int>
    var nextPageState: LiveData<Int>

    var orderListLiveData: LiveData<PagedList<ScangoOrderData.Data.Order>>

    init {
        executor = Executors.newFixedThreadPool(5)
        nextPageState = Transformations.switchMap(dataFactory.dataSourceLiveData) {
            it.nextPageLoadState
        }

        initialPageState = Transformations.switchMap(dataFactory.dataSourceLiveData) {
            it.initialPageLoadState
        }

        val pagedListConfig = PagedList.Config.Builder().apply {
            setEnablePlaceholders(ScangoOrderListEpoxyController.SUPPORT_PLACEHOLDERS)
            setInitialLoadSizeHint(20)
            setPageSize(20)
        }.build().apply{
            orderListLiveData = LivePagedListBuilder(dataFactory, this)
                .setFetchExecutor(executor)
                .build()
        }
    }

    fun loadOrderDetails(refNum: String) = launch {
        val orders = mOrderRepository.getOrderDetails(refNum)

        orders.onSuccess {

        }
        orders.onError {
        }

    }
}