package ntuc.fairprice.omni.scango.ui.screen.ageverify.scan

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyViewModel
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.ErrorType
import java.lang.Exception
import javax.inject.Inject

class ScangoAgeVerifyScanViewModel @Inject constructor(
    application: Application,
    private val scangoStoreRepository: ScangoStoreRepository

) : ScangoBaseViewModel(application) {

    fun handleDetectQrResult(result: String) {
        resultString.value = result
    }

    var errorType = MutableLiveData<Int>()

    val stateLiveData = MutableLiveData<ScangoAgeVerifyManuallyViewModel.AgeVerifyState>()
    val resultString = MutableLiveData<String>()

    fun verifyPincode(staffId: String, pincode: String) {
        launch {
            /* scangoStoreRepository.verifyAgePincode(staffId, pincode)
                 .onSuccess {
                     stateLiveData.value = ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationSuccess
                 }
                 .onError {
                     if (it is ScangoHttpServiceException && it.isConnectionError) {
                         stateLiveData.value = ScangoAgeVerifyManuallyViewModel.AgeVerifyState.NetworkError
                     } else {
                         stateLiveData.value = ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationError
                     }
                 }*/
            try {
                scangoStoreRepository.verifyAgePincode(staffId, pincode)
                    .onSuccess {
                        stateLiveData.value = ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationSuccess
                    }.onError {
                        super.onLoadFail(it)
                        if (it is ScangoHttpServiceException && it.isNotFound) {
                            errorType.value = ErrorType.SOMETHING_WRONG.value
                        } else if (it is ScangoHttpServiceException && it.isForbidden) {
                            errorType.value = ErrorType.USER_FORBIDDEN.value
                        } else {
                            onError(it)
                        }
                    }
            } catch (e: Exception) {
                super.onLoadFail(null)
                errorType.value = ErrorType.SOMETHING_WRONG.value
            }
        }
    }


    private fun onError(throwable: Throwable) {
        errorType.value = if (throwable is ScangoHttpServiceException && throwable.isConnectionError) {
            ErrorType.NETWORK_ERROR.value
        } else {
            ErrorType.SOMETHING_WRONG.value
        }
    }

}

