package ntuc.fairprice.omni

object Constants {
    const val packageName = "ntuc.fairprice.omni"
    const val scanGoClassname = "$packageName.scango.ScangoFeatureActivity"
    const val fairpriceOnClassname = "$packageName.fairpriceon.FairpriceOnFeatureActivity"
    const val instantClassname = "$packageName.instant.InstantActivity"
    const val fpOnSplashScreenClassname = "thor.zopsmart.com.thor.features.onboarding.ui.SplashScreenActivity"
    const val scanGoSplashScreenClassname = "ntuc.fairprice.omni.scango.SplashScreenActivity"
    const val scangoLinkPlusCardNumberActivityClassname = "$packageName.scango.ui.screen.linkplus.cardnumber.ScangoLinkPlusCardNumberActivity"
    const val scangoLinkPlusPhoneNumberActivityClassname = "$packageName.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity"

    const val scanGoCheckOutService = "ntuc.fairprice.omni.scango.SIGN_OUT"
    const val INSTABUG_TOKEN = "5d8de90de67d8314e438485739349f16"

    //SharedPreference Keys
    const val KEY_LP_IS_DISMISSED = "key_lp_isdismissed"
    const val SIGN_OUT_ACTION = "SIGN_OUT"
    const val LOGGED_IN_ACTION = "LOGGED_IN_ACTION"
    const val USER_NAME = "username"
    const val CUSTOMER_ID = "CUSTOMER_ID"

    const val SCAN_AND_GO_KEY = "scan_and_go"
    const val INSTABUG_KEY = "instabug_enabled"
}
