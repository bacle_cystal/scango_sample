package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder

@EpoxyModelClass(layout = R.layout.scango_layout_item_two_line_text_label)
abstract class ScangoTwoLineTextModel : EpoxyModelWithHolder<ScangoTwoLineTextModel.ScangoTwoLineTextHeaderHolder>() {
    @EpoxyAttribute
    var titleText: String? = null
    @EpoxyAttribute
    var contentText: String? = null

    override fun bind(holder: ScangoTwoLineTextHeaderHolder) {
        //holder.parent.setOnClickListener(clickListener)
        holder.apply {
            title.text = titleText
            content.text = contentText
        }
    }

    class ScangoTwoLineTextHeaderHolder : ScangoBaseEpoxyHolder() {
        val title: TextView by bind(R.id.tv_title)
        val content: TextView by bind(R.id.tv_content)
    }
}
