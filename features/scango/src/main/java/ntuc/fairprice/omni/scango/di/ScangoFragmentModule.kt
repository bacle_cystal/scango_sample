package ntuc.fairprice.omni.scango.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ntuc.fairprice.omni.scango.epoxy.ScangoDynamicPage
import ntuc.fairprice.omni.scango.ui.screen.ageverify.ScangoAgeNoticeFragment
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorNoticeFragment
import ntuc.fairprice.omni.scango.ui.screen.alcohol.LiquorRemoveItemFragment
import ntuc.fairprice.omni.scango.ui.screen.error.sessionexpired.ScangoSessionExpiredFragment
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackCommentDialogFragment
import ntuc.fairprice.omni.scango.ui.screen.feedback.FeedbackDialogFragment
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoHomeFragment
import ntuc.fairprice.omni.scango.ui.screen.home.ScangoTutorialFragment
import ntuc.fairprice.omni.scango.ui.screen.home.exitstore.ExitStoreFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.BarcodeErrorFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.manually.ScannerCodeManuallyFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.product.BarcodeProductRapidFragment
import ntuc.fairprice.omni.scango.ui.screen.receipt.ScangoReceiptConfirmationFragment
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListDetailPage
import ntuc.fairprice.omni.scango.ui.screen.shoppinglist.ScangoShoppingListPage
import ntuc.fairprice.omni.scango.ui.screen.uicheck.UiCheckFragment
import thor.zopsmart.com.thor.di.FragmentScope


@Module
abstract class BarcodeProductRapidFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindBarcodeProductRapidFragmentInjector(): BarcodeProductRapidFragment
}

@Module
abstract class QrCodeManuallyFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindQrCodeManuallyFragmentInjector(): ScannerCodeManuallyFragment
}


@Module
abstract class ExitStoreFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindExitStoreFragmentInjector(): ExitStoreFragment
}

@Module
abstract class FeedbackDialogFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindFeedbackDialogFragmentInjector(): FeedbackDialogFragment
}

@Module
abstract class FeedbackCommentDialogFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindFeedbackCommentDialogFragmentInjector(): FeedbackCommentDialogFragment
}

@Module
abstract class LiquorNoticeFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindLiquorNoticeFragmentInjector(): LiquorNoticeFragment
}

@Module
abstract class LiquorRemoveItemFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindLiquorRemoveItemFragmentInjector(): LiquorRemoveItemFragment
}

@Module
abstract class ScangoReceiptConfirmFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoReceiptConfirmationFragmentInjector(): ScangoReceiptConfirmationFragment
}

@Module
abstract class UiCheckFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindUiCheckFragmentInjector(): UiCheckFragment
}

@Module
abstract class BarcodeErrorFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindBarcodeErrorFragmentInjector(): BarcodeErrorFragment
}


@Module
abstract class ScangoHomeFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoHomeFragmentInjector(): ScangoHomeFragment
}

@Module
abstract class ScangoTutorialFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoTutorialFragmentInjector(): ScangoTutorialFragment
}

@Module
abstract class ScangoShoppingListPageModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoShoppingListPageInjector(): ScangoShoppingListPage
}

@Module
abstract class ScangoShoppingListDetailPageModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoShoppingListDetailPageInjector(): ScangoShoppingListDetailPage
}

@Module
abstract class ScangoDynamicPageModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoDynamicPageInjector(): ScangoDynamicPage
}

@Module
abstract class ScangoAgeNoticeFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoAgeNoticeFragmentInjector(): ScangoAgeNoticeFragment
}

@Module
abstract class ScangoSessionExpiredFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindScangoSessionExpiredFragmentInjector(): ScangoSessionExpiredFragment
}