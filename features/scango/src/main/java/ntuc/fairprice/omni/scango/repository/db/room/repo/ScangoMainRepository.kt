package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import ntuc.fairprice.omni.scango.epoxy.SGComponent
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoMainService
import org.json.JSONObject
import thor.zopsmart.com.thor.base.PageType
import thor.zopsmart.com.thor.base.extensions.string
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.epoxy.FPComponent
import thor.zopsmart.com.thor.features.widget.productcollection.data.ProductsWithFilter
import thor.zopsmart.com.thor.repository.webservice.model.Result
import javax.inject.Inject

@FeatureScope
open class ScangoMainRepository @Inject constructor(
    mApplication: Application,
    private val mainService: ScangoMainService
) : ScangoRepository(mApplication) {

    suspend fun loadHomePage(storeId: String) = mainService.loadHomePage(storeId)

    suspend fun loadProductDetails(url: String) = mainService.loadProductDetails(url)

    suspend fun loadPage(slug: String, query: String? = null) = mainService.loadPage(slug, query)

    suspend fun loadProducts(params: JSONObject, page: Int, pageType: PageType? = null, pageUrl: String? = null): Result<ProductsWithFilter> {
        var slugParams = ""
        params.keys().forEach {
            if(!params.string(it).isNullOrEmpty())
                slugParams += "$it=${params.get(it)}&"
        }
        slugParams += "page=$page${pageType?.run { "&pageType=${pageType.pageType}" } ?: ""}${pageUrl?.run { "&url=$pageUrl" } ?: ""}"

        if(pageType?.equals(PageType.SEARCH) == true)
            slugParams += "&q=$pageUrl"

        return mainService.loadProducts("/api/product?$slugParams")
    }

    suspend fun loadProductsPage(
        productAssociatedData: JSONObject,
        isPromo: Boolean
    ): Result<List<SGComponent<*>>> {
        var slugParams = ""
        productAssociatedData.keys().forEach {
            slugParams += "$it=${productAssociatedData.get(it)}&"
        }
        slugParams += "page=1"
        return mainService.loadProductsPage(productAssociatedData, slugParams, isPromo)
    }

}