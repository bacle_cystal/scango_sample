package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.boolean
import thor.zopsmart.com.thor.base.extensions.forEach
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.string

@Entity
data class FeedbackOption(
    val id: Int?,
    val option_text: String?,
    val option_point: Int?,
    val is_customer_option: Int?,
    val is_custom_feedback: Boolean?,
    var isChecked: Boolean
)

fun JSONArray.toListFeedbackOption(): List<FeedbackOption> {
    var feedbackOptions : MutableList<FeedbackOption> = arrayListOf()

    this.forEach {
        feedbackOptions.add(it.toFeedbackOption())
    }

    return feedbackOptions.toList()
}


fun JSONObject.toFeedbackOption(): FeedbackOption {
    val id = int("id")
    val option_text = string("option_text")
    val option_point = int("option_point")
    val is_customer_option = int("is_customer_option")
    val is_custom_feedback = boolean("is_custom_feedback")

    return FeedbackOption(id, option_text, option_point, is_customer_option, is_custom_feedback, false)
}