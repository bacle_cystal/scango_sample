package ntuc.fairprice.omni.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import ntuc.fairprice.omni.R

object CustomerHelpUtils {

    fun dialSupport(context: Context) {
        context.startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + context.getString(R.string.txt_link_plus_help_phone_no))))
    }

    fun emailSupport(context: Context) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", context.getString(R.string.txt_link_plus_help_email_id), null
            )
        )
        context.startActivity(Intent.createChooser(emailIntent, "Choose Email"))
    }

    fun openPlusAppInStore(contex: Context) {
        val intent = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(
                "https://play.google.com/store/apps/details?id=com.ntuclink.plus")
            setPackage("com.android.vending")
        }
        contex.startActivity(intent)
    }
}