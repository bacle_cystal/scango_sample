package ntuc.fairprice.omni.scango.repository.db.room.entity

import com.google.gson.annotations.SerializedName

data class RestrictionsConfig(
    val data: Data
)

data class Data(
    val categories: Map<String, Category>,
    val rules: Map<String, Rule>
)

data class Category(
    val id: Int,
    @SerializedName("ruleIDs") val ruleIds: List<Int>
)

data class Rule(
    val id: Int,
    val type: String,
    val order: Int,
    @SerializedName("definition") private val definitions: Map<String, List<Double>>
) {
    val definition: Map<String, List<Int>>
        get() {
            return definitions.mapValues {
                it.value.map { valueDouble ->
                    valueDouble.toInt()
                }
            }
        }
}