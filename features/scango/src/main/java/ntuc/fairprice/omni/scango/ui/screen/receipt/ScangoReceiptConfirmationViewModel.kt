package ntuc.fairprice.omni.scango.ui.screen.receipt

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import javax.inject.Inject

class ScangoReceiptConfirmationViewModel @Inject constructor(
    private val context: Application
) : ScangoBaseViewModel(context) {
    val receiptStatus = MutableLiveData<ReceiptConfirmStatus>()

    fun updateReceiptStatus(){
        receiptStatus.value = ReceiptPaymentComplete
    }

}

sealed class ReceiptConfirmStatus
object ReceiptPaymentComplete : ReceiptConfirmStatus()
object ReceiptVerifyComplete : ReceiptConfirmStatus()
object ReceiptBagItem : ReceiptConfirmStatus()