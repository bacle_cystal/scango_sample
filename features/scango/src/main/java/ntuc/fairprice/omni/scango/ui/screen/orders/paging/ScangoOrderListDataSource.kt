package ntuc.fairprice.omni.scango.ui.screen.orders.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoOrderRepository
import thor.zopsmart.com.thor.features.orderhistory.ui.epoxy.OrderListEpoxyController
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess

class ScangoOrderListDataSource constructor(val orderRepo: ScangoOrderRepository) :
    PageKeyedDataSource<Int, ScangoOrderData.Data.Order>() {

    companion object {

        // TODO: Convert this to intDef
        const val LOADING = 100
        const val LOADED = 101
        const val ERROR_LOADING = 102
    }

    private val _nextPageLoadState = MutableLiveData<Int>()
    val nextPageLoadState: LiveData<Int>
        get() = _nextPageLoadState

    private val _initialPageLoadState = MutableLiveData<Int>()
    val initialPageLoadState: LiveData<Int>
        get() = _initialPageLoadState

    override fun loadInitial(
        params: PageKeyedDataSource.LoadInitialParams<Int>,
        callback: PageKeyedDataSource.LoadInitialCallback<Int, ScangoOrderData.Data.Order>
    ) {

        _initialPageLoadState.postValue(LOADING)

        val orderResponse = orderRepo.getOrders()
        orderResponse.onSuccess {
            if (OrderListEpoxyController.SUPPORT_PLACEHOLDERS) {
                callback.onResult(it!!.data!!.orders, 0, it.data!!.count!!, null, 2)
            } else {
                callback.onResult(it!!.data!!.orders, null, 2)
            }
            _initialPageLoadState.postValue(LOADED)
        }

        orderResponse.onError {
            _initialPageLoadState.postValue(ERROR_LOADING)
        }
    }


    override fun loadBefore(
        params: PageKeyedDataSource.LoadParams<Int>,
        callback: PageKeyedDataSource.LoadCallback<Int, ScangoOrderData.Data.Order>
    ) {
    }

    override fun loadAfter(
        params: PageKeyedDataSource.LoadParams<Int>,
        callback: PageKeyedDataSource.LoadCallback<Int, ScangoOrderData.Data.Order>
    ) {

        _nextPageLoadState.postValue(LOADING)

        // Uncomment below code if you want to test pagination when we don't have enough orders
        val nextPage = /*if (params.key <= 4) 1 else */params.key
        val response = orderRepo.getOrders(nextPage)

        response.apply {
            onSuccess {
                if (it.data?.isLast == false) {
                    callback.onResult(it!!.data!!.orders, params.key + 1)
                }
                _nextPageLoadState.postValue(LOADED)
            }

            onError {
                _nextPageLoadState.postValue(ERROR_LOADING)
            }
        }
    }

}