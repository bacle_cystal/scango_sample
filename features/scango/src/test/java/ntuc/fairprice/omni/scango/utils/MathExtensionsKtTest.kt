package ntuc.fairprice.omni.scango.utils

import org.junit.Assert
import org.junit.Test

class MathExtensionsKtTest {

    @Test
    fun removeRedundant0_1() {
        Assert.assertEquals("1.234", 1.234.removeRedundant0())
    }

    @Test
    fun removeRedundant0_2() {
        Assert.assertEquals("1", 1.0000.removeRedundant0())
    }

    @Test
    fun removeRedundant0_3() {
        Assert.assertEquals("1.00001", 1.00001.removeRedundant0())
    }

    @Test
    fun removeRedundant0_4() {
        Assert.assertEquals("1", (1.toDouble()).removeRedundant0())
    }

    @Test
    fun removeRedundant0_5() {
        Assert.assertEquals("1.234", 1.23400000.removeRedundant0())
    }

    @Test
    fun removeRedundant0_6() {
        Assert.assertEquals("1.234", 1.2340.removeRedundant0())
    }

    @Test
    fun removeRedundant0_7() {
        Assert.assertEquals("100", 100.0.removeRedundant0())
    }

    @Test
    fun removeRedundant0_8() {
        Assert.assertEquals("0", 0.0.removeRedundant0())
    }
}