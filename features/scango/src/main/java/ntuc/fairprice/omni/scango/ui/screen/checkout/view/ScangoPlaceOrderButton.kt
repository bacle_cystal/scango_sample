package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.scango_place_order_button.view.*
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.base.extensions.setCurrencyText
import kotlin.properties.Delegates

class ScangoPlaceOrderButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var message: String by Delegates.observable("Pay & Place order") { _, _, newValue ->
        tv_place_order.text = newValue
        //postInvalidate()
    }

    var total: Double by Delegates.observable(0.0) { _, _, newValue ->
        tv_total.setCurrencyText(newValue)

    }

    var isActive: Boolean by Delegates.observable(false) { _, _, newValue ->

        if(newValue) {
            setBackgroundResource(R.drawable.bg_shape_rounded_big)
        } else {
            setBackgroundResource(R.drawable.bg_shape_rounded_big_inactive)
        }
    }

}