package ntuc.fairprice.omni.scango.domain.restrictionconfig

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import javax.inject.Inject

class ScangoSyncRestrictionsConfigUseCaseImpl @Inject constructor(
    private val scangoRestrictionsConfigRepository: ScangoRestrictionsConfigRepository
): ScangoSyncRestrictionsConfigUseCase {

    override fun execute() {
        CoroutineScope(Dispatchers.IO).launch {
            scangoRestrictionsConfigRepository.syncConfig()
                .onSuccess {
                    scangoRestrictionsConfigRepository.saveConfig(it)
                }
                .onError {
                    //Do nothing as we have a fall back json in app
                }
        }
    }
}