package ntuc.fairprice.omni.scango.ui.screen.checkout

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidator
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCheckoutResult
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoPaymentType
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoAccountRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoOrderRepository
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoStoreRepository
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.model.toCheckoutResult
import ntuc.fairprice.omni.scango.ui.screen.cart.*
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.LinkPointStatus
import ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview.ScangoPaymentSummary
import ntuc.fairprice.omni.scango.utils.DateTimeUtils
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.OpenForTesting
import thor.zopsmart.com.thor.base.extensions.forEach
import thor.zopsmart.com.thor.base.extensions.forEachIndexed
import thor.zopsmart.com.thor.model.Card
import thor.zopsmart.com.thor.repository.db.room.entity.Customer
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

@OpenForTesting
class ScangoCheckoutViewModel @Inject constructor(
    val mApplication: Application,
    private val orderRepository: ScangoOrderRepository,
    private val cartRepository: ScangoCartRepository,
    private val storeRepository: ScangoStoreRepository,
    private val accountRepository: ScangoAccountRepository,
    private val fpOnaccountRepository: AccountRepository,
    private val scangoLinkPointRepository: ScangoLinkPointRepository,
    private val restrictionsValidator: RestrictionsValidator
) : ScangoBaseViewModel(mApplication) {
    // Live data to observe
    val couponSelectionStatus = MutableLiveData<ScangoCouponSelection>()
    val orderPlacementStatus = MutableLiveData<OrderPlacementStatus>()
    val timeStampStatus = MutableLiveData<ScangoGetTimeStatus>()
    val cartSyncStatus = MutableLiveData<ScangoCartResponse>()
    var selectedCouponList = listOf<ScangoCoupon>()
    var placeOrderId = MutableLiveData<OrderPlacementStatus>()
    val linkPointLd = MutableLiveData<LinkPointStates>()
    val isActiveLinkpoint = MutableLiveData<Boolean>()

    val linkPointAndCartLd = createLinkPointAndCartLiveData()

    val isAgeVerified: Boolean
        get() = storeRepository.getAgeVerifyCached().isNotEmpty() && !getListAgeRestriction().isNullOrEmpty()

    fun createLinkPointAndCartLiveData(): LiveData<LoadCartAndLinkpointStates> {
        val liveLinkPoint = linkPointLd
        val liveCartSync = cartSyncStatus

        val result = MediatorLiveData<LoadCartAndLinkpointStates>()

        result.addSource(liveLinkPoint) { value ->
            result.value = combineLoadedData(liveLinkPoint, liveCartSync)
        }
        result.addSource(liveCartSync) { value ->
            result.value = combineLoadedData(liveLinkPoint, liveCartSync)
        }
        return result
    }

    private fun combineLoadedData(
        linkpointState: MutableLiveData<LinkPointStates>,
        scangoCartResponse: MutableLiveData<ScangoCartResponse>
    ): LoadCartAndLinkpointStates {
        val linkPointState = linkpointState.value
        val cartStatus = scangoCartResponse.value

        if (linkPointState == null || cartStatus == null) {
            return LoadCartAndLinkpointLoading()
        }

        //Ensure both link point and cart data is ready for checkout
        if (linkPointState !is LinkPointStatusLoading && cartStatus is ScangoCartSuccess) {
            return LoadCartAndLinkpointSuccess(
                cartStatus.data,
                ScangoPaymentSummary(
                    cartStatus.data.sellingPrice,
                    cartStatus.data.sellingPrice,
                    cartStatus.data.discount,
                    selectedCouponList,
                    null,
                    null,
                    false,
                    linkPointStatus = when (linkPointState) {
                        is LinkPointStatusSuccess -> linkPointState.linkPointStatus
                        else -> null
                    }
                )
            )
        }
        return LoadCartAndLinkpointLoading()
    }

    fun updateCoupons() {
        // If coupon is empty, don't update anything
        val coupons = cartRepository.getCartCoupons()
        if (coupons.isEmpty())
            return
        loadPromotions()
    }

    fun changeLinkPointStatus(isActive: Boolean) {
        isActiveLinkpoint.value = isActive
    }

    fun getCartCoupons() = cartRepository.getCartCoupons()

    fun loadPromotions() = launch {
        orderRepository.loadPromotions()
            .onSuccess {
                selectedCouponList = listOf(it[0], it[1], it[2])
                cartRepository.setCartCoupons(selectedCouponList)
            }
            .onError {
            }
    }


    fun loadCart() = launch {
        cartSyncStatus.value = ScangoCartLoading


        val listOffer = listOf<ScangoOffer>()
        val listCartItem = listOf(
            ScangoCartItem(
                name = "Budweiser",
                quantity = 12,
                fullName = "Budweiser",
                discount = 10.0,
                id = 1,
                image = null,
                images = null,
                itemId = 0,
                mrp = 0.0,
                sellingPrice = 12.9,
                url = null
            )
        )

        cartSyncStatus.value = ScangoCartSuccess(
            ScangoCartSyncData(
                cartOffers = listOffer,
                discount = 0.0,
                sellingPrice = 192.0,
                cartSyncItems = listCartItem,
                totalCouponDiscount = 0.0
            )
        )

//        cartRepository.sync()
//            .onSuccess {
//                cartSyncStatus.value = ScangoCartSuccess(it)
//            }
//            .onError {
//                if (it is ScangoHttpServiceException && it.isConnectionError) {
//                    cartSyncStatus.value = ScangoCartFailNetwork
//                } else {
//                    cartSyncStatus.value = ScangoCartFailGeneral
//                }
//            }
    }

    fun getListTimeRestriction(): List<ScangoCartItem>? =
        cartRepository.getCartItems().filter {
            restrictionsValidator.isTimeViolated(it)
        }

    fun checkTimeStamp() = launch {
        timeStampStatus.value = ScangoGetTimeSuccess
    }

    /**
     * Changes in place order to include, card id.
     */
    fun placeOrder() = launch {

//        Log.d("checkoutdebug", "----- placeOrder called")
//        val coupon = selectedCouponList
//        val paymentType = ScangoPaymentType.ONLINE
//        val customer = getCustomer()
//        val cartItems = cartRepository.getCartItems()
//        val storeId = storeRepository.getStoreIdCached()?.toLong() ?: 0L
////        val validation = validateCheckoutData(customer, storeId, selectedCard)
//        val linkPointAmount =
//            getLinkpointAvaiableValue((linkPointAndCartLd.value as LoadCartAndLinkpointSuccess).scangoPaymentSummary)
//
//        var sellingPrice: Long = 0
//        val t = cartSyncStatus.value
//        if (t is ScangoCartSuccess) {
//            sellingPrice = t.data.sellingPrice.toLong()
//        }
        orderPlacementStatus.value = OrderSuccess(ScangoCheckoutResult(123,"123",0.0,123))
                clearCart()


//        if (validation.first || !storeRepository.getAgeVerifyCached().isNullOrEmpty()) {
//            orderPlacementStatus.value = OrderPlacing
//            orderRepository.placeOrder(
//                paymentType,
//                customer!!.id,
//                cartItems,
//                storeId,
//                selectedCard!!.id,
//                coupon,
//                DateTimeUtils.getCurrentDayString(),
//                sellingPrice,
//                isActiveLinkpoint.value ?: false,
//                linkPointAmount,
//                !getListAgeRestriction().isNullOrEmpty(),
//                storeRepository.getAgeVerifyCached(),
//                constructMetaData()
//            ).onSuccess {
//                orderPlacementStatus.value = OrderSuccess(it.toCheckoutResult(storeId))
//                clearCart()
//                Log.d("checkoutdebug", "place order success")
//            }
//                .onError {
//                    if (it is ScangoHttpServiceException && it.isConnectionError) {
//                        orderPlacementStatus.value = OrderErrorNetwork
//                    } else if (it is ScangoHttpServiceException && it.isNotFound) {
//                        orderPlacementStatus.value = OrderCardError
//                    } else {
//                        orderPlacementStatus.value = OrderFailure(it)
//                    }
//                }
//        } else {
//            if (validation.second == "ageverify") {
//                orderPlacementStatus.value = OrderNeedAgeVerify
//            } else {
//                orderPlacementStatus.value = OrderFailure(Exception("Required field ${validation.second} is missing."))
//            }
//        }
    }

    private fun clearCart() {
        cartRepository.clearCart()
    }

    fun getAgeVerifyCached() = storeRepository.getAgeVerifyCached()


    fun getCustomer(): Customer? = fpOnaccountRepository.getCustomer()

    fun getListAgeRestriction(): List<ScangoCartItem>? =
        cartRepository.getCartItems().filter { restrictionsValidator.isAgeViolated(it) }

    private fun validateCheckoutData(
        customer: Customer?,
        storeId: Long,
        selectedCard: Card?
    ): Pair<Boolean, String> {
        //Just check item age when not yet verify and has item age restriction in cart
        if (!getListAgeRestriction().isNullOrEmpty() and getAgeVerifyCached().isNullOrEmpty())
            return Pair(false, "ageverify")

        if (customer == null)
            return Pair(false, "customer")

        if (storeId == -1L)
            return Pair(false, "storeId")

        if (selectedCard == null)
            return Pair(false, "selectedCard")

        return Pair(true, "")
    }

    // Card live data
    val cardLd = MutableLiveData<CardState>().apply {
        value = CardState.Success(Card(123,"123123","123123",true))
    }
//    final var selectedCard: Card? = null
//        private set
//        get() {
//            val cardVal = cardLd.value
//            return if (cardVal is CardState.Success) {
//                cardVal.card
//            } else {
//                null
//            }
//        }


    var cards = listOf<Card>()

    fun loadCards() = launch {

                cards = listOf()
                useDefaultCard(listOf())

    }

    fun changeCard(cardId: Long) {
        cardLd.value = CardState.Success(cards.filter { it.id == cardId }[0])
    }

    fun saveCard(param: JSONObject, default: Boolean) = launch {
        cardLd.value = CardState.Success(Card(123,"123123","123123",true))
//        accountRepository.saveCard(param)
//            .onSuccess {
//                detectAddedCardAndUse(it)
//                cards = it
//            }
//            .onError {
//                // Error saving the card, fallback to default card
//                if (it is ScangoHttpServiceException && it.isConnectionError) {
//                    cardLd.value = CardState.SaveCardErrorNetwork(param, default)
//                } else {
//                    cardLd.value = CardState.CardErrorGeneral()
//                }
//                useDefaultCard(cards)
//            }
    }

    /**
     * Detects a newly added card by diffing the previously saved card list and the current card list,
     * and uses it.
     */
    private fun detectAddedCardAndUse(allCards: List<Card>) {
        Log.d("Credit card test", "detecting")
        if (cards.isNullOrEmpty() || allCards.isNullOrEmpty()) {
            Log.d(
                "Credit card test",
                "Empty, go to default. prev empty${cards.isNullOrEmpty()}, curr cards empty ${allCards.isNullOrEmpty()}"
            )
            useDefaultCard(allCards)
            return
        }

        allCards.filterNot { card ->
            cards.any {
                card.id == it.id
            }
        }.getOrNull(0).also { newCard ->
            if (newCard != null) {
                Log.d("Credit card test", "Found, SUCCESS")
                cardLd.value = CardState.Success(newCard)
            } else {
                Log.d("Credit card test", "Not found, showing default")
                useDefaultCard(allCards)
            }
        }
    }

    fun useDefaultCard(cardList: List<Card>) {
        cardLd.value =
            if (cardList.isEmpty()) {
                CardState.NoCardFound()
            } else {
                val defaultCard = cardList.filter { it.isDefault }.getOrElse(0) {
                    // This should never happen, backend should always have a default card
                    Log.d("Credit card test", "Did not find a default card, using 0th card")
                    cardList[0]
                }
                CardState.Success(defaultCard)
            }
    }


    fun setSelectedCoupons(coupons: List<ScangoCoupon>) {
        if (coupons.isEmpty())
            return
        val t = cartSyncStatus.value
        if (t is ScangoCartSuccess) {
            t.let {
                couponSelectionStatus.value = ScangoCouponSelected(it.data, coupons)
                selectedCouponList = coupons
            }
        }
    }

    fun removeCoupon(coupon: ScangoCoupon) {
        val status = couponSelectionStatus.value
        if (status is ScangoCouponSelected) {
            val list = status.couponList
            val newList = mutableListOf<ScangoCoupon>()
            list.forEach {
                if (it.coupon != coupon.coupon) {
                    newList.add(it)
                }
            }
            couponSelectionStatus.value = ScangoCouponSelected(status.cartSyncData, newList)
            selectedCouponList = newList
        }
    }

    fun loadLinkPoint() = launch {
//        linkPointLd.value = LinkPointStatusLoading
//        getCustomer()?.let { customer ->
//            scangoLinkPointRepository.getCustomers(customer.id.toString())
//                .onSuccess {
//                    onSuccess()
//                    linkPointLd.value = LinkPointStatusSuccess(it)
//                }
//                .onError {
//                    if (it is ScangoHttpServiceException && it.isConnectionError) {
//                        linkPointLd.value = LinkPointStatusFailedNetwork(it)
//                    } else {
//                        linkPointLd.value = LinkPointStatusFailedGeneral(it)
//                    }
//                }
//        }
    }

    fun getLinkpointAvaiableValue(scangoPaymentSummary: ScangoPaymentSummary): Double {
        val youPay = scangoPaymentSummary.youPay
        val dollarBalance = scangoPaymentSummary.linkPointStatus?.dollarBalance ?: 0.0
        return when {
            youPay > dollarBalance -> dollarBalance
            else -> youPay
        }
    }

    private fun constructMetaData(): JSONObject {
        val metaData = JSONArray()
        val cartItems = cartRepository.getCartItems()
        cartItems.forEach { cartItem ->
            var isContains = false
            var currentIndex = 0
            metaData.forEachIndexed { index, jsonObject ->
                if (jsonObject.get("id") == cartItem.id) {
                    isContains = true
                    currentIndex = index
                    return@forEachIndexed
                } else {
                    isContains = false
                }
            }
            when (isContains) {
                true -> {
                    val jsonObject = metaData.getJSONObject(currentIndex)
                    jsonObject.apply {
                        put(
                            "q",
                            this.getDouble("q") + if (cartItem.isSoldByWeight()) cartItem.weight else cartItem.quantity.toDouble()
                        )
                        put("c", this.getInt("c") + cartItem.quantity)
                    }
                }
                false -> {
                    metaData.put(JSONObject().apply {
                        put("id", cartItem.id)
                        put("q", if (cartItem.isSoldByWeight()) cartItem.weight else cartItem.quantity.toDouble())
                        put("c", cartItem.quantity)
                    })
                }
            }
        }
        return JSONObject().apply {
            put("sngCart", metaData.toString())
        }
    }
}


sealed class LoadCartAndLinkpointStates
class LoadCartAndLinkpointLoading : LoadCartAndLinkpointStates()
class LoadCartAndLinkpointSuccess(
    val cartSyncData: ScangoCartSyncData,
    val scangoPaymentSummary: ScangoPaymentSummary
) : LoadCartAndLinkpointStates()

class LoadCartAndLinkpointFailed(val throwable: Throwable) : LoadCartAndLinkpointStates()


sealed class LinkPointStates
class LinkPointStatusSuccess(val linkPointStatus: LinkPointStatus) : LinkPointStates()
class LinkPointStatusFailedGeneral(val throwable: Throwable) : LinkPointStates()
class LinkPointStatusFailedNetwork(val throwable: Throwable) : LinkPointStates()
object LinkPointStatusLoading : LinkPointStates()

sealed class OrderPlacementStatus
object OrderPlacing : OrderPlacementStatus()
object OrderNeedAgeVerify : OrderPlacementStatus()
class OrderSuccess(val scangoCheckoutResult: ScangoCheckoutResult) : OrderPlacementStatus()
object OrderErrorNetwork : OrderPlacementStatus()
object OrderCardError : OrderPlacementStatus()
class OrderFailure(val e: Throwable) : OrderPlacementStatus()

sealed class CardState {
    class Success(val card: Card) : CardState()
    class NoCardFound : CardState()
    class SaveCardErrorNetwork(val param: JSONObject, val default: Boolean) : CardState()
    class LoadCardErrorNetwork : CardState()
    class CardErrorGeneral : CardState()
    class Loading : CardState()
}

sealed class ScangoCouponSelection
object ScangoCouponSelectionLoading : ScangoCouponSelection()
object ScangoCouponSelectionError : ScangoCouponSelection()
class ScangoCouponSelected(val cartSyncData: ScangoCartSyncData, val couponList: List<ScangoCoupon>) :
    ScangoCouponSelection()


sealed class ScangoGetTimeStatus
object ScangoGetTimeLoading : ScangoGetTimeStatus()
object ScangoGetTimeSuccess : ScangoGetTimeStatus()
object ScangoGetTimeIsViolated : ScangoGetTimeStatus()
object ScangoGetTimeErrorNetwork : ScangoGetTimeStatus()
