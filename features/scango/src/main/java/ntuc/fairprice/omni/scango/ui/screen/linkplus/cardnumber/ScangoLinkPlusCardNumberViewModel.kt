package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.repository.webservice.model.ScangoHttpServiceException
import ntuc.fairprice.omni.scango.repository.webservice.model.onError
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberViewModel
import thor.zopsmart.com.thor.repository.db.room.entity.Customer
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import javax.inject.Inject

class ScangoLinkPlusCardNumberViewModel @Inject constructor(
    application: Application,
    private val accountRepository: AccountRepository,
    private val scangoLinkPointRepository: ScangoLinkPointRepository
): ScangoBaseViewModel(application) {

    sealed class State{
        object UpdateCustomerCardSuccess: State()
        object AccountNotFound : State()
        object GeneralError: State()
        object NetworkError: State()
    }

    val stateLiveData = MutableLiveData<State>()

    fun updateCustomerRecordWithCardNumber(cardNumber: String, isHashed: Boolean) = launch {
        getCustomer()?.let { customer ->
            scangoLinkPointRepository.updateCustomerRecordWithCard(customer.id.toString(), cardNumber, isHashed)
                .onSuccess {
                    stateLiveData.value = State.UpdateCustomerCardSuccess
                }
                .onError {
                    stateLiveData.value = if (it is ScangoHttpServiceException && it.isNotFound) {
                        State.AccountNotFound
                    } else if (it is ScangoHttpServiceException && it.isConnectionError) {
                        State.NetworkError
                    } else {
                        State.GeneralError
                    }
                }
        }
    }

    private fun getCustomer(): Customer? =
        accountRepository.getCustomer()
}