package ntuc.fairprice.omni.scango.epoxy.model

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.annotation.Nullable
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_layout_view_carousel.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.loadImageOptimize
import thor.zopsmart.com.thor.features.widget.imageslideshow.data.ImageSlideShow

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ScangoSlideShowView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.scango_layout_view_carousel, this)
    }

    private lateinit var imageSlideShow: List<ImageSlideShow>

    private lateinit var clickHandler: ((String) -> Unit)

    @ModelProp
    fun updateSlides(imageSlideShow: List<ImageSlideShow>) {
        this.imageSlideShow = imageSlideShow
        renderSlideShow()
    }

    private fun renderSlideShow() {
        carousel_view.apply {
            bringToFront()
            visibility = View.VISIBLE

            setImageListener { position, imageView ->
                val carousel = imageSlideShow[position]

                imageView.apply {
                    scaleType = ImageView.ScaleType.FIT_CENTER
                    loadImageOptimize(carousel.imageUrl)
                    setOnClickListener {
                        carousel.link?.let {
                            clickHandler(carousel.link + "")
                        }
                    }
                }
            }
            pageCount = imageSlideShow.size
        }
    }

    @CallbackProp
    fun onSlideClick(@Nullable action: ((String) -> Unit)?) {
        clickHandler = action ?: {
            // Do nothing if not set
        }
    }

}