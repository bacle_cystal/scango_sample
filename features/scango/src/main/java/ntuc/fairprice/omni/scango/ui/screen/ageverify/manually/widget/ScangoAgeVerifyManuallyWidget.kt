package ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.widget

import android.view.View
import kotlinx.android.extensions.LayoutContainer

interface ScangoAgeVerifyManuallyWidget: LayoutContainer {

    fun initWidget(view: View)

    var onOtpInputComplete: (() -> Unit)?

    fun getStaffId(): String

    fun getPincode(): String

    fun displayErrorState()

    fun displayDefaultState()
}