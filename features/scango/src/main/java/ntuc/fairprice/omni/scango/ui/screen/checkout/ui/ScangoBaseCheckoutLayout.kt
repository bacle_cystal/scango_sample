package ntuc.fairprice.omni.scango.ui.screen.checkout.ui

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.constraintlayout.widget.ConstraintLayout

open class ScangoBaseCheckoutLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {


    private var blockAll: Boolean = false

    fun blockAllTouchEvents(block: Boolean) {
        alpha = if (block) 0.3f else 1f
        isEnabled = block.not()

        blockAll = block

    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return blockAll || super.onInterceptTouchEvent(ev)
    }
}