package ntuc.fairprice.omni.scango

import ntuc.fairprice.omni.BaseInjectionProvider
import ntuc.fairprice.omni.scango.di.DaggerScangoComponent
import thor.zopsmart.com.thor.BaseApplication
import thor.zopsmart.com.thor.di.InjectorFactoryMap
import javax.inject.Inject

class ScangoInjectionProvider: BaseInjectionProvider() {
    @Inject
    lateinit var scangoInjectorFactoryMap: InjectorFactoryMap

    override fun onCreate(): Boolean {
        DaggerScangoComponent.builder()
            .coreComponent(
                (context.applicationContext as BaseApplication).coreComponent)
            .build()
            .inject(this)

        (context.applicationContext as BaseApplication).registerInjectorFactoryMap(scangoInjectorFactoryMap)
        return true
    }
}