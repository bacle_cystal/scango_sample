package ntuc.fairprice.omni.scango.di

import dagger.Binds
import dagger.Module
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidator
import ntuc.fairprice.omni.scango.base.restrictions.RestrictionsValidatorImpl
import ntuc.fairprice.omni.scango.domain.restrictionconfig.ScangoSyncRestrictionsConfigUseCase
import ntuc.fairprice.omni.scango.domain.restrictionconfig.ScangoSyncRestrictionsConfigUseCaseImpl
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepository
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepositoryImpl
import thor.zopsmart.com.thor.di.FeatureScope

@Module
abstract class ScangoRestrictionsConfigModule {

    @Binds
    @FeatureScope
    abstract fun provideRestrictionsConfigRepository(
        scangoRestrictionsConfigRepository: ScangoRestrictionsConfigRepositoryImpl
    ): ScangoRestrictionsConfigRepository


    @Binds
    @FeatureScope
    abstract fun provideSyncRestrictionsConfigUseCase(
        scangoSyncRestrictionsConfigUseCase: ScangoSyncRestrictionsConfigUseCaseImpl
    ): ScangoSyncRestrictionsConfigUseCase

    @Binds
    @FeatureScope
    abstract fun provideRestrictionsValidator(
        restrictionsValidator: RestrictionsValidatorImpl
    ): RestrictionsValidator


}