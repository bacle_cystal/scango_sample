package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.scango_layout_payment_summary.view.*
import ntuc.fairprice.omni.scango.R as R
import ntuc.fairprice.omni.R as BR
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoLinkpointReceipt
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import ntuc.fairprice.omni.scango.repository.db.room.entity.getTotalPromoDiscount
import ntuc.fairprice.omni.scango.ui.screen.checkout.view.ScangoSwitchLinkpointListener
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.LinkPointStatus
import thor.zopsmart.com.thor.features.checkout.ui.BaseCheckoutLayout
import ntuc.fairprice.omni.scango.ui.screen.promo.view.ScangoPromoRemoveListener
import ntuc.fairprice.omni.scango.utils.setCreaditCardImage
import thor.zopsmart.com.thor.base.extensions.*

class ScangoPaymentSummaryLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseCheckoutLayout(context, attrs, defStyleAttr) {

    init {
        setBackgroundResource(R.drawable.scango_edit_text_border)
        LayoutInflater.from(context).inflate(R.layout.scango_layout_payment_summary, this)

        /*tv_add_promo.gone()
        tv_add_promo.setOnClickListener {
            addPromoClick?.onClick(it)
        }*/

        cl_link_point.apply {
            switchButtonClick = {
                onLinkPointChangeStatus(it, paymentSummary)
            }
        }
    }

    var addPromoClick: OnClickListener? = null

    var isCheckoutScreen: Boolean? = null
        set(value) {
            value?.let {
                when (it) {
                    true -> {
                        tv_label_total2.text = context.getString(R.string.title_checkout_total)
                        tv_label_subtotal.text = context.getString(R.string.label_total_payable)
                    }
                    false -> {
                        tv_label_total2.text = context.getString(R.string.label_total_payable)
                        tv_label_subtotal.text = context.getString(R.string.label_subtotal)
                    }
                }
            }
            field = value
        }

    var swithButtonListener: ScangoSwitchLinkpointListener? = null

    var promoRemoveListener: ScangoPromoRemoveListener? = null

    var paymentSummary: ScangoPaymentSummary? = null
        set(value) {
            value?.let {
                with(it) {
                    tv_subtotal.setCurrencyText(youPay + (totalCouponDiscount ?: 0.0) + jwcOffers.getTotalDiscount())
                    //Hide promo code function
                    val isShowOffer = promoList.isNotEmpty() || jwcOffers.isNotEmpty()
                    if (!isShowOffer) {
                        promo_list.gone()
                        tv_label_promo.gone()
                        line3.gone()
                    } else {
                        promo_list.visible()
                        tv_label_promo.visible()
                        line3.visible()
                    }
                    promo_list.addPromos(promoList, promoRemoveListener, jwcOffers)

                    cardType?.let {
                        iv_card_logo.setCreaditCardImage(cardType.toString())
                    } ?: iv_card_logo.setVisibility(false)

                    cardNumber?.let {
                        tv_card_number.text = cardNumber
                    } ?: tv_card_number.setVisibility(false)

                    //Default with switch on if linkpoint redeem available
                    //If linkPointStatus has value is meaning linkpoint redeem available
                    onLinkPointChangeStatus(false, it)
                }
            }
            field = value
        }


    private fun onLinkPointChangeStatus(isActive: Boolean, paymentSummary: ScangoPaymentSummary?) {
        paymentSummary?.apply {
            val availableLpCanApply = getLinkpointAvaiableToApply(youPay, linkPointStatus?.dollarBalance ?: 0.0)
            linkPointStatus?.let {
                if (it.isFullyLinked && it.dollarBalance != 0.0) {
                    line_link_point.visible()
                    cl_link_point.visible()
                } else {
                    line_link_point.gone()
                    cl_link_point.gone()
                }
                cl_link_point.setPointRedeem(
                    context.getString(BR.string.two_decimal_formatted_val, it.dollarBalance),
                    context.getString(BR.string.two_decimal_formatted_val, it.pointBalance)
                )
                cl_link_point.setDiscount(availableLpCanApply)
            }

            val couponDiscount = promoList.getTotalPromoDiscount()

            //When change switch linkpoint re calculation value fmor discount and total value
            tv_saved_amount.setSavings(discount + couponDiscount)

            val totalValue: Double = youPay - couponDiscount
            tv_saved_amount.setVisibility((discount + couponDiscount) > 0)

            when (isActive) {
                true -> {
                    tv_total_price2.setCurrencyText(totalValue - availableLpCanApply)
                    swithButtonListener?.onSwitchLinkpoint(isActive, totalValue - availableLpCanApply)
                    cl_link_point.setDiscount(availableLpCanApply)
                }
                false -> {
                    tv_total_price2.setCurrencyText(totalValue)
                    swithButtonListener?.onSwitchLinkpoint(isActive, totalValue)
                    cl_link_point.setDiscount(0.0)
                }
            }
        }
    }

    fun getLinkpointAvaiableToApply(youPay: Double, dollarBalance: Double) = when {
        youPay > dollarBalance -> dollarBalance
        else -> youPay
    }
}

fun List<ScangoOffer>.getTotalDiscount(): Double {
    var discount = 0.0
    forEach {
        discount += it.discount
    }
    return discount
}

data class ScangoPaymentSummary(
    val youPay: Double = 90.0,
    val subTotal: Double = 90.0,
    val discount: Double = 90.0,
    val promoList: List<ScangoCoupon> = emptyList(),
    val cardType: String? = null,
    val cardNumber: String? = null,
    val isEditable: Boolean = true,
    val jwcOffers: List<ScangoOffer> = emptyList(),
    val linkPointStatus: LinkPointStatus? = null,
    val linkPointReceipt: ScangoLinkpointReceipt? = null,
    val totalCouponDiscount: Double? = null
)