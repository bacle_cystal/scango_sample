package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import ntuc.fairprice.omni.scango.ui.screen.linkplus.model.*
import org.json.JSONObject
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoLinkPointService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) : ScangoBaseService(scangoRequestProvider)  {

    private object LinkType {
        const val MOBILE = "1"
        const val CARD = "2"
        const val HASHED_CARD = "6"
    }

    suspend fun getCustomers(customerId : String) = apiCall(
        "/link/v0.2/customers/$customerId",
        ScangoRequestProvider.Method.GET,
        forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
        it.toLinkPointStatus()
    }

    suspend fun updateCustomerRecordWithNumber(customerId: String, phoneNumber: String): Result<UpdateCustomerLinkPointResponse> {

        val params = JSONObject()
        val paramsValue = JSONObject()
        paramsValue.put("linkType", LinkType.MOBILE)
        paramsValue.put("linkID", phoneNumber)
        params.put("params", paramsValue)

        return apiCall(
            "/link/v0.2/customers/$customerId",
            ScangoRequestProvider.Method.PUT,
            params = params,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
            it.toUpdateCustomerLinkPointStatus()
        }
    }

    suspend fun updateCustomerRecordWithCard(customerId: String, cardNumber: String, isHashed: Boolean): Result<UpdateCustomerLinkPointResponse> {

        val params = JSONObject()
        val paramsValue = JSONObject()
        paramsValue.put("linkType", if (isHashed) LinkType.HASHED_CARD else LinkType.CARD)
        paramsValue.put("linkID", cardNumber)
        params.put("params", paramsValue)

        return apiCall(
            "/link/v0.2/customers/$customerId",
            ScangoRequestProvider.Method.PUT,
            params = params,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
            it.toUpdateCustomerLinkPointStatus()
        }
    }

    suspend fun sendOtp(resendOTP: Boolean): Result<SendOtpResponse> {
        val params = JSONObject()
        val paramsValue = JSONObject()
        paramsValue.put("resendOTP", resendOTP)
        params.put("params", paramsValue)

        return apiCall(
            "/link/v0.2/otp",
            ScangoRequestProvider.Method.POST,
            params = params,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
            it.toSendOtpResponse()
        }
    }

    suspend fun verifyOtp(otp: String): Result<VerifyOtpResponse> = apiCall(
        "/link/v0.2/otp/$otp",
        ScangoRequestProvider.Method.GET,
        forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
        it.toVerifyOtpResponse()
    }

    suspend fun unLinkCustomer(customerId : String) = apiCall(
        "/link/v0.2/customers/$customerId",
        ScangoRequestProvider.Method.DELETE,
        forWho = ScangoRequestProvider.RequestFor.SCANGO_V2) {
        it.toUnLinkCustomerResponse()
    }

}