package ntuc.fairprice.omni.dialog

import android.content.Context
import ntuc.fairprice.omni.R

class ScangoGeneralErrorDialog(context: Context) : ScangoBaseErrorDialog(context) {

    init {
        title.text = context.resources.getString(R.string.general_error_dialog_title)
        message.text = context.resources.getString(R.string.general_error_dialog_message)
        erroImage.setImageResource(R.drawable.sg_ic_something_went_wrong)
    }
}