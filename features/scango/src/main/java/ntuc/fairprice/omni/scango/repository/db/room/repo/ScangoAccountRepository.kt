package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderDetail
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartArray
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoOrderService
import thor.zopsmart.com.thor.di.FeatureScope
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoPaymentType
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoAccountService
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoRetrofitService
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.ResourceNotFoundException
import thor.zopsmart.com.thor.repository.webservice.model.getRetrofitResult
import javax.inject.Inject
import thor.zopsmart.com.thor.repository.webservice.model.Result

@FeatureScope
class ScangoAccountRepository @Inject constructor(
    mApplication: Application,
    private val scangoAccountService: ScangoAccountService
) : ScangoRepository(mApplication) {

    suspend fun loadCards() = scangoAccountService.loadCards()

    suspend fun saveCard(cardParams: JSONObject) = scangoAccountService.saveCard(cardParams)
}