package ntuc.fairprice.omni.scango.di

import android.app.Activity
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.dialog.*
import thor.zopsmart.com.thor.di.ActivityScope

@Module
class ScangoCommonDialogModule {

    @Provides
    @ActivityScope
    fun provideScanGoProgressDialog(activity: Activity)
            : ScanGoProgressDialog = ScanGoProgressDialog(activity)

    @Provides
    @ActivityScope
    fun provideScangoNetworkErrorDialog(activity: Activity)
            : ScangoNetworkErrorDialog = ScangoNetworkErrorDialog(activity)


    @Provides
    @ActivityScope
    fun provideScangoGeneralErrorDialog(activity: Activity)
            : ScangoGeneralErrorDialog = ScangoGeneralErrorDialog(activity)

    @Provides
    @ActivityScope
    fun provideScangoPaymentErrorDialog(activity: Activity)
            : ScangoPaymentErrorDialog = ScangoPaymentErrorDialog(activity)

    @Provides
    @ActivityScope
    fun provideScangoUserBlockedErrorDialog(activity: Activity)
            : ScangoUserBlockedErrorDialog = ScangoUserBlockedErrorDialog(activity)
}

