package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.scango_activity_link_plus_card_done.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import ntuc.fairprice.omni.util.CustomerHelpUtils

class ScangoLinkPlusCardDoneActivity : ScangoBaseActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, ScangoLinkPlusCardDoneActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_link_plus_card_done)
        tv_scango_link_plus_card_done_back.setOnClickListener { finish() }
        tb_scango_link_plus_card_done.setOnActionClickListener { finish() }
        tv_scango_link_plus_card_done_t9.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(this))
            finish()
        }
        img_link_plus_card_call_us.setOnClickListener {
            CustomerHelpUtils.dialSupport(this)
        }
        btn_link_plus_card_email_us.setOnClickListener {
            CustomerHelpUtils.emailSupport(this)
        }
    }
}
