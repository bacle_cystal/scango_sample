package ntuc.fairprice.omni.scango.ui.screen.linkplus.otp

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.widget.ScangoLinkPlusOtpWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.otp.widget.ScangoLinkPlusOtpWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
class ScangoLinkPlusOtpProvidesModule {


    @Provides
    @ActivityScope
    fun provideLinkPlusOtpViewModel(
        activity: ScangoLinkPlusOtpActivity,
        factory: ViewModelProvider.Factory
    ): ScangoLinkPlusOtpViewModel = ViewModelProviders.of(activity, factory).get()

}

@Module
abstract class ScangoLinkPlusOtpBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideScangoLinkPlusOtpWidget(widget: ScangoLinkPlusOtpWidgetImpl): ScangoLinkPlusOtpWidget

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoLinkPlusOtpActivity): Activity

}