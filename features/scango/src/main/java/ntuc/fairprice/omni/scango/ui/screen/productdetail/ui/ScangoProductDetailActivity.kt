package ntuc.fairprice.omni.scango.ui.screen.productdetail.ui

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.EpoxyVisibilityTracker
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_toolbar.*
import kotlinx.android.synthetic.main.scango_activity_product_detail.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.R as RSG
import thor.zopsmart.com.thor.R as RFP
import thor.zopsmart.com.thor.base.BaseActivity
import thor.zopsmart.com.thor.base.customview.edittext.QuantitySelectListener
import thor.zopsmart.com.thor.base.exceptions.StoreNotSelectedException
import ntuc.fairprice.omni.scango.databinding.ScangoActivityProductDetailBinding
import ntuc.fairprice.omni.scango.epoxy.*
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivity
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.epoxy.IncrementFail
import ntuc.fairprice.omni.scango.ui.screen.productdetail.viewmodel.*
import thor.zopsmart.com.thor.analytics.FPScreen
import thor.zopsmart.com.thor.epoxy.*
import thor.zopsmart.com.thor.features.shoppinglist.ui.CreateShoppingListBottomFragment
import thor.zopsmart.com.thor.features.shoppinglist.ui.CreateShoppingListBottomFragmentInterface
import thor.zopsmart.com.thor.features.shoppinglist.ui.ShoppingListBottomFragment
import thor.zopsmart.com.thor.features.shoppinglist.ui.ShoppingListBottomFragmentInterface
import thor.zopsmart.com.thor.features.widget.productdetails.ui.ProductImageViewer
import thor.zopsmart.com.thor.model.Item
import thor.zopsmart.com.thor.model.Offer
import thor.zopsmart.com.thor.model.toCartItem
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem
import thor.zopsmart.com.thor.repository.db.room.entity.ShoppingList
import javax.inject.Inject

@Suppress("SENSELESS_COMPARISON")
class ScangoProductDetailActivity : BaseActivity(), HasSupportFragmentInjector, FPAbstractDelegates, SGProductDelegate,
    SGProductDetailDelegate, FPProductDelegate {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override val layout = RSG.layout.scango_activity_product_detail

    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog

    lateinit var binding: ScangoActivityProductDetailBinding
    lateinit var item: Item
    lateinit var favItem: MenuItem
    lateinit var cartIcon: MenuItem

    val quantitySelectListener: QuantitySelectListener = object : QuantitySelectListener {
        override fun onQuantitySelected(quantity: Int) {
            productDetailViewModel.setQuantity(quantity, item.toCartItem())
        }
    }

    private val productDetailViewModel: ScangoProductDetailViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoProductDetailViewModel::class.java)
    }

    companion object {
        fun getIntent(context: Context, itemUrl: String, productId: Long): Intent {
            return Intent(context, ScangoProductDetailActivity::class.java)
                .putExtra(Constants.EXTRA_PRODUCT_DETAIL_URL, itemUrl)
                .putExtra(Constants.EXTRA_PRODUCT_DETAIL_ID, productId)
        }
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, RSG.layout.scango_activity_product_detail, null)

        if (intent.hasExtra(Constants.EXTRA_PRODUCT_DETAIL_ID))
            productDetailViewModel.productId.value =
                intent.getLongExtra(Constants.EXTRA_PRODUCT_DETAIL_ID, 0)

        // Extract the item Url from the intent and load the product details. If it is not present, finish the activity
        if (intent.hasExtra(Constants.EXTRA_PRODUCT_DETAIL_URL))
            renderViews(intent.getStringExtra(Constants.EXTRA_PRODUCT_DETAIL_URL))
        else
            finish()

        tv_edit_cart?.setOnClickListener { finish() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(RFP.menu.menu_product_detail, menu)
        menu?.let {
            favItem = menu.findItem(RFP.id.menu_fav)
            cartIcon = menu.findItem(RFP.id.menu_cart)

            toggleCartIcon(productDetailViewModel.isCartEmpty())
        }
        return true
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {

        when (menuItem.itemId) {
            RFP.id.menu_cart -> navigateToCart()
            RFP.id.menu_share -> shareItem()
            RFP.id.menu_fav -> addToShoppingList()
        }

        return super.onOptionsItemSelected(menuItem)
    }

    private fun navigateToCart() {
        startActivity(Intent(application.baseContext,ScangoCartActivity::class.java))
        finish()
    }

    override fun addToShoppingList(item: Item) {
        openShoppingListBottomSheet(item, FPScreen.PRODUCT_DETAIL)
        //TODO TUAN
    }

    // FPDelegate overrides
    override fun loadMoreProduct(sgProductGrid: SGProductGrid) {
        // TODO enable only for the last component if needed?
        productDetailViewModel.loadProduct(sgProductGrid)
    }

    override fun onAddToCart(cartItem: CartItem) {
        productDetailViewModel.addToCart(cartItem)
    }

    override fun onRemoveFromCart(cartItem: CartItem) {
        productDetailViewModel.removeFromCart(cartItem)
    }

    override fun onProductClick(item: Item) {
        val intent = Intent(mContext, ScangoProductDetailActivity::class.java)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_URL, item.url)
        intent.putExtra(Constants.EXTRA_PRODUCT_DETAIL_ID, item.id)
        changePage(intent)
    }

//    override fun onViewAllClick(associatedData: JSONObject, filterData: JSONObject) {
//        openProductListingPage(associatedData, filterData)
//    }

    override fun onQuantitySet(quantity: Int, cartItem: CartItem) {
        productDetailViewModel.setQuantity(quantity, cartItem)
    }

    @Suppress("UNCHECKED_CAST")
    private fun renderViews(itemUrl: String) {
//        // Set up toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
//            setHomeAsUpIndicator(RSG.drawable.sg_ic_close_blue)
        }

        // Set data in layout binding
        binding.vm = productDetailViewModel
        binding.context = this
        binding.lifecycleOwner = this

        // Set epoxy recycler view
        val gridLayoutManager = GridLayoutManager(mContext, 30)
        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(rv_epoxy_product)
        rv_epoxy_product.layoutManager = gridLayoutManager
        rv_epoxy_product.buildModelsWith {
            it.spanCount = 30
            productDetailViewModel.pageData.value?.forEachIndexed { index, component ->
                (component as? SGComponent<FPDelegate>)
                    ?.render(this@ScangoProductDetailActivity)
                    ?.forEach { model ->
                        model.addTo(it)
                    }
                if (component is SGProductDetail) {
                    updateItem(component.item)
                }

                if (component is SGOfferDetailPDP) {
                    if (component.offerDetail == null)
                        productDetailViewModel.loadOfferDetail(index, component.offer)
                }
            }
        }

        // Observer for page data
        productDetailViewModel.pageData.observe(this, Observer {
            if (it != null) {
                rv_epoxy_product.requestModelBuild()
            }
        })

        productDetailViewModel.cartChange().observe(this,
            Observer {
                toggleCartIcon(it.isNullOrEmpty())
            }
        )

        productDetailViewModel.incrementStatus.observe(this, Observer {
            when (it) {
                is IncrementFail -> {
                    if (it.e is StoreNotSelectedException) {
                        gotoDeliverySelection()
                        finish()
                    }
                }
            }
        })

        productDetailViewModel.status.observe(this, Observer {

            when (it) {
                is ProductDetailLoading -> scanGoProgressDialog.show()
                is ProductDetailLoadSuccess -> {
                    scanGoProgressDialog.dismiss()
                }
                is ProductDetailLoadGeneralFail -> {
                    scanGoProgressDialog.dismiss()
                    scangoGeneralErrorDialog.apply {
                        onActionClick = {
                            productDetailViewModel.loadProductDetail(itemUrl)
                        }
                        show()
                    }
                }
                is ProductDetailLoadNetworkFail -> {
                    scanGoProgressDialog.dismiss()
                    scangoNetworkErrorDialog.apply {
                        onActionClick = {
                            productDetailViewModel.loadProductDetail(itemUrl)
                        }
                        show()
                    }
                }
            }
        })

        // Load the product details
        productDetailViewModel.loadProductDetail(itemUrl)

        productDetailViewModel.loadWishlistedItems()
    }

    private fun toggleCartIcon(isCartEmpty: Boolean) {
        if (::cartIcon.isInitialized)
            cartIcon.setIcon(if (isCartEmpty) RFP.drawable.ic_cart_inactive_no_mask else RFP.drawable.ic_cart_active_nomask)
    }

    private fun updateItem(item: Item) {
        this.item = item
        binding.item = item.toCartItem()
        toggleFavIcon()
    }

    private fun toggleFavIcon() {
        if (::favItem.isInitialized)
            favItem.setIcon(if (item.isShoppingListed) RFP.drawable.ic_favourite_active else RFP.drawable.ic_favourite_inactive)
    }

    private fun addToShoppingList() {
        if (::item.isInitialized)
            openShoppingListBottomSheet(item, FPScreen.PRODUCT_DETAIL)
    }

    private fun shareItem() {
        if (!::item.isInitialized)
            return

        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
        } else {
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
        }
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, item.name)
        // TODO Share the url with proper formatting. Currently sending the website url.
        shareIntent.putExtra(Intent.EXTRA_TEXT, "http://thor.zopsmart.com/product/${item.url}")

//        trackShareEvent(item.toCartItem())

        startActivity(Intent.createChooser(shareIntent, "Share ${item.name}"))
    }

    override fun onImageClick(item: Item, pos: Int) {
        item.images?.let {
            ProductImageViewer.newInstance(it, pos).showNow(supportFragmentManager, "image_viewer")
        }
    }

    override fun onOfferSummaryClick(index: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun viewAllOfferProducts(offer: Offer) {

    }

    override fun viewFreeGift(itemId: Long) {

    }

    private fun openShoppingListBottomSheet(item: Item, from: FPScreen) {
        if (productDetailViewModel.isWishListsEmpty) {
            openCreateShoppingListBottomSheet(item, mutableListOf(), from)
        } else {
            ShoppingListBottomFragment.openShoppingListBottomSheet(
                item = item,
                addAction = { list ->
                    productDetailViewModel.addToShoppingList(item, list)
                },
                createAction = { list ->
                    openCreateShoppingListBottomSheet(item, list, from)
                },
                fragmentManager = supportFragmentManager
            )
        }
    }

    private fun openShoppingListBottomSheet(item: Item) {
        val shoppingListBottomFragment =
            ShoppingListBottomFragment.getInstance(item, object : ShoppingListBottomFragmentInterface {
                //FelixV: FPOn updated this interface, but we have not called this function yet
                //So I just comment the old implementation
                override fun addToList(shoppingLists: List<ShoppingList>) {

                }

                override fun createNewList(shoppingLists: List<ShoppingList>) {

                }
            })

        shoppingListBottomFragment.show(supportFragmentManager, "ShoppingListBottomFragment")
    }

    private fun openCreateShoppingListBottomSheet() {
        val createShoppingListBottomFragment = CreateShoppingListBottomFragment.getInstance(object :
            CreateShoppingListBottomFragmentInterface {
            override fun createList(name: String) {
                productDetailViewModel.createNewShoppingList(name)
            }

        })
        createShoppingListBottomFragment.show(supportFragmentManager, "CreateShoppingListBottomFragment")
    }

    private fun openCreateShoppingListBottomSheet(item: Item, list: List<ShoppingList>, from: FPScreen) {
        CreateShoppingListBottomFragment.openCreateListBottomFragment(
            createAction = {
                productDetailViewModel.createNewShoppingList(it, item, list.toMutableList(), from)
            },
            fragmentManager = supportFragmentManager
        )
    }
}