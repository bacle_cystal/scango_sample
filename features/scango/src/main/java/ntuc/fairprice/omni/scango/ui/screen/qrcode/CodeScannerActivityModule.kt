package ntuc.fairprice.omni.scango.ui.screen.qrcode

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.dialog.ScangoCartFullErrorDialog
import ntuc.fairprice.omni.scango.ui.screen.qrcode.scannerwidget.ScannerWidget
import ntuc.fairprice.omni.scango.ui.screen.qrcode.scannerwidget.ScannerWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class  CodeScannerActivityBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: CodeScannerActivity): Activity

    @Binds
    @ActivityScope
    abstract fun provideScannerWidget(widget: ScannerWidgetImpl): ScannerWidget
}


@Module
class CodeScannerActivityProvidesModule {

    @Provides
    @ActivityScope
    fun provideCodeScannerActivityViewModel(activity: CodeScannerActivity, factory: ViewModelProvider.Factory): CodeScannerActivityViewModel =
        ViewModelProviders.of(activity, factory).get(CodeScannerActivityViewModel::class.java)

    @Provides
    @ActivityScope
    fun provideScangoBasketFullErrorDialog(activity: CodeScannerActivity): ScangoCartFullErrorDialog = ScangoCartFullErrorDialog(activity)
}
