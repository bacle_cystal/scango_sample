package ntuc.fairprice.omni.scango.repository.db.room.entity

import android.os.Parcel
import android.os.Parcelable
import androidx.room.TypeConverters
//import kotlinx.android.parcel.Parcelize
//import kotlinx.android.parcel.RawValue
import org.json.JSONObject
import thor.zopsmart.com.thor.features.widget.categorycollection.data.Category
import thor.zopsmart.com.thor.repository.db.room.typeconverter.Converters
import thor.zopsmart.com.thor.repository.db.room.entity.CartItem

//@Parcelize
data class ScangoItem(
    var id: Long,
    var itemId: Long,
    var name: String,
    var fullName: String,
    var hasVariants: Boolean?,
    var item_type: ItemType?,
    var images: List<String>?,
    var url: String?,
    var mrp: Double,
    var discount: Double,
    var sellingPrice: Double,
    var productId: Int,
    var description: String?,
    var brand: String?,
//    var categories: @RawValue List<Category>,
//    var data: @RawValue JSONObject?,
//    var parentData: @RawValue JSONObject?,
    var categories: List<Category>,
    var data:  JSONObject?,
    var parentData: JSONObject?,
    var stock: Int,
    var quantity: Int = 0,
    var selectedVariantId: Long = 0,
    var variants: List<ScangoItem>? = mutableListOf(),
    var isShoppingListed: Boolean = false
) : Parcelable {
    @field:TypeConverters(Converters::class)
    var parentProduct: ScangoItem? = null

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        TODO("item_type"),
        parcel.createStringArrayList(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        TODO("categories"),
        TODO("data"),
        TODO("parentData"),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.createTypedArrayList(CREATOR),
        parcel.readByte() != 0.toByte()
    ) {
        parentProduct = parcel.readParcelable(ScangoItem::class.java.classLoader)
    }

    fun getImage(): String? {
        return images?.let {
            if (it.isNotEmpty())
                it[0]
            else
                null
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeLong(itemId)
        parcel.writeString(name)
        parcel.writeString(fullName)
        parcel.writeValue(hasVariants)
        parcel.writeStringList(images)
        parcel.writeString(url)
        parcel.writeDouble(mrp)
        parcel.writeDouble(discount)
        parcel.writeDouble(sellingPrice)
        parcel.writeInt(productId)
        parcel.writeString(description)
        parcel.writeString(brand)
        parcel.writeInt(stock)
        parcel.writeInt(quantity)
        parcel.writeLong(selectedVariantId)
        parcel.writeTypedList(variants)
        parcel.writeByte(if (isShoppingListed) 1 else 0)
        parcel.writeParcelable(parentProduct, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ScangoItem> {
        override fun createFromParcel(parcel: Parcel): ScangoItem {
            return ScangoItem(parcel)
        }

        override fun newArray(size: Int): Array<ScangoItem?> {
            return arrayOfNulls(size)
        }
    }
}

enum class ItemType(val code: String) {
    PRODUCT("PRODUCT"), VARIANT("VARIANT")
}

fun ScangoItem.toCartItem(): CartItem =
    CartItem(
        id,
        itemId,
        quantity,
        stock,
        name,
        fullName,
        images,
        url,
        mrp,
        discount,
        sellingPrice,
        brand,
        getImage()
    )


fun ScangoItem.hasStockLeft(): Boolean {
    return stock > quantity
}
