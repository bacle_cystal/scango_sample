package ntuc.fairprice.omni.scango.repository.db.room.entity.orders

import ntuc.fairprice.omni.scango.Constants.UOM_KG
import ntuc.fairprice.omni.scango.repository.db.room.entity.*
import ntuc.fairprice.omni.scango.utils.round
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.forEach
import java.text.SimpleDateFormat
import java.util.*

class ScangoOrderDetail(private val orderData: ScangoOrderDetailData.Data.Order) {

    var orderPlacedDate: Date? = null
    var orderCompletedDate: Date? = null
    var preferredDate: Date? = null
    var slotStartTime: Date? = null
    var slotEndTime: Date? = null
    var orderNumber: String? = null
    var deliveryMode: DeliveryMode = DeliveryMode.UNKNOWN
    var orderStatus: OrderStatus = OrderStatus.UNKNOWN
    var subtotal: Double? = null
    var invoiceAmount: Double? = null
    var totalAmount: Double? = null
    var discountAmount: String? = null
    var cardType: String? = null
    var cardNumber: String? = null
    var coupons: List<ScangoCoupon> = emptyList()
    var offers: List<ScangoOffer> = emptyList()
    var cartItems: List<ScangoCartItem> = emptyList()
    var storeAddress: String? = null
    var edited: Boolean = false
    var verificationStatus: VerificationStatus = VerificationStatus.VerificationStatusUnVerified

    var couponDiscount: Double = 0.0

    var linkPointReceipt: ScangoLinkpointReceipt? = null

    //Add param for invoice
    var store: ScangoOrderDetailData.Data.Order.Store? = null
    var customer: ScangoOrderDetailData.Data.Order.Customer? = null
    var gst7percent: Double? = null
    var listReturn: List<ScangoOrderDetailData.Data.Order.Return> = emptyList()

    init {
        // TODO improve this date parsing
        val ORDER_PLACED_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
        orderPlacedDate = orderData.createdAt?.let { ORDER_PLACED_DATE_FORMAT.parse(it) }
        orderCompletedDate = orderData.completedAt?.let { ORDER_PLACED_DATE_FORMAT.parse(it) }

        val PREFERRED_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        preferredDate = orderData.preferredDate?.let { PREFERRED_DATE_FORMAT.parse(it) }

        val TIMESLOT_FORMAT = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)
        slotStartTime = orderData.slotStartTime?.let { TIMESLOT_FORMAT.parse(it) }

        slotEndTime = orderData.slotEndTime?.let { TIMESLOT_FORMAT.parse(it) }


        orderNumber = orderData.referenceNumber
        deliveryMode = DeliveryMode.fromValue(orderData.type?.name)
        orderStatus = ApiOrderStatus.fromValue(orderData.status).orderStatus
        subtotal = orderData.invoiceAmount?.toDouble() ?: 0.0
        invoiceAmount = orderData.invoiceAmount?.toDouble() ?: 0.0
        totalAmount = orderData.payment?.let { paymentList ->
            paymentList.find { "ONLINE".equals(it.mode, true) }?.amount?.toDouble() ?: 0.0
        } ?: 0.0
        discountAmount = orderData.discount
        edited = orderData.edited

        offers = orderData.offers?.toScangoOffer() ?: emptyList()

        listReturn = orderData.returns ?: emptyList()

        orderData.payment?.firstOrNull()?.let { payment ->
            cardType = payment.metaData?.cardType
            cardNumber = payment.metaData?.cardNumber
        }

        coupons = orderData.couponDiscounts.map {
            ScangoCoupon(it.code, "", null, finalDiscount = it.discount, couponType = it.type)
        }
        val sngCartString = JSONObject(orderData.metaData.toString()).optString("sngCart")
        var sngCart = JSONArray()
        try {
            if (sngCartString != null) {
                sngCart = JSONArray(sngCartString)
            }
        } catch (e: Exception) {
        }

        cartItems = orderData.items.map {
            val quantity = it.orderDetails?.orderedQuantity?.toDouble() ?: 0.0
            val mrp = (it.orderDetails?.mrp?.toDouble() ?: 0.toDouble())
            val discount = (it.orderDetails?.discount?.toDouble() ?: 0.toDouble())
            couponDiscount = orderData.couponDiscount?.toDoubleOrNull() ?: 0.0
            val sp = (mrp - discount) * quantity
            val refundQuantity =
                it.orderDetails?.orderedQuantity?.toDouble()?.minus(
                    it.orderDetails?.deliveredQuantity?.toDouble() ?: 0.0
                ) ?: 0.0

            val mrpRefund = (it.orderDetails?.mrp?.toDouble() ?: 0.toDouble())
            val discountRefund = (it.orderDetails?.discount?.toDouble() ?: 0.toDouble())
            val refundPrice = (mrpRefund.minus(discountRefund).times(refundQuantity))

            val displayRefundQuantity = getDisplayRefundQuantity(sngCart, it)
            ScangoCartItem(
                id = it.id.toLong(),
                itemId = it.id.toLong(),
                quantity = getQuantity(sngCart, it),
                name = it.name ?: "",
                fullName = it.name ?: "",
                mrp = mrp,
                discount = discount,
                brand = it.brand?.name,
                url = it.slug,
                images = it.images,
                image = it.images?.firstOrNull(),
                sellingPrice = sp,
                soldByWeight = it.soldByWeight ?: 0,
                offer = it.offer?.toOffer(),
                _unitOfMeasurement = it.metaData?.unitOfMeasurement,
                refundQuantity = displayRefundQuantity,
                refundPrice = refundPrice,
                weight = getWeight(sngCart, it)
            )
//                .apply {
//                offer?.forEach { offerItem ->
//                    val offerDiscount = offerItem.offerRule?.discountType?.getDiscountAmount(mrp, discount) ?: discount
//                    offerItem.mrp = mrp
//                    offerItem.discount = offerDiscount
//                }
//            }
        }

        storeAddress = orderData.store?.address

        store = orderData.store
        customer = orderData.customer
        gst7percent = invoiceAmount?.div(107)?.times(7) ?: 0.0

        verificationStatus = orderData.verificationStatus

        val linkPointRedemption = orderData.linkPoint?.redemption
        val linkPointAward = orderData.linkPoint?.award

        linkPointReceipt = ScangoLinkpointReceipt(
            linkPointRedemption?.pointAmount ?: 0.0,
            linkPointRedemption?.dollarAmount ?: 0.0,
            linkPointAward?.pointAmount ?: 0.0
        )
    }

    private fun getQuantity(sngCart: JSONArray, item: ScangoOrderDetailData.Data.Order.Item): Int {
        var quantity = 0
        if (item.soldByWeight == 1) {
            sngCart.forEach {
                if (item.id.toLong() == it.optLong("id")) {
                    quantity = it.optInt("c")
                }
            }
        } else {
            quantity = item.orderDetails?.orderedQuantity?.toDouble()?.toInt() ?: 0
        }
        return quantity
    }

    private fun getWeight(sngCart: JSONArray, item: ScangoOrderDetailData.Data.Order.Item): Double {
        var weight = 0.0
        if (item.soldByWeight == 1) {
            sngCart.forEach {
                if (item.id.toLong() == it.optLong("id")) {
                    weight = it.optDouble("q")
                }
            }
        } else {
            weight = item.orderDetails?.deliveredQuantity?.toString()?.toDouble() ?: 0.0
        }
        return weight
    }


    private fun getDisplayRefundQuantity(sngCart: JSONArray, item: ScangoOrderDetailData.Data.Order.Item): Double {
        var displayRefundQuantity = 0.0
        if (item.soldByWeight == 1) {
            sngCart.forEach {
                if (item.id.toLong() == it.optLong("id")) {
                    if (!UOM_KG.equals(item.metaData?.unitOfMeasurement, false)) {
                        val q = it.optDouble("q")
                        val c = it.optInt("c")
                        val refundQuantity = item.orderDetails?.orderedQuantity?.toDouble()?.minus(
                            item.orderDetails?.deliveredQuantity?.toDouble() ?: 0.0
                        ) ?: 0.0

                        val piecePerPack = q.div(c.toDouble())
                        displayRefundQuantity = refundQuantity / piecePerPack
                    } else {
                        displayRefundQuantity = item.orderDetails?.orderedQuantity?.toDouble()?.minus(
                            item.orderDetails?.deliveredQuantity?.toDouble() ?: 0.0
                        ) ?: 0.0
                    }
                }
            }
        } else {
            displayRefundQuantity = item.orderDetails?.orderedQuantity?.toDouble()?.minus(
                item.orderDetails?.deliveredQuantity?.toDouble() ?: 0.0
            ) ?: 0.0
        }
        return displayRefundQuantity.round(3)
    }


//    private fun checkIfOrderedQuantityIsWeight(deliveredQuantity: Double?): Double {
//        return if (deliveredQuantity == null || deliveredQuantity % 1.0 == 0.0) {
//            0.0
//        } else {
//            deliveredQuantity
//        }
//    }
}

enum class DeliveryMode(val shortCode: String, var status: String? = "") {
    UNKNOWN(""),
    DELIVERY("HD"),
    PICKUP("CC");

    companion object {
        fun fromValue(value: String?): DeliveryMode {
            if (value.isNullOrEmpty()) {
                return UNKNOWN
            }
            try {
                return DeliveryMode.valueOf(value)
            } catch (e: Throwable) {
                return UNKNOWN.apply { status = value }
            }
        }
    }
}

enum class DeliveryStatus {
    DEFAULT,
    FINISHED,
    DELAYED,
    ONGOING;
}


enum class ApiOrderStatus(val orderStatus: OrderStatus) {
    UNKNOWN(OrderStatus.UNKNOWN),
    PENDING(OrderStatus.PENDING),
    CHECKING(OrderStatus.PICKING),
    CHECKED(OrderStatus.PICKING),
    PICKING(OrderStatus.PICKING),
    PICKED(OrderStatus.PICKING),
    PACKING(OrderStatus.PICKING),
    PACKED(OrderStatus.PACKED),
    DELIVERED(OrderStatus.PACKED),
    DISPATCHED(OrderStatus.DISPATCHED),
    UNDELIVERED(OrderStatus.UNDELIVERED),
    COMPLETED(OrderStatus.COMPLETED),
    CANCELLED(OrderStatus.CANCELLED);

    companion object {
        fun fromValue(value: String?): ApiOrderStatus {
            if (value.isNullOrEmpty()) {
                return UNKNOWN
            }
            return try {
                ApiOrderStatus.valueOf(value)
            } catch (e: Throwable) {
                UNKNOWN.apply { orderStatus.status = value }
            }
        }
    }
}

enum class OrderStatus(var status: String? = "") {
    UNKNOWN,
    PENDING,
    PICKING,
    PACKED,
    DISPATCHED,
    DELAYED, // TODO add api order mapping
    UNDELIVERED,
    COMPLETED,
    CANCELLED;

    fun toDeliveryStatus(currentOrderStatus: OrderStatus): DeliveryStatus {
        return when {
            this < currentOrderStatus -> DeliveryStatus.DEFAULT
            this > currentOrderStatus -> DeliveryStatus.FINISHED
            else -> DeliveryStatus.ONGOING
        }
    }
}
