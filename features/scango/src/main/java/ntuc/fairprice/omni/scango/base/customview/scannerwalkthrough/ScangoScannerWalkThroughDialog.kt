package ntuc.fairprice.omni.scango.base.customview.scannerwalkthrough

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.WindowManager
import android.widget.TextView
import android.widget.ViewFlipper
import ntuc.fairprice.omni.scango.R


class ScangoScannerWalkThroughDialog(context: Context) {

    private lateinit var dialog: Dialog

    var gotItLeftClickListener: () -> Unit = {}
    var gotItRightClickListener: () -> Unit = {}

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.scango_layout_scan_walkthrough, null, false)
        val gotItLeft = view.findViewById<TextView>(R.id.btn_scan_barcode_gotit_l)
        val gotItRight = view.findViewById<TextView>(R.id.btn_scan_barcode_gotit_r)
        gotItLeft.setOnClickListener {
            (view as ViewFlipper).displayedChild = 1
            gotItLeftClickListener.invoke()
        }
        gotItRight.setOnClickListener {
            gotItRightClickListener.invoke()
            dialog.dismiss()
        }

        dialog = AlertDialog.Builder(context).setView(view).setCancelable(false).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        dialog.window?.attributes?.gravity = Gravity.BOTTOM
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }

    val isShown: Boolean
        get() = dialog.isShowing
}