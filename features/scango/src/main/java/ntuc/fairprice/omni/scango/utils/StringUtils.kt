package ntuc.fairprice.omni.scango.utils

import ntuc.fairprice.omni.scango.utils.HashUtils.hashString

fun String.removeSpaces() = this.replace(" ", "")

fun String.toSHA512() = hashString("SHA-512", this)
