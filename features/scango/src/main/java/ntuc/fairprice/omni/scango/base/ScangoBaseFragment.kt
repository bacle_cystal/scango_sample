package ntuc.fairprice.omni.scango.base

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import ntuc.fairprice.omni.scango.BR
import ntuc.fairprice.omni.scango.Constants.packageName
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.interfaces.ScangoAppView
import thor.zopsmart.com.thor.interfaces.AppView

abstract class ScangoBaseFragment<ViewBinding : ViewDataBinding, ViewModel : androidx.lifecycle.ViewModel> :
    ScangoAppView, Fragment() {
    lateinit var viewBinding: ViewBinding

    lateinit var mainActivity: ScangoBaseActivity

    override lateinit var mContext: Context

    abstract val viewModel: ViewModel

    @get:LayoutRes
    abstract val layoutId: Int

    var mAlertDialog: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (activity is ScangoBaseActivity) {
            mainActivity = activity as ScangoBaseActivity
        }
        mContext = context ?: throw Exception("Cannot initalise context")
        viewBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinding.apply {
            setVariable(BR.viewModel, viewModel)
            root.isClickable = true
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }
    }

    open fun handleShowLoading(isLoading: Boolean) {
        if (isLoading) showLoading() else hideLoading()
    }

    fun showLoading() {
        hideLoading()
        mAlertDialog?.show()
    }

    fun hideLoading() {
        if (mAlertDialog != null && mAlertDialog!!.isShowing) {
            mAlertDialog?.cancel()
        }
    }

    /**
     * fragment transaction
     */

    fun findFragment(TAG: String): Fragment? {
        return activity?.supportFragmentManager?.findFragmentByTag(TAG)
    }

    fun findChildFragment(parentFragment: Fragment = this, TAG: String): Fragment? {
        return parentFragment.childFragmentManager.findFragmentByTag(TAG)
    }

    fun findLastChildFragment(parentFragment: Fragment): Fragment {
        return parentFragment.childFragmentManager.fragments.last()
    }

    fun addFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            setTransactionAnimation(this, transit)
            add(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun replaceFragment(
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            setTransactionAnimation(this, transit)
            replace(R.id.container, fragment, TAG)
            commitTransaction(this, addToBackStack)
        }
    }

    fun replaceChildFragment(
        parentFragment: Fragment = this,
        containerViewId: Int,
        fragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        val transaction = parentFragment.childFragmentManager.beginTransaction().apply {
            setTransactionAnimation(this, transit)
            replace(containerViewId, fragment, TAG)
        }
        commitTransaction(transaction, addToBackStack)
    }

    fun addChildFragment(
        parentFragment: Fragment = this,
        containerViewId: Int,
        targetFragment: Fragment,
        TAG: String?,
        addToBackStack: Boolean = false,
        transit: TransitionFrom = TransitionFrom.RIGHT
    ) {
        val transaction = parentFragment.childFragmentManager.beginTransaction().apply {
            setTransactionAnimation(this, transit)
            add(containerViewId, targetFragment, TAG)
        }
        commitTransaction(transaction, addToBackStack)
    }

    fun setTransactionAnimation(
        fragmentTransaction: FragmentTransaction,
        transitionFrom: TransitionFrom
    ) {
        fragmentTransaction.apply {
            when (transitionFrom) {
                TransitionFrom.BOTTOM -> setCustomAnimations(
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom,
                    R.anim.enter_from_bottom,
                    R.anim.exit_to_bottom
                )
                TransitionFrom.RIGHT -> setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_right,
                    R.anim.enter_from_right,
                    R.anim.exit_to_right
                )
                TransitionFrom.NONE -> { // do nothing
                }
                else -> { // do nothing
                }
            }
        }
    }

    private fun commitTransaction(
        transaction: FragmentTransaction,
        addToBackStack: Boolean = false
    ) {
        if (addToBackStack) transaction.addToBackStack(null)
        transaction.commit()
    }

    fun popChildFragment(parentFragment: Fragment = this) {
        parentFragment.childFragmentManager.popBackStack()
    }

    open fun onBack(): Boolean {
        activity?.onBackPressed()
        return false
    }

    fun popBackFragmentToPosition(toFragmentPosition: Int) {
        activity?.supportFragmentManager?.backStackEntryCount?.let {
            for (i in toFragmentPosition until it) {
                fragmentManager?.popBackStackImmediate()
            }
        }
    }

    enum class TransitionFrom {
        RIGHT, BOTTOM, LEFT, TOP, NONE
    }
}