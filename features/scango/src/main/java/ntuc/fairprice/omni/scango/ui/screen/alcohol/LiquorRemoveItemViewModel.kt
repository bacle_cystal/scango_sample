package ntuc.fairprice.omni.scango.ui.screen.alcohol

import android.app.Application
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.scango.repository.db.room.entity.toLiquorItem
import ntuc.fairprice.omni.scango.repository.db.room.repo.ScangoCartRepository
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import ntuc.fairprice.omni.toggle.SingleLiveEvent
import javax.inject.Inject

class LiquorRemoveItemViewModel @Inject constructor(
    application: Application,
    val cartRepository: ScangoCartRepository
) : ScangoBaseViewModel(application) {
    val liquorsLiveData = MutableLiveData<List<LiquorItem>>()
    val removeCompleteEvent = SingleLiveEvent<Unit>()

    fun checkRemoveLiquorItem(cartViewModel: ScangoCartViewModel) {
        //Remove item liquor after 10h30
        val listLiquor =
            cartViewModel.getListTimeRestriction()

        liquorsLiveData.value = listLiquor?.map {
            it.toLiquorItem()
        }
        listLiquor?.forEach {
            cartRepository.delete(it)
        }
    }

    fun sendEventRemoveSuccess(){
        removeCompleteEvent.call()
    }
}
