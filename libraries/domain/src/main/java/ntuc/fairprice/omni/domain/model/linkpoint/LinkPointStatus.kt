package ntuc.fairprice.omni.domain.model.linkpoint

import thor.zopsmart.com.thor.base.extensions.double
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.safeJson
import thor.zopsmart.com.thor.repository.webservice.model.ApiResponse

data class LinkPointStatus(
    var linkStatus: Int = 0,
    var pointBalance: Double = 0.0,
    var dollarBalance: Double = 0.0
) {
    val isPartiallyLinked
        get() = linkStatus == 2

    val isFullyLinked
        get() = linkStatus == 1

    val isNotLinked
        get() = linkStatus == 0
}

fun ApiResponse.toLinkPointStatus(): LinkPointStatus {

    val data = this safeJson "data"
    val linkStatus = data int "linkStatus" ?: 0
    val pointBalance = data double "pointBalance" ?: 0.0
    val dollarBalance = data double "dollarBalance" ?: 0.0
    return LinkPointStatus(linkStatus, pointBalance, dollarBalance)
}