package ntuc.fairprice.omni.scango.utils

import android.content.Context
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by Binh.TH on 4/4/2019.
 */

fun loadJSONFromAsset(context: Context, filename: String): String {
    var json: String = ""
    try {
        val `is` = context.assets.open(filename)
        val size = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()
        json = String(buffer, Charset.forName("UTF-8"))
    } catch (ex: IOException) {
        ex.printStackTrace()
        return ""
    }
    return json
}
