package ntuc.fairprice.omni.scango.utils

import android.content.Context
import android.content.res.Resources
import android.graphics.Paint
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.scango_activity_toolbar_base.view.*
import kotlinx.android.synthetic.main.scango_label_edit_text.view.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.Constants.UOM_KG
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.customview.ScangoLabelEditText
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import thor.zopsmart.com.thor.base.configurations.Config
import thor.zopsmart.com.thor.base.extensions.image
import thor.zopsmart.com.thor.base.extensions.toFormattedString
import thor.zopsmart.com.thor.features.navigation.ui.NavigationItem
import thor.zopsmart.com.thor.model.Item
import java.text.DecimalFormat


fun View.hideKeyBoard(): Boolean =
    try {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) {
        false
    }


fun Fragment.hideKeyboard(): Boolean =
    try {
        val inputMethodManager =
            this.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
    } catch (ignored: RuntimeException) {
        false
    }

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    requestFocus()
    imm.showSoftInput(this, 0)
}


fun ImageView.loadUrl(url: String) {
    Glide.with(context).load(url).into(this)
}


@BindingAdapter("strikeThroughText")
fun TextView.strikeThroughText(text: String) {
    setText(text)
    paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

fun ConstraintSet.match(view: View, parentView: View) {
    this.connect(view.id, ConstraintSet.TOP, parentView.id, ConstraintSet.TOP)
    this.connect(view.id, ConstraintSet.START, parentView.id, ConstraintSet.START)
    this.connect(view.id, ConstraintSet.END, parentView.id, ConstraintSet.END)
    this.connect(view.id, ConstraintSet.BOTTOM, parentView.id, ConstraintSet.BOTTOM)
}

@BindingAdapter("favItem")
fun ImageView.favItem(item: Item) {
    image(
        if (item.isShoppingListed)
            thor.zopsmart.com.thor.R.drawable.ic_favourite_active
        else
            thor.zopsmart.com.thor.R.drawable.ic_favourite_inactive
    )
}

/**
 * Allows to format two strings differently in the same text view.
 * In this case, it formats the name and full name of the item differently and prepends the brand to it if it exists.
 * */
@BindingAdapter("formatCartName")
fun TextView.formatCartItemName(item: ScangoCartItem) {

    text = with(item) {

        item.name
        /**
         * Need to indentify why this below code is used
         */
//        // A different color is set to the part of full name which is after name.
//        val attributeStr: Spannable = SpannableString(fullName)
//        if (name != fullName) {
//            attributeStr.setSpan(
//                ForegroundColorSpan(ContextCompat.getColor(context, R.color.cart_name_format)),
//                fullName.indexOf(name),
//                fullName.length,
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//            )
//        }
//
//        // Brand is prepended to the formatted name if it exists and the final formatted value is returned.
//        brand?.let {
//            val brandString: Spannable = SpannableString("$it ")
//            TextUtils.concat(brandString, attributeStr)
//        } ?: attributeStr
    }
}

fun Double.toFormattedString(): String {
    val roundedValue = this.round()
    val pattern = DecimalFormat("0.00")
    return pattern.format(roundedValue)
}

fun List<ScangoLabelEditText>.clearError() {
    forEach {
        it.error = ""
        it.isErrorEnabled = false
    }
}

@BindingAdapter("inputFormat")
fun ScangoLabelEditText.setInputType(type: String) {
    when (type.toLowerCase()) {
        "card" -> setCardInput()
        "cvv" -> setNumberPasswordInput(4)
        "date" -> setDateInput()
        "number" -> setNumberInput()
        "pincode" -> setPincodeInput(6)
        "staffid" -> setPincodeInput(20)
    }
}


/**
 * Set text to label edit text
 * */
@BindingAdapter("finalText")
fun ScangoLabelEditText.setFinalText(initialText: String?) =
    initialText?.let {
        setText(initialText, custom_edit_text.isEnabled, cursorEnd = true)
    }

fun List<ScangoLabelEditText>.textListener(textChangeAction: (String) -> Unit) {
    forEach {
        it.addTextListener(textChangeAction)
    }
}

fun List<ScangoLabelEditText>.containsEmpty(): Boolean {
    forEach {
        if (it.isEmpty())
            return true
    }
    return false
}

fun List<ScangoLabelEditText>.clearTexts() {
    forEach {
        it.clearText()
        it.setDefaultState()
    }
}

fun List<ScangoLabelEditText>.getEmpty(): List<ScangoLabelEditText> {
    val emptyFields = mutableListOf<ScangoLabelEditText>()
    forEach {
        if (it.isEmpty()) emptyFields.add(it)
    }
    return emptyFields
}

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()


@Suppress("DEPRECATION")
fun TextView.setTextAppearanceV2(context: Context, resId: Int) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
        setTextAppearance(context, resId)
    } else {
        setTextAppearance(resId)
    }
}

@BindingAdapter("app:textColor")
fun TextView.setTextColor(@ColorRes color: Int) {
    if (color != 0) {
        setTextColor(ContextCompat.getColor(context, color))
    }
}

/**
 * Automatically add currency symbol to the start of the double amount
 */
fun TextView.setCurrencyTextNegative(amount: Double?) {
    if (amount != 0.0) {
        text = "-" + Config.CURRENCY + (amount?.toFormattedString() ?: "0.00")
    } else {
        text = Config.CURRENCY + (amount?.toFormattedString() ?: "0.00")
    }
}

fun LinearLayout.appendNavItem(@DrawableRes drawableIcon: Int = 0, name: String): NavigationItem {
    return NavigationItem(context).apply {
        setPadding(dpToPx(context, 10))
        setText(name)
        setIcon(drawableIcon)
    }.also {
        this.addView(it)
    }
}

fun Double.toWeightString(): String = if (this >= 1.00) "$this kg" else "${(this * 1000).toInt()} g"


/**
 * Automatically add currency symbol to the start of the double amount
 */
@BindingAdapter("returnItemText")
fun TextView.setReturnItemText(item: ScangoCartItem) {
    if (item.isSoldByWeight()) {
        if (UOM_KG.equals(item.unitOfMeasurement, false)) {
            text = resources.getString(
                R.string.txt_receipt_weighted_items_refund,
                item.refundQuantity?.removeRedundant0() ?: 0.0
            )
        } else {
            text = resources.getString(
                R.string.txt_receipt_peices_items_refund,
                item.refundQuantity?.removeRedundant0() ?: 0.0
            )
        }
    } else {
        val quantity = item.refundQuantity ?: 0.0
        text = if (quantity > 0.0) {
            resources.getString(R.string.txt_receipt_item_refund, quantity.removeRedundant0())
        } else {
            resources.getString(R.string.txt_receipt_items_refund, quantity.removeRedundant0())
        }
    }

}
