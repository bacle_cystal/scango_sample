package ntuc.fairprice.omni.scango.utils

import ntuc.fairprice.omni.scango.mock
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.mockito.Mockito
import thor.zopsmart.com.thor.repository.db.room.entity.Customer

import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository

class AccountUtilsKtTest {

    var accountRepository: AccountRepository = mock()

    @Test
    fun isLoggedIn_loggedIn() {
        Mockito.`when`(accountRepository.getCustomer()).thenReturn(Customer(0,null, null,
            null, null, null, null, null, "", "", ""))
        assertTrue(accountRepository.isLoggedIn)
    }

    @Test
    fun isLoggedIn_notLoggedIn() {
        Mockito.`when`(accountRepository.getCustomer()).thenReturn(null)
        assertFalse(accountRepository.isLoggedIn)
    }
}