package ntuc.fairprice.omni.scango.ui.screen.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.databinding.ScangoItemStoreListBinding

class ScangoAddressAdapter : RecyclerView.Adapter<ScangoAddressAdapter.AddressHolder>() {
    var items: List<StoreAddressItem> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val binding: ScangoItemStoreListBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.scango_item_store_list, parent, false)
        return AddressHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: AddressHolder, position: Int) = holder.bind(items[position])

    inner class AddressHolder(val binding: ScangoItemStoreListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: StoreAddressItem) {
            binding.apply {
                textItemStoreName.text = item.storeName
                textStoreAddress.text = item.address
                /*tagNew.visibility = when (item.isNew) {
                    true -> View.VISIBLE
                    false -> View.GONE
                }*/
            }.executePendingBindings()
        }
    }
}