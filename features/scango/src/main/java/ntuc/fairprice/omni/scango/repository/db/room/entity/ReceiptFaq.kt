package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity

@Entity
data class ReceiptFaq(
    var title: String?,
    var content: String?
)