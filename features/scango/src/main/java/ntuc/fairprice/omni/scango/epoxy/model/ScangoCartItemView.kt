package ntuc.fairprice.omni.scango.epoxy.model

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import ntuc.fairprice.omni.scango.base.customview.swipeview.SimpleSwipeListener
import ntuc.fairprice.omni.scango.base.customview.swipeview.SwipeLayout
import ntuc.fairprice.omni.scango.databinding.ScangoLayoutCartItemBinding
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.R as RSG

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoCartItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var scangoCartItem: ScangoCartItem? = null

    val binding: ScangoLayoutCartItemBinding

    var cardActionListener: KeyedListener<*, SimpleSwipeListener>? = null

    init {
        val inflater = LayoutInflater.from(context)
        binding = ScangoLayoutCartItemBinding.inflate(inflater, this, true)
    }

    @ModelProp(ModelProp.Option.NullOnRecycle)
    fun setKeyedOnClickListener(listener: KeyedListener<*, SimpleSwipeListener>?) {
        cardActionListener = listener
    }

    @ModelProp
    fun cartItem(cartItem: ScangoCartItem) {
        this.scangoCartItem = cartItem
        binding.cartItem = cartItem
        binding.swipeLayout.setOnActionsListener(object : SwipeLayout.SwipeActionsListener {
            override fun onClose() {
            }

            override fun onOpen(direction: Int, isContinuous: Boolean) {
                if (isContinuous) {
                    cardActionListener?.callback?.onOpen(direction, isContinuous, cartItem)
                }
            }
        })
        invalidate()
    }

    @ModelProp
    fun allowToRemove(allowToRemove: Boolean) {
        binding.allowToRemove = allowToRemove
        invalidate()
    }

    @ModelProp
    fun showAddRemove(showAddRemove: Boolean) {
        binding.showAddRemove = showAddRemove
        invalidate()
    }

    @ModelProp
    fun disableEdit(disableEdit: Boolean) {
        binding.disableEdit = disableEdit
        invalidate()
    }

    @CallbackProp
    fun addToCart(action: ((ScangoCartItem?) -> Unit)?) {
        binding.ibAdd.setOnClickListener {
            action.let {
                it?.invoke(scangoCartItem)
            }
        }
    }

    @CallbackProp
    fun removeFromCart(action: ((ScangoCartItem?, SwipeLayout, Boolean) -> Unit)?) {
        binding.ibRemove.setOnClickListener {
            action.let {
                it?.invoke(scangoCartItem, binding.swipeLayout, binding.swipeLayout.isClosed)
            }
        }
    }

    @CallbackProp
    fun deleteClick(action: ((ScangoCartItem?) -> Unit)?) {
        binding.ivDelete.setOnClickListener {
            action.let {
                it?.invoke(scangoCartItem)
            }
        }
    }

    @CallbackProp
    fun productImageClick(action: ((ScangoCartItem?) -> Unit)?) {
        binding.dragItem.setOnClickListener {
            action.let {
                it?.invoke(scangoCartItem)
            }
        }
    }
}