package ntuc.fairprice.omni.scango.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class ScangoBaseViewModel(application: Application) : AndroidViewModel(application) {

    val isLoading = MutableLiveData<Boolean>().apply { value = false }
    val errorMessage = MutableLiveData<String>()
    val error = MutableLiveData<Error>()

    protected fun launch(block: suspend () -> Unit): Job {
        showLoading()
        return viewModelScope.launch {
            block()
        }
    }

    open fun onSuccess() {
        isLoading.value = false
    }

    open fun showLoading() {
        isLoading.value = true
    }

    open fun onLoadFail(throwable: Throwable?) {
        //FelixV the logic here is wrong, but cause we have never used it and just to remind that we already had a param nam errorMessage
        // So I just comment it
//        try {
//            when (throwable.cause) {
//                is UnknownHostException -> {
//                    errorMessage.value = "No Internet Connection"
//                }
//                is SocketTimeoutException -> {
//                    errorMessage.value = "Connect timeout, please retry"
//                }
//                else -> {
//                    errorMessage.value = throwable.message
//                }
//            }
//        } catch (e: Exception) {
//            errorMessage.value = throwable.message
//        }
        isLoading.value = false
    }
}
