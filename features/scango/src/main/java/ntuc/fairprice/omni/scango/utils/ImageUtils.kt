package ntuc.fairprice.omni.scango.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.R as BR

@BindingAdapter("loadImageOptimize")
fun ImageView.loadImageOptimize(imageURL: String? = "") {
    Glide.with(context)
        .load(imageURL)
        .apply(
            RequestOptions().placeholder(R.drawable.ic_place_holder_vector)
                .error(R.drawable.ic_place_holder_vector).override(600, 600)
        )
        .into(this)
}

@BindingAdapter("loadCreditCard")
fun ImageView.setCreaditCardImage(cardType: String) {
    setImageResource(
        when(cardType) {
            "VISA" -> BR.drawable.ic_visa
            "MasterCard" -> BR.drawable.ic_master_card
            "AMEX" -> R.drawable.ic_amex
            else -> BR.drawable.ic_visa
        }
    )
}