package ntuc.fairprice.omni.scango.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import ntuc.fairprice.omni.scango.R


class DialogUtils {
    companion object {

        fun showCameraPermissionDialog(activity: Activity, cancelAction: () -> Unit = {}) =
            AlertDialog.Builder(activity)
                .setMessage(activity.getString(R.string.permission_allow_camera_message_))
                .setPositiveButton(activity.getString(R.string.permission_txt_go_to_settings)) { _, _ ->
                    activity.startActivity(Intent().apply {
                        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        data = Uri.fromParts("package", activity.packageName, null)
                    })
                }
                .setNegativeButton(activity.getString(R.string.permission_txt_cancel)) { _,_ ->
                    cancelAction.invoke()
                }
                .create()
                .show()
    }
}