package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoPaymentType
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartArray
import ntuc.fairprice.omni.scango.repository.webservice.model.*
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoOrderService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) :
    ScangoBaseService(scangoRequestProvider) {

    suspend fun checkout(
        couponCodes: List<String>,
        cartAmount: Double,
        cartArray: JSONArray,
        storeId: String
    ): Result<ApiResponse> {

        val params = JSONObject()

        // Add coupon codes
        val couponArray = JSONArray()
        couponCodes.forEach {
            couponArray.put(it)
        }
        params.put("couponCodes", couponArray)
        // Add cart items
        val cartObject = JSONObject()
        cartObject.put("items", cartArray)
        params.put("cart", cartObject)
        params.put("orderAmount", cartAmount)
        params.put("storeId", storeId)
        return apiCall(
            "/api/checkout",
            ScangoRequestProvider.Method.POST,
            params = params,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it
        }
    }

    suspend fun applyCoupon(
        couponCodes: List<String>,
        cartAmount: Double,
        cartArray: JSONArray,
        storeId: String
    ): Result<List<ScangoCoupon>> {
        return checkout(couponCodes, cartAmount, cartArray, storeId).parse {
            it.toAppliedCoupons()
        }
    }

    suspend fun placeOrder(
        sessionID: String,
        paymentType: ScangoPaymentType,
        customerId: Long,
        cartItems: List<ScangoCartItem>,
        preferredStoreId: Long,
        cardId: Long,
        coupons: List<ScangoCoupon>,
        completeDate: String,
        amount: Long,
        isActiveLinkPoint: Boolean,
        linkPointValue: Double,
        isActiveAgeVerify: Boolean,
        ageVerifyCode: String,
        metaData: JSONObject
    ): Result<ApiResponse> {
        val params = JSONObject()
        // TODO ADD COUPON CODE
        params.put("sessionID", sessionID)
        params.put("customerId", customerId)
        params.put("paymentMethod", paymentType.value)
        params.put("storeId", preferredStoreId)
        params.put("cart", cartItems.toCartArray())
        params.put("cardId", cardId)
        params.put("type", "OFFLINE")
        params.put("completedAt", completeDate)
        params.put("amount", amount.toString())
        params.put("metaData", metaData)

        if (coupons.isNotEmpty()) {
            val couponArray = JSONArray()
            coupons.forEach {
                couponArray.put(it.coupon)
            }
            params.put("couponCodes", couponArray)
        }

        if (isActiveLinkPoint) {
            val linkPointArray =
                JSONArray().put(
                    JSONObject()
                        .put("amount", linkPointValue)
                        .put("paymentMode", "LINKPOINT")
                )
            params.put("payment", linkPointArray)
        }

        if (isActiveAgeVerify) {
            params.put("ageVerificationCode", ageVerifyCode)
        }

        return apiCall(
            "/order/v0.2/orders",
            ScangoRequestProvider.Method.POST,
            params = params,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.SCANGO_V2
        ) {
            it
        }
    }

    suspend fun loadPromotions(): Result<List<ScangoCoupon>> {
        return apiCall(
            "/api/coupon",
            ScangoRequestProvider.Method.GET,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            it.toCouponList()
        }
    }
}