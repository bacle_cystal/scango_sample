package ntuc.fairprice.omni.scango.ui.screen.home.exitstore

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import javax.inject.Inject

class ExitStoreViewModel @Inject constructor(application: Application) : ScangoBaseViewModel(application) {
    var storeName = MutableLiveData<String>()
}