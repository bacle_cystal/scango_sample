package ntuc.fairprice.omni.scango.ui.screen.tutorial

data class TutorialHomeItem(
    val header : String,
    val detail : String,
    val imageTutorialId: Int
)