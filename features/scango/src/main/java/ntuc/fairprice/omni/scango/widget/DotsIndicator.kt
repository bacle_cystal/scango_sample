package ntuc.fairprice.omni.scango.widget

import android.content.Context
import android.database.DataSetObserver
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.viewpager.widget.ViewPager
import ntuc.fairprice.omni.scango.R
import java.util.*

class DotsIndicator : LinearLayout {
    private lateinit var dots: MutableList<ImageView>
    private var viewPager: ViewPager? = null
    private var dotsSize: Float = 0.toFloat()
    private var dotsCornerRadius: Float = 0.toFloat()
    private var dotsSpacing: Float = 0.toFloat()
    private var currentPage: Int = 0
    private var dotsWidthFactor: Float = 0.toFloat()
    private var dotsColor: Int = 0

    private var dotsClickable: Boolean = false
    private var pageChangedListener: ViewPager.OnPageChangeListener? = null

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        dots = ArrayList()
        orientation = LinearLayout.HORIZONTAL

        dotsSize = dpToPx(16).toFloat() // 16dp
        dotsSpacing = dpToPx(4).toFloat() // 4dp
        dotsCornerRadius = dotsSize / 2

        dotsWidthFactor = DEFAULT_WIDTH_FACTOR
        dotsColor = DEFAULT_POINT_COLOR
        dotsClickable = true

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.DotsIndicator)

            dotsColor = a.getColor(R.styleable.DotsIndicator_dotsColor, DEFAULT_POINT_COLOR)
            setUpCircleColors(dotsColor)

            dotsWidthFactor = a.getFloat(R.styleable.DotsIndicator_dotsWidthFactor, 2.5f)
            if (dotsWidthFactor < 1) {
                dotsWidthFactor = 2.5f
            }

            dotsSize = a.getDimension(R.styleable.DotsIndicator_dotsSize, dotsSize)
            dotsCornerRadius =
                    a.getDimension(R.styleable.DotsIndicator_dotsCornerRadius, dotsSize / 2).toInt().toFloat()
            dotsSpacing = a.getDimension(R.styleable.DotsIndicator_dotsSpacing, dotsSpacing)

            a.recycle()
        } else {
            setUpCircleColors(DEFAULT_POINT_COLOR)
        }

        if (isInEditMode) {
            addDots(5)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        refreshDots()
    }

    private fun refreshDots() {
        viewPager?.let {
            it.adapter?.apply {
                // Check if we need to refresh the dots count
                if (dots.size < count) {
                    addDots(count - dots.size)
                } else if (dots.size > count) {
                    removeDots(dots.size - count)
                }
                setUpDotsAnimators()
            }
        }
    }

    private fun addDots(count: Int) {
        for (i in 0 until count) {
            val dot = LayoutInflater.from(context).inflate(R.layout.scango_dot_layout, this, false)
            val imageView = dot.findViewById<ImageView>(R.id.dot)
            val params = imageView.layoutParams as RelativeLayout.LayoutParams
            params.height = dotsSize.toInt()
            params.width = params.height
            params.setMargins(dotsSpacing.toInt(), 0, dotsSpacing.toInt(), 0)
            (imageView.background as GradientDrawable).cornerRadius = dotsCornerRadius
            (imageView.background as GradientDrawable).setColor(dotsColor)

            dot.setOnClickListener {
                if (dotsClickable) {
                    viewPager?.let { vp ->
                        vp.adapter?.apply {
                            if (i < count) {
                                vp.setCurrentItem(i, true)
                            }
                        }
                    }
                }
            }
            dots.add(imageView)
            addView(dot)
        }
    }

    private fun removeDots(count: Int) {
        for (i in 0 until count) {
            removeViewAt(childCount - 1)
            dots.removeAt(dots.size - 1)
        }
    }

    private fun setUpDotsAnimators() {
        viewPager?.let {
            it.adapter?.apply {
                if (count > 0 && currentPage < dots.size) {
                    if (currentPage < dots.size) {
                        val dot = dots[currentPage]

                        val params = dot.layoutParams as RelativeLayout.LayoutParams
                        params.width = dotsSize.toInt()
                        dot.layoutParams = params
                    }

                    currentPage = it.currentItem
                    if (currentPage >= dots.size) {
                        currentPage = dots.size - 1
                        it.setCurrentItem(currentPage, false)
                    }
                    val dot = dots[currentPage]

                    val params = dot.layoutParams as RelativeLayout.LayoutParams
                    params.width = (dotsSize * dotsWidthFactor).toInt()
                    dot.layoutParams = params

                    val pageChangeListener: ViewPager.OnPageChangeListener? = pageChangedListener
                    if (pageChangeListener != null) {
                        it.removeOnPageChangeListener(pageChangeListener)
                    }
                    setUpOnPageChangedListener()
                    pageChangedListener?.run {
                        it.addOnPageChangeListener(this)
                    }
                }
            }
        }
    }

    private fun setUpOnPageChangedListener() {
        pageChangedListener = object : ViewPager.OnPageChangeListener {
            private var lastPage: Int = 0

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                dots.let { dots ->
                    if (position != currentPage && positionOffset == 0f || currentPage < position) {
                        setDotWidth(dots[currentPage], dotsSize.toInt())
                        currentPage = position
                    }

                    if (Math.abs(currentPage - position) > 1) {
                        setDotWidth(dots[currentPage], dotsSize.toInt())
                        currentPage = lastPage
                    }

                    var dot = dots[currentPage]

                    var nextDot: ImageView? = null
                    if (currentPage == position && currentPage + 1 < dots.size) {
                        nextDot = dots[currentPage + 1]
                    } else if (currentPage > position) {
                        nextDot = dot
                        dot = dots[currentPage - 1]
                    }

                    val dotWidth = (dotsSize + dotsSize * (dotsWidthFactor - 1) * (1 - positionOffset)).toInt()
                    setDotWidth(dot, dotWidth)

                    if (nextDot != null) {
                        val nextDotWidth = (dotsSize + dotsSize * (dotsWidthFactor - 1) * positionOffset).toInt()
                        setDotWidth(nextDot, nextDotWidth)
                    }
                    lastPage = position
                }
            }

            private fun setDotWidth(dot: ImageView, dotWidth: Int) {
                val dotParams = dot.layoutParams
                dotParams.width = dotWidth
                dot.layoutParams = dotParams
            }

            override fun onPageSelected(position: Int) {}

            override fun onPageScrollStateChanged(state: Int) {}
        }
    }

    private fun setUpCircleColors(color: Int) {
        for (elevationItem in dots) {
            (elevationItem.background as GradientDrawable).setColor(color)
        }
    }

    private fun setUpViewPager() {
        viewPager?.let {
            it.adapter?.apply {
                registerDataSetObserver(object : DataSetObserver() {
                    override fun onChanged() {
                        super.onChanged()
                        refreshDots()
                    }
                })
            }
        }
    }

    private fun dpToPx(dp: Int): Int {
        return (context.resources.displayMetrics.density * dp).toInt()
    }

    //*********************************************************
    // Users Methods
    //*********************************************************

    fun setPointsColor(color: Int) {
        setUpCircleColors(color)
    }

    fun setDotsClickable(dotsClickable: Boolean) {
        this.dotsClickable = dotsClickable
    }

    fun setViewPager(viewPager: ViewPager) {
        this.viewPager = viewPager
        setUpViewPager()
        refreshDots()
    }

    companion object {
        private const val DEFAULT_POINT_COLOR = Color.CYAN
        const val DEFAULT_WIDTH_FACTOR = 2.5f
    }
}
