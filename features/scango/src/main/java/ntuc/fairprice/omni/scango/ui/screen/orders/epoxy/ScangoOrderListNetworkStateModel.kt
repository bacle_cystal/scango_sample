package ntuc.fairprice.omni.scango.ui.screen.orders.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.R2

@EpoxyModelClass(layout = R2.layout.order_list_network_state)
abstract class ScangoOrderListNetworkStateModel : EpoxyModelWithHolder<ScangoOrderListNetworkStateModel.HeaderHolder>() {

    @EpoxyAttribute var errorText : String? = null

    override fun bind(holder : HeaderHolder) {
        holder.errorTv.setText(errorText)
    }

    class HeaderHolder : ScangoBaseEpoxyHolder() {

        val progressBar : TextView by bind(R.id.progress_bar)
        val errorTv : TextView by bind(R.id.error_msg)

    }

}