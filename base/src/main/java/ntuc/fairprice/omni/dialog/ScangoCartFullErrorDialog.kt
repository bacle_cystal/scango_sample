package ntuc.fairprice.omni.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import ntuc.fairprice.omni.R

class ScangoCartFullErrorDialog(context: Context) {

    var onActionClick: () -> Unit = {}

    private var dialog: Dialog

    init {
        val view = LayoutInflater.from(context).inflate(R.layout.scango_cart_full_error_dialog_layout, null)
        view.findViewById<TextView>(R.id.scango_cart_full_error_ok_btn).setOnClickListener {
            onActionClick.invoke()
            dismiss()
        }

        dialog = AlertDialog.Builder(context).setView(view).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
    }

    fun show() {
        dialog.show()
    }

    fun dismiss() {
        dialog.dismiss()
    }
}