package ntuc.fairprice.omni.scango.ui.screen.qrcode.error

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseViewModel
import ntuc.fairprice.omni.ui.utils.default
import javax.inject.Inject

class BarcodeErrorViewModel @Inject constructor(
    application: Application
) : ScangoBaseViewModel(application) {
    var isVisiableBarcode = MutableLiveData<Boolean>().default(false)
    var errorMessageTitle = MutableLiveData<String>()
    var errorMessageContent = MutableLiveData<String>()
    var bottomButtonText = MutableLiveData<String>()

    fun setMessageFromType(context: Context?, errorType: Int) {
        context?.apply {
            when (errorType) {
                ErrorType.SOMETHING_WRONG.value -> {
                    errorMessageTitle.value = getString(R.string.text_barcode_error_title)
                    errorMessageContent.value = getString(R.string.text_barcode_error_content)
                    bottomButtonText.value = getString(R.string.text_barcode_error_button)
                }
                ErrorType.NOT_RECOGNISED_BAR_CODE.value -> {
                    errorMessageTitle.value = getString(R.string.text_barcode_error_barcode_not_reconised_title)
                    errorMessageContent.value = getString(R.string.text_barcode_error_barcode_not_reconised_content)
                    bottomButtonText.value = getString(R.string.text_barcode_error_barcode_not_reconised_button)
                }
                ErrorType.NOT_ABLE_SELL.value -> {
                    errorMessageTitle.value = getString(R.string.text_barcode_error_not_able_sell)
                    errorMessageContent.value = getString(R.string.text_barcode_error_not_able_sell_content)
                    bottomButtonText.value = getString(R.string.text_barcode_error_not_able_sell_button)
                }
                ErrorType.NETWORK_ERROR.value -> {
                    errorMessageTitle.value = getString(R.string.text_barcode_error_no_internet)
                    errorMessageContent.value = getString(R.string.text_barcode_error_no_internet_content)
                    bottomButtonText.value = getString(R.string.text_barcode_error_no_internet_button)
                }
                ErrorType.NOT_RECOGNISED_QR_CODE.value -> {
                    errorMessageTitle.value = getString(R.string.text_qrcode_error_barcode_not_reconised_title)
                    errorMessageContent.value = getString(R.string.text_qrcode_error_barcode_not_reconised_content)
                    bottomButtonText.value = getString(R.string.text_qrcode_error_barcode_not_reconised_button)
                }
            }
        }
    }
}

