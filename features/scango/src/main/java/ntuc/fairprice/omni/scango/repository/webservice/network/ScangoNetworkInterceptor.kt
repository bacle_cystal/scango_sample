package ntuc.fairprice.omni.scango.repository.webservice.network

import android.app.Application
import android.text.TextUtils
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import thor.zopsmart.com.thor.di.FeatureScope
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.getAccessToken
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import thor.zopsmart.com.thor.repository.webservice.service.AccountService
import javax.inject.Inject

@FeatureScope
class ScangoNetworkInterceptor @Inject constructor(private val application: Application) : Interceptor {

    private val accountService: AccountService = AccountService(application)

    private val localStorage = LocalStorage.init(application)

    override fun intercept(chain: Interceptor.Chain): Response {

        val response = chain.proceed(chain.request().withAdditionalHeader())

        if (response.code() == 401 && localStorage.getAccessToken().isNullOrEmpty().not()) {
            // try to renew the token
            runBlocking {
                accountService.reAuthenticateUser()
            }.onSuccess {
                return chain.proceed(chain.request().withAdditionalHeader())
            }
        }
        return response
    }

    private fun Request.withAdditionalHeader(): Request {
        val accessToken = localStorage.getAccessToken()
        return newBuilder().apply {
            if (TextUtils.isEmpty(accessToken)) {
                removeHeader("Authorization")
            } else {
                addHeader("Authorization", "Bearer $accessToken")
            }
        }.build()
    }

}