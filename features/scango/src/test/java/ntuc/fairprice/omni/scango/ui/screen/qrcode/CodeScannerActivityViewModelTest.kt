//package ntuc.fairprice.omni.scango.ui.screen.qrcode
//
///**
// * Created by Binh.TH on 2/25/2019.
// */
//
//import androidx.arch.core.executor.testing.InstantTaskExecutorRule
//import com.nhaarman.mockitokotlin2.any
//import com.nhaarman.mockitokotlin2.doReturn
//import com.nhaarman.mockitokotlin2.mock
//import com.nhaarman.mockitokotlin2.stub
//import kotlinx.coroutines.ExperimentalCoroutinesApi
//import ntuc.fairprice.omni.scango.InstantDispatcherTestRule
//import ntuc.fairprice.omni.scango.repository.db.room.entity.createProduct
//import ntuc.fairprice.omni.scango.repository.db.room.entity.createStore
//import ntuc.fairprice.omni.scango.repository.db.room.repo.ProductRepository
//import ntuc.fairprice.omni.scango.repository.webservice.model.Failure
//import ntuc.fairprice.omni.scango.repository.webservice.model.Success
//import org.junit.Assert.*
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.junit.rules.RuleChain
//import org.mockito.MockitoAnnotations
//import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
//
//class CodeScannerActivityViewModelTest {
//
//    private val repository: ProductRepository = mock()
//
//    private val accountRepository: AccountRepository = mock()
//
//    private lateinit var viewModel: CodeScannerActivityViewModel
//
//    @ExperimentalCoroutinesApi
//    @get:Rule
//    val rule = RuleChain.outerRule(InstantTaskExecutorRule()).around(InstantDispatcherTestRule())
//
//    @Before
//    @Throws(Exception::class)
//    fun prepare() {
//        MockitoAnnotations.initMocks(this)
//        viewModel = CodeScannerActivityViewModel(mock(), repository, accountRepository, mock)
//    }
//
//
//    @Test
//    fun `barcode and qrcode should be valid`() {
//
//        assertFalse(viewModel.validateBarcode(null))
//
//        assertFalse(viewModel.validateBarcode(""))
//
//        assertTrue(viewModel.validateBarcode("9556108211332"))
//
//        assertFalse(viewModel.validateQrcode(null))
//
//        assertFalse(viewModel.validateQrcode(""))
//
//        assertTrue(viewModel.validateQrcode("5"))
//    }
//
//    @Test
//    fun `get product from barcode error`() {
//
//        viewModel.productItem.value = null
//
//        repository.stub {
//            onBlocking { getProductByBarcode(any()) }.doReturn(Failure(Throwable("error")))
//        }
//
//        viewModel.getProductByBarcode("1231233")
//
//        assertTrue(viewModel.productItem.value == null)
//    }
//
//    @Test
//    fun `get product from barcode sucesss`() {
//
//        viewModel.productItem.value = null
//
//        repository.stub {
//            onBlocking { getProductByBarcode(any()) }.doReturn(Success(createProduct()))
//        }
//
//        viewModel.getProductByBarcode("123123123")
//
//        assertTrue(viewModel.productItem.value != null)
//    }
//
//    @Test
//    fun `get store from qrcode storeId error`() {
//
//        viewModel.storeItem.value = null
//
//        repository.stub {
//            onBlocking { getCheckinStore(any(), any(), any()) }.doReturn(Failure(Throwable("error")))
//        }
//
//        viewModel.getCheckIn(5)
//
//        assertTrue(viewModel.storeItem.value == null)
//    }
//
//
//    @Test
//    fun `get store from qrcode storeId success`() {
//
//        viewModel.storeItem.value = null
//
//        repository.stub {
//            onBlocking { getCheckinStore(any(), any(), any()) }.doReturn(Success(createStore()))
//        }
//
//        viewModel.getCheckIn(5)
//
//        assertTrue(viewModel.storeItem.value != null)
//    }
//}
