package ntuc.fairprice.omni.domain.repository.webservice.service

import ntuc.fairprice.omni.domain.model.linkpoint.MemberData
import ntuc.fairprice.omni.domain.model.linkpoint.MyAccLPStatusResponseModel
import retrofit2.Call
import retrofit2.http.*

interface LinkpointService {

    @GET("/link/v0.2/customers/{id}")
    fun getCustomerLPStatus(@Path("id") id : String, @Header("Authorization") authHeader: String): Call<MyAccLPStatusResponseModel>

    @DELETE("/link/v0.2/customers/{id}")
    fun unlinkLPAccount(@Path("id") id : String, @Header("Authorization") authHeader: String): Call<MemberData>

//    @PUT("/link/v0.2/customers/{id}")
//    fun putCustomers(@Path("id") id : String,
//                     @Field("link_type") link_type: Int,
//                     @Field("link_id") link_id: String): Call<MemberData>
//
//    @POST("/link/v0.2/otp")
//    fun sendOTP(): Call<MemberData>
//
//    @GET("/link/v0.2/otp/{otp}")
//    fun validateOTP(@Field("otp") otp: String
//    ): Call<MemberData>

}