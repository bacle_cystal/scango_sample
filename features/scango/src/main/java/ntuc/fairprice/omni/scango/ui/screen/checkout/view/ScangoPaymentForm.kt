package ntuc.fairprice.omni.scango.ui.screen.checkout.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_add_payment_view.view.*
import ntuc.fairprice.omni.scango.base.customview.ScangoLabelEditText
import ntuc.fairprice.omni.scango.databinding.ScangoAddPaymentViewBinding
import ntuc.fairprice.omni.scango.utils.clearTexts
import ntuc.fairprice.omni.scango.utils.containsEmpty
import ntuc.fairprice.omni.scango.utils.textListener
import org.json.JSONObject
import thor.zopsmart.com.thor.R
import thor.zopsmart.com.thor.base.extensions.visible
import thor.zopsmart.com.thor.interfaces.AppView
import thor.zopsmart.com.thor.model.getCardParams
import java.lang.Exception

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoPaymentForm @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), AppView {

    override val mContext = context

    val binding: ScangoAddPaymentViewBinding

    private val viewList: List<ScangoLabelEditText>

    init {
        val inflater = LayoutInflater.from(context)
        binding = ScangoAddPaymentViewBinding.inflate(inflater, this, true)

        viewList = listOf(
            binding.etCardNumber,
            binding.etCvv,
            binding.etExpiry
        )

        viewList.textListener {
            validateViews()
        }
    }

    @ModelProp
    fun setTitle(title: String) {
        tv_label_payment.text = title
        postInvalidate()
    }

    @ModelProp
    fun padded(isPadded: Boolean = false) {
        if (isPadded) {
            setBackgroundResource(R.drawable.edit_text_border)
            val padding = resources.getDimensionPixelOffset(R.dimen.activity_vertical_margin)
            setPadding(padding, padding, padding, padding)
            postInvalidate()
        }
    }

    @CallbackProp
    fun buttonAction(@Nullable buttonAction: ((JSONObject, Boolean) -> Unit)) {
        btn_add_payment.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN ->{
                    try {
                        val p = getCardParams(
                            binding.etExpiry.getString(),
                            binding.etCvv.getString(),
                            binding.etCardNumber.getString()
                        )
                        buttonAction.let {
                            it(p, binding.cbDefault.isChecked)
                        }
                    } catch (e: Exception) {
                        binding.etCardNumber.requestFocus()
                        binding.etCardNumber.setErrorState()
                        //showAlert("This card is not supported")
                    }
                }
            }
            v?.onTouchEvent(event) ?: true
        }
    }

    fun buttonText(btnText: String) {
        binding.btnAddPayment.text = btnText
    }

    fun cancelAction(action: () -> Unit) {
        binding.tvCancel.visible()
        binding.tvCancel.setOnClickListener {
            action()
        }
    }

    private fun validateViews() {
        if (viewList.containsEmpty()) {
            btn_add_payment.isEnabled = false
            return
        }
        btn_add_payment.isEnabled = true
    }

    fun clearViews() {
        //Clear text and reset focus to card number
        viewList.clearTexts()
        viewList[0].setFocusState()
    }
}