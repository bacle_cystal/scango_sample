package ntuc.fairprice.omni.scango.base.customview.tagview


interface DataTransform<T> {
    fun transfer(item: T): String
    fun transferIcon(item: T): Int
}