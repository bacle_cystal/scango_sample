package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.inputmethod.EditorInfo
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.scango_activity_link_plus_card_number.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.widget.ScangoLinkPlusCardNumberBoxWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.ScangoLinkPlusPhoneNumberActivity
import ntuc.fairprice.omni.scango.utils.observe
import ntuc.fairprice.omni.util.CustomerHelpUtils
import javax.inject.Inject


class ScangoLinkPlusCardNumberActivity : ScangoBaseActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, ScangoLinkPlusCardNumberActivity::class.java)
    }

    @Inject
    lateinit var viewModel: ScangoLinkPlusCardNumberViewModel
    @Inject
    lateinit var scangoLinkPlusCardNumberBoxWidget: ScangoLinkPlusCardNumberBoxWidget
    @Inject
    lateinit var keyboardManager: KeyboardManager
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scango_activity_link_plus_card_number)
        initWidget()
        initListeners()
        initViewModel()
    }

    private fun initViewModel() {
        observe(viewModel.stateLiveData, ::onViewModelStateChanged)
    }

    private fun initWidget() {
        scangoLinkPlusCardNumberBoxWidget.initWidget(ll_link_plus_card_container)
        scangoLinkPlusCardNumberBoxWidget.displayDefaultState()
    }

    private fun initListeners() {
        link_plus_card_number_toolbar.setOnActionClickListener {
            keyboardManager.hide(edt_link_plus_card_number)
            finish()
        }
        edt_link_plus_card_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                validateAndUpdateCustomer()
                true
            } else {
                false
            }
        }
        txt_link_plus_card_link_account.setOnClickListener {
            validateAndUpdateCustomer()
        }

        edt_link_plus_card_number.filters = arrayOf(
            InputFilter.LengthFilter(19),
            CardNumberFormatInputFilter()
        )
        btn_link_plus_try_with_number.setOnClickListener {
            startActivity(ScangoLinkPlusPhoneNumberActivity.getIntent(this))
            finish()
        }
        img_link_plus_card_call_us.setOnClickListener {
            CustomerHelpUtils.dialSupport(this)
        }
        img_link_plus_card_email_us.setOnClickListener {
            CustomerHelpUtils.emailSupport(this)
        }
        txt_link_plus_app.setOnClickListener {
            CustomerHelpUtils.openPlusAppInStore(this)
        }
    }

    private fun validateAndUpdateCustomer() {
        keyboardManager.hide(edt_link_plus_card_number)
        edt_link_plus_card_number.clearFocus()
        scangoLinkPlusCardNumberBoxWidget.run {
            if (isValidCardNumber) {
                scanGoProgressDialog.show()
                viewModel.updateCustomerRecordWithCardNumber(cardNumber.first, cardNumber.second)
            } else {
                displayInvalidState()
            }
        }
    }

    private fun onViewModelStateChanged(state: ScangoLinkPlusCardNumberViewModel.State) {
        when (state) {
            ScangoLinkPlusCardNumberViewModel.State.UpdateCustomerCardSuccess -> {
                scanGoProgressDialog.dismiss()
                startActivity(ScangoLinkPlusCardDoneActivity.getIntent(this))
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.INTENT_LINK_POINT_REFRESH))
                finish()
            }

            ScangoLinkPlusCardNumberViewModel.State.AccountNotFound -> {
                scanGoProgressDialog.dismiss()
                scangoLinkPlusCardNumberBoxWidget.displayInvalidState()

            }

            ScangoLinkPlusCardNumberViewModel.State.NetworkError -> {
                scanGoProgressDialog.dismiss()
                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        validateAndUpdateCustomer()
                    }
                    show()
                }

            }

            ScangoLinkPlusCardNumberViewModel.State.GeneralError -> {
                scanGoProgressDialog.dismiss()

                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        validateAndUpdateCustomer()
                    }
                    show()
                }

            }
        }
    }
}