package ntuc.fairprice.omni.scango.repository.restictionconfig

import android.app.Application
import android.content.SharedPreferences
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.given
import ntuc.fairprice.omni.scango.Constants.KEY_RESTRICTIONS_CONFIG
import ntuc.fairprice.omni.scango.repository.db.room.entity.RestrictionsConfig
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepository
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepositoryImpl
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ScangoRestrictionsConfigRepositoryTest {

    @Mock
    private lateinit var mockApplication: Application
    @Mock
    private lateinit var mockSharedPreferences: SharedPreferences
    @Mock
    private lateinit var mockGson: Gson
    @Mock
    private lateinit var mockRestrictionsConfig: RestrictionsConfig

    private lateinit var subject: ScangoRestrictionsConfigRepository

    companion object {
        private const val mockConfigString = ""
    }

    @Before
    fun setUp() {
        given(mockApplication.applicationContext).willReturn(mockApplication)

        subject = ScangoRestrictionsConfigRepositoryImpl(
            mockApplication,
            mockSharedPreferences,
            mockGson
        )
    }

    @Test
    fun `Test getConfig from shared preference`() {
        //GIVEN
        given(mockSharedPreferences.getString(KEY_RESTRICTIONS_CONFIG, null)).willReturn(mockConfigString)
        given(mockGson.fromJson(mockConfigString, RestrictionsConfig::class.java)).willReturn(mockRestrictionsConfig)

        //THEN
        subject.getConfig() === mockRestrictionsConfig
    }

    /**
     * TODO
     * Unable to test the file read as the json conversion happens from static method from FileUtils.kt
     * we will be able to test once the method is moved to some manager after refactor
     */
}