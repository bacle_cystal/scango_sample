package ntuc.fairprice.omni.scango

import android.app.Activity
import dagger.Binds
import dagger.Module
import thor.zopsmart.com.thor.di.ActivityScope

@Module
abstract class ScangoFeatureBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideActivity(activity: ScangoFeatureActivity): Activity
}