package ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.widget.ScangoLinkPlusCardNumberBoxWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.cardnumber.widget.ScangoLinkPlusCardNumberBoxWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
class  ScangoLinkPlusCardNumberProvidesModule {

    @Provides
    @ActivityScope
    fun provideScangoLinkPlusCardNumberViewModel(
        activity: ScangoLinkPlusCardNumberActivity,
        factory: ViewModelProvider.Factory
    ): ScangoLinkPlusCardNumberViewModel = ViewModelProviders.of(activity, factory).get()

}

@Module
abstract class  ScangoLinkPlusCardNumberBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideScangoLinkPlusCardNumberBoxWidget(widget: ScangoLinkPlusCardNumberBoxWidgetImpl): ScangoLinkPlusCardNumberBoxWidget

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoLinkPlusCardNumberActivity): Activity

}