package ntuc.fairprice.omni.scango.ui.screen.shoppinglist

import android.app.ActionBar
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_toolbar.*
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartActivity
import thor.zopsmart.com.thor.base.SimpleSinglePaneActivity
import thor.zopsmart.com.thor.base.toolbar.ui.ToolbarBackButtonView
import thor.zopsmart.com.thor.base.toolbar.ui.ToolbarWithSearchAndBack
import thor.zopsmart.com.thor.features.home.viewmodel.HomePageViewModel
import javax.inject.Inject
import thor.zopsmart.com.thor.R as RFP

class ScangoShoppingListDetailActivity : SimpleSinglePaneActivity(), HasSupportFragmentInjector {

    companion object {
        fun getIntent(context: Context, shoppingListId: Long, shoppingListName: String = ""): Intent {
            return Intent(context, ScangoShoppingListDetailActivity::class.java)
                .putExtra("shoppingListId", shoppingListId)
                .putExtra("shoppingListName", shoppingListName)
        }
    }

    lateinit var toolbar_ll : LinearLayout

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: HomePageViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(HomePageViewModel::class.java)
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        addToolbar()
    }

    override fun onCreatePane(): Fragment {
        return ScangoShoppingListDetailPage()
    }

    // TODO : just adding a demo toolbar. Can be changed accordingly
    private fun addToolbar() {
        toolbar_ll = findViewById(RFP.id.toolbar_ll)

        toolbar_ll.removeAllViews()

        val toolbarView = ToolbarBackButtonView(this).apply {
            title = intent?.extras?.getString("shoppingListName", "")?:""
            onBackClick(View.OnClickListener { finish() })
        }
        toolbarView.layoutParams =
            ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        toolbar_ll.addView(toolbarView)
    }
}
