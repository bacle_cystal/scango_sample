package ntuc.fairprice.omni.scango

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ScangoUnitTestU {

    @Test
    fun validateBarcode(barcode: String) {
        var last_digit = Integer.parseInt(barcode.last() + "")

        var sum = 0
        for (i in 0 until barcode.length) {
            sum += Integer.parseInt(barcode[i] + "") * if (i % 2 == 0) 1 else 3
        }

        var checksum_digit = 10 - sum % 10
        if (checksum_digit == 10) checksum_digit = 0


        assertEquals(last_digit, checksum_digit)
    }

}
