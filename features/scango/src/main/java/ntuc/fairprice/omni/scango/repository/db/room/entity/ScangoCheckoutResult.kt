package ntuc.fairprice.omni.scango.repository.db.room.entity

data class ScangoCheckoutResult(
    val orderId: Long,
    val amount: String,
    val discount: Double,
    val storeId: Long
    )