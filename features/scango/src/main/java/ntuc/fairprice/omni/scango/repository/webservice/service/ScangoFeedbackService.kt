package ntuc.fairprice.omni.scango.repository.webservice.service

import ntuc.fairprice.omni.scango.repository.db.room.entity.FeedbackOption
import ntuc.fairprice.omni.scango.repository.webservice.model.ApiResponse
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.model.toListFeedbackOption
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import org.json.JSONObject
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoFeedbackService @Inject constructor(
    scangoRequestProvider: ScangoRequestProvider
) : ScangoBaseService(scangoRequestProvider) {

    suspend fun getFeedbackOptions(): Result<List<FeedbackOption>> {
        val requestBody = JSONObject()
        return apiCall("/feedback-service/options", ScangoRequestProvider.Method.GET, params = requestBody) {
            it.toListFeedbackOption()
        }
    }

    suspend fun postFeedbackOptions(requestBody: JSONObject): Result<ApiResponse> =
        apiCall("/feedback-service/customer", ScangoRequestProvider.Method.POST, params = requestBody) { it }
}
