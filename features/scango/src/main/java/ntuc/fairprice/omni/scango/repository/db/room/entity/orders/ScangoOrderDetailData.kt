package ntuc.fairprice.omni.scango.repository.db.room.entity.orders

import com.google.gson.annotations.SerializedName
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoOffer
import java.text.SimpleDateFormat
import java.util.*

data class ScangoOrderDetailData(
    var code: Int?,
    var `data`: Data?,
    var status: String?
) {
    data class Data(
        var order: Order?
    ) {
        data class Order(
            var address: Address?,
            var amount: String?,
            var clientId: Any?,
            var completedAt: String?,
            var couponCode: String?,
            var couponDiscount: String?,
            var couponDiscounts: List<CouponDiscount> = emptyList(),
            var createdAt: String?,
            var customer: Customer?,
            var discount: String?,
            var edited: Boolean = false,
            var id: Int?,
            var invoiceAmount: String?,
            var is_cancellable: Boolean?,
            var items: List<Item> = emptyList(),
            var linkPoint: LinkpointMeta?,
            var offers: List<Offers>?,
            var metaData: Any?,
            var packingDetails: List<Any>?,
            var payment: List<Payment>?,
            var paymentStatus: String?,
            var pendingAmount: String?,
            var placedFrom: String?,
            var preferredDate: String?,
            var referenceNumber: String?,
            var refund: List<Any?>?,
            var refundAmount: String?,
            var shipping: String?,
            var slotEndTime: String?,
            var slotStartTime: String?,
            var slotType: String?,
            var status: String?,
            var store: Store?,
            var type: Type?,
            @SerializedName("verificationStatus") private var _verificationStatus: String?,
            val returns: List<Return>?
        ) {
            data class Return(
                val id: String?,
                val createdAt: String?,
                val items: List<Item>?,
                val total: Double?
            ) {
                fun getDate(): Date? {
                    return createdAt?.let {
                        SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH).parse(createdAt)}
                }

                data class Item(
                    val discount: Double?,
                    val id: Int?,
                    val mrp: Double?,
                    val quantity: Int?,
                    val reason: String?
                )
            }

            val verificationStatus: VerificationStatus
                get() =
                    when (_verificationStatus) {
                        "PASSED", "FAILED" -> VerificationStatus.VerificationStatusVerified
                        "PENDING", null -> VerificationStatus.VerificationStatusPending
                        else -> VerificationStatus.VerificationStatusUnVerified
                    }

            data class LinkpointMeta(
                var redemption: Linkpoint?,
                var award: Linkpoint? = null
            )

            data class Linkpoint(
                var dollarAmount: Double?,
                var pointAmount: Double?,
                var pointBalance: Double?
            )

            data class Offers(
                val clientId: Any?,
                val customerRedemptionLimit: Any?,
                val description: String?,
                val discount: Int?,
                val discountType: String?,
                val endDeliveryDate: Any?,
                val hasUniquePromocode: Boolean?,
                val id: Int?,
                val imageUrl: String?,
                val metaData: Any?,
                val offerType: String?,
                val offerValidFrom: String?,
                val offerValidTill: String?,
                val orderType: Any?,
                val paymentType: Any?,
                val promoCode: Any?,
                val rule: Rule?,
                val stackable: Boolean?,
                val startDeliveryDate: Any?,
                val status: String?,
                val storeId: String?,
                val totalRedemption: Any?,
                val userSet: Any?
            ) {
                data class CartDiscount(
                    val t: String?,
                    val v: Int?
                )

                data class Rule(
                    val cartDiscount: CartDiscount?,
                    val cartPrice: Int?
                )
            }

            data class CouponDiscount(
                var code: String = "",
                var discount: Double,
                var type: String = ""
            )

            data class Item(
                var barcodes: Any?,
                var brand: Brand?,
                var categories: List<Category?>?,
                var clientItemId: String?,
                var createdAt: String?,
                var description: Any?,
                var hasVariants: Int?,
                var id: Int,
                var images: List<String>?,
                var imagesExtra: Any?,
                var item: Item?,
                var metaData: MetaData?,
                var name: String?,
                var orderDetails: OrderDetails?,
                var offer: List<OfferData>?,
                var organizationId: String?,
                var primaryCategory: PrimaryCategory?,
                var product: Product?,
                var secondaryCategoryIds: List<Any?>?,
                var slug: String?,
                var soldByWeight: Int?,
                var status: String?,
                // TODO remove this for now var storeSpecificData: List<StoreSpecificData?>?,
                var tagIds: List<Int?>?,
                var variants: List<Any?>?
            ) {
                data class MetaData(
                    @SerializedName("Country of Origin")
                    var CountryOfOrigin: String?,
                    @SerializedName("Unit Of Measurement")
                    var unitOfMeasurement: String?
                )

                data class Item(
                    var id: Int?
                )

                data class Brand(
                    var clientId: Any?,
                    var description: Any?,
                    var id: Int?,
                    var image: Any?,
                    var logo: Any?,
                    var name: String?,
                    var productsCount: Int?,
                    var slug: String?,
                    var status: String?
                )

                data class Category(
                    var clientId: Any?,
                    var description: Any?,
                    var id: Int?,
                    var image: Any?,
                    var name: String?,
                    var parentCategory: ParentCategory?,
                    var productsCount: Int?,
                    var slug: String?,
                    var status: String?
                ) {
                    data class ParentCategory(
                        var clientId: Any?,
                        var description: Any?,
                        var id: Int?,
                        var image: Any?,
                        var name: String?,
                        var parentCategory: Any?,
                        var productsCount: Int?,
                        var slug: String?,
                        var status: String?
                    )
                }

                data class PrimaryCategory(
                    var clientId: Any?,
                    var description: Any?,
                    var id: Int?,
                    var image: Any?,
                    var name: String?,
                    var parentCategory: ParentCategory?,
                    var productsCount: Int?,
                    var slug: String?,
                    var status: String?
                ) {
                    data class ParentCategory(
                        var clientId: Any?,
                        var description: Any?,
                        var id: Int?,
                        var image: Any?,
                        var name: String?,
                        var parentCategory: Any?,
                        var productsCount: Int?,
                        var slug: String?,
                        var status: String?
                    )
                }

                data class Product(
                    var barcodes: Any?,
                    var brand: Brand?,
                    var categories: List<Category?>?,
                    var clientItemId: String?,
                    var createdAt: String?,
                    var description: Any?,
                    var hasVariants: Int?,
                    var id: Int?,
                    var images: List<String?>?,
                    var imagesExtra: Any?,
                    var item: Item?,
                    var metaData: MetaData?,
                    var name: String?,
                    var orderDetails: OrderDetails?,
                    var organizationId: String?,
                    var primaryCategory: PrimaryCategory?,
                    var secondaryCategoryIds: List<Any?>?,
                    var slug: String?,
                    var soldByWeight: Int?,
                    var status: String?,
                    // TODO remove this for now var storeSpecificData: List<StoreSpecificData?>?,
                    var tagIds: List<Int?>?,
                    var variants: List<Any?>?
                ) {
                    data class MetaData(
                        @SerializedName("Country of Origin")
                        var CountryOfOrigin: String?
                    )

                    data class Item(
                        var id: Int?
                    )

                    data class Brand(
                        var clientId: Any?,
                        var description: Any?,
                        var id: Int?,
                        var image: Any?,
                        var logo: Any?,
                        var name: String?,
                        var productsCount: Int?,
                        var slug: String?,
                        var status: String?
                    )

                    data class Category(
                        var clientId: Any?,
                        var description: Any?,
                        var id: Int?,
                        var image: Any?,
                        var name: String?,
                        var parentCategory: ParentCategory?,
                        var productsCount: Int?,
                        var slug: String?,
                        var status: String?
                    ) {
                        data class ParentCategory(
                            var clientId: Any?,
                            var description: Any?,
                            var id: Int?,
                            var image: Any?,
                            var name: String?,
                            var parentCategory: Any?,
                            var productsCount: Int?,
                            var slug: String?,
                            var status: String?
                        )
                    }

                    data class PrimaryCategory(
                        var clientId: Any?,
                        var description: Any?,
                        var id: Int?,
                        var image: Any?,
                        var name: String?,
                        var parentCategory: ParentCategory?,
                        var productsCount: Int?,
                        var slug: String?,
                        var status: String?
                    ) {
                        data class ParentCategory(
                            var clientId: Any?,
                            var description: Any?,
                            var id: Int?,
                            var image: Any?,
                            var name: String?,
                            var parentCategory: Any?,
                            var productsCount: Int?,
                            var slug: String?,
                            var status: String?
                        )
                    }

                    data class OrderDetails(
                        var deliveredQuantity: Any?,
                        var discount: String?,
                        var disputeQuantity: Any?,
                        var isPickupPending: Int?,
                        var mrp: String?,
                        var orderItemId: Int?,
                        var orderedQuantity: String?,
                        var status: String?,
                        var tripId: Any?
                    )
                }

                data class OrderDetails(
                    var deliveredQuantity: String?,
                    var discount: String?,
                    var disputeQuantity: Any?,
                    var isPickupPending: Int?,
                    var mrp: String?,
                    var orderItemId: Int?,
                    var orderedQuantity: String?,
                    var status: String?,
                    var tripId: Any?
                )

                data class OfferData(
                    var appliedCount: Int?,
                    var clientId: Any?,
                    var customerRedemptionLimit: Any?,
                    var description: String?,
                    var discount: Double?,
                    var endDeliveryDate: Any?,
                    var id: Long?,
                    var imageUrl: String?,
                    var metaData: Any?,
                    var offerType: String?,
                    var offerValidFrom: String?,
                    var offerValidTill: String?,
                    var orderType: Any?,
                    var paymentType: Any?,
                    var rule: Rule?,
                    var startDeliveryDate: Any?,
                    var status: String?,
                    var storeId: String?,
                    var totalRedemption: Any?,
                    var userSet: Any?
                ) {
                    data class Rule(
                        var buy: Map<Int, MapValue?>?,
                        var total: Total?,
                        var get: Total?
                    ) {
                        data class MapValue(
                            var q: Int?
                        )
                        data class Total(
                            var t: String?,
                            var v: Double?
                        )
                    }
                }
            }

            data class Store(
                var address: String?,
                var clientStoreId: Any?,
                var id: Int?,
                var latitude: String?,
                var longitude: String?,
                var metaData: MetaData?,
                var name: String?
            ) {
                data class MetaData(
                    var Phone: String?
                )
            }

            data class Type(
                var id: Int?,
                var name: String?
            )

            data class Address(
                var address: String?,
                var city: String?,
                var clientId: Any?,
                var id: Int?,
                var json: String?,
                var landmark: String?,
                var latitude: String?,
                var longitude: String?,
                var metaData: Any?,
                var pincode: Int?
            )

            data class Customer(
                var addresses: List<Addresse?>?,
                var clientId: Any?,
                var clientIds: List<Any?>?,
                var defaultAddress: DefaultAddress?,
                var defaultEmail: DefaultEmail?,
                var defaultPhone: DefaultPhone?,
                var emails: List<Email?>?,
                var id: Int?,
                var image: String?,
                var isRegistered: Boolean?,
                var joinedOn: String?,
                var joinedTime: String?,
                var lastLoginTime: String?,
                var lastOrderedOn: String?,
                var metaData: MetaData?,
                var name: String?,
                var orderSummary: OrderSummary?,
                var phones: List<Phone?>?,
                var totalAmount: String?,
                var totalOrders: Int?,
                var updatedAt: String?
            ) {
                data class DefaultEmail(
                    var email: String?,
                    var id: Int?,
                    var status: String?
                )

                data class Email(
                    var email: String?,
                    var id: Int?,
                    var status: String?
                )

                data class OrderSummary(
                    var monthlyPurchaseTrend: List<MonthlyPurchaseTrend?>?,
                    var totalAmount: String?
                ) {
                    data class MonthlyPurchaseTrend(
                        var month: String?,
                        var orderAmount: String?,
                        var orderCount: Int?
                    )
                }

                data class Phone(
                    var id: Int?,
                    var phone: String?,
                    var status: String?
                )

                data class DefaultAddress(
                    var address: String?,
                    var city: String?,
                    var clientId: Any?,
                    var id: Int?,
                    var json: String?,
                    var landmark: String?,
                    var latitude: String?,
                    var longitude: String?,
                    var metaData: Any?,
                    var pincode: Int?
                )

                data class MetaData(
                    var Gender: String?,
                    @SerializedName("JWC Member")
                    var JWCMember: Boolean?,
                    @SerializedName("Marital Status")
                    var MaritalStatus: String?,
                    var NRIC: String?
                )

                data class Addresse(
                    var address: String?,
                    var city: String?,
                    var clientId: Any?,
                    var id: Int?,
                    var json: String?,
                    var landmark: String?,
                    var latitude: String?,
                    var longitude: String?,
                    var metaData: Any?,
                    var pincode: Int?
                )

                data class DefaultPhone(
                    var id: Int?,
                    var phone: String?,
                    var status: String?
                )
            }

            data class Payment(
                var amount: String?,
                var bankTransactionId: Any?,
                var completedAt: Any?,
                var createdAt: String?,
                var gatewayTransactionId: Any?,
                var id: Int?,
                var metaData: PaymentMeta?,
                var mode: String?,
                var paymentServiceId: Any?,
                var status: String?,
                var transactionId: String?
            )

            data class PaymentMeta(
                var cardNumber: String?,
                var cardType: String?
            )
        }
    }

}

sealed class VerificationStatus {
    object VerificationStatusLoading : VerificationStatus()
    object VerificationStatusVerified : VerificationStatus()
    object VerificationStatusPending : VerificationStatus()
    object VerificationStatusUnVerified : VerificationStatus()
}