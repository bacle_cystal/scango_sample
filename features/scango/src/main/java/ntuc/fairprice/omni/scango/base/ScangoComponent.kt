package ntuc.fairprice.omni.scango.base

import com.airbnb.epoxy.EpoxyModel
import ntuc.fairprice.omni.scango.CardPromoBindingModel_
import ntuc.fairprice.omni.scango.SelectCardBindingModel_
import ntuc.fairprice.omni.scango.TitleBindingModel_
import ntuc.fairprice.omni.scango.epoxy.model.ScangoSlideShowViewModel_
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.ui.screen.promo.view.ScangoCouponEntryViewModel_
import thor.zopsmart.com.thor.GapBindingModel_
import thor.zopsmart.com.thor.base.extensions.getAutoGenId
import thor.zopsmart.com.thor.epoxy.FPDelegate
import thor.zopsmart.com.thor.features.widget.imageslideshow.data.ImageSlideShow
import thor.zopsmart.com.thor.model.Card

interface ScangoDelegate

interface ScangoAbstractDelegates : ScangoManuallyInputDelegate

interface ScangoManuallyInputDelegate : ScangoDelegate {
    fun onInputBarcodeManually(productId: String)
    fun onInputQrcodeManually(storeIdString: String)
}

interface ScangoPaymentEventDelegate : ScangoDelegate {
    fun onVerifyStatusChange()
}


enum class ScanFragmentRapidType(val value: Int) {
    MAX_ITEM(1),
    LIQUOR_ITEM(2)
}


interface ScangoMaxItemDelegate : ScangoDelegate {
    fun onOpenCartToEdit()
}

abstract class ScangoComponent<in T : ScangoDelegate> {

    protected val viewId = getAutoGenId()

    abstract fun render(delegate: T): List<EpoxyModel<*>>
}


interface ScangoHomeCampaignDelegate : ScangoDelegate {
    fun onCampaignClick(campaign: String)
}

interface ScangoLiquourRemoveDelegate : ScangoDelegate {
    fun onRefreshCart()
}

interface SGCartDetailDelegate : FPDelegate {
    fun promoClick()
    fun onPromoRemoved(coupon: ScangoCoupon)
}

class ScangoImageSlideShow(val d: List<ImageSlideShow>) : ScangoComponent<ScangoHomeCampaignDelegate>() {
    override fun render(delegate: ScangoHomeCampaignDelegate): List<EpoxyModel<*>> {
        return listOf(
            ScangoSlideShowViewModel_()
                .id(viewId)
                .updateSlides(d)
                .onSlideClick {
                    // TODO use it (link from slide show) to open the appropriate page
                    delegate.onCampaignClick(it)
                })
    }
}

class ScangoTitle(val title: String) : ScangoComponent<ScangoDelegate>() {
    override fun render(delegate: ScangoDelegate): List<EpoxyModel<*>> {
        return listOf(
            TitleBindingModel_()
                .id(viewId)
                .title(title)
        )
    }
}


interface ScangoSelectionDelegate : ScangoDelegate {
    fun onCardSelection(card: Card)
}

interface ScangoCouponEditTextDelegate : ScangoDelegate {
    fun applyAction(couponName: String)
}

interface ScangoCouponCellDelegate : ScangoDelegate {
    fun selectAction(coupon: ScangoCoupon)
    fun removeAction(coupon: ScangoCoupon)
    fun tncAction()
}

interface ScangoCouponPageDelegate : ScangoCouponCellDelegate, ScangoCouponEditTextDelegate

data class ScangoCardSelection(val cards: List<Card>, val selectedId: Long) :
    ScangoComponent<ScangoSelectionDelegate>() {
    override fun render(delegate: ScangoSelectionDelegate): List<EpoxyModel<*>> {
        return cards.map {
            SelectCardBindingModel_()
                .id("$viewId ${it.id}")
                .card(it)
                .selectedCardId(selectedId)
                .cardClickListener { _ ->
                    delegate.onCardSelection(it)
                }
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        }
    }
}

data class ScangoCouponEditText(var isError: Boolean = false) : ScangoComponent<ScangoCouponEditTextDelegate>() {
    override fun render(delegate: ScangoCouponEditTextDelegate): List<EpoxyModel<*>> {
        return listOf(
            ScangoCouponEntryViewModel_()
                .id(viewId)
                .onApply {
                    delegate.applyAction(it)
                }
                .promoError(isError)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}



data class ScangoCouponList(val coupons: List<ScangoCoupon>, val isSelected: Boolean = false) :
    ScangoComponent<ScangoCouponCellDelegate>() {
    override fun render(delegate: ScangoCouponCellDelegate): List<EpoxyModel<*>> {
        return coupons.map {
            CardPromoBindingModel_()
                .id("$viewId ${it.coupon}")
                .coupon(it)
                .selected(isSelected)
                .selectAction { _ ->
                    delegate.selectAction(it)
                }
                .removeAction { _ ->
                    delegate.removeAction(it)
                }
                .spanSizeOverride { span, _, _ ->
                    span
                }
        }
    }
}


data class ScangoVerticalGap(val margin: Float) : ScangoComponent<ScangoDelegate>() {
    override fun render(delegate: ScangoDelegate): List<EpoxyModel<*>> {
        return listOf(
            GapBindingModel_()
                .id(viewId)
                .gap(margin)
                .spanSizeOverride { totalSpanCount, _, _ ->
                    totalSpanCount
                }
        )
    }
}