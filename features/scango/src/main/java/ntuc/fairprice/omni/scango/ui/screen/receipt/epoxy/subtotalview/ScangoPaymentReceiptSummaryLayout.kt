package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy.subtotalview

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.scango_layout_payment_summary.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.R as RB
import ntuc.fairprice.omni.scango.repository.db.room.entity.getTotalPromoDiscount
import ntuc.fairprice.omni.scango.utils.px
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.features.checkout.ui.BaseCheckoutLayout

class ScangoPaymentReceiptSummaryLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseCheckoutLayout(context, attrs, defStyleAttr) {

    init {
        val padding = 24.px
        setPadding(padding, padding, padding, padding);
        setBackgroundResource(R.drawable.scango_edit_text_border)
        LayoutInflater.from(context).inflate(R.layout.scango_layout_payment_summary_receipt, this)
        promo_list.isReceipt = true
    }

    var paymentSummary: ScangoPaymentSummary? = null
        @SuppressLint("SetTextI18n")
        set(value) {
            value?.let {
                with(it) {
                    val couponDiscount = promoList.getTotalPromoDiscount()
                    tv_subtotal.setCurrencyText(subTotal)
                    tv_total_price2.setCurrencyText(youPay - couponDiscount)

                    tv_saved_amount.setVisibility((discount + couponDiscount) > 0)
                    tv_saved_amount.setSavings(discount + couponDiscount)

                    promo_list.addPromos(promoList, null, jwcOffers, linkPointReceipt)

                    if (linkPointReceipt != null && linkPointReceipt.pointExtra != 0.0) {
                        iv_link_point.visible()
                        tv_link_point.visible()
                        tv_link_point.text =
                            context.getString(RB.string.two_decimal_formatted_val, linkPointReceipt.pointExtra)
                    } else {
                        iv_link_point.gone()
                        tv_link_point.gone()
                    }
                }
            }
            field = value
        }
}
