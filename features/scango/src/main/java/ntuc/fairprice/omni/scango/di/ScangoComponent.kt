package ntuc.fairprice.omni.scango.di

import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import ntuc.fairprice.omni.scango.ScangoInjectionProvider
import ntuc.fairprice.omni.scango.repository.webservice.service.StoreCheckoutService
import thor.zopsmart.com.thor.di.CoreComponent
import thor.zopsmart.com.thor.di.FeatureScope


@FeatureScope
@Component(
    modules = [
        ScangoActivityModule::class,
        ScangoServiceModule::class,
        ScangoViewModelModule::class,
        AndroidSupportInjectionModule::class,
        AndroidInjectionModule::class,
        ScangoNetworkModule::class,
        ScangoAppModule::class,
        ScangoRestrictionsConfigModule::class,
        ScangoLinkPointCommonModule::class
    ],
    dependencies = [CoreComponent::class]
)

interface ScangoComponent {

    fun inject(scangoInjectionProvider: ScangoInjectionProvider)

    fun inject(service: StoreCheckoutService)
}