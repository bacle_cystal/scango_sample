package ntuc.fairprice.omni.scango.ui.screen.alcohol

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_liquor_notice_fragment.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoLiquorNoticeFragmentBinding
import ntuc.fairprice.omni.scango.ui.screen.cart.ScangoCartViewModel
import javax.inject.Inject

class LiquorNoticeFragment :
    ScangoBaseBottomSheetFragment<ScangoLiquorNoticeFragmentBinding, LiquorNoticeViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "LiquorNoticeFragment"

        fun newInstance() = LiquorNoticeFragment()
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: LiquorNoticeViewModel by lazy {
        viewModelFactory.create(LiquorNoticeViewModel::class.java)
    }

    private val cartViewModel: ScangoCartViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ScangoCartViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_liquor_notice_fragment

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_liquor_got_it.setOnClickListener {
            dismiss()
        }

        txt_liquor_detail.text = cartViewModel.getTextTimeWarning(null)
    }
}
