package ntuc.fairprice.omni.scango.ui.screen.qrcode.error

enum class ErrorType(val value: Int) {
    SOMETHING_WRONG(1),
    NOT_RECOGNISED_BAR_CODE(2),
    NOT_ABLE_SELL(3),
    NETWORK_ERROR(4),
    NOT_RECOGNISED_QR_CODE(5),
    USER_FORBIDDEN(6)
}
