package ntuc.fairprice.omni.scango.ui.screen.signup

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

import ntuc.fairprice.omni.scango.R
import thor.zopsmart.com.thor.repository.db.room.repo.AccountRepository
import thor.zopsmart.com.thor.viewmodel.AccountViewModel
import kotlinx.android.synthetic.main.scango_fragment_sign_up.*
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import thor.zopsmart.com.thor.features.account.ui.LoginActivity
import thor.zopsmart.com.thor.features.account.ui.RegisterActivity

class RequireSignUpFragment : BottomSheetDialogFragment() {

    companion object {
        const val TAG = "RequireSignUpFragment"
        fun newInstance() = RequireSignUpFragment()
    }

    val layout: Int
        get() = R.layout.scango_fragment_sign_up

    private val accountViewModel: AccountViewModel by lazy {
        ViewModelProviders.of(this).get(AccountViewModel::class.java)
    }

    lateinit var accountRepository: AccountRepository

    private lateinit var viewModel: RequireSignUpViewModel

    lateinit var mainActivity: ScangoBaseActivity
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (activity is ScangoBaseActivity) {
            mainActivity = activity as ScangoBaseActivity
        }
        return inflater.inflate(layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(RequireSignUpViewModel::class.java)

        btn_signin_screen.setOnClickListener {
            activity?.startActivity(CodeScannerActivity.getLoginIntent(mainActivity))
            activity?.finish()
        }

        text_sign_up.setOnClickListener {
            activity?.startActivity(Intent(activity, RegisterActivity::class.java))
            activity?.finish()
        }
//        accountRepository = AccountRepository(application)
        // TODO: Use the ViewModel
    }

}
