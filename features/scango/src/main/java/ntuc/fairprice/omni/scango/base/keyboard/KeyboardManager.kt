package ntuc.fairprice.omni.scango.base.keyboard

import android.view.View

interface KeyboardManager {

    fun hide(view: View)
}