package ntuc.fairprice.omni.scango.di

import dagger.Binds
import dagger.Module
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepository
import ntuc.fairprice.omni.scango.repository.linkpoint.ScangoLinkPointRepositoryImpl
import thor.zopsmart.com.thor.di.FeatureScope

@Module
abstract class ScangoLinkPointCommonModule {

    @Binds
    @FeatureScope
    abstract fun provideScangoLinkPointRepository(repository: ScangoLinkPointRepositoryImpl): ScangoLinkPointRepository
}