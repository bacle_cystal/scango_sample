package ntuc.fairprice.omni.scango.repository.webservice.service

import com.zendesk.util.FileUtils
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartArray
import ntuc.fairprice.omni.scango.repository.webservice.model.Result
import ntuc.fairprice.omni.scango.repository.webservice.serviceclient.ScangoRequestProvider
import ntuc.fairprice.omni.scango.ui.screen.cart.data.ScangoCartSyncData
import ntuc.fairprice.omni.scango.ui.screen.cart.data.toCartSyncData
import ntuc.fairprice.omni.scango.utils.loadJSONFromAsset
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.safeJson
import thor.zopsmart.com.thor.di.FeatureScope
import javax.inject.Inject

@FeatureScope
class ScangoSyncCartService @Inject constructor(scangoRequestProvider: ScangoRequestProvider) : ScangoBaseService(scangoRequestProvider)  {

    suspend fun sync(cartItems: List<ScangoCartItem>, storeId: Int): Result<ScangoCartSyncData> {

        val params = JSONObject()
        // Create cart object
        val cart = cartItems.toCartArray()
        val cartObject = JSONObject()
        cartObject.put("items", cart)
        // Input param key-value
        params.put("storeId", storeId.toString())
        params.put("cart", cartObject)
        params.put("orderType", "OFFLINE")

        return apiCall(
            "/api/cart",
            ScangoRequestProvider.Method.POST,
            params = params,
            isGuestRequest = false,
            forWho = ScangoRequestProvider.RequestFor.ZOPSMART
        ) {
            val data = it.toJSONObject().safeJson("data")
            val cartJ = data.safeJson("cart")
            cartJ.toCartSyncData()
        }
    }
}