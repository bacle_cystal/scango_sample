package ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.get
import dagger.Binds
import dagger.Module
import dagger.Provides
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget.ScangoLinkPlusPhoneNumberBoxWidget
import ntuc.fairprice.omni.scango.ui.screen.linkplus.phonenumber.widget.ScangoLinkPlusPhoneNumberBoxWidgetImpl
import thor.zopsmart.com.thor.di.ActivityScope

@Module
class ScangoLinkPlusPhoneNumberProvidesModule {

    @Provides
    @ActivityScope
    fun provideLinkPlusPhoneNumberViewModel(
        activity: ScangoLinkPlusPhoneNumberActivity,
        factory: ViewModelProvider.Factory
    ): ScangoLinkPlusPhoneNumberViewModel = ViewModelProviders.of(activity, factory).get()

}

@Module
abstract class ScangoLinkPlusPhoneNumberBindsModule {

    @Binds
    @ActivityScope
    abstract fun provideLinkPlusPhoneNumberBoxWidget(widget: ScangoLinkPlusPhoneNumberBoxWidgetImpl): ScangoLinkPlusPhoneNumberBoxWidget

    @Binds
    @ActivityScope
    abstract fun provideContext(activity: ScangoLinkPlusPhoneNumberActivity): Activity

}