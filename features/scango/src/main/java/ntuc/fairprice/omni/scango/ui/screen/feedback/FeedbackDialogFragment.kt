package ntuc.fairprice.omni.scango.ui.screen.feedback

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_feedback_dialog.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseBottomSheetFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentFeedbackDialogBinding
import javax.inject.Inject

/**
 *
 * A fragment that shows a list of options as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    FeedbackListDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [FeedbackDialogFragment.Listener].
 */
class FeedbackDialogFragment :
    ScangoBaseBottomSheetFragment<ScangoFragmentFeedbackDialogBinding, FeedbackDialogViewModel>(),
    HasSupportFragmentInjector {

    companion object {
        val TAG = "FeedbackListDialogFragment"
        val GOOD_RATING = 3

        fun newInstance(customerName: String) = FeedbackDialogFragment().apply {
            arguments = Bundle().apply {
                putString(Constants.EXTRA_FEEDBACK_CUSTOMER_NAME, customerName)
            }
        }
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: FeedbackDialogViewModel by lazy {
        viewModelFactory.create(FeedbackDialogViewModel::class.java)
    }

    override val layoutId: Int = R.layout.scango_fragment_feedback_dialog

    var actionAfterDismiss: () -> Unit = {}

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        txtSkipRate.setOnClickListener {
            dismiss()
        }

        ratingBarShopping.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (rating > GOOD_RATING) {
                txtSkipRate.visibility = View.INVISIBLE
                txtHeaderMore.text = getString(R.string.text_result_rating_bar_bottom)
                txtHeader.text = getText(R.string.text_result_rating_bar_header)
                ratingBarShopping.setIsIndicator(true)
                Handler().postDelayed({
                    dismiss()
                    actionAfterDismiss.invoke()
                }, 500)
            } else {
                Handler().postDelayed({
                    val fragment = FeedbackCommentDialogFragment.newInstance(rating)
                    fragment.show(
                        childFragmentManager,
                        FeedbackCommentDialogFragment.TAG
                    )
                }, 300)
            }
        }

        val customerName = arguments?.getString(Constants.EXTRA_FEEDBACK_CUSTOMER_NAME, "")
        txtHeader.text = String.format(resources.getString(R.string.text_rating_bar_header), customerName)
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        if (childFragment is FeedbackCommentDialogFragment) childFragment.actionAfterDismiss = {
            dismiss()
            actionAfterDismiss.invoke()
        }
    }
}
