package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.app.Application
import android.graphics.Bitmap
import android.view.View
import ntuc.fairprice.omni.scango.repository.db.room.entity.Payment
import thor.zopsmart.com.thor.features.order.OrderViewData
import thor.zopsmart.com.thor.model.orders.OrderDetail

/**
 * Created by Binh.TH on 3/13/2019.
 */

class ScangoReceiptDataView(
    val context: Application,
    val payment: Payment,
    val qrcodeBitmap: Bitmap
)