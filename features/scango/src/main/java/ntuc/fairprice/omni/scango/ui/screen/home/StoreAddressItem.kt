package ntuc.fairprice.omni.scango.ui.screen.home

data class StoreAddressItem(
        val storeName: String?,
        val address: String,
        var isNew: Boolean
)