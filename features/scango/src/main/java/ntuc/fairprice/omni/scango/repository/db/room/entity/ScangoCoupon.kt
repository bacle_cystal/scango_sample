package ntuc.fairprice.omni.scango.repository.db.room.entity

import android.util.Log
import org.json.JSONObject
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.model.Coupon
import java.io.Serializable

data class ScangoCoupon(
    val coupon: String,
    val expiryOn: String,
    val description: String?,
    val redemptionLimit: Int? = null,
    val finalDiscount: Double = 0.0,
    val couponType: String = "ORDER" // Can be ORDER or SHIPPING now
) : Serializable

fun JSONObject.toCoupon(): ScangoCoupon {
    val coupon = this string "coupon" ?: ""
    val description = this string "description" ?: ""
    val expiryOn = this string "expiryOn" ?: ""
    return ScangoCoupon(coupon, expiryOn.legacyDateWithoutTime(), description)
}

fun JSONObject.toAppliedCoupon(): ScangoCoupon {
    // Check if Error
    string("error")?.let {
        Log.d("EXCEPTION", it)
    }
    // Parse coupon
    val couponJSON = json("coupon")
    val coupon = couponJSON string "couponCode" ?: ""
    val description = couponJSON string "description" ?: couponJSON string "comment" ?: ""
    val expiryOn = couponJSON string "expiryDate" ?: ""
    val couponType = couponJSON string "couponType" ?: "ORDER"

    // Parse discount
    val discountJSON = json("discount")
    val discountPercent = discountJSON double "discount" ?: 0.0

    return ScangoCoupon(coupon, expiryOn.legacyDateWithoutTime(), description, null, discountPercent, couponType)
}

fun List<ScangoCoupon>.toCouponCodeList(): MutableList<String> {
    val couponCodes = mutableListOf<String>()
    forEach {
        couponCodes.add(it.coupon)
    }
    return couponCodes
}



fun  List<ScangoCoupon>.getTotalPromoDiscount(): Double {
    var discount = 0.0
    forEach {
        if(it.couponType != "SHIPPING")
            discount += it.finalDiscount
    }
    return discount
}