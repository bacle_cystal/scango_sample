package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder
import ntuc.fairprice.omni.scango.utils.px

@EpoxyModelClass(layout = R.layout.scango_layout_receipt_top_free_gift)
abstract class ScangoReceiptHeaderFreegiftModel :
    EpoxyModelWithHolder<ScangoReceiptHeaderFreegiftModel.ScangoReceiptHeaderFreegiftHolder>() {
    class ScangoReceiptHeaderFreegiftHolder : ScangoBaseEpoxyHolder()

    override fun buildView(parent: ViewGroup): View {
        val view = super.buildView(parent)
        (view.layoutParams as? ViewGroup.MarginLayoutParams)?.run {
            leftMargin = 16.px
            rightMargin = 16.px
        }
        return view
    }
}
