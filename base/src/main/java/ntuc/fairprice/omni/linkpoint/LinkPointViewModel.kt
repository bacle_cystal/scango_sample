package ntuc.fairprice.omni.linkpoint

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import ntuc.fairprice.omni.domain.model.BaseViewModel
import ntuc.fairprice.omni.domain.model.linkpoint.LinkPointStatus
import ntuc.fairprice.omni.domain.repository.db.room.repo.LinkpointRepository
import thor.zopsmart.com.thor.base.JSONUtil
import thor.zopsmart.com.thor.repository.db.room.repo.DeviceInfo
import thor.zopsmart.com.thor.repository.db.sharedpref.LocalStorage
import thor.zopsmart.com.thor.repository.db.sharedpref.get
import thor.zopsmart.com.thor.repository.db.sharedpref.getAccessToken
import thor.zopsmart.com.thor.repository.webservice.model.onError
import thor.zopsmart.com.thor.repository.webservice.model.onSuccess
import java.util.concurrent.TimeoutException

class LinkPointViewModel(application: Application ) : BaseViewModel(application) {

    private var localStorage: SharedPreferences = LocalStorage.init(application)
    private val linkPointRepository : LinkpointRepository = LinkpointRepository.newInstance(application)

    val resultLiveData = MutableLiveData<LinkPointStates>()

    companion object {
        const val DEVICE_INFO: String = "device_info"
    }

    private fun getCustomerId(): String {
        val deviceInfo: DeviceInfo = localStorage.get(DEVICE_INFO, null as String?)?.let {
            JSONUtil.fromJson<DeviceInfo>(it)
        }?: DeviceInfo()

        return deviceInfo.userId.toString()

    }

    fun getLinkPointStatus () {
        launch {
                linkPointRepository.getCustomerLPStatus(getCustomerId(), localStorage.getAccessToken()!!).onSuccess {
                resultLiveData.value = LinkPointStates.LinkPointStatusSuccess(it)
            }.onError {
                    if(it is TimeoutException){
                        resultLiveData.value = LinkPointStates.LinkPointStatusFailedNetwork(it)
                    } else {
                        resultLiveData.value = LinkPointStates.LinkPointStatusFailedGeneral(it)
                    }
            }
        }

    }

    fun unlinkLPAccount () {
        launch {
            showLoading()
            linkPointRepository.unlinkLPAccount(getCustomerId(), localStorage.getAccessToken()!!)
                .onSuccess {
                    resultLiveData.value = LinkPointStates.UnlinkSuccess
                }.onError {
                    if(it is TimeoutException){
                        resultLiveData.value = LinkPointStates.UnlinkFailedNetwork(it)
                    } else {
                        resultLiveData.value = LinkPointStates.UnlinkFailedGeneral
                    }

            }
        }

    }
}

sealed class LinkPointStates {
    class LinkPointStatusSuccess(val linkPointStatus: LinkPointStatus) : LinkPointStates()
    class LinkPointStatusFailedGeneral(val throwable: Throwable) : LinkPointStates()
    class LinkPointStatusFailedNetwork(val throwable: Throwable) : LinkPointStates()

    object UnlinkSuccess : LinkPointStates()
    object UnlinkFailedGeneral : LinkPointStates()
    class  UnlinkFailedNetwork(val throwable: Throwable) : LinkPointStates()
}