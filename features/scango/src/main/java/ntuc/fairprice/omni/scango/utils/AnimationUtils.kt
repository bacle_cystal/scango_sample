package ntuc.fairprice.omni.scango.utils

import android.os.Handler
import android.view.View
import android.view.animation.*
import com.airbnb.lottie.LottieAnimationView
import ntuc.fairprice.omni.scango.Constants
import thor.zopsmart.com.thor.base.extensions.gone
import thor.zopsmart.com.thor.base.extensions.setVisibility
import thor.zopsmart.com.thor.base.extensions.visible


/**
 * Created by Binh.TH on 3/25/2019.
 */

object AnimationUtils {

    open fun startRedJsonCartAnimation(animationView: LottieAnimationView) {
        Handler().postDelayed({
            animationView.setVisibility(true)
            animationView.playAnimation()
        }, 1400L.toTimeConfiged())
    }

    open fun startDimAnimation(view: View) {
        view.setVisibility(true)
        view.alpha = 1f
        val animSet = AnimationSet(true)

        val animOpacityFirst = AlphaAnimation(0f, 0.8f)
        animOpacityFirst.duration = 400L.toTimeConfiged()
        animSet.addAnimation(animOpacityFirst)

        val animOpacitySecond = AlphaAnimation(0.8f, 0.0f)
        animOpacitySecond.duration = 400L.toTimeConfiged()
        animOpacitySecond.startOffset = 1400L.toTimeConfiged()
        animSet.addAnimation(animOpacitySecond)
        animSet.fillAfter = true

        animSet.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                view.setVisibility(false)
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
        view.startAnimation(animSet)
    }

    fun startAlphaAnim(
        view: View,
        viewWarning: View,
        animationListener: Animation.AnimationListener,
        isShowWarningAge: Boolean
    ) {
        //Anim of add to cart view flow
        //0->0.4(display opacity change 0->100)
        //0.4->1.4(keep display 1s)
        //1.4->1.8(hcange position of imageview + start red json)
        //1.6->1.8(change position of imageview + size change)
        //1.7->1.8(opacity change 100->10)
        val animSet = AnimationSet(true)

        val animOpacityFirst = AlphaAnimation(0f, 1.0f)
        animOpacityFirst.duration = 400L.toTimeConfiged()
        animSet.addAnimation(animOpacityFirst)

        val animOpacitySecond = AlphaAnimation(1.0f, 0f)
        animOpacitySecond.duration = 100L.toTimeConfiged()
        animOpacitySecond.startOffset = 1700L.toTimeConfiged()
        animSet.addAnimation(animOpacitySecond)

        val taStart = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, -0.025f
        )
        taStart.duration = 400L.toTimeConfiged()
        animSet.addAnimation(taStart)

        val taMiddle = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, -0.08f
        )

        taMiddle.duration = 1000L.toTimeConfiged()
        taMiddle.startOffset = 400L.toTimeConfiged()
        animSet.addAnimation(taMiddle)

        /*val taEnd = TranslateAnimation(
            0f, 36f.toDpDimention(view), (-35f).toDpDimention(view), (-50f).toDpDimention(view)
        )*/
        val taEnd = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, 0.1f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, -0.039f
        )
        taEnd.duration = 400L.toTimeConfiged()
        taEnd.startOffset = 1400L.toTimeConfiged()
        animSet.addAnimation(taEnd)


        val sa = ScaleAnimation(1f, 0.81f, 1f, 0.818f)
        sa.duration = 200L.toTimeConfiged()
        sa.startOffset = 1600L.toTimeConfiged()
        animSet.addAnimation(sa)

        animSet.setAnimationListener(animationListener)

        animSet.fillAfter = true

        view.startAnimation(animSet)

        if (isShowWarningAge) {
            viewWarning.visible()
            viewWarning.startAnimation(animSet)
        } else {
            viewWarning.gone()
        }
    }

    fun startAlphaAnimCart(view: View) {
        val animOpacity = AlphaAnimation(0f, 1.0f)
        animOpacity.duration = 200L.toTimeConfiged()
        Handler().postDelayed({
            view.startAnimation(animOpacity)
        }, 2000L.toTimeConfiged())
    }


    fun startCartWarningAnim(
        view: View,
        animationListener: Animation.AnimationListener
    ) {
        //Anim of add to cart view flow
        //0->0.125(display opacity change 0->100)
        //0.125->3.875(keep display 3.5s)
        //3.875->4.0(change position warning)
        //3.875->4.0(opacity change 100->0)
        val animSet = AnimationSet(true)

        val animOpacityFirst = AlphaAnimation(0f, 1.0f)
        animOpacityFirst.duration = 125L.toTimeConfiged()
        animSet.addAnimation(animOpacityFirst)

        val animOpacitySecond = AlphaAnimation(1.0f, 0f)
        animOpacitySecond.duration = 125L.toTimeConfiged()
        animOpacitySecond.startOffset = 3850L.toTimeConfiged()
        animSet.addAnimation(animOpacitySecond)

        val taStart = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, -0.255f
        )
        taStart.duration = 125L.toTimeConfiged()
        animSet.addAnimation(taStart)

        val taEnd = TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_SELF, 0f,
            Animation.RELATIVE_TO_PARENT, 0.255f
        )
        taEnd.duration = 125L.toTimeConfiged()
        taEnd.startOffset = 3875L.toTimeConfiged()
        animSet.addAnimation(taEnd)

        animSet.setAnimationListener(animationListener)

        animSet.fillAfter = true

        view.startAnimation(animSet)
    }

}


fun Long.toTimeConfiged(): Long = this.times(Constants.ANIM_TIME_CONFIG).toLong()
