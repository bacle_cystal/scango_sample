package ntuc.fairprice.omni.scango.widget

import android.app.Activity
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import ntuc.fairprice.omni.scango.R

class WelcomeToast {

    companion object {
        fun show(activity: Activity, storeName: String) {
            val layout = LayoutInflater.from(activity).inflate(R.layout.scango_welcome_toast, null)
            val tvStoreName = layout.findViewById<TextView>(R.id.tvStoreAddress)
            tvStoreName.text = storeName
            with (Toast(activity)) {
                setGravity(Gravity.FILL, 0, 0)
                duration = Toast.LENGTH_SHORT
                view = layout
                layout.findViewById<ConstraintLayout>(R.id.welcomeToast).setOnClickListener { cancel() }
                show()
            }
        }
    }

}