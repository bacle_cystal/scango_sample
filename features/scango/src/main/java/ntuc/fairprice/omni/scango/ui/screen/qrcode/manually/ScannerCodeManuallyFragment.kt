package ntuc.fairprice.omni.scango.ui.screen.qrcode.manually

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_qrcode_manually.*
import kotlinx.android.synthetic.main.scango_layout_title_edit_text.*
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.base.ScangoManuallyInputDelegate
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentQrcodeManuallyBinding
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.utils.hideKeyBoard
import ntuc.fairprice.omni.scango.utils.showKeyboard
import javax.inject.Inject


class ScannerCodeManuallyFragment : ScangoBaseFragment<ScangoFragmentQrcodeManuallyBinding, QrCodeManuallyViewModel>(),
    HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override val layoutId: Int = R.layout.scango_fragment_qrcode_manually

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override val viewModel: QrCodeManuallyViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(QrCodeManuallyViewModel::class.java)
    }

    companion object {
        val TAG = "ScannerCodeManuallyFragment"
        const val IS_BARCODE = "scan_type"
        fun newInstance(isBarcode: Boolean): ScannerCodeManuallyFragment {
            val qrCodeManuallyFragment = ScannerCodeManuallyFragment()
            qrCodeManuallyFragment.apply {
                arguments = Bundle().apply {
                    putBoolean(IS_BARCODE, isBarcode)
                }
            }
            return qrCodeManuallyFragment
        }
    }

    var scanType: ScanType? = ScanType.QR_CODE


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (arguments?.getBoolean(IS_BARCODE, false) == true) {
            scanType = ScanType.BAR_CODE
        } else {
            scanType = ScanType.QR_CODE
        }

        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }

        viewModel.apply {
            viewModel.isBarcode.value = (scanType == ScanType.BAR_CODE)
            storeItem.observe(viewLifecycleOwner, Observer {
                it?.let {
                    activity?.apply {
                        val resultIntent = Intent()
                        resultIntent.putExtra(Constants.INTENT_RESULT_CITY_NAME, it.name)
                        setResult(Activity.RESULT_OK, resultIntent)
                    }
                    activity?.finish()
                }
            })

            isBarcode.observe(viewLifecycleOwner, Observer {
                when (it) {
                    true -> {
                        title_edit.viewBinding.textTitle.text = getString(R.string.barcode_number_edit_text_title)
                        btn_confirm.text = getString(R.string.text_barcode_btn_confirm)
                    }
                    else -> {
                        title_edit.viewBinding.textTitle.text = getString(R.string.qrcode_number_edit_text_title)
                        btn_confirm.text = getString(R.string.text_qrcode_btn_confirm)
                    }
                }
            })

            error.observe(viewLifecycleOwner, Observer {
                startActivity(CodeScannerActivity.getLoginIntent(mainActivity))
            })
        }

        btnBack.setOnClickListener {
            activity?.onBackPressed()
        }
        btn_confirm.setOnClickListener {
            if (!title_edit.content.get().isNullOrEmpty()) {
                it?.hideKeyBoard()
                title_edit.content.get()?.let { codeId ->
                    onBack()
                    when (scanType) {
                        ScanType.BAR_CODE -> callBackListener?.onInputBarcodeManually(codeId)
                        ScanType.QR_CODE -> callBackListener?.onInputQrcodeManually(codeId)
                        else -> {}
                    }
                }
            }
        }

        edt_content.apply {
            requestFocus()
            showKeyboard()
        }

    }

    private var callBackListener: ScangoManuallyInputDelegate? = null

    fun setCallbackListener(callback: ScangoManuallyInputDelegate) {
        this.callBackListener = callback
    }
}
