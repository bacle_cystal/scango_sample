package ntuc.fairprice.omni.scango.base.toolbar

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import ntuc.fairprice.omni.scango.R

class ScangoToolBarWithTitleAndAction(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    private var toolBarActionView: ImageView
    private var toolBarTitleView: TextView

    private object ActionType {
        const val BLUE_BACK = 0
        const val BLUE_CLOSE = 1
        const val WHITE_CLOSE = 2
        const val WHITE_BACK = 3
    }

    //Title for ToolBar
    var title: String
        get() = toolBarTitleView.text.toString()
        set(value) {
            toolBarTitleView.text = value
        }

    init {
        inflate(context, R.layout.scango_toolbar_with_title_and_action, this)

        toolBarActionView = findViewById(R.id.scango_toolbar_with_title_action_actionview)
        toolBarTitleView = findViewById(R.id.scango_toolbar_with_title_action_titleview)

        val typedArr = context.obtainStyledAttributes(attrs, R.styleable.ScangoToolBarWithTitleAndAction, 0, 0)

        //Setup Title
        title = typedArr.getString(R.styleable.ScangoToolBarWithTitleAndAction_title) ?: ""

        //Setup CloseOrBackView
        val buttonType = typedArr.getInt(R.styleable.ScangoToolBarWithTitleAndAction_actionType, 0)
        toolBarActionView.setImageResource(getImageResource(buttonType))

        //Setup background color
        val defaultBackgroundColor = ContextCompat.getColor(context, R.color.pale_grey)
        val backgroundColor =
            typedArr.getColor(R.styleable.ScangoToolBarWithTitleAndAction_backgroundColor, defaultBackgroundColor)
        val defaultTitleColor = ContextCompat.getColor(context, getTitleColor(buttonType))
        val titleColor =
            typedArr.getColor(R.styleable.ScangoToolBarWithTitleAndAction_titleColor, defaultTitleColor)
        toolBarActionView.setBackgroundColor(backgroundColor)
        toolBarTitleView.setBackgroundColor(backgroundColor)
        toolBarTitleView.setTextColor(titleColor)
        typedArr.recycle()
    }

    fun setOnActionClickListener(action: () -> Unit) {
        toolBarActionView.setOnClickListener { action.invoke() }
    }

    private fun getImageResource(buttonType: Int): Int = when (buttonType) {
        ActionType.BLUE_BACK -> R.drawable.sg_ic_back_blue
        ActionType.BLUE_CLOSE -> R.drawable.sg_ic_close_blue
        ActionType.WHITE_BACK -> R.drawable.sg_ic_back_white
        ActionType.WHITE_CLOSE -> R.drawable.sg_ic_close_white
        else -> R.drawable.sg_ic_back_blue
    }

    private fun getTitleColor(buttonType: Int): Int = when (buttonType) {
        ActionType.BLUE_BACK -> R.color.black
        ActionType.BLUE_CLOSE -> R.color.black
        ActionType.WHITE_BACK -> R.color.white
        ActionType.WHITE_CLOSE -> R.color.white
        else -> R.color.black
    }
}