package ntuc.fairprice.omni.scango.ui.screen.ageverify.scan

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_activity_scan_age_verify.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.scango.ui.screen.ageverify.AgeVerifyType
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.ScangoAgeVerifyManuallyViewModel
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.widget.ScangoAgeScannerWidget
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.BarcodeErrorFragment
import ntuc.fairprice.omni.scango.ui.screen.qrcode.error.ErrorType
import ntuc.fairprice.omni.scango.utils.observe
import ntuc.fairprice.omni.scango.utils.stopSoundService
import javax.inject.Inject


class ScangoAgeVerifyScanActivity : ScangoBaseActivity(),
    HasSupportFragmentInjector {

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var viewModel: ScangoAgeVerifyScanViewModel
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog

    @Inject
    lateinit var scannerWidget: ScangoAgeScannerWidget
    @Inject
    lateinit var keyboardManager: KeyboardManager

    private lateinit var viewBinding: ViewDataBinding

    companion object {
        const val INTENT_SCANGO_TYPE_SCAN = "INTENT_SCANGO_TYPE_SCAN"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding =
            DataBindingUtil.setContentView(this@ScangoAgeVerifyScanActivity, R.layout.scango_activity_scan_age_verify)
        viewBinding.lifecycleOwner = this
        viewBinding.apply {
            executePendingBindings()
        }
        initWidget()
        initListeners()
        initViewModel()
    }

    override fun onAttachFragment(fragment: Fragment?) {
        super.onAttachFragment(fragment)
        when (fragment) {
            is BarcodeErrorFragment -> {
                fragment.onManuallyInputClick = {
                    sendResultChangeScreen()
                }
            }
        }
    }

    private fun initWidget() {
        scannerWidget.initWidget(container, ScanType.QR_CODE.value)
    }

    private fun initViewModel() {
        viewModel.run {
            observe(stateLiveData, ::onViewModelStateChanged)
            observe(resultString, ::onViewModelStringResultChange)
            observe(errorType, ::onError)
        }
    }

    private fun onError(errorType: Int?) {
        showErrorBarcodeFragment()
    }

    private fun initListeners() {
        btn_back.setOnClickListener {
            finish()
        }
        text_code_bottom.setOnClickListener {
            sendResultChangeScreen()
        }
    }

    private fun onViewModelStateChanged(state: ScangoAgeVerifyManuallyViewModel.AgeVerifyState) {
        when (state) {
            is ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationSuccess -> {
                //Handle verify success
                sendResultSuccess()
            }
        }
    }

    private fun onViewModelStringResultChange(detectedQRString: String) {
        if (detectedQRString.isNotEmpty()) {
            val uri = Uri.parse(detectedQRString)
            val staffId = uri.getQueryParameter("staffID") ?: ""
            val pin = uri.getQueryParameter("pin") ?: ""
            viewModel.verifyPincode(staffId, pin)
        }
    }

    /*override fun handleBackPress(): Boolean {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        return super.handleBackPress()
    }

      private fun sendResultSuccess(staffID: String, pincode: String) {
          val intent = Intent()
          intent.putExtra(INTENT_SCANGO_STAFFID, staffID)
          intent.putExtra(INTENT_SCANGO_PIN, pincode)
          setResult(Activity.RESULT_OK, intent)
          finish()
      }*/

    /**
     * Keeping this method to access from BarcodeProductFragment.
     * Need to find a better way to handle this scenario
     */
    fun startCamera() {
        scannerWidget.start(this)
    }


    override fun onResume() {
        super.onResume()
        startCamera()
    }

    override fun onStop() {
        super.onStop()
        stopSoundService(this@ScangoAgeVerifyScanActivity)
    }

    /** Stops the camera.  */
    override fun onPause() {
        super.onPause()
        scannerWidget.stop()
    }

    public override fun onDestroy() {
        super.onDestroy()
        scannerWidget.destroy()
    }


    private fun sendResultSuccess() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun sendResultChangeScreen() {
        val intent = Intent()
        intent.putExtra(INTENT_SCANGO_TYPE_SCAN, AgeVerifyType.QR_CODE.value)
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun showErrorBarcodeFragment() {
        val barcodeErrorFragment =
            BarcodeErrorFragment.newInstance(ScanType.QR_CODE.value, viewModel.errorType.value ?: 1)
        barcodeErrorFragment.onScanAgainClick = {
            startCamera()
        }
        addFragment(
            fragment = barcodeErrorFragment,
            TAG = BarcodeErrorFragment.TAG,
            transit = TransitionFrom.RIGHT,
            addToBackStack = true
        )
    }
}