package ntuc.fairprice.omni.scango.ui.screen.promo.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import kotlinx.android.synthetic.main.scango_label_edit_text.view.*
import kotlinx.android.synthetic.main.scango_layout_coupon_edit.view.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.utils.clearError
import thor.zopsmart.com.thor.base.extensions.setImeAction
import kotlin.properties.Delegates

@ModelView(
    autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT,
    saveViewState = true
)
class ScangoCouponEntryView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var isError: Boolean by Delegates.observable(false) { _, _, _ ->
        updateErrorView()
    }

    private fun updateErrorView() {
        if (isError) {
            et_promo_code.error = context.getString(R.string.text_promo_invalid)
            et_promo_code.clearFocus()
        } else {
            listOf(et_promo_code).clearError()
        }
    }

    init {
        View.inflate(context, R.layout.scango_layout_coupon_edit, this)
    }

    @ModelProp
    fun promoError(isError: Boolean) {
        this.isError = isError
    }

    @CallbackProp
    fun onApply(@Nullable action: ((String) -> Unit)?) {
        et_promo_code.applyClick = OnClickListener {
            action?.let {
                it(et_promo_code.getString())
            }
        }

        et_promo_code.custom_edit_text.imeOptions = EditorInfo.IME_ACTION_DONE
        et_promo_code.custom_edit_text.setImeAction {
            action?.let {
                it(et_promo_code.getString())
            }
        }
    }

}