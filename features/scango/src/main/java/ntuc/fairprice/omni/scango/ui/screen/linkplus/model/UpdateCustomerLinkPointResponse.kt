package ntuc.fairprice.omni.scango.ui.screen.linkplus.model

import ntuc.fairprice.omni.scango.repository.webservice.model.ApiResponse
import thor.zopsmart.com.thor.base.extensions.double
import thor.zopsmart.com.thor.base.extensions.int
import thor.zopsmart.com.thor.base.extensions.safeJson
import thor.zopsmart.com.thor.base.extensions.string

data class UpdateCustomerLinkPointResponse(
    val linkStatus: Int,
    val pointBalance: Double,
    val dollarBalance: Double,
    val suffixMobileNumber: String
)

fun ApiResponse.toUpdateCustomerLinkPointStatus(): UpdateCustomerLinkPointResponse {

    val data = this.toJSONObject() safeJson "data"
    val linkStatus = data int "linkStatus" ?: 0
    val pointBalance = data double "pointBalance" ?: 0.0
    val dollarBalance = data double "dollarBalance" ?: 0.0
    val suffixMobileNumber = data string  "suffixMobileNumber" ?: ""
    return UpdateCustomerLinkPointResponse(linkStatus, pointBalance, dollarBalance, suffixMobileNumber)
}