package ntuc.fairprice.omni.scango.repository.db.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import org.json.JSONArray
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.InvalidIdentifierException
import thor.zopsmart.com.thor.base.extensions.*
import thor.zopsmart.com.thor.repository.db.room.typeconverter.Converters

@Entity
data class Campaign(
    @PrimaryKey var id: Long,
    var name: String?,
    var content: String?
)
