package ntuc.fairprice.omni.scango.utils

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import ntuc.fairprice.omni.scango.Constants.FIREBASE_MAX_CART_SIZE_CONFIG_KEY
import ntuc.fairprice.omni.scango.Constants.MAX_CART_SIZE

object RemoteConfigUtils {

    val maxCartSize: Int
        get() {
            val size = FirebaseRemoteConfig.getInstance().getLong(FIREBASE_MAX_CART_SIZE_CONFIG_KEY).toInt()
            return if (size == 0) MAX_CART_SIZE else size
        }
}