package ntuc.fairprice.omni.scango.ui.screen.ageverify.manually

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.scango_activity_age_verify_manually.*
import ntuc.fairprice.omni.dialog.ScanGoProgressDialog
import ntuc.fairprice.omni.dialog.ScangoGeneralErrorDialog
import ntuc.fairprice.omni.dialog.ScangoNetworkErrorDialog
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.keyboard.KeyboardManager
import ntuc.fairprice.omni.scango.ui.screen.ageverify.AgeVerifyType
import ntuc.fairprice.omni.scango.ui.screen.ageverify.manually.widget.ScangoAgeVerifyManuallyWidget
import ntuc.fairprice.omni.scango.ui.screen.ageverify.scan.ScangoAgeVerifyScanActivity
import ntuc.fairprice.omni.scango.utils.observe
import javax.inject.Inject


class ScangoAgeVerifyManuallyActivity : ScangoBaseActivity() {

    @Inject
    lateinit var viewModel: ScangoAgeVerifyManuallyViewModel
    @Inject
    lateinit var scangoAgePincodeWidget: ScangoAgeVerifyManuallyWidget
    @Inject
    lateinit var scanGoProgressDialog: ScanGoProgressDialog
    @Inject
    lateinit var scangoNetworkErrorDialog: ScangoNetworkErrorDialog
    @Inject
    lateinit var scangoGeneralErrorDialog: ScangoGeneralErrorDialog

    @Inject
    lateinit var keyboardManager: KeyboardManager

    private lateinit var viewBinding: ViewDataBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewBinding =
            DataBindingUtil.setContentView(this@ScangoAgeVerifyManuallyActivity, R.layout.scango_activity_age_verify_manually)
        viewBinding.lifecycleOwner = this
        viewBinding.apply {
            executePendingBindings()
        }

        initWidget()
        initListeners()
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.run {
            observe(stateLiveData, ::onViewModelStateChanged)
        }
    }

    private fun initWidget() {
        scangoAgePincodeWidget.initWidget(cl_age_pin_container)
        /*scangoAgePincodeWidget.onOtpInputComplete = {
            keyboardManager.hide(card_age_pin)
            scanGoProgressDialog.show()
            viewModel.verifyPincode(scangoAgePincodeWidget.getStaffId(), scangoAgePincodeWidget.getPincode())
        }*/
        scangoAgePincodeWidget.displayDefaultState()
    }

    private fun initListeners() {
        iv_close_back.setOnClickListener {
            finish()
        }
        txt_age_pin_scan.setOnClickListener {
            sendResultChangeScreen()
        }
        iv_age_pin_scan.setOnClickListener {
            sendResultChangeScreen()
        }

        btn_submit.setOnClickListener {
            viewModel.verifyPincode(scangoAgePincodeWidget.getStaffId(), scangoAgePincodeWidget.getPincode())
        }
    }

    private fun onViewModelStateChanged(state: ScangoAgeVerifyManuallyViewModel.AgeVerifyState) {
        when (state) {
            is ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerifyFailed -> {
                scanGoProgressDialog.dismiss()
                scangoGeneralErrorDialog.apply {
                    onActionClick = {
                        scanGoProgressDialog.show()
                        viewModel.verifyPincode(
                            scangoAgePincodeWidget.getStaffId(),
                            scangoAgePincodeWidget.getPincode()
                        )
                    }
                    show()
                }

            }

            is ScangoAgeVerifyManuallyViewModel.AgeVerifyState.NetworkError -> {
                scanGoProgressDialog.dismiss()

                scangoNetworkErrorDialog.apply {
                    onActionClick = {
                        scanGoProgressDialog.show()
                        viewModel.verifyPincode(
                            scangoAgePincodeWidget.getStaffId(),
                            scangoAgePincodeWidget.getPincode()
                        )
                    }
                    show()
                }
            }

            is ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationSuccess -> {
                //Handle verify success
                sendResultSuccess()
            }

            is ScangoAgeVerifyManuallyViewModel.AgeVerifyState.AgeVerificationError -> {
                scanGoProgressDialog.dismiss()
                scangoAgePincodeWidget.displayErrorState()
            }
        }
    }

    /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
         if (requestCode == Constants.REQUEST_AGE_VERIFY_SCAN) {
             if (resultCode == Activity.RESULT_OK) {
                 data?.let {
                     val statffId = data.getStringExtra(ScangoAgeVerifyScanActivity.INTENT_SCANGO_STAFFID) ?: ""
                     val pin = data.getStringExtra(ScangoAgeVerifyScanActivity.INTENT_SCANGO_PIN) ?: ""
                     viewModel.verifyPincode(statffId, pin)
                 }
             }
         }
     }*/

    override fun handleBackPress(): Boolean {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        return super.handleBackPress()
    }

    private fun sendResultSuccess() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun sendResultChangeScreen() {
        val intent = Intent()
        intent.putExtra(ScangoAgeVerifyScanActivity.INTENT_SCANGO_TYPE_SCAN, AgeVerifyType.MANUALLY_INPUT.value)
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }
}