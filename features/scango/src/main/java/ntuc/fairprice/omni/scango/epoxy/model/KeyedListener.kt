package ntuc.fairprice.omni.scango.epoxy.model


/**
 * Created by Binh.TH on 6/26/2019.
 */
class KeyedListener<Key, SimpleSwipeListener> private constructor(val identifier: Key, val callback: SimpleSwipeListener) {

    companion object {
        @JvmStatic
        fun <Key, SimpleSwipeListener> create(
            identifier: Key,
            callback: SimpleSwipeListener
        ): KeyedListener<Key, SimpleSwipeListener> {
            return KeyedListener(identifier, callback)
        }
    }

    // Only include the key, and not the listener, in equals/hashcode
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is KeyedListener<*, *>) return false
        return identifier == other.identifier
    }

    override fun hashCode() = identifier?.hashCode() ?: 0
}