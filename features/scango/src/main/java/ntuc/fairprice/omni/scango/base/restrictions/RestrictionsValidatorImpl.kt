package ntuc.fairprice.omni.scango.base.restrictions

import android.app.Application
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.repository.db.room.entity.RestrictionsConfig
import ntuc.fairprice.omni.scango.repository.db.room.entity.Rule
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.restrictionsconfig.ScangoRestrictionsConfigRepository
import ntuc.fairprice.omni.scango.utils.DateTimeUtils
import javax.inject.Inject

class RestrictionsValidatorImpl @Inject constructor(
    private val scangoRestrictionsConfigRepository: ScangoRestrictionsConfigRepository,
    val application: Application
) : RestrictionsValidator {
    private object RestrictionType {
        const val AGE = "age"
        const val TIME = "time"
        const val DOW = "DOW"
    }

    private val restrictionsConfig: RestrictionsConfig? by lazy {
        scangoRestrictionsConfigRepository.getConfig()
    }

    override fun isTimeViolated(cartItem: ScangoCartItem): Boolean {
        return try {
            val rules = getAssociatedRules(cartItem)
            val filterResult = rules.filter { it.type == RestrictionType.TIME }
            if (filterResult.isNotEmpty()) {
                val startTime = filterResult[0].definition["inboundRange"]?.get(0) ?: 0
                val endTime = filterResult[0].definition["inboundRange"]?.get(1) ?: 0
                DateTimeUtils.checkTimeViolated(application, startTime, endTime)
            } else false
        } catch (e: Exception) {
            false
        }
    }

    override fun getTimeWarningText(cartItem: ScangoCartItem?): String {
        val rules = mutableSetOf<Rule>()
        restrictionsConfig?.let { config ->
            config.data.rules.forEach { rule ->
                rules.add(rule.value)
            }
        }
        val filterResult = rules.filter { it.type == RestrictionType.TIME }
        if (filterResult.isNotEmpty()) {
            val startTime = filterResult[0].definition["inboundRange"]?.get(0) ?: 0
            val endTime = filterResult[0].definition["inboundRange"]?.get(1) ?: 0
            val startTimeFormatted = DateTimeUtils.getDateFromDistancetime(startTime)
            val endTimeFormatted = DateTimeUtils.getDateFromDistancetime(endTime)

            return application.getString(
                R.string.txt_liquor_remove_detail_config,
                endTimeFormatted,
                startTimeFormatted
            )
        } else return ""
        /* cartItem?.let {
             return try {
                 val rules = getAssociatedRules(cartItem)
                 val filterResult = rules.filter { it.type == RestrictionType.TIME }
                 if (filterResult.isNotEmpty()) {
                     val startTime = filterResult[0].definition["inboundRange"]?.get(0) ?: 0
                     val endTime = filterResult[0].definition["inboundRange"]?.get(1) ?: 0
                     val startTimeFormatted = DateTimeUtils.getDateFromDistancetime(startTime)
                     val endTimeFormatted = DateTimeUtils.getDateFromDistancetime(endTime)

                     application.getString(
                         R.string.txt_liquor_remove_detail_config,
                         endTimeFormatted,
                         startTimeFormatted
                     )
                 } else ""
             } catch (e: Exception) {
                 ""
             }
         }
         return ""*/
    }

    override fun getTimeWarningInMilis(): Long {
        val rules = mutableSetOf<Rule>()
        restrictionsConfig?.let { config ->
            config.data.rules.forEach { rule ->
                rules.add(rule.value)
            }
        }
        val filterResult = rules.filter { it.type == RestrictionType.TIME }
        val startTime = filterResult[0].definition["inboundRange"]?.get(0) ?: 0
        val endTime = filterResult[0].definition["inboundRange"]?.get(1) ?: 0
        return DateTimeUtils.getDateFromTimeBound(endTime)
    }


    override fun isAgeViolated(cartItem: ScangoCartItem): Boolean {
        val rules = getAssociatedRules(cartItem)
        return rules.filter { it.type == RestrictionType.AGE }.any()
    }

    override fun getAssociatedRules(cartItem: ScangoCartItem): List<Rule> {
        val rules = mutableSetOf<Rule>()
        restrictionsConfig?.let { config ->
            cartItem.categoryId?.let { itemCategoryId ->
                config.data.categories[itemCategoryId.toString()]?.let { category ->
                    category.ruleIds.forEach { ruleId ->
                        config.data.rules[ruleId.toString()]?.let { rule ->
                            rules.add(rule)
                        }
                    }
                }
            }
        }
        return rules.toList()
    }
}