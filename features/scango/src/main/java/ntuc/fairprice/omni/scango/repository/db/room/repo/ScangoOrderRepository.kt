package ntuc.fairprice.omni.scango.repository.db.room.repo

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ntuc.fairprice.omni.scango.Constants
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCartItem
import ntuc.fairprice.omni.scango.repository.db.room.entity.ScangoCoupon
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderData
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoOrderDetail
import ntuc.fairprice.omni.scango.repository.db.room.entity.toCartArray
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoOrderService
import thor.zopsmart.com.thor.di.FeatureScope
import ntuc.fairprice.omni.scango.repository.db.room.entity.orders.ScangoPaymentType
import ntuc.fairprice.omni.scango.repository.webservice.model.onSuccess
import ntuc.fairprice.omni.scango.repository.webservice.service.ScangoRetrofitService
import org.json.JSONObject
import thor.zopsmart.com.thor.base.exceptions.ResourceNotFoundException
import thor.zopsmart.com.thor.model.orders.OrderData
import thor.zopsmart.com.thor.repository.db.sharedpref.set
import thor.zopsmart.com.thor.repository.webservice.model.getRetrofitResult
import javax.inject.Inject
import thor.zopsmart.com.thor.repository.webservice.model.Result

@FeatureScope
class ScangoOrderRepository @Inject constructor(
    mApplication: Application,
    private val scangoOrderService: ScangoOrderService,
    private val scangoRetrofitService: ScangoRetrofitService
) : ScangoRepository(mApplication) {

    fun getOrders(page: Int = 0): Result<ScangoOrderData> =
        scangoRetrofitService.getOrders(page).getRetrofitResult {
            it
        }

    suspend fun placeOrder(
        paymentType: ScangoPaymentType,
        customerId: Long,
        cartItems: List<ScangoCartItem>,
        storeId: Long,
        cardId: Long,
        coupon: List<ScangoCoupon>,
        completeDate: String,
        amount: Long,
        isActiveLinkPoint: Boolean,
        linkPointValue: Double,
        isActiveAgeVerify: Boolean,
        ageVerifyCode: String,
        metaData: JSONObject
    ) = scangoOrderService.placeOrder(
        getSessionId(),
        paymentType,
        customerId,
        cartItems,
        storeId,
        cardId,
        coupon,
        completeDate,
        amount,
        isActiveLinkPoint,
        linkPointValue,
        isActiveAgeVerify,
        ageVerifyCode,
        metaData
    ).onSuccess {
        if (isActiveAgeVerify) {
            localStorage.set(Constants.SHARE_PRE_AGE_VERIFY_CODE, "")
        }
    }

    suspend fun loadPromotions() = scangoOrderService.loadPromotions()

    suspend fun applyCoupon(
        couponCodes: List<String>,
        cartAmount: Double,
        cartItems: List<ScangoCartItem>,
        storeID: String
    ) =
        scangoOrderService.applyCoupon(couponCodes, cartAmount, cartItems.toCartArray(), storeID)

    fun getSessionId() = localStorage.getString(Constants.SHARE_PRE_STORE_SESSION_ID, "")

    suspend fun getOrderDetails(refNo: String): Result<ScangoOrderDetail> = withContext(Dispatchers.IO) {
        scangoRetrofitService.getOrderDetails(refNo).getRetrofitResult {
            if (it.data?.order != null) {
                ScangoOrderDetail(it.data!!.order!!)
            } else {
                throw ResourceNotFoundException("Could not find the order")
            }
        }
    }
}