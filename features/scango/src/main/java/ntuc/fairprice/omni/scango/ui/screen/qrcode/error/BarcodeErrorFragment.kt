package ntuc.fairprice.omni.scango.ui.screen.qrcode.error

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.scango_fragment_barcode_error.*
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseActivity
import ntuc.fairprice.omni.scango.base.ScangoBaseFragment
import ntuc.fairprice.omni.scango.databinding.ScangoFragmentBarcodeErrorBinding
import ntuc.fairprice.omni.scango.ui.screen.qrcode.CodeScannerActivity
import ntuc.fairprice.omni.scango.ui.screen.qrcode.ScanType
import ntuc.fairprice.omni.scango.ui.screen.qrcode.manually.ScannerCodeManuallyFragment
import javax.inject.Inject


class BarcodeErrorFragment : ScangoBaseFragment<ScangoFragmentBarcodeErrorBinding, BarcodeErrorViewModel>(),
    HasSupportFragmentInjector {
    companion object {
        fun newInstance() = BarcodeErrorFragment()
        val TAG = "BarcodeErrorFragment"

        const val SCAN_TYPE = "scan_type"
        const val ERROR_TYPE = "error_type"

        fun newInstance(scanType: Int, errorType: Int): BarcodeErrorFragment {
            val barcodeErrorFragment = BarcodeErrorFragment()
            barcodeErrorFragment.apply {
                arguments = Bundle().apply {
                    putInt(SCAN_TYPE, scanType)
                    putInt(ERROR_TYPE, errorType)
                }
            }
            return barcodeErrorFragment
        }
    }

    override val layoutId: Int = R.layout.scango_fragment_barcode_error

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    override val viewModel: BarcodeErrorViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BarcodeErrorViewModel::class.java)
    }

    var scanType: Int? = 1

    var onManuallyInputClick: () -> Unit = {}

    var onScanAgainClick: () -> Unit = {}

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        scanType = arguments?.getInt(SCAN_TYPE, 1)

        viewModel.isVisiableBarcode.value = (scanType == ScanType.BAR_CODE.value)
        viewModel.setMessageFromType(context, arguments?.getInt(ERROR_TYPE, 1) ?: 1)
        viewBinding.apply {
            lifecycleOwner = viewLifecycleOwner
            executePendingBindings()
        }

        btn_back.setOnClickListener {
            (activity as CodeScannerActivity).reloadData()
            activity?.onBackPressed()
        }

        btn_scan_again.setOnClickListener {
            activity?.onBackPressed()
            onScanAgainClick.invoke()
        }

        ic_code_manually.setOnClickListener {
            onManuallyInputClick.invoke()
        }
    }
}
