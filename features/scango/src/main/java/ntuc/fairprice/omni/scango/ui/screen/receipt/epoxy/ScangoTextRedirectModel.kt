package ntuc.fairprice.omni.scango.ui.screen.receipt.epoxy

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import ntuc.fairprice.omni.scango.R
import ntuc.fairprice.omni.scango.base.ScangoBaseEpoxyHolder
import ntuc.fairprice.omni.scango.utils.px
import ntuc.fairprice.omni.scango.utils.setTextDirectFAQ

@EpoxyModelClass(layout = R.layout.scango_layout_text_redirect_bottom)
abstract class ScangoTextRedirectModel : EpoxyModelWithHolder<ScangoTextRedirectModel.ScangoTextRedirectHolder>() {

    override fun buildView(parent: ViewGroup): View {
        val view = super.buildView(parent)
        (view.layoutParams as? ViewGroup.MarginLayoutParams)?.run {
            leftMargin = 16.px
            rightMargin = 16.px
        }
        return view
    }

    override fun bind(holder: ScangoTextRedirectHolder) {
        //holder.parent.setOnClickListener(clickListener)
        holder.apply {
            title.setTextDirectFAQ()
        }
    }

    class ScangoTextRedirectHolder : ScangoBaseEpoxyHolder() {
        val title: TextView by bind(R.id.tv_title)
    }
}
